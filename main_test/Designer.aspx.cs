﻿using System;
using System.Drawing;
using System.Diagnostics;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using main_test.TableRows;
using main_test.QuestionsStructur;
using Microsoft.AspNet.Identity;
using System.Data;
using System.Threading;

namespace main_test
{
    public partial class Designer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ErrorMes.Visible = false;
            if (!IsPostBack)
            {
                List<string> Codes = new List<string>();
                Session["CompetenceFilterCodes"] = Codes;
                //заполняем поля сегодняшней датой
                CreateDefoultDates();
            }
            else
            {
                try
                {
                    Control control = GetPostBackControl();
                    string controlid = control.ID;
                    if (controlid == "reload")
                    {
                        RebuildTables(0);
                        if (switcher.Checked == false)
                            DefaultBindLists();
                    }
                    if (switcher.Checked == true)
                        SetFilters();
                    BuildDinamycControls();
                }
                catch { }
            }
        }

        private Control GetPostBackControl()
        {
            Control control = null;
            string ctrlname = Page.Request.Params["__EVENTTARGET"];
            if (ctrlname != null && ctrlname != String.Empty)
            {
                control = Page.FindControl(ctrlname);
            }

            else
            {
                string ctrlStr = String.Empty;
                Control c = null;
                foreach (string ctl in Page.Request.Form)
                {
                    if (ctl.EndsWith(".x") || ctl.EndsWith(".y"))
                    {
                        ctrlStr = ctl.Substring(0, ctl.Length - 2);
                        c = Page.FindControl(ctrlStr);
                    }
                    else
                    {
                        c = Page.FindControl(ctl);
                    }
                    if (c is System.Web.UI.WebControls.Button ||
                             c is System.Web.UI.WebControls.ImageButton)
                    {
                        control = c;
                        break;
                    }
                }
            }
            return control;
        }
        
        //устновка связей между списками
        #region SetFilters

        private void DefaultBindLists()
        {
            DropDown_Profil.DataTextField = "Name";
            DropDown_Profil.DataValueField = "id";
            DropDown_Profil.DataBind();

            DrafterName.DataTextField = "UserName";
            DrafterName.DataValueField = "UserId";
            DrafterName.DataBind();

            Competence_FondTable.DataBind();
            Competence_CriterionsTable.DataBind();
            PFTCom.DataBind();
            DropDown_PCTZ.DataBind();
            DropDown_PCTE.DataBind();
            DropDown_PCTR.DataBind();
            DropDown_PCTPR.DataBind();
            DropDown_PCTKT.DataBind();
            DropDown_PCTT.DataBind();
            DropDown_PCTC.DataBind();
        }

        private void SetFilters()
        {
            SetFilter_Profiles();
            SetFilter_PersonsOfDepartment();
            SetFilter_Competences();
        }

        private void SetFilter_Competences()
        {
            string condition = " WHERE Code IN ";
            string commandText = "SELECT * FROM [Competences]";
            List<string> codes = (List<string>)Session["CompetenceFilterCodes"];
            condition += "(";
            for (int i = 0; i < codes.Count; i++)
            {
                if (i != codes.Count - 1)
                    condition += "'" + codes[i] + "', ";
                else condition += "'" + codes[i] + "') ";
            }
            if (codes.Count != 0)
                commandText += condition;
            SqlDataSourceCompetencesFiltered.SelectCommand = commandText;
            Competence_FondTable.DataBind();
            Competence_CriterionsTable.DataBind();
            PFTCom.DataBind();
            DropDown_PCTZ.DataBind();
            DropDown_PCTE.DataBind();
            DropDown_PCTR.DataBind();
            DropDown_PCTKT.DataBind();
            DropDown_PCTPR.DataBind();
            DropDown_PCTT.DataBind();
            DropDown_PCTC.DataBind();
        }

        private void SetFilter_PersonsOfDepartment()
        {
            string filterPersons = "SELECT UserName, PersonalId, DepartmentId FROM [AspNetUsers] " +
                "INNER JOIN [Cross_Departments_Personal] ON AspNetUsers.Id=Cross_Departments_Personal.PersonalId " +
                "INNER JOIN [Departments] ON Departments.id = Cross_Departments_Personal.DepartmentId " +
                "WHERE DepartmentId = " + DropDown_Department.SelectedItem.Value;
            SqlDataSourceDepartmentPersonal.SelectCommand = filterPersons;
            DrafterName.DataTextField = "UserName";
            DrafterName.DataValueField = "PersonalId";
            DrafterName.DataBind();
        }

        private void SetFilter_Profiles()
        {
            string filterProfile = "SELECT TrainingDirectionId, ProfileId, Name FROM [Profiles] " +
                "INNER JOIN [Cross_TrainingDirections_Profiles] ON Profiles.id=Cross_TrainingDirections_Profiles.ProfileId " +
                "INNER JOIN [Training_Directions] ON Training_Directions.id = Cross_TrainingDirections_Profiles.TrainingDirectionId " +
                "WHERE TrainingDirectionId = " + Training_Direction.SelectedItem.Value;
            SqlDataSourceProfiles.SelectCommand = filterProfile;
            DropDown_Profil.DataTextField = "Name";
            DropDown_Profil.DataValueField = "ProfileId";
            DropDown_Profil.DataBind();
        }

        private void SetFilter_OOPDisciplines(int disciplineId)
        {
            string filterDisciplinesBefore = "SELECT * FROM [Cross_Disciplines_Before] INNER JOIN [Disciplines] ON Cross_Disciplines_Before.DisciplineId_Before = Disciplines.id WHERE DisciplineId = " + disciplineId;
            string filterDisciplinesAfter = "SELECT * FROM [Cross_Disciplines_After] INNER JOIN [Disciplines] ON Cross_Disciplines_After.DisciplineId_After = Disciplines.id WHERE DisciplineId = " + disciplineId;
            SqlDataSourceDisciplinesBefore.SelectCommand = filterDisciplinesBefore;
            SqlDataSourceDisciplinesAfter.SelectCommand = filterDisciplinesAfter;
            DropDown_DisciplineInOOPAfter.DataSourceID = SqlDataSourceDisciplinesAfter.ID;
            DropDown_DisciplineInOOPBefore.DataSourceID = SqlDataSourceDisciplinesBefore.ID;
            DropDown_DisciplineInOOPAfter.DataBind();
            DropDown_DisciplineInOOPBefore.DataBind();
        }

        protected void Training_Direction_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (switcher.Checked == true)
                SetFilter_Profiles();
            RebuildTables(0);
        }

        protected void DropDown_Department_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (switcher.Checked == true)
                SetFilter_PersonsOfDepartment();
            RebuildTables(0);
        }

        protected void Discipline_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetFilter_OOPDisciplines(Convert.ToInt32(Discipline.SelectedItem.Value));
            RebuildTables(0);
        }

        #endregion

        //включение динамически созданных элементов
        private void BuildDinamycControls()
        {
            int drafterscount = Convert.ToInt32(AdditionalDraftersCount.Value);
            if (drafterscount != 0)
                for (int i = 0; i < drafterscount; i++)
                {
                    Panel panel = new Panel();
                    panel.ID = "DraftersPanel_" + i.ToString();
                    panel.Style["margin-top"] = "2px";
                    TextBox newTextBox = new TextBox();
                    newTextBox.CssClass = "form-control";
                    newTextBox.Attributes["placeholder"] = "Академ. статус";
                    newTextBox.ID = "AdditionalDrafterStatus_" + i.ToString();

                    DropDownList newList = new DropDownList();
                    newList.CssClass = "form-control";
                    newList.DataSourceID = "SqlDataSourceDepartmentPersonal";
                    newList.Style["margin-left"] = "3px";
                    newList.DataTextField = "UserName";
                    newList.DataValueField = DrafterName.DataValueField;
                    newList.ID = "AdditionalDrafterId_" + i.ToString();

                    panel.Controls.Add(newTextBox);
                    panel.Controls.Add(newList);
                    drafters_html_block.Controls.Add(panel);
                }
        }

        //нахождение строки списка с текстом равному itemText
        private void FindItem(DropDownList list, string itemText)
        {
            list.DataBind();
            for (int i = 0; i < list.Items.Count; i++)
                if (list.Items[i].Text == itemText)
                {
                    list.Items[i].Selected = true;
                    break;
                }
        }

        //private void FillControlls(int programid)
        //{
        //    Pro program = new Pro();
        //    BaseWork task = new BaseWork();
        //    program = task.GetProgram(programid);
        //    Institute.Text = program.Institute;
        //    Approver_Post.Text = program.Approver_Post;
        //    FindItem(Approver_Name, program.Approver_Name);
        //    Approve_Date.Text = program.Approve_Date.ToString("yyyy-MM-dd");
        //    Discipline.Text = program.Discipline;
        //    Training_Direction.Text = program.Training_Direction;
        //    FindItem(DropDown_Profil, program.Profil);
        //    Graduate_Qualification.Text = program.Graduate_Qualification;
        //    Training_Form.Text = program.Training_Form;
        //    StudyPeriod.Text = program.StudPeriod;
        //    City.Text = program.City;
        //    ObjectivesOfDiscipline.Text = program.ObjectivesOfDiscipline;
        //    DisciplineInOOP.Text = program.DisciplineInOOP;
        //    PlanDescription.Text = program.PlanDescription;
        //    DisciplineStructureContent.Text = program.DisciplineStructureContent;
        //    SemesteresContent.Text = program.SemesteresContent;
        //    EducationalTechnology.Text = program.EducationalTechnology;
        //    MonitoringTools_Description.Text = program.MonitoringTools_Description;
        //    ZachetDescription.Text = program.ZachetDescription;
        //    Zachteno.Text = program.ZachetTable.ZachetDone;
        //    NeZachteno.Text = program.ZachetTable.ZachetUnDone;
        //    ExamDescription.Text = program.ExamDescription;
        //    AttestatGreat.Text = program.ExamTable.AttestatGreat;
        //    AttestatGood.Text = program.ExamTable.AttestatGood;
        //    AttestatSatisfactorily.Text = program.ExamTable.AttestatSatisfactorily;
        //    AttestatNotSatisfactorily.Text = program.ExamTable.AttestatNotSatisfactorily;
        //    BasicLiterature.Text = program.BasicLiterature;
        //    AdditionalLiterature.Text = program.AdditionalLiterature;
        //    ITResources.Text = program.ITResources;
        //    MaterialTechnicalSupport.Text = program.MaterialTechnicalSupport;
        //    StudentRecommendations.Text = program.StudentRecommendations;
        //    TeacherRecommendations.Text = program.TeacherRecommendations;
        //    DrafterStatus.Text = program.DrafterStatus;
        //    FindItem(DrafterName, program.DrafterName);
        //    FindItem(DropDown_Department, program.Department);
        //    DraftDate.Text = program.DraftDate.ToString("yyyy-MM-dd");
        //    Protocol.Text = program.Protocol;
        //    DepartmentHeadStatus.Text = program.DepartmentHeadStatus;
        //    FindItem(DepartmentHeadName, program.DepartmentHeadName);
        //    AgreementDepartmentHeadStatus.Text = program.AgreementDepartmentHeadStatus;
        //    FindItem(AgreementDepartmentHeadName, program.AgreementDepartmentHeadName);
        //    AgreementDirectorStatus.Text = program.AgreementDirectorStatus;
        //    FindItem(AgreementDirectorName, program.AgreementDirectorName);

        //    try
        //    {
        //        ZachetQuestions.Text = program.Questions_Zachet.Questions;
        //        FindItem(DeveloperZachet, program.Questions_Zachet.Developer);
        //        ZachetQuestionsDate.Text = program.Questions_Zachet.Date.ToString("yyyy-MM-dd");
        //    }
        //    catch { }

        //    try
        //    {
        //        ExamQuestions.Text = program.Questions_Exam.Questions;
        //        FindItem(DeveloperExam, program.Questions_Exam.Developer);
        //        ExamQuestionsDate.Text = program.Questions_Exam.Date.ToString("yyyy-MM-dd");
        //    }
        //    catch { }

        //    try
        //    {
        //        ReferatTopics.Text = program.Questions_Referat.Questions;
        //        FindItem(DeveloperReferat,  program.Questions_Referat.Developer);
        //        ReferatTopicsDate.Text = program.Questions_Referat.Date.ToString("yyyy-MM-dd");
        //    }
        //    catch { }

        //    try
        //    {
        //        KTTopics.Text = program.Questions_KT.Questions;
        //        FindItem(DeveloperKT, program.Questions_KT.Developer);
        //        KTDate.Text = program.Questions_KT.Date.ToString("yyyy-MM-dd");
        //    }
        //    catch { }

        //    try
        //    {
        //        PraktikTopics.Text = program.Questions_Praktika.Questions;
        //        FindItem(DeveloperPraktika, program.Questions_Praktika.Developer);
        //        PraktikaTopicsDate.Text = program.Questions_Praktika.Date.ToString("yyyy-MM-dd");
        //    }
        //    catch { }

        //    try
        //    {
        //        TestsFond.Text = program.Questions_Test.Questions;
        //        FindItem(DeveloperTest, program.Questions_Test.Developer);
        //        TestsFondDate.Text = program.Questions_Test.Date.ToString("yyyy-MM-dd");
        //    }
        //    catch { }

        //DSTrowscount.Value = program.DSTTable.Count.ToString();
        //    int g = 0;
        //    for (int i = 0; i<program.DSTTable.Count; i++)
        //    {
        //        TableRow row = new TableRow();
        //        for (int j = 0; j< 15; j++)
        //        {
        //            TableCell cell = new TableCell();
        //row.Cells.Add(cell);
        //        }

        //        if ((program.DSTTable[i].Topic == "Часы за семестр") ||
        //            (program.DSTTable[i].Topic == "Часы за весь период"))
        //        { g++; }
        //        else { row.Cells[0].Text = (i + 1 - g).ToString(); }

        //        row.Cells[1].Text = program.DSTTable[i].Topic;
        //        row.Cells[2].Text = program.DSTTable[i].Semestr.ToString();
        //        row.Cells[3].Text = program.DSTTable[i].Week.ToString();
        //        row.Cells[4].Text = program.DSTTable[i].L.ToString();
        //        row.Cells[5].Text = program.DSTTable[i].PS.ToString();
        //        row.Cells[6].Text = program.DSTTable[i].Lab.ToString();
        //        row.Cells[7].Text = program.DSTTable[i].SRS.ToString();
        //        row.Cells[8].Text = program.DSTTable[i].KSR.ToString();
        //        row.Cells[9].Text = program.DSTTable[i].KR.ToString();
        //        row.Cells[10].Text = program.DSTTable[i].KP.ToString();
        //        row.Cells[11].Text = program.DSTTable[i].RGR.ToString();
        //        row.Cells[12].Text = program.DSTTable[i].PrepToLR;
        //        row.Cells[13].Text = program.DSTTable[i].KRend.ToString();
        //        row.Cells[14].Text = program.DSTTable[i].AttestatForm;

        //        for (int k = 0; k< 15; k++)
        //            if (row.Cells[k].Text == "0") row.Cells[k].Text = "";

        //        DisciplineStructureTable.Rows.Add(row);
        //    }

        //    ptrowscount.Value = program.PlanTable.Count.ToString();
        //    for (int i = 0; i < program.PlanTable.Count; i++)
        //    {
        //        TableRow row = new TableRow();
        //        for (int j = 0; j < 3; j++)
        //        {
        //            TableCell cell = new TableCell();
        //            row.Cells.Add(cell);
        //        }
        //        row.Cells[0].Text = program.PlanTable[i].Сompetence;
        //        row.Cells[1].Text = program.PlanTable[i].Ability;
        //        row.Cells[2].Text = program.PlanTable[i].Enumeration;
        //        PlanTable.Rows.Add(row);
        //    }

        //    ftrowscount.Value = program.FondTable.Count.ToString();
        //    for (int i = 0; i < program.FondTable.Count; i++)
        //    {
        //        TableRow row = new TableRow();
        //        for (int j = 0; j < 2; j++)
        //        {
        //            TableCell cell = new TableCell();
        //            row.Cells.Add(cell);
        //        }
        //        row.Cells[0].Text = program.FondTable[i].Сompetence;
        //        row.Cells[1].Text = program.FondTable[i].Ability;
        //        MonitoringTools_FondTable.Rows.Add(row);
        //    }

        //    CTrowscount.Value = program.CriterionsTable.Count.ToString();
        //    for (int i = 0; i < program.CriterionsTable.Count; i++)
        //    {
        //        TableRow row = new TableRow();
        //        for (int j = 0; j < 6; j++)
        //        {
        //            TableCell cell = new TableCell();
        //            row.Cells.Add(cell);
        //        }
        //        row.Cells[0].Text = program.CriterionsTable[i].Competence;
        //        row.Cells[1].Text = program.CriterionsTable[i].ToKnow_Description;
        //        row.Cells[2].Text = program.CriterionsTable[i].ToKnow[0];
        //        row.Cells[3].Text = program.CriterionsTable[i].ToKnow[1];
        //        row.Cells[4].Text = program.CriterionsTable[i].ToKnow[2];
        //        row.Cells[5].Text = program.CriterionsTable[i].ToKnow[3];
        //        CriterionsTable.Rows.Add(row);

        //        TableRow row_1 = new TableRow();
        //        for (int j = 0; j < 6; j++)
        //        {
        //            TableCell cell = new TableCell();
        //            row_1.Cells.Add(cell);
        //        }
        //        row_1.Cells[0].Text = "-//-//-";
        //        row_1.Cells[1].Text = program.CriterionsTable[i].ToDo_Description;
        //        row_1.Cells[2].Text = program.CriterionsTable[i].ToDo[0];
        //        row_1.Cells[3].Text = program.CriterionsTable[i].ToDo[1];
        //        row_1.Cells[4].Text = program.CriterionsTable[i].ToDo[2];
        //        row_1.Cells[5].Text = program.CriterionsTable[i].ToDo[3];
        //        CriterionsTable.Rows.Add(row_1);

        //        TableRow row_2 = new TableRow();
        //        for (int j = 0; j < 6; j++)
        //        {
        //            TableCell cell = new TableCell();
        //            row_2.Cells.Add(cell);
        //        }
        //        row_2.Cells[0].Text = "-//-//-";
        //        row_2.Cells[1].Text = program.CriterionsTable[i].ToMaster_Description;
        //        row_2.Cells[2].Text = program.CriterionsTable[i].ToMaster[0];
        //        row_2.Cells[3].Text = program.CriterionsTable[i].ToMaster[1];
        //        row_2.Cells[4].Text = program.CriterionsTable[i].ToMaster[2];
        //        row_2.Cells[5].Text = program.CriterionsTable[i].ToMaster[3];
        //        CriterionsTable.Rows.Add(row_2);
        //    }

        //    PFT_1rowscount.Value = program.PFT_1.Count.ToString();
        //    for (int i = 0; i < program.PFT_1.Count; i++)
        //    {
        //        TableRow row = new TableRow();
        //        for (int j = 0; j < 6; j++)
        //        {
        //            TableCell cell = new TableCell();
        //            row.Cells.Add(cell);
        //        }
        //        row.Cells[0].Text = program.PFT_1[i].Index;
        //        row.Cells[1].Text = program.PFT_1[i].Formul;
        //        row.Cells[2].Text = program.PFT_1[i].KomponentsList;
        //        row.Cells[3].Text = program.PFT_1[i].CompetenceTechnology;
        //        row.Cells[4].Text = program.PFT_1[i].AssessForm;
        //        row.Cells[5].Text = program.PFT_1[i].Steps;
        //        PrilFondTable_1.Rows.Add(row);
        //    }

        //    PFT_2rowscount.Value = program.PFT_2.Count.ToString();
        //    for (int i = 0; i < program.PFT_2.Count; i++)
        //    {
        //        TableRow row = new TableRow();
        //        for (int j = 0; j < 6; j++)
        //        {
        //            TableCell cell = new TableCell();
        //            row.Cells.Add(cell);
        //        }
        //        row.Cells[0].Text = program.PFT_2[i].Index;
        //        row.Cells[1].Text = program.PFT_2[i].Formul;
        //        row.Cells[2].Text = program.PFT_2[i].KomponentsList;
        //        row.Cells[3].Text = program.PFT_2[i].CompetenceTechnology;
        //        row.Cells[4].Text = program.PFT_2[i].AssessForm;
        //        row.Cells[5].Text = program.PFT_2[i].Steps;
        //        PrilFondTable_2.Rows.Add(row);
        //    }

        //    PFT_3rowscount.Value = program.PFT_3.Count.ToString();
        //    for (int i = 0; i < program.PFT_3.Count; i++)
        //    {
        //        TableRow row = new TableRow();
        //        for (int j = 0; j < 6; j++)
        //        {
        //            TableCell cell = new TableCell();
        //            row.Cells.Add(cell);
        //        }
        //        row.Cells[0].Text = program.PFT_3[i].Index;
        //        row.Cells[1].Text = program.PFT_3[i].Formul;
        //        row.Cells[2].Text = program.PFT_3[i].KomponentsList;
        //        row.Cells[3].Text = program.PFT_3[i].CompetenceTechnology;
        //        row.Cells[4].Text = program.PFT_3[i].AssessForm;
        //        row.Cells[5].Text = program.PFT_3[i].Steps;
        //        PrilFondTable_3.Rows.Add(row);
        //    }

        //    PATrowscount.Value = program.PAT.Count.ToString();
        //    for (int i = 0; i < program.PAT.Count; i++)
        //    {
        //        TableRow row = new TableRow();
        //        for (int j = 0; j < 4; j++)
        //        {
        //            TableCell cell = new TableCell();
        //            row.Cells.Add(cell);
        //        }
        //        row.Cells[0].Text = (i + 1).ToString();
        //        row.Cells[1].Text = program.PAT[i].Name;
        //        row.Cells[2].Text = program.PAT[i].Spec;
        //        row.Cells[3].Text = program.PAT[i].InFOS;
        //        PrilAssessTable.Rows.Add(row);
        //    }

        //    PCT_zachetrowscount.Value = program.PCT_Zachet.Count.ToString();
        //    for (int i = 0; i < program.PCT_Zachet.Count; i++)
        //    {
        //        TableRow row = new TableRow();
        //        for (int j = 0; j < 5; j++)
        //        {
        //            TableCell cell = new TableCell();
        //            row.Cells.Add(cell);
        //        }
        //        row.Cells[0].Text = program.PCT_Zachet[i].Discription;
        //        row.Cells[1].Text = program.PCT_Zachet[i].Rezult;
        //        row.Cells[2].Text = program.PCT_Zachet[i].Topics;
        //        row.Cells[3].Text = program.PCT_Zachet[i].Zachet;
        //        row.Cells[4].Text = program.PCT_Zachet[i].NeZachet;
        //        PrilCompetenceTable_Zachet.Rows.Add(row);
        //    }

        //    PCT_examrowscount.Value = program.PCT_Exam.Count.ToString();
        //    for (int i = 0; i < program.PCT_Exam.Count; i++)
        //    {
        //        TableRow row = new TableRow();
        //        for (int j = 0; j < 7; j++)
        //        {
        //            TableCell cell = new TableCell();
        //            row.Cells.Add(cell);
        //        }
        //        row.Cells[0].Text = program.PCT_Exam[i].Discription;
        //        row.Cells[1].Text = program.PCT_Exam[i].Rezult;
        //        row.Cells[2].Text = program.PCT_Exam[i].Topics;
        //        row.Cells[3].Text = program.PCT_Exam[i].On2;
        //        row.Cells[4].Text = program.PCT_Exam[i].On3;
        //        row.Cells[5].Text = program.PCT_Exam[i].On4;
        //        row.Cells[6].Text = program.PCT_Exam[i].On5;
        //        PrilCompetenceTable_Exam.Rows.Add(row);
        //    }

        //    PCT_referatrowscount.Value = program.PCT_Referat.Count.ToString();
        //    for (int i = 0; i < program.PCT_Referat.Count; i++)
        //    {
        //        TableRow row = new TableRow();
        //        for (int j = 0; j < 7; j++)
        //        {
        //            TableCell cell = new TableCell();
        //            row.Cells.Add(cell);
        //        }
        //        row.Cells[0].Text = program.PCT_Referat[i].Discription;
        //        row.Cells[1].Text = program.PCT_Referat[i].Rezult;
        //        row.Cells[2].Text = program.PCT_Referat[i].Topics;
        //        row.Cells[3].Text = program.PCT_Referat[i].On2;
        //        row.Cells[4].Text = program.PCT_Referat[i].On3;
        //        row.Cells[5].Text = program.PCT_Referat[i].On4;
        //        row.Cells[6].Text = program.PCT_Referat[i].On5;
        //        PrilCompetenceTable_Referat.Rows.Add(row);
        //    }

        //    PCT_ktrowscount.Value = program.PCT_KT.Count.ToString();
        //    for (int i = 0; i < program.PCT_KT.Count; i++)
        //    {
        //        TableRow row = new TableRow();
        //        for (int j = 0; j < 7; j++)
        //        {
        //            TableCell cell = new TableCell();
        //            row.Cells.Add(cell);
        //        }
        //        row.Cells[0].Text = program.PCT_KT[i].Discription;
        //        row.Cells[1].Text = program.PCT_KT[i].Rezult;
        //        row.Cells[2].Text = program.PCT_KT[i].Topics;
        //        row.Cells[3].Text = program.PCT_KT[i].On2;
        //        row.Cells[4].Text = program.PCT_KT[i].On3;
        //        row.Cells[5].Text = program.PCT_KT[i].On4;
        //        row.Cells[6].Text = program.PCT_KT[i].On5;
        //        PrilCompetenceTable_KT.Rows.Add(row);
        //    }

        //    PCT_praktikarowscount.Value = program.PCT_Praktika.Count.ToString();
        //    for (int i = 0; i < program.PCT_Praktika.Count; i++)
        //    {
        //        TableRow row = new TableRow();
        //        for (int j = 0; j < 7; j++)
        //        {
        //            TableCell cell = new TableCell();
        //            row.Cells.Add(cell);
        //        }
        //        row.Cells[0].Text = program.PCT_Praktika[i].Discription;
        //        row.Cells[1].Text = program.PCT_Praktika[i].Rezult;
        //        row.Cells[2].Text = program.PCT_Praktika[i].Topics;
        //        row.Cells[3].Text = program.PCT_Praktika[i].On2;
        //        row.Cells[4].Text = program.PCT_Praktika[i].On3;
        //        row.Cells[5].Text = program.PCT_Praktika[i].On4;
        //        row.Cells[6].Text = program.PCT_Praktika[i].On5;
        //        PrilCompetenceTable_Praktika.Rows.Add(row);
        //    }

        //    PCT_testrowscount.Value = program.PCT_Test.Count.ToString();
        //    for (int i = 0; i < program.PCT_Test.Count; i++)
        //    {
        //        TableRow row = new TableRow();
        //        for (int j = 0; j < 7; j++)
        //        {
        //            TableCell cell = new TableCell();
        //            row.Cells.Add(cell);
        //        }
        //        row.Cells[0].Text = program.PCT_Test[i].Discription;
        //        row.Cells[1].Text = program.PCT_Test[i].Rezult;
        //        row.Cells[2].Text = program.PCT_Test[i].Topics;
        //        row.Cells[3].Text = program.PCT_Test[i].On2;
        //        row.Cells[4].Text = program.PCT_Test[i].On3;
        //        row.Cells[5].Text = program.PCT_Test[i].On4;
        //        row.Cells[6].Text = program.PCT_Test[i].On5;
        //        PrilCompetenceTable_Test.Rows.Add(row);
        //    }

        //}

        //расстановка дат

        private void CreateDefoultDates()
        {
            Approve_Date.Text = DateTime.Today.ToString("yyyy-MM-dd");
            DraftDate.Text = DateTime.Today.ToString("yyyy-MM-dd");
        }

        //обработка таблиц
        #region TableTasks

        //обновление всех таблиц
        //task 1 вставить строку,  task -1 удалить строку, task 0 обновить таблицу
        private void RebuildTables(int index)
        {
            switch (index)
            {
                case 0:
                    PlanTableTask(0);
                    FondTableTask(0);
                    CriterionsTableTask(0);
                    DSTTask(0);
                    PFTTask(0, 0);
                    PATTask(0);
                    PCTzachetTask(0);
                    PCTexamTask(0);
                    PCTreferatTask(0);
                    PCTktTask(0);
                    PCTpraktikaTask(0);
                    PCTtestTask(0);
                    PCTcursTask(0);
                    break;
                case 1:
                    FondTableTask(0);
                    CriterionsTableTask(0);
                    DSTTask(0);
                    PFTTask(0, 0);
                    PATTask(0);
                    PCTzachetTask(0);
                    PCTexamTask(0);
                    PCTreferatTask(0);
                    PCTktTask(0);
                    PCTpraktikaTask(0);
                    PCTtestTask(0);
                    PCTcursTask(0);
                    break;
                case 2:
                    PlanTableTask(0);
                    CriterionsTableTask(0);
                    DSTTask(0);
                    PFTTask(0, 0);
                    PATTask(0);
                    PCTzachetTask(0);
                    PCTexamTask(0);
                    PCTreferatTask(0);
                    PCTktTask(0);
                    PCTpraktikaTask(0);
                    PCTtestTask(0);
                    PCTcursTask(0);
                    break;
                case 3:
                    PlanTableTask(0);
                    FondTableTask(0);
                    DSTTask(0);
                    PFTTask(0, 0);
                    PATTask(0);
                    PCTzachetTask(0);
                    PCTexamTask(0);
                    PCTreferatTask(0);
                    PCTktTask(0);
                    PCTpraktikaTask(0);
                    PCTtestTask(0);
                    PCTcursTask(0);
                    break;
                case 4:
                    PlanTableTask(0);
                    FondTableTask(0);
                    CriterionsTableTask(0);
                    PFTTask(0, 0);
                    PATTask(0);
                    PCTzachetTask(0);
                    PCTexamTask(0);
                    PCTreferatTask(0);
                    PCTktTask(0);
                    PCTpraktikaTask(0);
                    PCTtestTask(0);
                    PCTcursTask(0);
                    break;
                case 5:
                    PlanTableTask(0);
                    FondTableTask(0);
                    CriterionsTableTask(0);
                    DSTTask(0);
                    PFTTask(0, 2);
                    PFTTask(0, 3);
                    PATTask(0);
                    PCTzachetTask(0);
                    PCTexamTask(0);
                    PCTreferatTask(0);
                    PCTktTask(0);
                    PCTpraktikaTask(0);
                    PCTtestTask(0);
                    PCTcursTask(0);
                    break;
                case 6:
                    PlanTableTask(0);
                    FondTableTask(0);
                    CriterionsTableTask(0);
                    DSTTask(0);
                    PFTTask(0, 1);
                    PFTTask(0, 3);
                    PATTask(0);
                    PCTzachetTask(0);
                    PCTexamTask(0);
                    PCTreferatTask(0);
                    PCTktTask(0);
                    PCTpraktikaTask(0);
                    PCTtestTask(0);
                    PCTcursTask(0);
                    break;
                case 7:
                    PlanTableTask(0);
                    FondTableTask(0);
                    CriterionsTableTask(0);
                    DSTTask(0);
                    PFTTask(0, 1);
                    PFTTask(0, 2);
                    PATTask(0);
                    PCTzachetTask(0);
                    PCTexamTask(0);
                    PCTreferatTask(0);
                    PCTktTask(0);
                    PCTpraktikaTask(0);
                    PCTtestTask(0);
                    PCTcursTask(0);
                    break;
                case 8:
                    PlanTableTask(0);
                    FondTableTask(0);
                    CriterionsTableTask(0);
                    DSTTask(0);
                    PFTTask(0, 0);
                    PCTzachetTask(0);
                    PCTexamTask(0);
                    PCTreferatTask(0);
                    PCTktTask(0);
                    PCTpraktikaTask(0);
                    PCTtestTask(0);
                    PCTcursTask(0);
                    break;
                case 9:
                    PlanTableTask(0);
                    FondTableTask(0);
                    CriterionsTableTask(0);
                    DSTTask(0);
                    PFTTask(0, 0);
                    PATTask(0);
                    PCTexamTask(0);
                    PCTreferatTask(0);
                    PCTktTask(0);
                    PCTpraktikaTask(0);
                    PCTtestTask(0);
                    PCTcursTask(0);
                    break;
                case 10:
                    PlanTableTask(0);
                    FondTableTask(0);
                    CriterionsTableTask(0);
                    DSTTask(0);
                    PFTTask(0, 0);
                    PATTask(0);
                    PCTzachetTask(0);
                    PCTreferatTask(0);
                    PCTktTask(0);
                    PCTpraktikaTask(0);
                    PCTtestTask(0);
                    PCTcursTask(0);
                    break;
                case 11:
                    PlanTableTask(0);
                    FondTableTask(0);
                    CriterionsTableTask(0);
                    DSTTask(0);
                    PFTTask(0, 0);
                    PATTask(0);
                    PCTzachetTask(0);
                    PCTexamTask(0);
                    PCTktTask(0);
                    PCTpraktikaTask(0);
                    PCTtestTask(0);
                    PCTcursTask(0);
                    break;
                case 12:
                    PlanTableTask(0);
                    FondTableTask(0);
                    CriterionsTableTask(0);
                    DSTTask(0);
                    PFTTask(0, 0);
                    PATTask(0);
                    PCTzachetTask(0);
                    PCTexamTask(0);
                    PCTreferatTask(0);
                    PCTpraktikaTask(0);
                    PCTtestTask(0);
                    PCTcursTask(0);
                    break;
                case 13:
                    PlanTableTask(0);
                    FondTableTask(0);
                    CriterionsTableTask(0);
                    DSTTask(0);
                    PFTTask(0, 0);
                    PATTask(0);
                    PCTzachetTask(0);
                    PCTexamTask(0);
                    PCTreferatTask(0);
                    PCTktTask(0);
                    PCTtestTask(0);
                    PCTcursTask(0);
                    break;
                case 14:
                    PlanTableTask(0);
                    FondTableTask(0);
                    CriterionsTableTask(0);
                    DSTTask(0);
                    PFTTask(0, 0);
                    PATTask(0);
                    PCTzachetTask(0);
                    PCTexamTask(0);
                    PCTreferatTask(0);
                    PCTktTask(0);
                    PCTpraktikaTask(0);
                    PCTcursTask(0);
                    break;
                case 15:
                    PlanTableTask(0);
                    FondTableTask(0);
                    CriterionsTableTask(0);
                    DSTTask(0);
                    PFTTask(0, 0);
                    PATTask(0);
                    PCTzachetTask(0);
                    PCTexamTask(0);
                    PCTreferatTask(0);
                    PCTktTask(0);
                    PCTpraktikaTask(0);
                    PCTtestTask(0);
                    break;
            }
        }

        private void PlanTableTask(int task)
        {
            int count = Convert.ToInt32(ptrowscount.Value);

            if (task == -1)
                if (count == 0) goto end;
                else count -= 1;

            if (task == 1) count += 1;
            ptrowscount.Value = count.ToString();

            int i, j;

            for (i = 1; i <= count; i++)
            {
                TableRow tRow = new TableRow();
                PlanTable.Rows.Add(tRow);
                for (j = 0; j < 3; j++)
                {
                    TableCell tCell = new TableCell();
                    tRow.Cells.Add(tCell);
                }
                
                if ((i == count) && (task == 1))
                {
                    BaseWork basetask = new BaseWork();
                    Competence com = new Competence();
                    tRow.Cells[0].Text = Сompetence_PlanTable.SelectedItem.Text;
                    tRow.Cells[1].Text = Сompetence_PlanTable.SelectedItem.Value;
                    tRow.Cells[2].Text = Enumeration_PlanTable.Text;
                }
            }
        end: { }

        }

        private void FondTableTask(int task)
        {
            int count = Convert.ToInt32(ftrowscount.Value);
            if (task == -1)
                if (count == 0) goto end;
                else count -= 1;

            if (task == 1) count += 1;
            ftrowscount.Value = count.ToString();

            int i, j;
            for (i = 1; i <= count; i++)
            {
                TableRow tRow = new TableRow();
                MonitoringTools_FondTable.Rows.Add(tRow);
                for (j = 0; j < 2; j++)
                {
                    TableCell tCell = new TableCell();
                    tRow.Cells.Add(tCell);
                }
                if ((i == count) && (task == 1))
                {
                    Competence com = new Competence();
                    BaseWork basetask = new BaseWork();
                    tRow.Cells[0].Text = Competence_FondTable.SelectedItem.Text;
                    tRow.Cells[1].Text = Competence_FondTable.SelectedItem.Value;
                }
            }
        end: { }

        }

        private void CriterionsTableTask(int task)
        {
            BaseWork basetask = new BaseWork();

            int count = Convert.ToInt32(CTrowscount.Value);

            if (task == -1)
                if (count == 1) goto end;
                else count -= 3;

            if (task == 1) count += 3;
            CTrowscount.Value = count.ToString();

            int i, j;
            for (i = 2; i <= count; i++)
            {
                TableRow tRow = new TableRow();
                CriterionsTable.Rows.Add(tRow);
                for (j = 0; j <= 5; j++)
                {
                    TableCell tCell = new TableCell();
                    tRow.Cells.Add(tCell);
                }
                
                if ((i == count - 2) && (task == 1))
                {

                    tRow.Cells[0].Text = Competence_CriterionsTable.SelectedItem.Text + " - " + Competence_CriterionsTable.SelectedItem.Value;
                    tRow.Cells[1].Text = IndicatorCriterionsTable_Know.Text;
                    tRow.Cells[2].Text = CriterionCriterionsTable_Know_2.Text;
                    tRow.Cells[3].Text = CriterionCriterionsTable_Know_3.Text;
                    tRow.Cells[4].Text = CriterionCriterionsTable_Know_4.Text;
                    tRow.Cells[5].Text = CriterionCriterionsTable_Know_5.Text;
                }
                if ((i == count - 1) && (task == 1))
                {
                    tRow.Cells[0].Text = "-//-//-";
                    tRow.Cells[1].Text = IndicatorCriterionsTable_Can.Text;
                    tRow.Cells[2].Text = CriterionCriterionsTable_Can_2.Text;
                    tRow.Cells[3].Text = CriterionCriterionsTable_Can_3.Text;
                    tRow.Cells[4].Text = CriterionCriterionsTable_Can_4.Text;
                    tRow.Cells[5].Text = CriterionCriterionsTable_Can_5.Text;
                }
                if ((i == count) && (task == 1))
                {
                    tRow.Cells[0].Text = "-//-//-";
                    tRow.Cells[1].Text = IndicatorCriterionsTable_Wield.Text;
                    tRow.Cells[2].Text = CriterionCriterionsTable_Wield_2.Text;
                    tRow.Cells[3].Text = CriterionCriterionsTable_Wield_3.Text;
                    tRow.Cells[4].Text = CriterionCriterionsTable_Wield_4.Text;
                    tRow.Cells[5].Text = CriterionCriterionsTable_Wield_5.Text;
                }
            }
        end: { }
        }

        private void DSTTask(int task)
        {
            int count = Convert.ToInt32(DSTrowscount.Value);
            for (int i = 0; i < count; i++)
            {
                TableRow row = new TableRow();
                DisciplineStructureTable.Rows.Add(row);
                for (int j = 0; j < 15; j++)
                {
                    TableCell cell = new TableCell();
                    row.Cells.Add(cell);
                }
            }

            switch (task)
            {
                case 1:
                    try
                    {
                        if ((Convert.ToInt32(DSCT_sem.Text) <= 0) || (Convert.ToInt32(DSCT_sem.Text) > 10))
                            break;
                    }
                    catch { }

                    TableRow row = new TableRow();
                    DisciplineStructureTable.Rows.Add(row);
                    for (int j = 0; j < 15; j++)
                    {
                        TableCell cell = new TableCell();
                        row.Cells.Add(cell);
                    }
                    row.Cells[1].Text = DSCT_topic.Text;
                    row.Cells[2].Text = DSCT_sem.Text;
                    row.Cells[3].Text = DSCT_week.Text;
                    row.Cells[4].Text = DSCT_L.Text;
                    row.Cells[5].Text = DSCT_PS.Text;
                    row.Cells[6].Text = DSCT_Lab.Text;
                    row.Cells[7].Text = DSCT_SRS.Text;
                    row.Cells[8].Text = DSCT_KSR.Text;
                    row.Cells[9].Text = DSCT_KR.Text;
                    row.Cells[10].Text = DSCT_KP.Text;
                    row.Cells[11].Text = DSCT_RGR.Text;
                    row.Cells[12].Text = DSCT_preptolr.Text;
                    row.Cells[13].Text = DSCT_KRend.Text;
                    row.Cells[14].Text = DSCT_AttestatForm.Text;
                    count++;
                    DSTrowscount.Value = count.ToString();
                    break;

                case -1:
                    bool deleteble = false;
                    string rownumber = rowNumberDSCT.Text;
                    TableRow rowtoDelete = new TableRow();
                    for (int i = 2; i < DisciplineStructureTable.Rows.Count; i++)
                        if (DisciplineStructureTable.Rows[i].Cells[0].Text == rownumber)
                        {
                            rowtoDelete = DisciplineStructureTable.Rows[i];
                            deleteble = true;
                            break;
                        }
                    if (deleteble == true)
                    {
                        DisciplineStructureTable.Rows.Remove(rowtoDelete);
                        count--;
                    }
                    DSTrowscount.Value = count.ToString();
                    break;

                case 2:

                    List<TableRow> delrows = new List<TableRow>();
                    for (int i = 2; i < DisciplineStructureTable.Rows.Count; i++)
                        if ((DisciplineStructureTable.Rows[i].Cells[1].Text == "Часы за семестр")
                            || (DisciplineStructureTable.Rows[i].Cells[1].Text == "Часы за весь период"))
                            delrows.Add(DisciplineStructureTable.Rows[i]);
                    for (int i = 0; i < delrows.Count; i++)
                        DisciplineStructureTable.Rows.Remove(delrows[i]);

                    Table bufTable = new Table();
                    bufTable = DisciplineStructureTable;

                    string attestat = "";
                    List<int> sems = new List<int>();
                    List<int> summs = new List<int>();
                    List<int> summsglobal = new List<int>();
                    for (int i = 0; i < 9; i++)
                        summs.Add(0);

                    for (int i = 0; i < 9; i++)
                        summsglobal.Add(0);

                    for (int i = 2; i < bufTable.Rows.Count; i++)
                    {
                        int semNumber = Convert.ToInt32(bufTable.Rows[i].Cells[2].Text);
                            sems.Add(semNumber);
                    }

                    sems = sems.Distinct().ToList();
                    sems.Sort();


                    for (int i = 0; i < sems.Count; i++)
                    {
                        for (int s = 0; s < 9; s++)
                            summs[s] = 0;
                        attestat = "";
                        List<TableRow> tablerows = new List<TableRow>();
                        for (int ti = 2; ti < bufTable.Rows.Count; ti++)
                        {
                            TableRow trow = bufTable.Rows[ti];
                            for (int c = 4; c < trow.Cells.Count; c++)
                                if (trow.Cells[c].Text == "")
                                    trow.Cells[c].Text = "0";

                            if (trow.Cells[2].Text == sems[i].ToString())
                                tablerows.Add(trow);
                        }

                        for (int tr = 0; tr < tablerows.Count; tr++)
                        {
                            attestat = tablerows[tr].Cells[14].Text;
                            tablerows[tr].Cells[14].Text = "";
                            DisciplineStructureTable.Rows.Add(tablerows[tr]);
                            summs[0] += Convert.ToInt32(tablerows[tr].Cells[4].Text);
                            summs[1] += Convert.ToInt32(tablerows[tr].Cells[5].Text);
                            summs[2] += Convert.ToInt32(tablerows[tr].Cells[6].Text);
                            summs[3] += Convert.ToInt32(tablerows[tr].Cells[7].Text);
                            summs[4] += Convert.ToInt32(tablerows[tr].Cells[8].Text);
                            summs[5] += Convert.ToInt32(tablerows[tr].Cells[9].Text);
                            summs[6] += Convert.ToInt32(tablerows[tr].Cells[10].Text);
                            summs[7] += Convert.ToInt32(tablerows[tr].Cells[11].Text);
                            summs[8] += Convert.ToInt32(tablerows[tr].Cells[13].Text);
                        }
                        
                        TableRow clockrow = new TableRow();
                        DisciplineStructureTable.Rows.Add(clockrow);
                        for (int j = 0; j < 15; j++)
                        {
                            TableCell clockcell = new TableCell();
                            clockrow.Cells.Add(clockcell);
                        }
                        clockrow.Cells[1].Text = "Часы за семестр";
                        clockrow.Cells[2].Text = sems[i].ToString();
                        clockrow.Cells[4].Text = summs[0].ToString();
                        clockrow.Cells[5].Text = summs[1].ToString();
                        clockrow.Cells[6].Text = summs[2].ToString();
                        clockrow.Cells[7].Text = summs[3].ToString();
                        clockrow.Cells[8].Text = summs[4].ToString();
                        clockrow.Cells[9].Text = summs[5].ToString();
                        clockrow.Cells[10].Text = summs[6].ToString();
                        clockrow.Cells[11].Text = summs[7].ToString();
                        clockrow.Cells[13].Text = summs[8].ToString();
                        clockrow.Cells[14].Text = attestat;

                        summsglobal[0] += summs[0];
                        summsglobal[1] += summs[1];
                        summsglobal[2] += summs[2];
                        summsglobal[3] += summs[3];
                        summsglobal[4] += summs[4];
                        summsglobal[5] += summs[5];
                        summsglobal[6] += summs[6];
                        summsglobal[7] += summs[7];
                        summsglobal[8] += summs[8];
                    }

                    TableRow clockgrow = new TableRow();
                    DisciplineStructureTable.Rows.Add(clockgrow);
                    for (int j = 0; j < 15; j++)
                    {
                        TableCell clockcell = new TableCell();
                        clockgrow.Cells.Add(clockcell);
                    }
                    clockgrow.Cells[1].Text = "Часы за весь период";
                    clockgrow.Cells[4].Text = summsglobal[0].ToString();
                    clockgrow.Cells[5].Text = summsglobal[1].ToString();
                    clockgrow.Cells[6].Text = summsglobal[2].ToString();
                    clockgrow.Cells[7].Text = summsglobal[3].ToString();
                    clockgrow.Cells[8].Text = summsglobal[4].ToString();
                    clockgrow.Cells[9].Text = summsglobal[5].ToString();
                    clockgrow.Cells[10].Text = summsglobal[6].ToString();
                    clockgrow.Cells[11].Text = summsglobal[7].ToString();
                    clockgrow.Cells[13].Text = summsglobal[8].ToString();

                    count = DisciplineStructureTable.Rows.Count - 2;
                    DSTrowscount.Value = count.ToString();
                    break;
            }

            int g = 0;
            for (int i = 2; i < DisciplineStructureTable.Rows.Count; i++)
            {
                for (int j = 3; j < 15; j++)
                    if (DisciplineStructureTable.Rows[i].Cells[j].Text == "0")
                        DisciplineStructureTable.Rows[i].Cells[j].Text = "";

                if ((DisciplineStructureTable.Rows[i].Cells[1].Text == "Часы за семестр")||(DisciplineStructureTable.Rows[i].Cells[1].Text == "Часы за весь период"))
                {
                    g++;
                    continue;
                }

                DisciplineStructureTable.Rows[i].Cells[0].Text = (i - 1 - g).ToString();
            }
        }

        public List<DSTTableRow> DSTTable_FillTask()
        {
            List<DSTTableRow> rows = new List<DSTTableRow>();
            for (int i = 2; i < DisciplineStructureTable.Rows.Count; i++)
            {
                DSTTableRow row = new DSTTableRow();

                try { row.Topic = DisciplineStructureTable.Rows[i].Cells[1].Text; }
                catch { }

                try { row.Semestr = Convert.ToInt32(DisciplineStructureTable.Rows[i].Cells[2].Text); }
                catch { }

                try { row.Week = Convert.ToInt32(DisciplineStructureTable.Rows[i].Cells[3].Text); }
                catch { }

                try { row.L = Convert.ToInt32(DisciplineStructureTable.Rows[i].Cells[4].Text); }
                catch { }

                try { row.PS = Convert.ToInt32(DisciplineStructureTable.Rows[i].Cells[5].Text); }
                catch { }

                try { row.Lab = Convert.ToInt32(DisciplineStructureTable.Rows[i].Cells[6].Text); }
                catch { }

                try { row.SRS = Convert.ToInt32(DisciplineStructureTable.Rows[i].Cells[7].Text); }
                catch { }

                try { row.KSR = Convert.ToInt32(DisciplineStructureTable.Rows[i].Cells[8].Text); }
                catch { }

                try { row.KR = Convert.ToInt32(DisciplineStructureTable.Rows[i].Cells[9].Text); }
                catch { }

                try { row.KP = Convert.ToInt32(DisciplineStructureTable.Rows[i].Cells[10].Text); }
                catch { }

                try { row.RGR = Convert.ToInt32(DisciplineStructureTable.Rows[i].Cells[11].Text); }
                catch { }

                try { row.PrepToLR = DisciplineStructureTable.Rows[i].Cells[12].Text; }
                catch { }

                try { row.KRend = Convert.ToInt32(DisciplineStructureTable.Rows[i].Cells[13].Text); }
                catch { }

                try { row.AttestatForm = DisciplineStructureTable.Rows[i].Cells[14].Text; }
                catch { }

                rows.Add(row);
            }
            
            return rows;
        }

        private void PFTTask(int task, int id)
        {

            Table table = new Table();
            int count = 0;
            int i, j;

            switch (id)
            {
                case 0:
                    PFTTask(0, 1);
                    PFTTask(0, 2);
                    PFTTask(0, 3);
                    break;
                case 1:
                    table = PrilFondTable_1;
                    count = Convert.ToInt32(PFT_1rowscount.Value);
                    break;
                case 2:
                    table = PrilFondTable_2;
                    count = Convert.ToInt32(PFT_2rowscount.Value);
                    break;
                case 3:
                    table = PrilFondTable_3;
                    count = Convert.ToInt32(PFT_3rowscount.Value);
                    break;
            }

            if (task == -1)
                if (count == 0) goto end;
                else count -= 1;
            else if (task == 1) count += 1;

            for (i = 0; i < count; i++)
            {
                TableRow row = new TableRow();
                table.Rows.Add(row);
                for (j = 0; j < 6; j++)
                {
                    TableCell cell = new TableCell();
                    row.Cells.Add(cell);
                }
                if ((task == 1) && (i == count - 1))
                {
                    BaseWork basetask = new BaseWork();
                    Competence com = new Competence();
                    row.Cells[0].Text = PFTCom.SelectedItem.Text;
                    row.Cells[1].Text = PFTCom.SelectedItem.Value;
                    row.Cells[2].Text = PFTKomponentsList.Text;
                    row.Cells[3].Text = PFTCompetenceTechnology.Text;
                    row.Cells[4].Text = PFTAssessForm.Text;
                    row.Cells[5].Text = PFTSteps.Text;
                }
            }

            switch (id)
            {
                case 1:
                    PrilFondTable_1 = table;
                    PFT_1rowscount.Value = count.ToString();
                    break;
                case 2:
                    PrilFondTable_2 = table;
                    PFT_2rowscount.Value = count.ToString();
                    break;
                case 3:
                    PrilFondTable_3 = table;
                    PFT_3rowscount.Value = count.ToString();
                    break;
            }

        end: { }

        }

        private void PATTask(int task)
        {
            int count = Convert.ToInt32(PATrowscount.Value);
            int i, j;
            if (task == -1)
                if (count == 0) goto end;
                else count -= 1;
            else if (task == 1) count += 1;

            PATrowscount.Value = count.ToString();

            for (i = 0; i < count; i++)
            {
                TableRow row = new TableRow();
                PrilAssessTable.Rows.Add(row);
                for (j = 0; j < 4; j++)
                {
                    TableCell cell = new TableCell();
                    row.Cells.Add(cell);
                }
                if ((i == count - 1) && (task == 1))
                {
                    row.Cells[0].Text = count.ToString();
                    row.Cells[1].Text = PATName.Text;
                    row.Cells[2].Text = PATSpec.Text;
                    row.Cells[3].Text = PATinFOS.Text;
                }
            }
        end: { }
        }

        private void PCTzachetTask(int task)
        {
            int count = Convert.ToInt32(PCT_zachetrowscount.Value);
            int i, j;
            if (task == -1)
                if (count == 0) goto end;
                else count -= 1;
            else if (task == 1) count += 1;

            PCT_zachetrowscount.Value = count.ToString();

            for (i = 0; i < count; i++)
            {
                TableRow row = new TableRow();
                PrilCompetenceTable_Zachet.Rows.Add(row);
                for (j = 0; j < 5; j++)
                {
                    TableCell cell = new TableCell();
                    row.Cells.Add(cell);
                }
                if ((i == count - 1) && (task == 1))
                {
                    row.Cells[0].Text = DropDown_PCTZ.SelectedItem.Text + " - " + DropDown_PCTZ.SelectedItem.Value.ToString();
                    row.Cells[1].Text = PCTZRezult.Text;
                    row.Cells[2].Text = PCTZTopics.Text;
                    row.Cells[3].Text = PCTZZachet.Text;
                    row.Cells[4].Text = PCTZNeZachet.Text;
                }
            }

        end: { }
        }

        private void PCTexamTask(int task)
        {
            int count = Convert.ToInt32(PCT_examrowscount.Value);
            int i, j;
            if (task == -1)
                if (count == 0) goto end;
                else count -= 1;
            else if (task == 1) count += 1;

            PCT_examrowscount.Value = count.ToString();

            for (i = 0; i < count; i++)
            {
                TableRow row = new TableRow();
                PrilCompetenceTable_Exam.Rows.Add(row);
                for (j = 0; j < 7; j++)
                {
                    TableCell cell = new TableCell();
                    row.Cells.Add(cell);
                }
                if ((i == count - 1) && (task == 1))
                {
                    row.Cells[0].Text = DropDown_PCTE.SelectedItem.Text + " - " + DropDown_PCTE.SelectedItem.Value.ToString(); 
                    row.Cells[1].Text = PCTERezult.Text;
                    row.Cells[2].Text = PCTETopics.Text;
                    row.Cells[3].Text = PCTE2.Text;
                    row.Cells[4].Text = PCTE3.Text;
                    row.Cells[5].Text = PCTE4.Text;
                    row.Cells[6].Text = PCTE5.Text;
                }
            }

        end: { }
        }

        private void PCTreferatTask(int task)
        {
            int count = Convert.ToInt32(PCT_referatrowscount.Value);
            int i, j;
            if (task == -1)
                if (count == 0) goto end;
                else count -= 1;
            else if (task == 1) count += 1;

            PCT_referatrowscount.Value = count.ToString();

            for (i = 0; i < count; i++)
            {
                TableRow row = new TableRow();
                PrilCompetenceTable_Referat.Rows.Add(row);
                for (j = 0; j < 7; j++)
                {
                    TableCell cell = new TableCell();
                    row.Cells.Add(cell);
                }
                if ((i == count - 1) && (task == 1))
                {
                    row.Cells[0].Text = DropDown_PCTR.SelectedItem.Text + " - " + DropDown_PCTR.SelectedItem.Value.ToString();
                    row.Cells[1].Text = PCTRRezult.Text;
                    row.Cells[2].Text = PCTRTopics.Text;
                    row.Cells[3].Text = PCTR2.Text;
                    row.Cells[4].Text = PCTR3.Text;
                    row.Cells[5].Text = PCTR4.Text;
                    row.Cells[6].Text = PCTR5.Text;
                }
            }

        end: { }
        }

        private void PCTktTask(int task)
        {
            int count = Convert.ToInt32(PCT_ktrowscount.Value);
            int i, j;
            if (task == -1)
                if (count == 0) goto end;
                else count -= 1;
            else if (task == 1) count += 1;

            PCT_ktrowscount.Value = count.ToString();

            for (i = 0; i < count; i++)
            {
                TableRow row = new TableRow();
                PrilCompetenceTable_KT.Rows.Add(row);
                for (j = 0; j < 7; j++)
                {
                    TableCell cell = new TableCell();
                    row.Cells.Add(cell);
                }
                if ((i == count - 1) && (task == 1))
                {
                    row.Cells[0].Text = DropDown_PCTKT.SelectedItem.Text + " - " + DropDown_PCTKT.SelectedItem.Value.ToString();
                    row.Cells[1].Text = PCTKTRezult.Text;
                    row.Cells[2].Text = PCTKTTopics.Text;
                    row.Cells[3].Text = PCTKT2.Text;
                    row.Cells[4].Text = PCTKT3.Text;
                    row.Cells[5].Text = PCTKT4.Text;
                    row.Cells[6].Text = PCTKT5.Text;
                }
            }

        end: { }
        }

        private void PCTpraktikaTask(int task)
        {
            int count = Convert.ToInt32(PCT_praktikarowscount.Value);
            int i, j;
            if (task == -1)
                if (count == 0) goto end;
                else count -= 1;
            else if (task == 1) count += 1;

            PCT_praktikarowscount.Value = count.ToString();

            for (i = 0; i < count; i++)
            {
                TableRow row = new TableRow();
                PrilCompetenceTable_Praktika.Rows.Add(row);
                for (j = 0; j < 7; j++)
                {
                    TableCell cell = new TableCell();
                    row.Cells.Add(cell);
                }
                if ((i == count - 1) && (task == 1))
                {
                    row.Cells[0].Text = DropDown_PCTPR.SelectedItem.Text + " - " + DropDown_PCTPR.SelectedItem.Value.ToString();
                    row.Cells[1].Text = PCTPRezult.Text;
                    row.Cells[2].Text = PCTPTopics.Text;
                    row.Cells[3].Text = PCTP2.Text;
                    row.Cells[4].Text = PCTP3.Text;
                    row.Cells[5].Text = PCTP4.Text;
                    row.Cells[6].Text = PCTP5.Text;
                }
            }

        end: { }
        }

        private void PCTtestTask(int task)
        {
            int count = Convert.ToInt32(PCT_testrowscount.Value);
            int i, j;
            if (task == -1)
                if (count == 0) goto end;
                else count -= 1;
            else if (task == 1) count += 1;

            PCT_testrowscount.Value = count.ToString();

            for (i = 0; i < count; i++)
            {
                TableRow row = new TableRow();
                PrilCompetenceTable_Test.Rows.Add(row);
                for (j = 0; j < 7; j++)
                {
                    TableCell cell = new TableCell();
                    row.Cells.Add(cell);
                }
                if ((i == count - 1) && (task == 1))
                {
                    row.Cells[0].Text = DropDown_PCTT.SelectedItem.Text + " - " + DropDown_PCTT.SelectedItem.Value.ToString();
                    row.Cells[1].Text = PCTTRezult.Text;
                    row.Cells[2].Text = PCTTTopics.Text;
                    row.Cells[3].Text = PCTT2.Text;
                    row.Cells[4].Text = PCTT3.Text;
                    row.Cells[5].Text = PCTT4.Text;
                    row.Cells[6].Text = PCTT5.Text;
                }
            }

        end: { }
        }

        private void PCTcursTask(int task)
        {
            int count = Convert.ToInt32(PCT_cursrowscount.Value);
            int i, j;
            if (task == -1)
                if (count == 0) goto end;
                else count -= 1;
            else if (task == 1) count += 1;

            PCT_cursrowscount.Value = count.ToString();

            for (i = 0; i < count; i++)
            {
                TableRow row = new TableRow();
                PrilCompetenceTable_Curs.Rows.Add(row);
                for (j = 0; j < 7; j++)
                {
                    TableCell cell = new TableCell();
                    row.Cells.Add(cell);
                }
                if ((i == count - 1) && (task == 1))
                {
                    row.Cells[0].Text = DropDown_PCTC.SelectedItem.Text + " - " + DropDown_PCTC.SelectedItem.Value.ToString();
                    row.Cells[1].Text = PCTCRezult.Text;
                    row.Cells[2].Text = PCTCTopics.Text;
                    row.Cells[3].Text = PCTC2.Text;
                    row.Cells[4].Text = PCTC3.Text;
                    row.Cells[5].Text = PCTC4.Text;
                    row.Cells[6].Text = PCTC5.Text;
                }
            }

        end: { }
        }

        #endregion TableTasks

        //обработчики кнопок по редактированию таблиц
        #region RowsControll
        protected void CreateRowPlanTable_Click(object sender, EventArgs e)
        {
            PlanTableTask(1);
            RebuildTables(1);
            List<string> codes = (List<string>)Session["CompetenceFilterCodes"];
            codes.Add(Сompetence_PlanTable.SelectedItem.Text);
            if(switcher.Checked == true)
                SetFilter_Competences();
        }
        protected void DeleteRowPlanTable_Click(object sender, EventArgs e)
        {
            PlanTableTask(-1);
            RebuildTables(1);
            List<string> codes = (List<string>)Session["CompetenceFilterCodes"];
            codes.Remove(Сompetence_PlanTable.SelectedItem.Text);
            if(switcher.Checked == true)
                SetFilter_Competences();
        }

        protected void CreateRowFondTable_Click(object sender, EventArgs e)
        {
            FondTableTask(1);
            RebuildTables(2);
        }
        protected void DeleteRowFondTable_Click(object sender, EventArgs e)
        {
            FondTableTask(-1);
            RebuildTables(2);
        }

        protected void CreateRowsCriterionsTable_Click(object sender, EventArgs e)
        {
            CriterionsTableTask(1);
            RebuildTables(3);
        }
        protected void DeleteRowsCriterionsTable_Click(object sender, EventArgs e)
        {
            CriterionsTableTask(-1);
            RebuildTables(3);
        }

        protected void AddRowDSCT_Button_Click(object sender, EventArgs e)
        {
            DSTTask(1);
            RebuildTables(4);
        }
        protected void DeleteRowDSCT_Button_Click(object sender, EventArgs e)
        {
            DSTTask(-1);
            RebuildTables(4);
        }
        
        protected void button_createRowInPFT_1_Click(object sender, EventArgs e)
        {
            PFTTask(1, 1);
            RebuildTables(5);
        }
        protected void button_createRowInPFT_2_Click(object sender, EventArgs e)
        {
            PFTTask(1, 2);
            RebuildTables(6);
        }
        protected void button_createRowInPFT_3_Click(object sender, EventArgs e)
        {
            PFTTask(1, 3);
            RebuildTables(7);
        }
        protected void button_deleteRowInPFT_1_Click(object sender, EventArgs e)
        {
            PFTTask(-1, 1);
            RebuildTables(5);
        }
        protected void button_deleteRowInPFT_2_Click(object sender, EventArgs e)
        {
            PFTTask(-1, 2);
            RebuildTables(6);
        }
        protected void button_deleteRowInPFT_3_Click(object sender, EventArgs e)
        {
            PFTTask(-1, 3);
            RebuildTables(7);
        }

        protected void button_addRowInPAT_Click(object sender, EventArgs e)
        {
            PATTask(1);
            RebuildTables(8);
        }
        protected void button_deleteRowInPAT_Click(object sender, EventArgs e)
        {
            PATTask(-1);
            RebuildTables(8);
        }

        protected void addRowInPCTzachet_Click(object sender, EventArgs e)
        {
            PCTzachetTask(1);
            RebuildTables(9);
        }
        protected void deleteRowInPCTzachet_Click(object sender, EventArgs e)
        {
            PCTzachetTask(-1);
            RebuildTables(9);
        }

        protected void addRowInPCTExamTable_Click(object sender, EventArgs e)
        {
            PCTexamTask(1);
            RebuildTables(10);
        }
        protected void deleteRowInPCTExamTable_Click(object sender, EventArgs e)
        {
            PCTexamTask(-1);
            RebuildTables(10);
        }

        protected void addRowInPCTReferatTable_Click(object sender, EventArgs e)
        {
            PCTreferatTask(1);
            RebuildTables(11);
        }
        protected void deleteRowInPCTReferatTable_Click(object sender, EventArgs e)
        {
            PCTreferatTask(-1);
            RebuildTables(11);
        }

        protected void addRowInPCTKTTable_Click(object sender, EventArgs e)
        {
            PCTktTask(1);
            RebuildTables(12);
        }
        protected void deleteRowInPCTKTTable_Click(object sender, EventArgs e)
        {
            PCTktTask(-1);
            RebuildTables(12);
        }

        protected void addRowInPCTPraktikaTable_Click(object sender, EventArgs e)
        {
            PCTpraktikaTask(1);
            RebuildTables(13);
        }
        protected void deleteRowInPCTPraktikaTable_Click(object sender, EventArgs e)
        {
            PCTpraktikaTask(-1);
            RebuildTables(13);
        }

        protected void addRowInPCTtestTable_Click(object sender, EventArgs e)
        {
            PCTtestTask(1);
            RebuildTables(14);
        }
        protected void deleteRowInPCTtestTable_Click(object sender, EventArgs e)
        {
            PCTtestTask(-1);
            RebuildTables(14);
        }

        protected void addRowInPCTcursTable_Click(object sender, EventArgs e)
        {
            PCTcursTask(1);
            RebuildTables(15);
        }
        protected void deleteRowInPCTcursTable_Click(object sender, EventArgs e)
        {
            PCTcursTask(-1);
            RebuildTables(15);
        }

        #endregion RowsControlls

        #region Создание РПД
        //создание новой рпд
        protected void Button_Create_Click(object sender, EventArgs e)
        {
            try
            {
                RebuildTables(0);
                Pro pro = new Pro();
                pro.Institute = Institute.Text;
                pro.Approver_Post = Approver_Post.Text;
                pro.Approver_Name = Approver_Name.SelectedItem.Text;
                pro.Approver_Id = Approver_Name.SelectedItem.Value;

                try
                {
                    string dateInput = Approve_Date.Text;
                    DateTime d = DateTime.Parse(dateInput);
                    pro.Approve_Date = d;
                }
                catch { }

                pro.Discipline = Discipline.Text;
                pro.Training_Direction = Training_Direction.SelectedItem.Text;
                pro.Profil = DropDown_Profil.SelectedItem.Text;
                pro.Graduate_Qualification = Graduate_Qualification.SelectedItem.Text;
                pro.Training_Form = Training_Form.SelectedItem.Text;
                pro.StudPeriod = StudyPeriod.Text;
                pro.City = City.Text;

                pro.ObjectivesOfDiscipline = ObjectivesOfDiscipline.Text;
                pro.DisciplineInOOP = DisciplineInOOP.Text; // + "\n" + DisciplineInOOPBefore.Text + "\n" + DisciplineInOOPAfter.Text;
                pro.PlanDescription = "";

                List<PlanTableRow> rows = new List<PlanTableRow>();
                for (int i = 1; i < PlanTable.Rows.Count; i++)
                {
                    PlanTableRow row = new PlanTableRow();
                    row.Сompetence = PlanTable.Rows[i].Cells[0].Text;
                    row.Ability = PlanTable.Rows[i].Cells[1].Text;
                    row.Enumeration = PlanTable.Rows[i].Cells[2].Text;
                    rows.Add(row);
                }
                pro.PlanTable = rows;

                pro.DisciplineStructureContent = DisciplineStructureContent.Text;
                pro.SemesteresContent = SemesteresContent.Text;
                pro.EducationalTechnology = EducationalTechnology.Text;
                pro.MonitoringTools_Description = MonitoringTools_Description.Text;

                List<FondTableRow> rows_1 = new List<FondTableRow>();
                for (int i = 1; i < MonitoringTools_FondTable.Rows.Count; i++)
                {
                    FondTableRow row_1 = new FondTableRow();
                    row_1.Сompetence = MonitoringTools_FondTable.Rows[i].Cells[0].Text;
                    row_1.Ability = MonitoringTools_FondTable.Rows[i].Cells[1].Text;
                    rows_1.Add(row_1);
                }
                pro.FondTable = rows_1;

                List<CriterionsTableRow> rows_2 = new List<CriterionsTableRow>();
                for (int i = 2; i < CriterionsTable.Rows.Count; i += 3)
                {
                    CriterionsTableRow row_2 = new CriterionsTableRow();
                    row_2.Competence = CriterionsTable.Rows[i].Cells[0].Text;

                    row_2.ToKnow_Description = CriterionsTable.Rows[i].Cells[1].Text;
                    List<string> toknow = new List<string>();
                    for (int j = 2; j <= 5; j++)
                    {
                        toknow.Add(CriterionsTable.Rows[i].Cells[j].Text);
                    }
                    row_2.ToKnow = toknow;

                    row_2.ToDo_Description = CriterionsTable.Rows[i + 1].Cells[1].Text;
                    List<string> todo = new List<string>();
                    for (int j = 2; j <= 5; j++)
                    {
                        todo.Add(CriterionsTable.Rows[i + 1].Cells[j].Text);
                    }
                    row_2.ToDo = todo;

                    row_2.ToMaster_Description = CriterionsTable.Rows[i + 2].Cells[1].Text;
                    List<string> tomaster = new List<string>();
                    for (int j = 2; j <= 5; j++)
                    {
                        tomaster.Add(CriterionsTable.Rows[i + 2].Cells[j].Text);
                    }
                    row_2.ToMaster = tomaster;
                    rows_2.Add(row_2);
                }
                pro.CriterionsTable = rows_2;

                pro.ZachetDescription = ZachetDescription.Text;
                ZachetTableRow zachet = new ZachetTableRow();
                zachet.ZachetDone = Zachteno.Text;
                zachet.ZachetUnDone = NeZachteno.Text;
                pro.ZachetTable = zachet;
                pro.ExamDescription = ExamDescription.Text;
                ExamTableRow exam = new ExamTableRow();
                exam.AttestatGreat = AttestatGreat.Text;
                exam.AttestatGood = AttestatGood.Text;
                exam.AttestatSatisfactorily = AttestatSatisfactorily.Text;
                exam.AttestatNotSatisfactorily = AttestatNotSatisfactorily.Text;
                pro.ExamTable = exam;
                pro.BasicLiterature = BasicLiterature.Text;
                pro.AdditionalLiterature = AdditionalLiterature.Text;
                pro.ITResources = ITResources.Text;
                pro.MaterialTechnicalSupport = MaterialTechnicalSupport.Text;
                pro.StudentRecommendations = StudentRecommendations.Text;
                pro.TeacherRecommendations = TeacherRecommendations.Text;

                pro.DrafterStatus = DrafterStatus.Text;
                pro.DrafterName = DrafterName.SelectedItem.Text;
                pro.DrafterId = DrafterName.SelectedItem.Value;
                pro.Department = DropDown_Department.SelectedItem.Text;
                try
                {
                    string dateInput = DraftDate.Text;
                    DateTime d = DateTime.Parse(dateInput);
                    pro.DraftDate = d;
                }
                catch { }

                pro.Protocol = Protocol.Text;
                pro.DepartmentHeadStatus = DepartmentHeadStatus.Text;
                pro.DepartmentHeadName = DepartmentHeadName.SelectedItem.Text;
                pro.DepartmentHeadId = DepartmentHeadName.SelectedItem.Value;
                pro.AgreementDepartmentHeadStatus = AgreementDepartmentHeadStatus.Text;
                pro.AgreementDepartmentHeadName = AgreementDepartmentHeadName.SelectedItem.Text;
                pro.AgreementDepartmentHeadId = AgreementDepartmentHeadName.SelectedItem.Value;
                pro.AgreementDirectorStatus = AgreementDirectorStatus.Text;
                pro.AgreementDirectorName = AgreementDirectorName.SelectedItem.Text;
                pro.AgreementDirectorId = AgreementDirectorName.SelectedItem.Value;

                pro.DSTTable = DSTTable_FillTask();


                List<PrilFondTableRow> rows_3 = new List<PrilFondTableRow>();
                for (int i = 3; i < PrilFondTable_1.Rows.Count; i++)
                {
                    PrilFondTableRow row = new PrilFondTableRow();
                    row.Index = PrilFondTable_1.Rows[i].Cells[0].Text;
                    row.Formul = PrilFondTable_1.Rows[i].Cells[1].Text;
                    row.KomponentsList = PrilFondTable_1.Rows[i].Cells[2].Text;
                    row.CompetenceTechnology = PrilFondTable_1.Rows[i].Cells[3].Text;
                    row.AssessForm = PrilFondTable_1.Rows[i].Cells[4].Text;
                    row.Steps = PrilFondTable_1.Rows[i].Cells[5].Text;
                    rows_3.Add(row);
                }
                List<PrilFondTableRow> pft1 = new List<PrilFondTableRow>();
                pft1 = rows_3;

                List<PrilFondTableRow> rows_4 = new List<PrilFondTableRow>();
                for (int i = 3; i < PrilFondTable_2.Rows.Count; i++)
                {
                    PrilFondTableRow row = new PrilFondTableRow();
                    row.Index = PrilFondTable_2.Rows[i].Cells[0].Text;
                    row.Formul = PrilFondTable_2.Rows[i].Cells[1].Text;
                    row.KomponentsList = PrilFondTable_2.Rows[i].Cells[2].Text;
                    row.CompetenceTechnology = PrilFondTable_2.Rows[i].Cells[3].Text;
                    row.AssessForm = PrilFondTable_2.Rows[i].Cells[4].Text;
                    row.Steps = PrilFondTable_2.Rows[i].Cells[5].Text;
                    rows_4.Add(row);
                }
                List<PrilFondTableRow> pft2 = new List<PrilFondTableRow>();
                pft2 = rows_4;

                List<PrilFondTableRow> rows_5 = new List<PrilFondTableRow>();
                for (int i = 3; i < PrilFondTable_3.Rows.Count; i++)
                {
                    PrilFondTableRow row = new PrilFondTableRow();
                    row.Index = PrilFondTable_3.Rows[i].Cells[0].Text;
                    row.Formul = PrilFondTable_3.Rows[i].Cells[1].Text;
                    row.KomponentsList = PrilFondTable_3.Rows[i].Cells[2].Text;
                    row.CompetenceTechnology = PrilFondTable_3.Rows[i].Cells[3].Text;
                    row.AssessForm = PrilFondTable_3.Rows[i].Cells[4].Text;
                    row.Steps = PrilFondTable_3.Rows[i].Cells[5].Text;
                    rows_5.Add(row);
                }
                List<PrilFondTableRow> pft3 = new List<PrilFondTableRow>();
                pft3 = rows_5;

                pro.PFT_1 = pft1;
                pro.PFT_2 = pft2;
                pro.PFT_3 = pft3;

                List<PrilAssessTableRow> rows_6 = new List<PrilAssessTableRow>();
                for (int i = 1; i < PrilAssessTable.Rows.Count; i++)
                {
                    PrilAssessTableRow row = new PrilAssessTableRow();
                    row.Name = PrilAssessTable.Rows[i].Cells[1].Text;
                    row.Spec = PrilAssessTable.Rows[i].Cells[2].Text;
                    row.InFOS = PrilAssessTable.Rows[i].Cells[3].Text;
                    rows_6.Add(row);
                }
                pro.PAT = rows_6;

                List<PCTZachetTableRow> rows_7 = new List<PCTZachetTableRow>();
                for (int i = 3; i < PrilCompetenceTable_Zachet.Rows.Count; i++)
                {
                    PCTZachetTableRow row = new PCTZachetTableRow();
                    row.Discription = PrilCompetenceTable_Zachet.Rows[i].Cells[0].Text;
                    row.Rezult = PrilCompetenceTable_Zachet.Rows[i].Cells[1].Text;
                    row.Topics = PrilCompetenceTable_Zachet.Rows[i].Cells[2].Text;
                    row.Zachet = PrilCompetenceTable_Zachet.Rows[i].Cells[3].Text;
                    row.NeZachet = PrilCompetenceTable_Zachet.Rows[i].Cells[4].Text;
                    rows_7.Add(row);
                }
                pro.PCT_Zachet = rows_7;

                Questions_Zachet questions_zachet = new Questions_Zachet();
                try
                {
                    questions_zachet.Questions = ZachetQuestions.Text;
                    questions_zachet.Developer = DeveloperZachet.SelectedItem.Text;
                    string dateInput = ZachetQuestionsDate.Text;
                    DateTime d = DateTime.Parse(dateInput);
                    questions_zachet.Date = d;
                    pro.Questions_Zachet = questions_zachet;
                }
                catch { }

                List<PCTExamTableRow> rows_8 = new List<PCTExamTableRow>();
                for (int i = 3; i < PrilCompetenceTable_Exam.Rows.Count; i++)
                {
                    PCTExamTableRow row = new PCTExamTableRow();
                    row.Discription = PrilCompetenceTable_Exam.Rows[i].Cells[0].Text;
                    row.Rezult = PrilCompetenceTable_Exam.Rows[i].Cells[1].Text;
                    row.Topics = PrilCompetenceTable_Exam.Rows[i].Cells[2].Text;
                    row.On2 = PrilCompetenceTable_Exam.Rows[i].Cells[3].Text;
                    row.On3 = PrilCompetenceTable_Exam.Rows[i].Cells[4].Text;
                    row.On4 = PrilCompetenceTable_Exam.Rows[i].Cells[5].Text;
                    row.On5 = PrilCompetenceTable_Exam.Rows[i].Cells[6].Text;
                    rows_8.Add(row);
                }
                pro.PCT_Exam = rows_8;

                Questions_Exam questions_exam = new Questions_Exam();
                try
                {
                    questions_exam.Questions = ExamQuestions.Text;
                    questions_exam.Developer = DeveloperExam.SelectedItem.Text;
                    string date = ExamQuestionsDate.Text;
                    DateTime d = DateTime.Parse(date);
                    questions_exam.Date = d;
                    pro.Questions_Exam = questions_exam;
                }
                catch { }

                List<PCTReferatTableRow> rows_9 = new List<PCTReferatTableRow>();
                for (int i = 3; i < PrilCompetenceTable_Referat.Rows.Count; i++)
                {
                    PCTReferatTableRow row = new PCTReferatTableRow();
                    row.Discription = PrilCompetenceTable_Referat.Rows[i].Cells[0].Text;
                    row.Rezult = PrilCompetenceTable_Referat.Rows[i].Cells[1].Text;
                    row.Topics = PrilCompetenceTable_Referat.Rows[i].Cells[2].Text;
                    row.On2 = PrilCompetenceTable_Referat.Rows[i].Cells[3].Text;
                    row.On3 = PrilCompetenceTable_Referat.Rows[i].Cells[4].Text;
                    row.On4 = PrilCompetenceTable_Referat.Rows[i].Cells[5].Text;
                    row.On5 = PrilCompetenceTable_Referat.Rows[i].Cells[6].Text;
                    rows_9.Add(row);
                }
                pro.PCT_Referat = rows_9;

                Questions_Referat questions_referat = new Questions_Referat();
                try
                {
                    questions_referat.Questions = ReferatTopics.Text;
                    questions_referat.Developer = DeveloperReferat.SelectedItem.Text;
                    string date = ReferatTopicsDate.Text;
                    DateTime d = DateTime.Parse(date);
                    questions_referat.Date = d;
                    pro.Questions_Referat = questions_referat;
                }
                catch { }

                List<PCTKtTableRow> rows_10 = new List<PCTKtTableRow>();
                for (int i = 3; i < PrilCompetenceTable_KT.Rows.Count; i++)
                {
                    PCTKtTableRow row = new PCTKtTableRow();
                    row.Discription = PrilCompetenceTable_KT.Rows[i].Cells[0].Text;
                    row.Rezult = PrilCompetenceTable_KT.Rows[i].Cells[1].Text;
                    row.Topics = PrilCompetenceTable_KT.Rows[i].Cells[2].Text;
                    row.On2 = PrilCompetenceTable_KT.Rows[i].Cells[3].Text;
                    row.On3 = PrilCompetenceTable_KT.Rows[i].Cells[4].Text;
                    row.On4 = PrilCompetenceTable_KT.Rows[i].Cells[5].Text;
                    row.On5 = PrilCompetenceTable_KT.Rows[i].Cells[6].Text;
                    rows_10.Add(row);
                }
                pro.PCT_KT = rows_10;

                Questions_KT questions_kt = new Questions_KT();
                try
                {
                    questions_kt.Questions = KTTopics.Text;
                    questions_kt.Developer = DeveloperKT.SelectedItem.Text;
                    string date = KTDate.Text;
                    DateTime d = DateTime.Parse(date);
                    questions_kt.Date = d;
                    pro.Questions_KT = questions_kt;
                }
                catch { }

                List<PCTPraktikaTableRow> rows_11 = new List<PCTPraktikaTableRow>();
                for (int i = 3; i < PrilCompetenceTable_Praktika.Rows.Count; i++)
                {
                    PCTPraktikaTableRow row = new PCTPraktikaTableRow();
                    row.Discription = PrilCompetenceTable_Praktika.Rows[i].Cells[0].Text;
                    row.Rezult = PrilCompetenceTable_Praktika.Rows[i].Cells[1].Text;
                    row.Topics = PrilCompetenceTable_Praktika.Rows[i].Cells[2].Text;
                    row.On2 = PrilCompetenceTable_Praktika.Rows[i].Cells[3].Text;
                    row.On3 = PrilCompetenceTable_Praktika.Rows[i].Cells[4].Text;
                    row.On4 = PrilCompetenceTable_Praktika.Rows[i].Cells[5].Text;
                    row.On5 = PrilCompetenceTable_Praktika.Rows[i].Cells[6].Text;
                    rows_11.Add(row);
                }
                pro.PCT_Praktika = rows_11;

                Questions_Praktika questions_praktika = new Questions_Praktika();
                try
                {
                    questions_praktika.Questions = PraktikTopics.Text;
                    questions_praktika.Developer = DeveloperPraktika.SelectedItem.Text;
                    string date = PraktikaTopicsDate.Text;
                    DateTime d = DateTime.Parse(date);
                    questions_praktika.Date = d;
                    pro.Questions_Praktika = questions_praktika;
                }
                catch { }

                List<PCTTestTableRow> rows_12 = new List<PCTTestTableRow>();
                for (int i = 3; i < PrilCompetenceTable_Test.Rows.Count; i++)
                {
                    PCTTestTableRow row = new PCTTestTableRow();
                    row.Discription = PrilCompetenceTable_Test.Rows[i].Cells[0].Text;
                    row.Rezult = PrilCompetenceTable_Test.Rows[i].Cells[1].Text;
                    row.Topics = PrilCompetenceTable_Test.Rows[i].Cells[2].Text;
                    row.On2 = PrilCompetenceTable_Test.Rows[i].Cells[3].Text;
                    row.On3 = PrilCompetenceTable_Test.Rows[i].Cells[4].Text;
                    row.On4 = PrilCompetenceTable_Test.Rows[i].Cells[5].Text;
                    row.On5 = PrilCompetenceTable_Test.Rows[i].Cells[6].Text;
                    rows_12.Add(row);
                }
                pro.PCT_Test = rows_12;

                Questions_Test questions_test = new Questions_Test();
                try
                {
                    questions_test.Questions = TestsFond.Text;
                    questions_test.Developer = DeveloperTest.SelectedItem.Text;
                    string date = TestsFondDate.Text;
                    DateTime d = DateTime.Parse(date);
                    questions_test.Date = d;
                    pro.Questions_Test = questions_test;
                }
                catch { }

                List<PCTCursTableRow> rows_13 = new List<PCTCursTableRow>();
                for (int i = 3; i < PrilCompetenceTable_Curs.Rows.Count; i++)
                {
                    PCTCursTableRow row = new PCTCursTableRow();
                    row.Discription = PrilCompetenceTable_Curs.Rows[i].Cells[0].Text;
                    row.Rezult = PrilCompetenceTable_Curs.Rows[i].Cells[1].Text;
                    row.Topics = PrilCompetenceTable_Curs.Rows[i].Cells[2].Text;
                    row.On2 = PrilCompetenceTable_Curs.Rows[i].Cells[3].Text;
                    row.On3 = PrilCompetenceTable_Curs.Rows[i].Cells[4].Text;
                    row.On4 = PrilCompetenceTable_Curs.Rows[i].Cells[5].Text;
                    row.On5 = PrilCompetenceTable_Curs.Rows[i].Cells[6].Text;
                    rows_13.Add(row);
                }
                pro.PCT_Curs = rows_13;

                Questions_Curs questions_curs = new Questions_Curs();
                try
                {
                    questions_curs.Questions = CursesFond.Text;
                    questions_curs.Developer = DeveloperCurs.SelectedItem.Text;
                    string date = CursDate.Text;
                    DateTime d = DateTime.Parse(date);
                    questions_curs.Date = d;
                    pro.Questions_Curs = questions_curs;
                }
                catch { }

                List<AdditionalDrafter> drafters = new List<AdditionalDrafter>();
                int drafterscount = Convert.ToInt32(AdditionalDraftersCount.Value);
                if (drafterscount != 0)
                {
                    for (int i = 0; i < drafterscount; i++)
                    {
                        AdditionalDrafter newDrafter = new AdditionalDrafter();
                        Panel panel = new Panel();
                        panel = (Panel)drafters_html_block.FindControl("DraftersPanel_" + i.ToString());
                        TextBox textbox = new TextBox();
                        DropDownList list = new DropDownList();
                        textbox = (TextBox)panel.FindControl("AdditionalDrafterStatus_" + i.ToString());
                        list = (DropDownList)panel.FindControl("AdditionalDrafterId_" + i.ToString());
                        newDrafter.Status = textbox.Text;
                        newDrafter.UserId = list.SelectedValue.ToString();
                        drafters.Add(newDrafter);
                    }

                    pro.AdditionalDrafters = drafters;
                }

                pro.Status = "Ожидает утверждения";

                BaseWork task = new BaseWork();
                int programid = 0;

                try
                {
                    programid = task.CreateNewProgram(pro);
                    string userid = User.Identity.GetUserId();
                    string history = "Создана";
                    task.SetHistory(userid, programid, history);
                }
                catch
                {
                    ErrorMes.Text = "Ошибка в отправке данных";
                    ErrorMes.Visible = true;
                    goto end;
                }

                try
                {
                    Report(programid);
                }
                catch
                {
                    ErrorMes.Text = "Данные о РПД отправлены, но произошел сбой в генерации Word-файла";
                    ErrorMes.Visible = true;
                    goto end;
                }

                try
                {
                    string fileName = "Program_" + programid.ToString() + ".docx";
                    string url = HttpContext.Current.Server.MapPath("~/Files/" + fileName);

                    FileStream MyFileStream = new FileStream(url, FileMode.Open);
                    int filesizeINT = 0;
                    long fileSize = MyFileStream.Length;
                    filesizeINT = Convert.ToInt32(fileSize);
                    byte[] Buffer = new byte[fileSize];
                    MyFileStream.Read(Buffer, 0, filesizeINT);
                    MyFileStream.Close();
                    Response.AppendHeader("content-disposition", "attachment; filename=" + fileName);
                    Response.BinaryWrite(Buffer);
                    Response.End();
                }
                catch
                {
                    ErrorMes.Text = "Данные о РПД внесены в базу, Word-файл сформирован, но произошла ошибка при его загрузке";
                    ErrorMes.Visible = true;
                    goto end;
                }

            end: { }
            }
            catch
            {
                ErrorMes.Text = "Неизвестная ошибка";
                ErrorMes.Visible = true;
            }
        }
        
        private void Report(int programid)
        {
            HtmlToOpenXMLBuilder builder = new HtmlToOpenXMLBuilder();
            Converter converter = new Converter();
            List<string> htmles = new List<string>();
            Pro pro = new Pro();
            BaseWork task = new BaseWork();
            pro = task.GetProgram(programid);
            htmles = converter.ToHtmlList(pro);
            builder.Build(htmles, programid);
        }
        #endregion

        //управление таблицей контента и связанных с ней элементов
        #region ContentTableControlls

        struct NumberString
        {
            public int number;
            public string text;
            public NumberString(int num, string txt)
            {
                this.number = num;
                this.text = txt;
            }
        }
        
        //считывание строк из таблицы контента и заполнение поля разделов семестров
        protected void SemesteresContentFill_Button_Click(object sender, EventArgs e)
        {
            SemesteresContent.Text = "";
            RebuildTables(0);
            List<NumberString> sems = new List<NumberString>();
            NumberString sem1 = new NumberString(1, "Первый");
            NumberString sem2 = new NumberString(2, "Второй");
            NumberString sem3 = new NumberString(3, "Третий");
            NumberString sem4 = new NumberString(4, "Четвертый");
            NumberString sem5 = new NumberString(5, "Пятый");
            NumberString sem6 = new NumberString(6, "Шестой");
            NumberString sem7 = new NumberString(7, "Седьмой");
            NumberString sem8 = new NumberString(8, "Восьмой");
            sems.Add(sem1);
            sems.Add(sem2);
            sems.Add(sem3);
            sems.Add(sem4);
            sems.Add(sem5);
            sems.Add(sem6);
            sems.Add(sem7);
            sems.Add(sem8);

            List<int> tsems = new List<int>();
            for (int i = 2; i < DisciplineStructureTable.Rows.Count; i++)
            {
                try
                {
                    int s = Convert.ToInt32(DisciplineStructureTable.Rows[i].Cells[2].Text);
                    tsems.Add(s);
                }
                catch { }
            }

            tsems = tsems.Distinct().ToList();
            
            string str = "";
            for (int i = 0; i < 8; i++)
                if (sems[i].number == Convert.ToInt32(DisciplineStructureTable.Rows[2].Cells[2].Text))
                    str = sems[i].text;

            SemesteresContent.Text += "<p style=\"margin-top:0px; margin-bottom:0px;\"><strong>" + str + " семестр.</strong></p>";
            for (int i = 2; i < DisciplineStructureTable.Rows.Count; i++)
            {
                try
                {
                    if (DisciplineStructureTable.Rows[i + 1].Cells[1].Text == "Часы за весь период") break;
                }
                catch { }

                if (DisciplineStructureTable.Rows[i].Cells[1].Text == "Часы за семестр")
                {
                    try
                    {
                        for (int n = 0; n < 8; n++)
                            if (sems[n].number == Convert.ToInt32(DisciplineStructureTable.Rows[i + 1].Cells[2].Text))
                                str = sems[n].text;

                        SemesteresContent.Text += "<p style=\"margin-top:0px; margin-bottom:0px;\"><strong>" + str + " семестр.</strong></p>";
                    }
                    catch { SemesteresContent.Text += DisciplineStructureTable.Rows[i + 1].Cells[2].Text + " семестр.<br/>"; }
                    
                }
                else 
                    SemesteresContent.Text += "<p style=\"margin-top:0px; margin-bottom:0px;\">" + DisciplineStructureTable.Rows[i].Cells[1].Text + ":" + "</p> " +
                        "<p style=\"margin-top:0px; margin-bottom:0px;\"></p>";
            }
            SemesteresContent.Focus();
        }

        //считывание строк из таблицы контента и заполнение поля структуры дисциплины
        protected void DisciplineStructureContentFill_Button_Click(object sender, EventArgs e)
        {
            RebuildTables(0);
            int count = DisciplineStructureTable.Rows.Count;
            int generalsumm = 0;
            int zed = 0;
            int srs = 0;
            int maxsem = 0;
            string zach;
            string gen;
            string srsstr;

            List<NumberString> sems = new List<NumberString>();
            NumberString sem1 = new NumberString(1, "В первом");
            NumberString sem2 = new NumberString(2, "Во втором ");
            NumberString sem3 = new NumberString(3, "В третьем");
            NumberString sem4 = new NumberString(4, "В четвертом");
            NumberString sem5 = new NumberString(5, "В пятом");
            NumberString sem6 = new NumberString(6, "В шестом");
            NumberString sem7 = new NumberString(7, "В седьмом");
            NumberString sem8 = new NumberString(8, "В восьмом");
            sems.Add(sem1);
            sems.Add(sem2);
            sems.Add(sem3);
            sems.Add(sem4);
            sems.Add(sem5);
            sems.Add(sem6);
            sems.Add(sem7);
            sems.Add(sem8);

            List<int> tsems = new List<int>();
            for (int i = 0; i < DisciplineStructureTable.Rows.Count; i++)
            {
                try
                {
                    int s = Convert.ToInt32(DisciplineStructureTable.Rows[i].Cells[2].Text);
                    tsems.Add(s);
                }
                catch { }
                
            }

            tsems = tsems.Distinct().ToList();

            Table table = DisciplineStructureTable;
            for (int i = 4; i < 15; i++)
            {
                int buf = 0;
                try
                {
                    buf = Convert.ToInt32(table.Rows[count - 1].Cells[i].Text);
                    generalsumm += buf;
                }
                catch { }
            }

            zed = Convert.ToInt32(Math.Truncate(Convert.ToDecimal(generalsumm / 36)));
            try { srs = Convert.ToInt32(table.Rows[count - 1].Cells[7].Text);}
            catch { }

            if (zed % 10 < 5) zach = " зачетные единицы";
            else zach = " зачетных единиц";
            if (zed % 10 == 1) zach = " зачетная единица";
            if ((zed > 9) && (zed < 21)) zach = " зачетных единиц";

            if (generalsumm % 10 < 5) gen = " академических часа";
            else gen = " академических часов";
            if (generalsumm % 10 == 1) gen = " академический час";
            if((generalsumm > 9) && (generalsumm < 21)) gen = " академических часов";

            if (srs % 10 < 5) srsstr = " часа";
            else srsstr = " часов";
            if (srs % 10 == 1) srsstr = " час";
            if ((srs > 9) && (srs < 21)) srsstr = " часов";

            DisciplineStructureContent.Text = "<p style=\"margin-top:0px; margin-bottom:0px\">Общая трудоемкость дисциплины составляет <strong>" + zed.ToString() + "</strong>" 
                + zach + ", или <strong>" + generalsumm.ToString() + "</strong>" + gen +
                " (из них <strong>" + srs.ToString() + "</strong>" + srsstr + " – самостоятельная работа студентов).</p>";

            for (int i = 2; i < count; i++)
            {
                try
                {
                    if (Convert.ToInt32(table.Rows[i].Cells[2].Text) > maxsem)
                        maxsem = Convert.ToInt32(table.Rows[i].Cells[2].Text);
                }
                catch { }
            }

            for (int i = 0; i < tsems.Count; i++)
            {
                if (i == 8) break;

                generalsumm = 0;
                zed = 0;
                srs = 0;
                zach = "";
                gen = "";
                srsstr = "";
                int l = 0;
                int p = 0;
                string lstr, pstr;
                string attestatform = "";
                for (int j = 2; j < count; j++)
                {
                    if ((table.Rows[j].Cells[1].Text == "Часы за семестр") && 
                        (Convert.ToInt32(table.Rows[j].Cells[2].Text) == (tsems[i])))
                    {
                       
                        for (int k = 4; k < 15; k++)
                        {
                            int buf = 0;
                            try
                            {
                                buf = Convert.ToInt32(table.Rows[j].Cells[k].Text);
                                generalsumm += buf;
                            }
                            catch { }
                        }
                        zed = Convert.ToInt32(Math.Truncate(Convert.ToDecimal(generalsumm / 36)));
                        try { srs = Convert.ToInt32(table.Rows[j].Cells[7].Text); }
                        catch { }

                        try { l = Convert.ToInt32(table.Rows[j].Cells[4].Text); }
                        catch { }

                        try { p = Convert.ToInt32(table.Rows[j].Cells[5].Text); }
                        catch { }
                        
                        attestatform = table.Rows[j].Cells[14].Text;
                        break;
                    }
                }

                if (zed % 10 < 5) zach = " зачетные единицы";
                else zach = " зачетных единиц";
                if (zed % 10 == 1) zach = " зачетная единица";
                if ((zed > 9) && (zed < 21)) zach = " зачетных единиц";

                if (generalsumm % 10 < 5) gen = " академических часа";
                else gen = " академических часов";
                if (generalsumm % 10 == 1) gen = " академический час";
                if ((generalsumm > 9) && (generalsumm < 21)) gen = " академических часов";

                if (srs % 10 < 5) srsstr = " часа";
                else srsstr = " часов";
                if (srs % 10 == 1) srsstr = " час";
                if ((srs > 9) && (srs < 21)) srsstr = " часов";

                if (l % 10 < 5) lstr = " часа";
                else lstr = " часов";
                if (l % 10 == 1) lstr= " час";
                if ((l > 9) && (l < 21)) lstr = " часов";

                if (p % 10 < 5) pstr = " часа";
                else pstr = " часов";
                if (p % 10 == 1) pstr = " час";
                if ((p > 9) && (p < 21)) pstr = " часов";

                string str = "";
                for (int n = 0; n < sems.Count; n++)
                    if (sems[n].number == tsems[i])
                        str = sems[n].text;

                DisciplineStructureContent.Text += "<p style=\"margin-top:0px; margin-bottom:0px\">" + "<strong>" + str + "</strong>" + 
                    " семестре выделяется <strong>" + zed.ToString() + "</strong>" 
                    + zach + ", или <strong>" + generalsumm.ToString() + "</strong>" + gen +
                    " (из них <strong>" + srs.ToString() + "</strong>" + srsstr + " – самостоятельная работа студентов). " +
                    "Лекции – <strong>" + l.ToString() + "</strong>" + lstr + ". " +
                    "Практические занятия – <strong>" + p.ToString() + "</strong>" + pstr +  ". " +
                    "Форма аттестации – " + attestatform + ".</p>";
            }

            DisciplineStructureContent.Focus();
        }

        //сортировка таблицы контента, расстановка номеров семестров и подсчет часов
        protected void SortDSCT_Button_Click(object sender, EventArgs e)
        {
            DSTTask(2);
            RebuildTables(4);
            DisciplineStructureContent.Focus();
        }

        //очищение таблицы контента
        protected void DSTClear_Button_Click(object sender, EventArgs e)
        {
            RebuildTables(0);
            while (DisciplineStructureTable.Rows.Count > 2)
                DisciplineStructureTable.Rows.RemoveAt(2);
            DSTrowscount.Value = "0";
            AddRowDSCT_Button.Focus();
        }

        //импорт таблицы расчасовки
        protected void ImportDSCT_Button_Click(object sender, EventArgs e)
        {
            if (uploader.HasFile)
            {
                try
                {
                    RebuildTables(0);

                    //очищаю таблицу
                    while (DisciplineStructureTable.Rows.Count > 2)
                        DisciplineStructureTable.Rows.RemoveAt(2);
                    AddRowDSCT_Button.Enabled = true;
                    DeleteRowDSCT_Button.Enabled = true;
                    SortDSCT_Button.Enabled = true;
                    DSTrowscount.Value = "0";

                    //получаю файл в байтовом массиве
                    byte[] file = uploader.FileBytes;
                    Excel excel = new Excel();
                    DataTable dt = new DataTable();

                    //заполняю dt данными из файла 
                    dt = excel.ReadClocks(file);

                    //строю таблицу
                    foreach (DataRow row in dt.Rows)
                    {
                        if (row == dt.Rows[0])
                            continue;

                        TableRow trow = new TableRow();
                        DisciplineStructureTable.Rows.Add(trow);
                        for (int j = 0; j < 15; j++)
                        {
                            TableCell cell = new TableCell();
                            trow.Cells.Add(cell);
                        }
                        trow.Cells[1].Text = row[1].ToString();
                        trow.Cells[2].Text = row[2].ToString();
                        trow.Cells[3].Text = row[3].ToString();
                        trow.Cells[4].Text = row[4].ToString();
                        trow.Cells[5].Text = row[5].ToString();
                        trow.Cells[6].Text = row[6].ToString();
                        trow.Cells[7].Text = row[6].ToString();
                        trow.Cells[8].Text = row[7].ToString();
                        trow.Cells[9].Text = row[8].ToString();
                        trow.Cells[10].Text = row[10].ToString();
                        trow.Cells[11].Text = row[11].ToString();
                        trow.Cells[12].Text = row[12].ToString();
                        trow.Cells[13].Text = row[13].ToString();
                        trow.Cells[14].Text = row[14].ToString();
                    }
                    DSTrowscount.Value = dt.Rows.Count.ToString();

                    //нумерую строки и заменяю нули на пустые строки
                    int g = 0;
                    for (int i = 2; i < DisciplineStructureTable.Rows.Count; i++)
                    {
                        for (int j = 2; j < 15; j++)
                            if (DisciplineStructureTable.Rows[i].Cells[j].Text == "0")
                                DisciplineStructureTable.Rows[i].Cells[j].Text = "";

                        if ((DisciplineStructureTable.Rows[i].Cells[1].Text == "Часы за семестр") || (DisciplineStructureTable.Rows[i].Cells[1].Text == "Часы за весь период"))
                        {
                            g++;
                            continue;
                        }

                        DisciplineStructureTable.Rows[i].Cells[0].Text = (i - 1 - g).ToString();
                    }
                }
                catch
                {
                    ErrorMes.Text = "Ошибка";
                    ErrorMes.Visible = true;
                }
            }
        }

        //экспорт таблицы расчасовки
        protected void ExportDSCT_Button_Click(object sender, EventArgs e)
        {
            try
            {
                RebuildTables(0);
                Pro pro = new Pro();

                //собираю данные с html таблицы 
                pro.DSTTable = DSTTable_FillTask();

                //конвертирую данные в объект дататэйбл
                DataTable dt = new DataTable();
                dt.Columns.Add("Раздел", typeof(string));
                dt.Columns.Add("Семестр", typeof(int));
                dt.Columns.Add("Неделя", typeof(int));
                dt.Columns.Add("Л", typeof(int));
                dt.Columns.Add("ПС", typeof(int));
                dt.Columns.Add("Лаб", typeof(int));
                dt.Columns.Add("СРС", typeof(int));
                dt.Columns.Add("КСР", typeof(int));
                dt.Columns.Add("КР", typeof(int));
                dt.Columns.Add("КП", typeof(int));
                dt.Columns.Add("РГР", typeof(int));
                dt.Columns.Add("Под. к л/р", typeof(string));
                dt.Columns.Add("К/р", typeof(int));
                dt.Columns.Add("Форма аттестации", typeof(string));

                for (int i = 0; i < pro.DSTTable.Count; i++)
                {
                    DataRow row = dt.NewRow();
                    row[0] = pro.DSTTable[i].Topic;
                    row[1] = pro.DSTTable[i].Semestr;
                    row[2] = pro.DSTTable[i].Week;
                    row[3] = pro.DSTTable[i].L;
                    row[4] = pro.DSTTable[i].PS;
                    row[5] = pro.DSTTable[i].Lab;
                    row[6] = pro.DSTTable[i].SRS;
                    row[7] = pro.DSTTable[i].KSR;
                    row[8] = pro.DSTTable[i].KR;
                    row[9] = pro.DSTTable[i].KP;
                    row[10] = pro.DSTTable[i].RGR;
                    row[11] = pro.DSTTable[i].PrepToLR;
                    row[12] = pro.DSTTable[i].KRend;
                    row[13] = pro.DSTTable[i].AttestatForm;
                    dt.Rows.Add(row);
                }

                string url = HttpContext.Current.Server.MapPath("~/Files/Clocks.xlsx");
                //удаляю clocks
                if (File.Exists(url))
                    File.Delete(url);

                //создаю новый Clocks
                Excel excel = new Excel();
                excel.Create(dt, url);

                //скачиваю новый clocks на клиента
                Response.ClearContent();
                Response.TransmitFile(url);
                Response.AddHeader("Content-Disposition", "attachment; filename=Clocks.xlsx");
                Response.AddHeader("Content-Length", new FileInfo(url).Length.ToString());
                Response.ContentType = "application/vnd.ms-excel";
                Response.TransmitFile(url);
                Response.ContentEncoding = System.Text.Encoding.Default;
                Response.End();
            }
            catch
            {
                ErrorMes.Text = "Ошибка";
                ErrorMes.Visible = true;
            }
        }

        #endregion

        //управление сущностью "составители"
        #region DraftersControlls
        //добавление составителя
        protected void Button_AddDrafter_Click(object sender, EventArgs e)
        {
            int count = Convert.ToInt32(AdditionalDraftersCount.Value);
            Panel newPanel = new Panel();
            newPanel.Style["margin-top"] = "2px";
            newPanel.ID = "DraftersPanel_" + count.ToString(); ;
            TextBox newTextBox = new TextBox();
            newTextBox.CssClass = "form-control";
            newTextBox.Attributes["placeholder"] = "Академ. статус";
            newTextBox.ID = "AdditionalDrafterStatus_" + count.ToString();
            
            DropDownList newList = new DropDownList();
            newList.CssClass = "form-control";
            newList.Style["margin-left"] = "3px";
            newList.DataSourceID = "SqlDataSourceDepartmentPersonal";
            newList.DataTextField = "UserName";
            newList.DataValueField = DrafterName.DataValueField;
            newList.ID = "AdditionalDrafterId_" + count.ToString();

            newPanel.Controls.Add(newTextBox);
            newPanel.Controls.Add(newList);
            drafters_html_block.Controls.Add(newPanel);
            count++;
            AdditionalDraftersCount.Value = count.ToString();

            RebuildTables(0);
        }

        //удаление составителя
        protected void Button_DeleteDrafter_Click(object sender, EventArgs e)
        {
            int count = Convert.ToInt32(AdditionalDraftersCount.Value);
            if (count != 0)
            {
                string panelid = "DraftersPanel_" + (count - 1).ToString();
                drafters_html_block.Controls.Remove(drafters_html_block.FindControl(panelid));
                count--;
                AdditionalDraftersCount.Value = count.ToString();
            }
            RebuildTables(0);
        }
        #endregion

        //определение места дисциплины в ООП
        #region OOPControlls
        protected void Button_AddDisciplineInOOPBefore_Click(object sender, EventArgs e)
        {
            string text = DropDown_DisciplineInOOPBefore.SelectedItem.Text;
            if(!DisciplineInOOPBefore.Text.Contains(text))
                DisciplineInOOPBefore.Text += "\n– " + text;
            RebuildTables(0);
        }

        protected void Button_AddDisciplineInOOPAfter_Click(object sender, EventArgs e)
        {
            string text = DropDown_DisciplineInOOPAfter.Text;
            if(!DisciplineInOOPAfter.Text.Contains(text))
                DisciplineInOOPAfter.Text += "\n– " + text;
            RebuildTables(0);
        }

        protected void Button_DeleteDisciplineInOOPAfter_Click(object sender, EventArgs e)
        {
            string text = "\n– " + DropDown_DisciplineInOOPAfter.SelectedItem.Text;
            DisciplineInOOPAfter.Text = DisciplineInOOPAfter.Text.Replace(text, "");
            RebuildTables(0);
        }

        protected void Button_DeleteDisciplineInOOPBefore_Click(object sender, EventArgs e)
        {
            string text = "\n– " + DropDown_DisciplineInOOPBefore.SelectedItem.Text;
            DisciplineInOOPBefore.Text = DisciplineInOOPBefore.Text.Replace(text, "");
            RebuildTables(0);
        }

        protected void Button_PasteInOOP_Click(object sender, EventArgs e)
        {
            string designation_1 = "Изучение данной дисциплины базируется на следующих дисциплинах, прохождении практик:";
            string before = DisciplineInOOPBefore.Text.Replace("– ", "").Replace(designation_1, "");
            before = "<li>" + before.Replace("\n", "</li><li>") + "</li>";
            before = "<p>" + designation_1 + "</p>" + "<ul>" + before + "</ul>";
            before = before.Replace("<li></li>", "");
            DisciplineInOOP.Text += before;

            string designation_2 = "Основные положения дисциплины должны быть использованы в дальнейшем при изучении следующих дисциплин:";
            string after = DisciplineInOOPAfter.Text.Replace("– ", "").Replace(designation_2, "");
            after = "<li>" + after.Replace("\n", "</li><li>") + "</li>";
            after = "<p>" + designation_2 + "</p>" + "<ul>" + after + "</ul>";
            after = after.Replace("<li></li>", "");
            DisciplineInOOP.Text += after;
            RebuildTables(0);
        }
        #endregion

       
    }
}