﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace main_test
{
    public partial class ProgramHistory : System.Web.UI.Page
    {
        private int ProgramId;
        Pro pro = new Pro();

        protected void Page_Load(object sender, EventArgs e)
        {
            ProgramId = Convert.ToInt32(Request.QueryString["ProgramID"]);
            if (!IsPostBack)
            {
                if (ProgramId != 0)
                {
                    try
                    {
                        program_html.InnerHtml = "Рабочая программа №" + ProgramId.ToString();
                        BaseWork task = new BaseWork();
                        pro = task.GetProgram(ProgramId);
                        History_TextBox.Text = task.GetFullHistory(ProgramId);
                        DownLoadError.Visible = false;
                    }
                    catch
                    {
                        DownLoadError.Visible = true;
                    }
                }
                else
                {
                    DownLoadError.Visible = true;
                }
            }
        }
        
        //очищение полей ввода для периода
        protected void Period_Clear_Button_Click(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                History_TextBox.Text = task.GetFullHistory(ProgramId);
                ErrorMes.Visible = false;
                Period_html.InnerHtml = "Полная история";
            }
            catch
            {
                ErrorMes.Text = "Не удалось выполнить действие";
                ErrorMes.Visible = true;
            }
        }

        //установка периода для истории
        protected void Period_Button_Click(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                DateTime period_1 = Convert.ToDateTime(Period_1_TextBox.Text);
                DateTime period_2 = Convert.ToDateTime(Period_2_TextBox.Text);

                if (period_1 > period_2)
                {
                    DateTime buf = period_1;
                    period_1 = period_2;
                    period_2 = buf;
                }

                History_TextBox.Text = task.GetHistoryWithPeriodRange(ProgramId, period_1, period_2);
                ErrorMes.Visible = false;
                Period_html.InnerHtml = "История с " + period_1.Date.ToString("D") + " по " + period_2.Date.ToString("D");
            }
            catch
            {
                ErrorMes.Text = "Не удалось установить период истории";
                ErrorMes.Visible = true;
            }
        }
    }
}