﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Microsoft.AspNet.Identity;

namespace main_test
{
    public partial class ProgramsViewRedactor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SelectErrorMes.Visible = false;
            UnKnownErrorMessage.Visible = false;
            try
            {
                BaseWork task = new BaseWork();
                task.TryConnect();
            }
            catch { ConnectErrorMes.Visible = true; }
        }

        //вычисление числа выбранных рпд
        private int GetSelectedProgramsCount()
        {
            int count = 0;
            for (int i = 0; i < Programs_CheckBoxList.Items.Count; i++)
                if (Programs_CheckBoxList.Items[i].Selected == true)
                    count++;
            return count;
        }

        //выбор всех элементов списка рпд
        protected void Button_SelectAll_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < Programs_CheckBoxList.Items.Count; i++)
                Programs_CheckBoxList.Items[i].Selected = true;
        }

        //снять выбор со всех элементов списка рпд
        protected void Button_UnSelectAll_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < Programs_CheckBoxList.Items.Count; i++)
                Programs_CheckBoxList.Items[i].Selected = false;
        }

        //отправки рпд на другие формы и другие задачи
        #region ProgramTasks
        protected void Button_ShowHistory_Click(object sender, EventArgs e)
        {
            if (GetSelectedProgramsCount() > 1)
            {
                UnKnownErrorMessage.Text = "Отмечено несколько программ";
                UnKnownErrorMessage.Visible = true;
            }
            else
            {
                UnKnownErrorMessage.Visible = false;
                if (Programs_CheckBoxList.SelectedIndex == -1)
                    SelectErrorMes.Visible = true;
                else
                {
                    SelectErrorMes.Visible = false;
                    Response.Redirect("ProgramHistory.aspx?ProgramId=" + Programs_CheckBoxList.SelectedValue);
                }
            }
        }

        protected void Button_ProgramSendToRedactor_Click(object sender, EventArgs e)
        {
            if (GetSelectedProgramsCount() > 1)
            {
                List<int> ides = new List<int>();
                for (int i = 0; i < Programs_CheckBoxList.Items.Count; i++)
                    if (Programs_CheckBoxList.Items[i].Selected)
                        ides.Add(Convert.ToInt32(Programs_CheckBoxList.Items[i].Value));
                Session["IDes"] = ides;
                Response.Redirect("SeveralProgramsRedactor.aspx");
            }
            else
            {
                UnKnownErrorMessage.Visible = false;
                if (Programs_CheckBoxList.SelectedIndex == -1)
                    SelectErrorMes.Visible = true;
                else
                {
                    SelectErrorMes.Visible = false;
                    Response.Redirect("ProgramsRedactor.aspx?ProgramID=" + Programs_CheckBoxList.SelectedValue);
                }
            }
        }
        
        //создание копий выбранных рпд
        protected void Button_ProgramCopy_Click(object sender, EventArgs e)
        {
            UnKnownErrorMessage.Visible = false;
            if (Programs_CheckBoxList.SelectedIndex == -1)
                SelectErrorMes.Visible = true;
            else
            {
                SelectErrorMes.Visible = false;
                BaseWork task = new BaseWork();
                List<int> IDes = new List<int>();
                for (int i = 0; i < Programs_CheckBoxList.Items.Count; i++)
                {
                    if (Programs_CheckBoxList.Items[i].Selected)
                        IDes.Add(Convert.ToInt32(Programs_CheckBoxList.Items[i].Value));
                }
                task.ProgramCopy(IDes, User.Identity.GetUserId());
                Programs_CheckBoxList.DataBind();
            }
        }

        //восстановление удаленной рпд
        protected void Button_RecoverProgram_Click(object sender, EventArgs e)
        {
            int programidToRecover;
            if (GetSelectedProgramsCount() == 0)
            {
                SelectErrorMes.Visible = true;
            }
            else
            {
                SelectErrorMes.Visible = false;
                for (int i = 0; i < Programs_CheckBoxList.Items.Count; i++)
                {
                    if (Programs_CheckBoxList.Items[i].Selected == true)
                    {
                        programidToRecover = Convert.ToInt32(Programs_CheckBoxList.Items[i].Value);
                        try
                        {
                            BaseWork task = new BaseWork();
                            task.RecoverProgram(programidToRecover);
                            task.SetHistory(User.Identity.GetUserId(), programidToRecover, "Восстановлена");
                        }
                        catch
                        {
                            UnKnownErrorMessage.Text = "Произошла ошибка при восстановлении";
                            UnKnownErrorMessage.Visible = true;
                        }
                    }
                }
                Programs_CheckBoxList.DataBind();
            }
        }

        //удаление рпд
        protected void Button_DeleteProgram_Click(object sender, EventArgs e)
        {
            int programidToDelete;
            if (GetSelectedProgramsCount() == 0)
            {
                SelectErrorMes.Visible = true;
            }
            else
            {
                SelectErrorMes.Visible = false;
                for (int i = 0; i < Programs_CheckBoxList.Items.Count; i++)
                {
                    if (Programs_CheckBoxList.Items[i].Selected == true)
                    {
                        programidToDelete = Convert.ToInt32(Programs_CheckBoxList.Items[i].Value);
                        try
                        {
                            BaseWork task = new BaseWork();
                            task.TransferToBasket(programidToDelete);
                            task.SetHistory(User.Identity.GetUserId(), programidToDelete, "Перемещена в корзину");
                        }
                        catch
                        {
                            UnKnownErrorMessage.Text = "Произошла ошибка при удалении";
                            UnKnownErrorMessage.Visible = true;
                        }
                    }
                }
                Programs_CheckBoxList.DataBind();
            }
        }

        //очистка корзины
        protected void Button_ClearBasket_Click(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                task.ClearBasket(User.Identity.GetUserId());
            }
            catch
            {
                UnKnownErrorMessage.Text = "Произошла ошибка очистики корзины";
                UnKnownErrorMessage.Visible = true;
            }
            Programs_CheckBoxList.DataBind();
        }

        #endregion ProgramTasks

        #region ListTasks

        protected void FilterDepartment_List_DataBound(object sender, EventArgs e)
        {
            ListItem item = new ListItem();
            item.Text = "Все кафедры";
            item.Value = "";
            FilterDepartment_List.Items.Add(item);
            item.Selected = true;
        }

        protected void FilterProfile_List_DataBound(object sender, EventArgs e)
        {
            ListItem item = new ListItem();
            item.Text = "Все профили";
            item.Value = "";
            FilterProfile_List.Items.Add(item);
            item.Selected = true;
        }

        protected void FilterYear_List_DataBound(object sender, EventArgs e)
        {
            ListItem item = new ListItem();
            item.Text = "Все года";
            item.Value = "";
            FilterYear_List.Items.Add(item);
            item.Selected = true;
        }

        protected void FilterDiscipline_List_DataBound(object sender, EventArgs e)
        {
            ListItem item = new ListItem();
            item.Text = "Все дисциплины";
            item.Value = "";
            FilterDiscipline_List.Items.Add(item);
            item.Selected = true;
        }

        protected void FilterTrainingForm_List_DataBound(object sender, EventArgs e)
        {
            ListItem item = new ListItem();
            item.Text = "Все формы";
            item.Value = "";
            FilterTrainingForm_List.Items.Add(item);
            item.Selected = true;
        }

        protected void FilterStatus_List_DataBound(object sender, EventArgs e)
        {
            ListItem item = new ListItem();
            item.Text = "Выбрать все";
            item.Value = "";
            FilterStatus_List.Items.Add(item);
            item.Selected = true;
        }
        #endregion ListTasks
        
        #region FilterTasks
        private void Filter()
        {
            bool firstcondition = true;
            string commandstring = string.Format("SELECT [id], [Discipline], [Approve_Date], (Discipline+' ('+CONVERT(varchar,Approve_Date,105)+')'+' ('+Status+')') AS Viewer, Status FROM [Programs]");
            SqlDataSourcePrograms.SelectCommand = commandstring;

            if (FilterStatus_List.SelectedValue != "")
            {
                if (firstcondition)
                    commandstring += " WHERE Status = '" + FilterStatus_List.SelectedItem + "'";
                else commandstring += " AND Status = '" + FilterStatus_List.SelectedItem + "'";
                firstcondition = false;
            }

            if (FilterYear_List.SelectedValue != "")
            {
                if (firstcondition)
                    commandstring += " WHERE YEAR(Approve_Date) = " + FilterYear_List.SelectedItem;
                else commandstring += " AND YEAR(Approve_Date) = " + FilterYear_List.SelectedItem;
                firstcondition = false;
            }

            if (FilterTrainingForm_List.SelectedValue != "")
            {
                if (firstcondition)
                    commandstring += " WHERE Training_Form = '" + FilterTrainingForm_List.SelectedItem + "'";
                else commandstring += " AND Training_Form = '" + FilterTrainingForm_List.SelectedItem + "'";
                firstcondition = false;
            }

            if (FilterProfile_List.SelectedValue != "")
            {
                if (firstcondition)
                    commandstring += " WHERE Profil = '" + FilterProfile_List.SelectedItem.Text + "'";
                else commandstring += " AND Profil = '" + FilterProfile_List.SelectedItem.Text + "'";
                firstcondition = false;
            }

            if (FilterDepartment_List.SelectedValue != "")
            {
                if (firstcondition)
                    commandstring += " WHERE Department = '" + FilterDepartment_List.SelectedItem + "'";
                else commandstring += " AND Department = '" + FilterDepartment_List.SelectedItem + "'";
                firstcondition = false;
            }

            if (FilterDiscipline_List.SelectedValue != "")
            {
                if (firstcondition)
                    commandstring += " WHERE Discipline = '" + FilterDiscipline_List.SelectedItem + "'";
                else commandstring += " AND Discipline = '" + FilterDiscipline_List.SelectedItem + "'";
                firstcondition = false;
            }

            SqlDataSourcePrograms.SelectCommand = commandstring;
            
        }

        protected void FiltersUse_Button_Click(object sender, EventArgs e)
        {
            Filter();
            Programs_CheckBoxList.DataBind();
        }

        protected void FiltersClear_Button_Click(object sender, EventArgs e)
        {
            FilterYear_List.SelectedValue = "";
            FilterDiscipline_List.SelectedValue = "";
            FilterTrainingForm_List.SelectedValue = "";
            FilterStatus_List.SelectedValue = "";
            FilterProfile_List.SelectedValue = "";
            FilterDepartment_List.SelectedValue = "";
            TextBox_Search.Text = "";
            Filter();
            Programs_CheckBoxList.DataBind();
        }
        #endregion FilterTasks
        
        //поиск рпд по названию
        protected void Button_Search_Click(object sender, EventArgs e)
        {
            Filter();
            string commandstring = string.Format("SELECT [id], [Discipline], [Approve_Date], (Discipline+' ('+CONVERT(varchar,Approve_Date,105)+')'+' ('+Status+')') AS Viewer, Status FROM [Programs]");
            if (SqlDataSourcePrograms.SelectCommand == commandstring)
                SqlDataSourcePrograms.SelectCommand += " WHERE";
            else SqlDataSourcePrograms.SelectCommand += " AND";
            SqlDataSourcePrograms.SelectCommand += " Discipline LIKE '%" + TextBox_Search.Text + "%'";
            Programs_CheckBoxList.DataBind();
        }

        
    }
}