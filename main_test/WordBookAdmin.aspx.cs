﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using main_test.WordBooks;
using System.Data;
using System.Threading.Tasks;

namespace main_test
{
    public partial class WordBookAdmin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Message_Label.Visible = false;
            ErrorMes.Visible = false;
            if (!IsPostBack)
            {
                DataTable DataTableCompetences = new DataTable();
                DataTable DataTableDisciplines = new DataTable();
                List<int> DirectionsInTrainingPlanIDes = new List<int>();
                List<int> ProfilesInTrainingDirectionIDes = new List<int>();
                List<int> ProgramsInProfile = new List<int>();
                List<int> DepartmentsInInstitute = new List<int>();
                List<string> PersonalInDepartment = new List<string>();
                List<int> PlansInFGOS = new List<int>();
                List<int> Disciplines_BeforeIDes = new List<int>();
                List<int> Disciplines_AfterIDes = new List<int>();
                Session["DirectionsToTrainingPlanIDes"] = DirectionsInTrainingPlanIDes;
                Session["ProfilesToTrainingDirectionIDes"] = ProfilesInTrainingDirectionIDes;
                Session["ProgramsToProfile"] = ProgramsInProfile;
                Session["DepartmentsToInstitute"] = DepartmentsInInstitute;
                Session["PersonalToDepartment"] = PersonalInDepartment;
                Session["PlansToFGOS"] = PlansInFGOS;
                Session["Disciplines_BeforeIDes"] = Disciplines_BeforeIDes;
                Session["Disciplines_AfterIDes"] = Disciplines_AfterIDes;
                Session["DataTableCompetences"] = DataTableCompetences;
                Session["DataTableDisciplines"] = DataTableDisciplines;
                BindAllData();
                FirstFill();
            }
            else
            {
                try { BuildPreview(PreviewCompetences_Table, (DataTable)Session["DataTableCompetences"]); } catch { }
                try { BuildPreview(PreviewDisciplines_Table, (DataTable)Session["DataTableDisciplines"]); } catch { }
            }
        }

        #region Заполнение данных
        private void BuildPreview(Table table, DataTable data)
        {
            for (int i = table.Rows.Count - 1; i == 0; i--)
                table.Rows.RemoveAt(i);
            for(int i = 0; i < data.Rows.Count; i++)
            {
                TableRow rowT = new TableRow();
                table.Rows.Add(rowT);
                for (int j = 0; j < data.Columns.Count; j++)
                {
                    TableCell cellT = new TableCell();
                    rowT.Cells.Add(cellT);
                    rowT.Cells[j].Text = data.Rows[i][j].ToString();
                }
            }
        }

        private void BindAllData()
        {
            Competence_DropDownList.DataBind();
            DropDown_CompetencesFGOS.DataBind();
            Disciplines_DropDownList.DataBind();
            DropDown_DisciplineInOOPAfter.DataBind();
            DropDown_DisciplineInOOPBefore.DataBind();
            Qualifications_DropDownList.DataBind();
            TrainingDirections_DropDownList.DataBind();
            DropDown_TrainingDirectionProfiles.DataBind();
            DropDownList_Profiles.DataBind();
            DropDown_ProfilePrograms.DataBind();
            DropDownList_Departments.DataBind();
            DropDown_DepartmentPersonal.DataBind();
            DropDown_Institutes.DataBind();
            DropDown_InstituteDepartments.DataBind();
            DropDown_TrainingPlans.DataBind();
            DropDown_TrainingPlanDirections.DataBind();
            DropDown_FGOS.DataBind();
            DropDown_FGOSTrainingPlans.DataBind();
        }

        private void FirstFill()
        {
            try
            {
                Competence com = new Competence();
                BaseWork task = new BaseWork();
                if (Competence_DropDownList.Items.Count != 0)
                {
                    com = task.GetCompetence(Convert.ToInt32(Competence_DropDownList.SelectedItem.Value));
                    CompetenceCode_TextBox.Text = com.Code;
                    CompetenceDescription_TextBox.Text = com.Description;
                    try { DropDown_CompetencesFGOS.SelectedValue = com.FGOSId.ToString(); }
                    catch { DropDown_CompetencesFGOS.SelectedValue = "0"; }
                }

                if (Disciplines_DropDownList.Items.Count != 0)
                {
                    Discipline discipline = new Discipline();
                    discipline = task.GetDiscipline(Convert.ToInt32(Disciplines_DropDownList.SelectedItem.Value));
                    Discipline_TextBox.Text = discipline.Name;
                    Session["Disciplines_BeforeIDes"] = discipline.Disciplines_Before;
                    DisciplineInOOPBefore_TextBox.Text = "";
                    Session["Disciplines_AfterIDes"] = discipline.Disciplines_After;
                    DisciplineInOOPAfter_TextBox.Text = "";
                    for (int i = 0; i < DropDown_DisciplineInOOPBefore.Items.Count; i++)
                        if (discipline.Disciplines_Before.Contains(Convert.ToInt32(DropDown_DisciplineInOOPBefore.Items[i].Value)))
                            DisciplineInOOPBefore_TextBox.Text += DropDown_DisciplineInOOPBefore.Items[i].Text + "\n";
                    for (int i = 0; i < DropDown_DisciplineInOOPAfter.Items.Count; i++)
                        if (discipline.Disciplines_After.Contains(Convert.ToInt32(DropDown_DisciplineInOOPAfter.Items[i].Value)))
                            DisciplineInOOPAfter_TextBox.Text += DropDown_DisciplineInOOPAfter.Items[i].Text + "\n";
                }
                
                TrainingDirection direction = new TrainingDirection();
                int id = Convert.ToInt32(TrainingDirections_DropDownList.SelectedValue);
                direction = task.GetTrainingDirection_WordBook(id);
                TrainingDirectionName_TextBox.Text = direction.Designation;
                TrainingDirectionCode_TextBox.Text = direction.Code;

                TextBox_TrainingDirectionProfiles.Text = "";
                Session["ProfilesToTrainingDirectionIDes"] = direction.ProfilesIDes;

                for (int i = 0; i < DropDown_TrainingDirectionProfiles.Items.Count; i++)
                    if (direction.ProfilesIDes.Contains(Convert.ToInt32(DropDown_TrainingDirectionProfiles.Items[i].Value)))
                        TextBox_TrainingDirectionProfiles.Text += DropDown_TrainingDirectionProfiles.Items[i].Text + "\n";

                Profile profile = new Profile();
                id = Convert.ToInt32(DropDownList_Profiles.SelectedItem.Value);
                profile = task.GetProfile_WordBook(id);
                TextBox_Profiles.Text = profile.Name;
                TextBox_ProfilePrograms.Text = "";
                Session["ProgramsToProfile"] = profile.ProgramsIDes;

                for (int i = 0; i < DropDown_ProfilePrograms.Items.Count; i++)
                    if (profile.ProgramsIDes.Contains(Convert.ToInt32(DropDown_ProfilePrograms.Items[i].Value)))
                        TextBox_ProfilePrograms.Text += DropDown_ProfilePrograms.Items[i].Text + "\n";

                Department dep = new Department();
                id = Convert.ToInt32(DropDownList_Departments.SelectedItem.Value);
                dep = task.GetDepartment_WordBook(id);
                TextBox_Department.Text = dep.Name;
                TextBox_DepartmentPersonal.Text = "";
                Session["PersonalToDepartment"] = dep.PersonalIDes;

                for (int i = 0; i < DropDown_DepartmentPersonal.Items.Count; i++)
                    if (dep.PersonalIDes.Contains(DropDown_DepartmentPersonal.Items[i].Value.ToString()))
                        TextBox_DepartmentPersonal.Text += DropDown_DepartmentPersonal.Items[i].Text + "\n";

                Institute inst = new Institute();
                id = Convert.ToInt32(DropDown_Institutes.SelectedItem.Value);
                inst = task.GetInstitute_WordBook(id);
                TextBox_Institute.Text = inst.Name;
                TextBox_InstituteDepartments.Text = "";
                Session["DepartmentsToInstitute"] = inst.DepartmentsIDes;

                for (int i = 0; i < DropDown_InstituteDepartments.Items.Count; i++)
                    if (inst.DepartmentsIDes.Contains(Convert.ToInt32(DropDown_InstituteDepartments.Items[i].Value)))
                        TextBox_InstituteDepartments.Text += DropDown_InstituteDepartments.Items[i].Text + "\n";

                TrainingPlan plan = new TrainingPlan();
                id = Convert.ToInt32(DropDown_TrainingPlans.SelectedItem.Value);
                plan = task.GetTrainingPlan(id);
                TextBox_PlanName.Text = plan.Name;
                DropDown_TrainingPlansYears.Text = plan.Year;
                TextBox_TrainingPlanDirections.Text = "";
                Session["DirectionsToTrainingPlanIDes"] = plan.DirectionIDes;

                for (int i = 0; i < DropDown_TrainingPlanDirections.Items.Count; i++)
                    if (plan.DirectionIDes.Contains(Convert.ToInt32(DropDown_TrainingPlanDirections.Items[i].Value)))
                        TextBox_TrainingPlanDirections.Text += DropDown_TrainingPlanDirections.Items[i].Text + "\n";

                FGOS fgos = new FGOS();
                id = Convert.ToInt32(DropDown_FGOS.SelectedItem.Value);
                fgos = task.GetFGOS(id);
                TextBox_FGOSName.Text = fgos.Name;
                TextBox_FGOS.Text = fgos.Designation;
                TextBox_FGOSTRainingPlans.Text = "";
                Session["PlansToFGOS"] = fgos.TrainingPlansIDes;

                for (int i = 0; i < DropDown_FGOSTrainingPlans.Items.Count; i++)
                    if (fgos.TrainingPlansIDes.Contains(Convert.ToInt32(DropDown_FGOSTrainingPlans.Items[i].Value)))
                        TextBox_FGOSTRainingPlans.Text += DropDown_FGOSTrainingPlans.Items[i].Text + "\n";
            }
            catch
            {
                ErrorMes.Text = "Ошибка в загрузке данных";
                ErrorMes.Visible = true;
            }
        }
        #endregion

        #region Компетенции
        protected void UploadCompetences_Button_Click(object sender, EventArgs e)
        {
            if (uploader_competences.HasFile)
            {
                try
                {
                    PreviewCompetences_Div.Visible = true;
                    //получаю файл в байтовом массиве
                    byte[] file = uploader_competences.FileBytes;
                    Excel excel = new Excel();
                    DataTable data = new DataTable();
                    data = excel.GetDataTable(file);
                    BuildPreview(PreviewCompetences_Table, data);
                    CodeInExcelCompetences_TextBox.Attributes.Add("max", data.Columns.Count.ToString());
                    DescriptionInExcelCompetences_TextBox.Attributes.Add("max", data.Columns.Count.ToString());
                    RowStartInExcelCompetences_TextBox.Attributes.Add("max", data.Rows.Count.ToString());
                    Session["DataTableCompetences"] = data;
                }
                catch(Exception ex)
                {
                    ErrorMes.Text = "Произошла ошибка в процессе выгрузки файла: " + ex.Message;
                    ErrorMes.Visible = true;
                }
            }
            else
            {
                ErrorMes.Text = "Файл не выбран";
                ErrorMes.Visible = true;
            }
        }

        protected void PushUploadedCompetences_Button_Click(object sender, EventArgs e)
        {
            if (DescriptionInExcelCompetences_TextBox.Text != "" || CodeInExcelCompetences_TextBox.Text != "")
            {
                try
                {
                    int col_1 = Convert.ToInt32(CodeInExcelCompetences_TextBox.Text) - 1;
                    int col_2 = Convert.ToInt32(DescriptionInExcelCompetences_TextBox.Text) - 1;
                    int row_start = Convert.ToInt32(RowStartInExcelCompetences_TextBox.Text) - 1;
                    if (row_start == -1) row_start = 0;
                    DataTable data = new DataTable();
                    data = (DataTable)Session["DataTableCompetences"];
                    List<Competence> coms = new List<Competence>();
                    for (int i = row_start; i < data.Rows.Count; i++)
                    {
                        DataRow row = data.Rows[i];
                        Competence com = new Competence();
                        com.Code = row[col_1].ToString();
                        com.Description = row[col_2].ToString();
                        com.FGOSId = Convert.ToInt32(FGOSForUploadCompetences_DropDown.SelectedItem.Value);
                        coms.Add(com);
                    }
                    BaseWork task = new BaseWork();
                    coms.ForEach(item => task.CreateCompetence_WordBook(item));
                    PreviewCompetences_Div.Visible = false;
                    Competence_DropDownList.DataBind();
                    Message_Label.InnerHtml = "Группа компетенций успешно внесена в справочную систему";
                    Message_Label.Visible = true;
                }
                catch(Exception ex)
                {
                    ErrorMes.Text = "Произошла ошибка в процессе внесения данных: " + ex.Message;
                    ErrorMes.Visible = true;
                }
            }
            else
            {
                ErrorMes.Text = "Выберите столбцы";
                ErrorMes.Visible = true;
            }
        }

        protected void FGOSForUploadCompetences_DropDown_DataBound(object sender, EventArgs e)
        {
            ListItem item = new ListItem();
            item.Text = "Без ФГОС";
            item.Value = "0";
            FGOSForUploadCompetences_DropDown.Items.Add(item);
            item.Selected = true;
        }

        protected void DropDown_CompetencesFGOS_DataBound(object sender, EventArgs e)
        {
            ListItem item = new ListItem();
            item.Text = "Без ФГОС";
            item.Value = "0";
            DropDown_CompetencesFGOS.Items.Add(item);
            item.Selected = true;
        }

        private void FillCompetence(Competence com)
        {
            com.Code = CompetenceCode_TextBox.Text;
            com.Description = CompetenceDescription_TextBox.Text;
            com.FGOSId = Convert.ToInt32(DropDown_CompetencesFGOS.SelectedItem.Value);
        }

        protected void CreateCompetence_Button_Click(object sender, EventArgs e)
        {
            try
            {
                Competence com = new Competence();
                BaseWork task = new BaseWork();
                FillCompetence(com);
                bool success = task.CreateCompetence_WordBook(com);
                Competence_DropDownList.DataBind();
                if (success)
                {
                    Message_Label.InnerHtml = "Компетенция успешно внесена в систему";
                    Message_Label.Visible = true;
                }
                else
                {
                    ErrorMes.Text = "Компетенция не была внесена в систему, возможно, таковая уже есть в системе";
                    ErrorMes.Visible = true;
                }
            }
            catch
            {
                ErrorMes.Text = "Не удалось внести компетенцию в систему";
                ErrorMes.Visible = true;
            }
        }

        protected void RedactCompetence_Button_Click(object sender, EventArgs e)
        {
            try
            {
                Competence com = new Competence();
                BaseWork task = new BaseWork();
                FillCompetence(com);
                com.Id = Convert.ToInt32(Competence_DropDownList.SelectedItem.Value);
                task.UpDateCompetence_WordBook(com);
                Competence_DropDownList.DataBind();
                Message_Label.InnerHtml = "Компетенция успешно обновлена";
                Message_Label.Visible = true;
                ErrorMes.Visible = false;
            }
            catch
            {
                ErrorMes.Text = "Не удалось внести правку в компетенцию";
                ErrorMes.Visible = true;
            }
        }

        protected void Competence_DropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Competence com = new Competence();
                BaseWork task = new BaseWork();
                com = task.GetCompetence(Convert.ToInt32(Competence_DropDownList.SelectedValue));
                CompetenceCode_TextBox.Text = com.Code;
                CompetenceDescription_TextBox.Text = com.Description;
                try { DropDown_CompetencesFGOS.SelectedValue = com.FGOSId.ToString(); }
                catch { DropDown_CompetencesFGOS.SelectedValue = "0"; }
            }
            catch
            {
                ErrorMes.Text = "Не удалось загрузить компетенцию";
                ErrorMes.Visible = true;
            }
        }
        
        protected void DeleteCompetence_Button_Click(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                int id = Convert.ToInt32(Competence_DropDownList.SelectedItem.Value);
                task.DeleteCompetence_WordBook(id);
                Message_Label.InnerHtml = "Кометенция " + Competence_DropDownList.SelectedItem.Text +
                    " успешно удалена";
                Competence_DropDownList.DataBind();
                Message_Label.Visible = true;
                ErrorMes.Visible = false;
            }
            catch
            {
                ErrorMes.Text = "Произошел сбой в процессе удаления";
                ErrorMes.Visible = true;
            }
        }

        #endregion

        #region Дисциплины
        protected void UploadDisciplines_Button_Click(object sender, EventArgs e)
        {
            if (uploader_disciplines.HasFile)
            {
                try
                {
                    PreviewDisciplines_Div.Visible = true;
                    //получаю файл в байтовом массиве
                    byte[] file = uploader_disciplines.FileBytes;
                    Excel excel = new Excel();
                    DataTable data = new DataTable();
                    data = excel.GetDataTable(file);
                    BuildPreview(PreviewDisciplines_Table, data);
                    Session["DataTableDisciplines"] = data;
                }
                catch (Exception ex)
                {
                    ErrorMes.Text = "Произошла ошибка в процессе выгрузки файла: " + ex.Message;
                    ErrorMes.Visible = true;
                }
            }
            else
            {
                ErrorMes.Text = "Файл не выбран";
                ErrorMes.Visible = true;
            }
        }

        protected void PushUploadedDisciplines_Button_Click(object sender, EventArgs e)
        {
            if (PreviewDisciplines_Table.Rows.Count > 3)
            {
                try
                {
                    DataTable data = new DataTable();
                    BaseWork task = new BaseWork();
                    data = (DataTable)Session["DataTableDisciplines"];
                    List<Discipline> dises = new List<Discipline>();
                    for (int i = 2; i < data.Rows.Count; i++)
                    {
                        Discipline dis = new Discipline();
                        string name = data.Rows[i][0].ToString();
                        dis.Name = name;
                        dises.Add(dis);
                    }
                    dises.ForEach(item => task.CreateDiscipline_WordBook(item.Name));

                    for (int i = 2; i < data.Rows.Count; i++)
                    {
                        Discipline dis = new Discipline();
                        dis = dises.Find(item => item.Name == data.Rows[i][0].ToString());
                        dis.Id = task.GetDisciplineIdByName(dis.Name);

                        if (dis.Id != 0 && dis.Name.Replace(" ", "") != "")
                        {
                            for (int j = 2; j < data.Columns.Count; j++)
                            {
                                if (data.Rows[i][j].ToString() != "")
                                {
                                    Discipline dis_after = new Discipline();
                                    dis_after.Name = data.Rows[0][j].ToString();
                                    dis_after.Id = task.GetDisciplineIdByName(dis_after.Name);

                                    if (dis_after.Id != 0 && dis_after.Name.Replace(" ", "") != "")
                                    {
                                        task.CreateCrossDisciplineAfter_WordBook(dis.Id, dis_after.Id);
                                        task.CreateCrossDisciplineBefore_WordBook(dis_after.Id, dis.Id);
                                    }
                                }
                            }
                        }
                    }

                    Disciplines_DropDownList.DataBind();
                    DropDown_DisciplineInOOPAfter.DataBind();
                    DropDown_DisciplineInOOPBefore.DataBind();
                    PreviewDisciplines_Div.Visible = false;
                    Message_Label.InnerHtml = "Группа дисциплин успешна внесена в систему";
                    Message_Label.Visible = true;
                }
                catch (Exception ex)
                {
                    ErrorMes.Text = "Ошибка в процессе внесения дисциплин: " + ex.Message;
                    ErrorMes.Visible = true;
                }
            }
            else
            {
                ErrorMes.Text = "В группе не найдено ни одной дисциплины";
                ErrorMes.Visible = true;
            }
        }

        protected void Disciplines_DropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                Discipline discipline = new Discipline();
                discipline = task.GetDiscipline(Convert.ToInt32(Disciplines_DropDownList.SelectedItem.Value));
                Discipline_TextBox.Text = discipline.Name;
                Session["Disciplines_BeforeIDes"] = discipline.Disciplines_Before;
                Session["Disciplines_AfterIDes"] = discipline.Disciplines_After;
                DisciplineInOOPAfter_TextBox.Text = "";
                DisciplineInOOPBefore_TextBox.Text = "";
                for (int i = 0; i < DropDown_DisciplineInOOPBefore.Items.Count; i++)
                    if (discipline.Disciplines_Before.Contains(Convert.ToInt32(DropDown_DisciplineInOOPBefore.Items[i].Value)))
                        DisciplineInOOPBefore_TextBox.Text += DropDown_DisciplineInOOPBefore.Items[i].Text + "\n";
                for (int i = 0; i < DropDown_DisciplineInOOPAfter.Items.Count; i++)
                    if (discipline.Disciplines_After.Contains(Convert.ToInt32(DropDown_DisciplineInOOPAfter.Items[i].Value)))
                        DisciplineInOOPAfter_TextBox.Text += DropDown_DisciplineInOOPAfter.Items[i].Text + "\n";
            }
            catch
            {
                ErrorMes.Text = "Ошибка";
                ErrorMes.Visible = true;
            }
        }

        protected void CreateDiscipline_Button_Click(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                string name = Discipline_TextBox.Text;
                bool success = task.CreateDiscipline_WordBook(name);
                Disciplines_DropDownList.DataBind();
                if (success)
                {
                    Message_Label.InnerHtml = "Дисциплина успешно внесена в систему";
                    Message_Label.Visible = true;
                }
                else
                {
                    ErrorMes.Text = "Не удалось внести дисциплину в систему. Проверьте введенные данные";
                    ErrorMes.Visible = true;
                }
            }
            catch
            {
                ErrorMes.Text = "Не удалось внести дисциплину в систему. " +
                    "Возможно, дисциплина под названием \"" + Discipline_TextBox.Text + "\" уже существует";
                ErrorMes.Visible = true;
            }
        }

        protected void RedactDiscipline_Button_Click(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                string newname = Discipline_TextBox.Text;
                int id = Convert.ToInt32(Disciplines_DropDownList.SelectedValue);
                task.UpDateDiscipline_WordBook(id, newname);
                Disciplines_DropDownList.DataBind();
                Message_Label.InnerHtml = "Дисциплина успешно обновлена";
                Message_Label.Visible = true;
            }
            catch
            {
                ErrorMes.Text = "Не удалось внести правку в дисциплину. " +
                    "Возможно, дисциплина под названием \"" + Discipline_TextBox.Text + "\" уже существует";
                ErrorMes.Visible = true;
            }
        }

        protected void DeleteDiscipline_Button_Click(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                int id = Convert.ToInt32(Disciplines_DropDownList.SelectedValue);
                task.DeleteDiscipline_WordBook(id);
                Message_Label.InnerHtml = "Дисциплина " + Disciplines_DropDownList.SelectedItem.Text +
                    " успешно удалена";
                Disciplines_DropDownList.DataBind();
                Message_Label.Visible = true;
            }
            catch
            {
                ErrorMes.Text = "Произошел сбой в процессе удаления";
                ErrorMes.Visible = true;
            }
        }

        protected void Button_AddDisciplineInOOPBefore_Click(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                int disciplineId = Convert.ToInt32(Disciplines_DropDownList.SelectedItem.Value);
                int disciplineId_before = Convert.ToInt32(DropDown_DisciplineInOOPBefore.SelectedItem.Value);

                List<int> IDes = (List<int>)Session["Disciplines_BeforeIDes"];
                if (IDes.Contains(disciplineId_before))
                {
                    ErrorMes.Text = "Дисциплина уже в списке";
                    ErrorMes.Visible = true;
                }
                else
                {
                    IDes.Add(disciplineId_before);
                    task.CreateCrossDisciplineBefore_WordBook(disciplineId, disciplineId_before);
                    task.CreateCrossDisciplineAfter_WordBook(disciplineId_before, disciplineId);
                    DisciplineInOOPBefore_TextBox.Text += DropDown_DisciplineInOOPBefore.SelectedItem.Text + "\n";
                    Message_Label.InnerHtml = "Дисциплина " + DropDown_DisciplineInOOPBefore.SelectedItem.Text +
                        " помечена в структуре ООП перед дисциплиной " + Disciplines_DropDownList.SelectedItem.Text;
                    Message_Label.Visible = true;
                }
            }
            catch
            {
                ErrorMes.Text = "Произошел сбой в процессе добавления";
                ErrorMes.Visible = true;
            }
        }

        protected void Button_DeleteDisciplineInOOPBefore_Click(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                int disciplineId = Convert.ToInt32(Disciplines_DropDownList.SelectedItem.Value);
                int disciplineId_before = Convert.ToInt32(DropDown_DisciplineInOOPBefore.SelectedItem.Value);
                task.DeleteCrossDisciplineBefore_WordBook(disciplineId, disciplineId_before);
                task.DeleteCrossDisciplineAfter_WordBook(disciplineId_before, disciplineId);
                List<int> IDes = (List<int>)Session["Disciplines_BeforeIDes"];
                IDes.Remove(disciplineId_before);
                string text = DropDown_DisciplineInOOPBefore.SelectedItem.Text + "\n";
                DisciplineInOOPBefore_TextBox.Text = DisciplineInOOPBefore_TextBox.Text.Replace(text, "");
                Message_Label.InnerHtml = "Дисциплина " + DropDown_DisciplineInOOPBefore.SelectedItem.Text +
                    " удалена из очереди в структуре ООП перед дисциплиной " + Disciplines_DropDownList.SelectedItem.Text;
                Message_Label.Visible = true;
            }
            catch
            {
                ErrorMes.Text = "Произошел сбой в процессе удаления";
                ErrorMes.Visible = true;
            }
        }

        protected void Button_AddDisciplineInOOPAfter_Click(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                int disciplineId = Convert.ToInt32(Disciplines_DropDownList.SelectedItem.Value);
                int disciplineId_after = Convert.ToInt32(DropDown_DisciplineInOOPAfter.SelectedItem.Value);

                List<int> IDes = (List<int>)Session["Disciplines_AfterIDes"];
                if (IDes.Contains(disciplineId_after))
                {
                    ErrorMes.Text = "Дисциплина уже в списке";
                    ErrorMes.Visible = true;
                }
                else
                {
                    IDes.Add(disciplineId_after);
                    task.CreateCrossDisciplineAfter_WordBook(disciplineId, disciplineId_after);
                    task.CreateCrossDisciplineBefore_WordBook(disciplineId_after, disciplineId);
                    DisciplineInOOPAfter_TextBox.Text += DropDown_DisciplineInOOPAfter.SelectedItem.Text + "\n";
                    Message_Label.InnerHtml = "Дисциплина " + DropDown_DisciplineInOOPAfter.SelectedItem.Text +
                        " помечена в структуре ООП после дисциплины " + Disciplines_DropDownList.SelectedItem.Text;
                    Message_Label.Visible = true;
                }
            }
            catch
            {
                ErrorMes.Text = "Произошел сбой в процессе добавления";
                ErrorMes.Visible = true;
            }
        }

        protected void Button_DeleteDisciplineInOOPAfter_Click(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                int disciplineId = Convert.ToInt32(Disciplines_DropDownList.SelectedItem.Value);
                int disciplineId_after = Convert.ToInt32(DropDown_DisciplineInOOPAfter.SelectedItem.Value);
                task.DeleteCrossDisciplineAfter_WordBook(disciplineId, disciplineId_after);
                task.DeleteCrossDisciplineBefore_WordBook(disciplineId_after, disciplineId);
                List<int> IDes = (List<int>)Session["Disciplines_AfterIDes"];
                IDes.Remove(disciplineId_after);
                string text = DropDown_DisciplineInOOPAfter.SelectedItem.Text + "\n";
                DisciplineInOOPAfter_TextBox.Text = DisciplineInOOPAfter_TextBox.Text.Replace(text, "");
                Message_Label.InnerHtml = "Дисциплина " + DropDown_DisciplineInOOPAfter.SelectedItem.Text +
                    " удалена из очереди в структуре ООП после дисциплины " + Disciplines_DropDownList.SelectedItem.Text;
                Message_Label.Visible = true;
            }
            catch
            {
                ErrorMes.Text = "Произошел сбой в процессе удаления";
                ErrorMes.Visible = true;
            }
        }

        #endregion

        #region Квалификации
        protected void RedactQualifications_Button_Click(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                string newname = Qualifications_TextBox.Text;
                int id = Convert.ToInt32(Qualifications_DropDownList.SelectedValue);
                task.UpDateQalifications_WordBook(id, newname);
                Qualifications_DropDownList.DataBind();
                Message_Label.InnerHtml = "Степень успешно обновлена";
                Message_Label.Visible = true;
            }
            catch
            {
                ErrorMes.Text = "Не удалось внести правку в степень. " +
                    "Возможно, степень под названием \"" + Qualifications_TextBox.Text + "\" уже существует";
                ErrorMes.Visible = true;
            }
        }

        protected void CreateQualifications_Button_Click(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                string name = Qualifications_TextBox.Text;
                task.CreateQalifications_WordBook(name);
                Qualifications_DropDownList.DataBind();
                Message_Label.InnerHtml = "Степень успешно внесена в систему";
                Message_Label.Visible = true;
            }
            catch
            {
                ErrorMes.Text = "Не удалось внести степень в систему. " +
                    "Возможно, степень под названием \"" + Qualifications_TextBox.Text + "\" уже существует";
                ErrorMes.Visible = true;
            }
        }

        protected void DeleteQualifications_Button_Click(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                int id = Convert.ToInt32(Qualifications_DropDownList.SelectedValue);
                task.DeleteQalifications_WordBook(id);
                Message_Label.InnerHtml = "Степень " + Qualifications_DropDownList.SelectedItem.Text +
                    " успешно удалена";
                Qualifications_DropDownList.DataBind();
                Message_Label.Visible = true;
            }
            catch
            {
                ErrorMes.Text = "Произошел сбой в процессе удаления";
                ErrorMes.Visible = true;
            }
        }
        #endregion

        #region Направления
        protected void TrainingDirections_DropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                TrainingDirection direction = new TrainingDirection();
                int id = Convert.ToInt32(TrainingDirections_DropDownList.SelectedValue);
                direction = task.GetTrainingDirection_WordBook(id);
                TrainingDirectionName_TextBox.Text = direction.Designation;
                TrainingDirectionCode_TextBox.Text = direction.Code;

                TextBox_TrainingDirectionProfiles.Text = "";
                Session["ProfilesToTrainingDirectionIDes"] = direction.ProfilesIDes;

                for (int i = 0; i < DropDown_TrainingDirectionProfiles.Items.Count; i++)
                    if (direction.ProfilesIDes.Contains(Convert.ToInt32(DropDown_TrainingDirectionProfiles.Items[i].Value)))
                        TextBox_TrainingDirectionProfiles.Text += DropDown_TrainingDirectionProfiles.Items[i].Text + "\n";
            }
            catch
            {
                ErrorMes.Text = "Не удалось получить информацию о направлении";
                ErrorMes.Visible = true;
            }
        }

        protected void DeleteTrainingDirection_Button_Click(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                int id = Convert.ToInt32(TrainingDirections_DropDownList.SelectedValue);
                task.DeleteTrainingDirection_WordBook(id);
                Message_Label.InnerHtml = "Направление " + TrainingDirections_DropDownList.SelectedItem.Text +
                    " успешно удалено";
                TrainingDirections_DropDownList.DataBind();
                Message_Label.Visible = true;
            }
            catch
            {
                ErrorMes.Text = "Произошел сбой в процессе удаления";
                ErrorMes.Visible = true;
            }
        }

        protected void RedactTrainingDirection_Button_Click(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                TrainingDirection direction = new TrainingDirection();
                direction.Designation = TrainingDirectionName_TextBox.Text;
                direction.Code = TrainingDirectionCode_TextBox.Text;
                direction.Id = Convert.ToInt32(TrainingDirections_DropDownList.SelectedValue);
                direction.ProfilesIDes = (List<int>)Session["ProfilesToTrainingDirectionIDes"];
                task.UpDateTrainingDirection_WordBook(direction);
                TrainingDirections_DropDownList.DataBind();
                Message_Label.InnerHtml = "Направление успешно обновлено";
                Message_Label.Visible = true;
            }
            catch
            {
                ErrorMes.Text = "Не удалось внести правку в направление. " +
                    "Возможно, направление с кодом (" + TrainingDirectionCode_TextBox.Text + ") уже существует";
                ErrorMes.Visible = true;
            }
        }

        protected void CreateTrainingDirection_Button_Click(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                TrainingDirection direction = new TrainingDirection();
                direction.Designation = TrainingDirectionName_TextBox.Text;
                direction.Code = TrainingDirectionCode_TextBox.Text;
                direction.ProfilesIDes = (List<int>)Session["ProfilesToTrainingDirectionIDes"];
                task.CreateTrainingDirection_WordBook(direction);
                TrainingDirections_DropDownList.DataBind();
                Message_Label.InnerHtml = "Направление успешно внесено в систему";
                Message_Label.Visible = true;
            }
            catch
            {
                ErrorMes.Text = "Не удалось внести направление в систему. " +
                    "Возможно, направление с кодом (" + TrainingDirectionCode_TextBox.Text + ") уже существует";
                ErrorMes.Visible = true;
            }
        }

        protected void Button_CreateTrainingDirectionProfile_Click(object sender, EventArgs e)
        {
            try
            {
                List<int> IDes = (List<int>)Session["ProfilesToTrainingDirectionIDes"];
                int id = Convert.ToInt32(DropDown_TrainingDirectionProfiles.SelectedItem.Value);
                string text = DropDown_TrainingDirectionProfiles.SelectedItem.Text + "\n";
                IDes.Add(id);
                TextBox_TrainingDirectionProfiles.Text += text;
            }
            catch
            {
                ErrorMes.Text = "Ошибка";
                ErrorMes.Visible = true;
            }
        }

        protected void Button_DeleteTrainingDirectionProfile_Click(object sender, EventArgs e)
        {
            try
            {
                List<int> IDes = (List<int>)Session["ProfilesToTrainingDirectionIDes"];
                int id = Convert.ToInt32(DropDown_TrainingDirectionProfiles.SelectedItem.Value);
                string text = DropDown_TrainingDirectionProfiles.SelectedItem.Text + "\n";
                IDes.Remove(id);
                TextBox_TrainingDirectionProfiles.Text = TextBox_TrainingDirectionProfiles.Text.Replace(text, "");
            }
            catch
            {
                ErrorMes.Text = "Ошибка";
                ErrorMes.Visible = true;
            }
        }
        #endregion

        #region Профили
        protected void Button_Create_Profile_Click(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                Profile profile = new Profile();
                profile.Name = TextBox_Profiles.Text;
                profile.ProgramsIDes = (List<int>)Session["ProgramsToProfile"];
                task.CreateProfile_WordBook(profile);
                DropDownList_Profiles.DataBind();
                Message_Label.InnerHtml = "Профиль успешно внесен в систему";
                Message_Label.Visible = true;
            }
            catch
            {
                ErrorMes.Text = "Не удалось внести профиль в систему. " +
                    "Возможно, профиль под названием \"" + TextBox_Profiles.Text + "\" уже существует";
                ErrorMes.Visible = true;
            }
        }

        protected void Button_Update_Profile_Click(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                Profile profile = new Profile();
                profile.Name = TextBox_Profiles.Text;
                profile.Id = Convert.ToInt32(DropDownList_Profiles.SelectedValue);
                profile.ProgramsIDes = (List<int>)Session["ProgramsToProfile"];
                task.UpdateProfile_WordBook(profile);
                DropDownList_Profiles.DataBind();
                Message_Label.InnerHtml = "Профиль успешно обновлен";
                Message_Label.Visible = true;
            }
            catch
            {
                ErrorMes.Text = "Не удалось внести правку в профиль. " +
                    "Возможно, профиль под названием \"" + TextBox_Profiles.Text + "\" уже существует";
                ErrorMes.Visible = true;
            }
        }

        protected void Button_Delete_Profile_Click(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                int id = Convert.ToInt32(DropDownList_Profiles.SelectedValue);
                task.DeleteProfile_WordBook(id);
                Message_Label.InnerHtml = "Профиль " + Disciplines_DropDownList.SelectedItem.Text +
                    " успешно удален";
                DropDownList_Profiles.DataBind();
                Message_Label.Visible = true;
            }
            catch
            {
                ErrorMes.Text = "Произошел сбой в процессе удаления";
                ErrorMes.Visible = true;
            }
        }

        protected void DropDownList_Profiles_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                Profile profile = new Profile();
                int id = Convert.ToInt32(DropDownList_Profiles.SelectedItem.Value);
                profile = task.GetProfile_WordBook(id);
                TextBox_Profiles.Text = profile.Name;
                TextBox_ProfilePrograms.Text = "";
                Session["ProgramsToProfile"] = profile.ProgramsIDes;

                for (int i = 0; i < DropDown_ProfilePrograms.Items.Count; i++)
                    if (profile.ProgramsIDes.Contains(Convert.ToInt32(DropDown_ProfilePrograms.Items[i].Value)))
                        TextBox_ProfilePrograms.Text += DropDown_ProfilePrograms.Items[i].Text + "\n";

            }
            catch
            {
                ErrorMes.Text = "Ошибка";
                ErrorMes.Visible = true;
            }
        }
        
        protected void Button_CreateProfileProgram_Click(object sender, EventArgs e)
        {
            try
            {
                List<int> IDes = (List<int>)Session["ProgramsToProfile"];
                int id = Convert.ToInt32(DropDown_ProfilePrograms.SelectedItem.Value);
                string text = DropDown_ProfilePrograms.SelectedItem.Text + "\n";
                IDes.Add(id);
                TextBox_ProfilePrograms.Text += text;
            }
            catch
            {
                ErrorMes.Text = "Ошибка";
                ErrorMes.Visible = true;
            }
        }

        protected void Button_DeleteProfileProgram_Click(object sender, EventArgs e)
        {
            try
            {
                List<int> IDes = (List<int>)Session["ProgramsToProfile"];
                int id = Convert.ToInt32(DropDown_ProfilePrograms.SelectedItem.Value);
                string text = DropDown_ProfilePrograms.SelectedItem.Text + "\n";
                IDes.Remove(id);
                TextBox_ProfilePrograms.Text = TextBox_ProfilePrograms.Text.Replace(text, "");
            }
            catch
            {
                ErrorMes.Text = "Ошибка";
                ErrorMes.Visible = true;
            }
        }
        #endregion

        #region Кафедры
        protected void Button_Create_Click(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                Department dep = new Department();
                dep.Name = TextBox_Department.Text;
                dep.PersonalIDes = (List<string>)Session["PersonalToDepartment"];
                task.CreateDepartment_WordBook(dep);
                DropDownList_Departments.DataBind();
                Message_Label.InnerHtml = "Кафедра успешно внесена в систему";
                Message_Label.Visible = true;
            }
            catch
            {
                ErrorMes.Text = "Не удалось внести кафедру в систему. " +
                    "Возможно, кафедра под названием \"" + TextBox_Department.Text + "\" уже существует";
                ErrorMes.Visible = true;
            }
        }

        protected void Button_Update_Click(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                Department dep = new Department();
                dep.Name = TextBox_Department.Text;
                dep.Id = Convert.ToInt32(DropDownList_Departments.SelectedValue);
                dep.PersonalIDes = (List<string>)Session["PersonalToDepartment"];
                task.UpdateDepartment_WordBook(dep);
                DropDownList_Departments.DataBind();
                Message_Label.InnerHtml = "Кафедра успешно обновлена";
                Message_Label.Visible = true;
            }
            catch
            {
                ErrorMes.Text = "Не удалось внести правку в кафедру. " +
                    "Возможно, кафедра под названием \"" + TextBox_Department.Text + "\" уже существует";
                ErrorMes.Visible = true;
            }
        }

        protected void Button_Delete_Click(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                int id = Convert.ToInt32(DropDownList_Departments.SelectedValue);
                task.DeleteDepartment_WordBook(id);
                Message_Label.InnerHtml = "Кафедра " + DropDownList_Departments.SelectedItem.Text +
                    " успешно удалена";
                DropDownList_Departments.DataBind();
                Message_Label.Visible = true;
            }
            catch
            {
                ErrorMes.Text = "Произошел сбой в процессе удаления";
                ErrorMes.Visible = true;
            }
        }

        protected void DropDownList_Departments_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                Department dep = new Department();
                int id = Convert.ToInt32(DropDownList_Departments.SelectedItem.Value);
                dep = task.GetDepartment_WordBook(id);
                TextBox_Department.Text = dep.Name;
                TextBox_DepartmentPersonal.Text = "";
                Session["PersonalToDepartment"] = dep.PersonalIDes;

                for (int i = 0; i < DropDown_DepartmentPersonal.Items.Count; i++)
                    if (dep.PersonalIDes.Contains(DropDown_DepartmentPersonal.Items[i].Value.ToString()))
                        TextBox_DepartmentPersonal.Text += DropDown_DepartmentPersonal.Items[i].Text + "\n";

            }
            catch
            {
                ErrorMes.Text = "Ошибка";
                ErrorMes.Visible = true;
            }
        }

        protected void Button_CreateDepartmentPersonal_Click(object sender, EventArgs e)
        {
            try
            {
                List<string> IDes = (List<string>)Session["PersonalToDepartment"];
                string id = DropDown_DepartmentPersonal.SelectedItem.Value.ToString();
                string text = DropDown_DepartmentPersonal.SelectedItem.Text + "\n";
                if (IDes.Contains(id))
                {
                    ErrorMes.Text = "Этот пользователь уже есть в списке кафедры";
                    ErrorMes.Visible = true;
                }
                else
                {
                    IDes.Add(id);
                    TextBox_DepartmentPersonal.Text += text;
                    Message_Label.InnerHtml = "Пользователь " + DropDown_DepartmentPersonal.SelectedItem.Text + 
                        " успешно внесен в список кафедры " + DropDownList_Departments.SelectedItem.Text;
                    Message_Label.Visible = true;
                }
            }
            catch
            {
                ErrorMes.Text = "Ошибка";
                ErrorMes.Visible = true;
            }
        }

        protected void Button_DeleteDepartmentPersonal_Click(object sender, EventArgs e)
        {
            try
            {
                List<string> IDes = (List<string>)Session["PersonalToDepartment"];
                string id = DropDown_DepartmentPersonal.SelectedItem.Value.ToString();
                string text = DropDown_DepartmentPersonal.SelectedItem.Text + "\n";
                IDes.Remove(id);
                TextBox_DepartmentPersonal.Text = TextBox_DepartmentPersonal.Text.Replace(text, "");
                Message_Label.InnerHtml = "Пользователь " + DropDown_DepartmentPersonal.SelectedItem.Text +
                    " успешно удален из списка кафедры " + DropDownList_Departments.SelectedItem.Text;
                Message_Label.Visible = true;
            }
            catch
            {
                ErrorMes.Text = "Ошибка";
                ErrorMes.Visible = true;
            }
        }
        #endregion

        #region Факультеты
        protected void Button_CreateInstitute_Click(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                Institute inst = new Institute();
                inst.Name = TextBox_Institute.Text;
                inst.DepartmentsIDes = (List<int>)Session["DepartmentsToInstitute"];
                task.CreateInstitute_WordBook(inst);
                DropDown_Institutes.DataBind();
                Message_Label.InnerHtml = "Факультет успешно внесен в систему";
                Message_Label.Visible = true;
            }
            catch
            {
                ErrorMes.Text = "Не удалось внести факультет в систему. " +
                    "Возможно, факультет под названием \"" + TextBox_Institute.Text + "\" уже существует";
                ErrorMes.Visible = true;
            }
        }

        protected void Button_UpdateInstitute_Click(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                Institute inst = new Institute();
                inst.Name = TextBox_Institute.Text;
                inst.Id = Convert.ToInt32(DropDown_Institutes.SelectedValue);
                inst.DepartmentsIDes = (List<int>)Session["DepartmentsToInstitute"];
                task.UpdateInstitute_WordBook(inst);
                DropDown_Institutes.DataBind();
                Message_Label.InnerHtml = "Факультет успешно обновлен";
                Message_Label.Visible = true;
            }
            catch
            {
                ErrorMes.Text = "Не удалось внести правку в факультет. " +
                    "Возможно, факультет под названием \"" + TextBox_Institute.Text + "\" уже существует";
                ErrorMes.Visible = true;
            }
        }

        protected void Button_DeleteInstitute_Click(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                int id = Convert.ToInt32(DropDown_Institutes.SelectedValue);
                task.DeleteInstitute_WordBook(id);
                Message_Label.InnerHtml = "Факультет " + DropDown_Institutes.SelectedItem.Text +
                    " успешно удален";
                DropDown_Institutes.DataBind();
                Message_Label.Visible = true;
            }
            catch
            {
                ErrorMes.Text = "Произошел сбой в процессе удаления";
                ErrorMes.Visible = true;
            }
        }

        protected void DropDown_Institutes_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                Institute inst = new Institute();
                int id = Convert.ToInt32(DropDown_Institutes.SelectedItem.Value);
                inst = task.GetInstitute_WordBook(id);
                TextBox_Institute.Text = inst.Name;
                TextBox_InstituteDepartments.Text = "";
                Session["DepartmentsToInstitute"] = inst.DepartmentsIDes;

                for (int i = 0; i < DropDown_InstituteDepartments.Items.Count; i++)
                    if (inst.DepartmentsIDes.Contains(Convert.ToInt32(DropDown_InstituteDepartments.Items[i].Value)))
                        TextBox_InstituteDepartments.Text += DropDown_InstituteDepartments.Items[i].Text + "\n";

            }
            catch
            {
                ErrorMes.Text = "Ошибка";
                ErrorMes.Visible = true;
            }
        }
        
        protected void Button_CreateInstituteDepartment_Click(object sender, EventArgs e)
        {
            try
            {
                List<int> IDes = (List<int>)Session["DepartmentsToInstitute"];
                int id = Convert.ToInt32(DropDown_InstituteDepartments.SelectedItem.Value);
                string text = DropDown_InstituteDepartments.SelectedItem.Text + "\n";
                IDes.Add(id);
                TextBox_InstituteDepartments.Text += text;
            }
            catch
            {
                ErrorMes.Text = "Ошибка";
                ErrorMes.Visible = true;
            }
        }

        protected void Button_DeleteInstituteDepartment_Click(object sender, EventArgs e)
        {
            try
            {
                List<int> IDes = (List<int>)Session["DepartmentsToInstitute"];
                int id = Convert.ToInt32(DropDown_InstituteDepartments.SelectedItem.Value);
                string text = DropDown_InstituteDepartments.SelectedItem.Text + "\n";
                IDes.Remove(id);
                TextBox_InstituteDepartments.Text = TextBox_InstituteDepartments.Text.Replace(text, "");
            }
            catch
            {
                ErrorMes.Text = "Ошибка";
                ErrorMes.Visible = true;
            }
        }
        #endregion

        #region Учебные планы
        protected void Button_DeleteTrainingPlan_Click(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                int id = Convert.ToInt32(DropDown_TrainingPlans.SelectedValue);
                task.DeleteTrainingPlan_WordBook(id);
                Message_Label.InnerHtml = "Учебный план " + DropDown_TrainingPlans.SelectedItem.Text +
                    " успешно удален";
                DropDown_TrainingPlans.DataBind();
                Message_Label.Visible = true;
            }
            catch
            {
                ErrorMes.Text = "Произошел сбой в процессе удаления";
                ErrorMes.Visible = true;
            }
        }

        protected void Button_UpdateTrainingPlan_Click(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                TrainingPlan plan = new TrainingPlan();
                plan.Name = TextBox_PlanName.Text;
                plan.Year = DropDown_TrainingPlansYears.SelectedItem.Text;
                plan.Id = Convert.ToInt32(DropDown_TrainingPlans.SelectedValue);
                plan.DirectionIDes = (List<int>)Session["DirectionsToTrainingPlanIDes"];
                task.UpdateTrainingPlan_WordBook(plan);
                DropDown_TrainingPlans.DataBind();
                Message_Label.InnerHtml = "Учебный план успешно обновлен";
                Message_Label.Visible = true;
            }
            catch
            {
                ErrorMes.Text = "Не удалось внести правку в учебный план. ";
                ErrorMes.Visible = true;
            }
        }

        protected void Button_CreateTrainingPlan_Click(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                TrainingPlan plan = new TrainingPlan();
                plan.Name = TextBox_PlanName.Text;
                plan.Year = DropDown_TrainingPlansYears.SelectedItem.Text;
                plan.DirectionIDes = (List<int>)Session["DirectionsToTrainingPlanIDes"];
                task.CreateTrainingPlan_WordBook(plan);
                DropDown_TrainingPlans.DataBind();
                Message_Label.InnerHtml = "Учебный план успешно внесен в систему";
                Message_Label.Visible = true;
            }
            catch
            {
                ErrorMes.Text = "Не удалось внести учебный план в систему. ";
                ErrorMes.Visible = true;
            }
        }

        protected void DropDown_TrainingPlans_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                TrainingPlan plan = new TrainingPlan();
                int id = Convert.ToInt32(DropDown_TrainingPlans.SelectedItem.Value);
                plan = task.GetTrainingPlan(id);
                TextBox_PlanName.Text = plan.Name;
                DropDown_TrainingPlansYears.Text = plan.Year;
                TextBox_TrainingPlanDirections.Text = "";
                Session["DirectionsToTrainingPlanIDes"] = plan.DirectionIDes;

                for (int i = 0; i < DropDown_TrainingPlanDirections.Items.Count; i++)
                    if (plan.DirectionIDes.Contains(Convert.ToInt32(DropDown_TrainingPlanDirections.Items[i].Value)))
                        TextBox_TrainingPlanDirections.Text += DropDown_TrainingPlanDirections.Items[i].Text + "\n";

            }
            catch
            {
                ErrorMes.Text = "Ошибка";
                ErrorMes.Visible = true;
            }
        }
        
        protected void Button_AddDirectionInTrainingPlan_Click(object sender, EventArgs e)
        {
            try
            {
                List<int> IDes = (List<int>)Session["DirectionsToTrainingPlanIDes"];
                int id = Convert.ToInt32(DropDown_TrainingPlanDirections.SelectedItem.Value);
                string text = DropDown_TrainingPlanDirections.SelectedItem.Text + "\n";
                IDes.Add(id);
                TextBox_TrainingPlanDirections.Text += text;
            }
            catch
            {
                ErrorMes.Text = "Ошибка";
                ErrorMes.Visible = true;
            }
        }

        protected void Button_DeleteDirectionInTrainingPlan_Click(object sender, EventArgs e)
        {
            try
            {
                List<int> IDes = (List<int>)Session["DirectionsToTrainingPlanIDes"];
                int id = Convert.ToInt32(DropDown_TrainingPlanDirections.SelectedItem.Value);
                string text = DropDown_TrainingPlanDirections.SelectedItem.Text + "\n";
                IDes.Remove(id);
                TextBox_TrainingPlanDirections.Text = TextBox_TrainingPlanDirections.Text.Replace(text, "");
            }
            catch
            {
                ErrorMes.Text = "Ошибка";
                ErrorMes.Visible = true;
            }
        }

        #endregion

        #region ФГОСы
        protected void DropDown_FGOS_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                FGOS fgos = new FGOS();
                int id = Convert.ToInt32(DropDown_FGOS.SelectedItem.Value);
                fgos = task.GetFGOS(id);
                TextBox_FGOSName.Text = fgos.Name;
                TextBox_FGOS.Text = fgos.Designation;
                TextBox_FGOSTRainingPlans.Text = "";
                Session["PlansToFGOS"] = fgos.TrainingPlansIDes;

                for (int i = 0; i < DropDown_FGOSTrainingPlans.Items.Count; i++)
                    if (fgos.TrainingPlansIDes.Contains(Convert.ToInt32(DropDown_FGOSTrainingPlans.Items[i].Value)))
                        TextBox_FGOSTRainingPlans.Text += DropDown_FGOSTrainingPlans.Items[i].Text + "\n";

            }
            catch
            {
                ErrorMes.Text = "Ошибка";
                ErrorMes.Visible = true;
            }

        }

        protected void Button_CreateTrainingPlanFGOS_Click(object sender, EventArgs e)
        {
            try
            {
                List<int> IDes = (List<int>)Session["PlansToFGOS"];
                int id = Convert.ToInt32(DropDown_FGOSTrainingPlans.SelectedItem.Value);
                string text = DropDown_FGOSTrainingPlans.SelectedItem.Text + "\n";
                IDes.Add(id);
                TextBox_FGOSTRainingPlans.Text += text;
            }
            catch
            {
                ErrorMes.Text = "Ошибка";
                ErrorMes.Visible = true;
            }
        }

        protected void Button_DeleteTrainingPlanFGOS_Click(object sender, EventArgs e)
        {
            try
            {
                List<int> IDes = (List<int>)Session["PlansToFGOS"];
                int id = Convert.ToInt32(DropDown_FGOSTrainingPlans.SelectedItem.Value);
                string text = DropDown_FGOSTrainingPlans.SelectedItem.Text + "\n";
                IDes.Remove(id);
                TextBox_FGOSTRainingPlans.Text = TextBox_FGOSTRainingPlans.Text.Replace(text, "");
            }
            catch
            {
                ErrorMes.Text = "Ошибка";
                ErrorMes.Visible = true;
            }
            
        }

        protected void Button_DeleteFGOS_Click(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                int id = Convert.ToInt32(DropDown_FGOS.SelectedValue);
                task.DeleteFGOS_WordBook(id);
                Message_Label.InnerHtml = "Стандарт № " + DropDown_FGOS.SelectedItem.Value.ToString() + " успешно удален";
                DropDown_FGOS.DataBind();
                Message_Label.Visible = true;
            }
            catch
            {
                ErrorMes.Text = "Произошел сбой в процессе удаления";
                ErrorMes.Visible = true;
            }
        }

        protected void Button_CreateFGOS_Click(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                FGOS fgos = new FGOS();
                fgos.Name = TextBox_FGOSName.Text;
                fgos.Designation = TextBox_FGOS.Text;
                fgos.TrainingPlansIDes = (List<int>)Session["PlansToFGOS"];
                task.CreateFGOS_WordBook(fgos);
                DropDown_FGOS.DataBind();
                Message_Label.InnerHtml = "Стандарт успешно внесен в систему";
                Message_Label.Visible = true;
            }
            catch
            {
                ErrorMes.Text = "Не удалось внести стандарт в систему.";
                ErrorMes.Visible = true;
            }
        }

        protected void Button_UpdateFGOS_Click(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                FGOS fgos = new FGOS();
                fgos.Name = TextBox_FGOSName.Text;
                fgos.Designation = TextBox_FGOS.Text;
                fgos.Id = Convert.ToInt32(DropDown_FGOS.SelectedValue);
                fgos.TrainingPlansIDes = (List<int>)Session["PlansToFGOS"];
                task.UpdateFGOS_WordBook(fgos);
                DropDown_FGOS.DataBind();
                Message_Label.InnerHtml = "Стандарт успешно обновлен";
                Message_Label.Visible = true;
            }
            catch
            {
                ErrorMes.Text = "Не удалось внести правку в стандарт. ";
                ErrorMes.Visible = true;
            }
        }
        #endregion
    }
}