﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.IO;
using OfficeOpenXml;
namespace main_test
{
    public class Excel
    {
        //метод возвращает таблицу данных DataTable на основе введенного excel-файла
        public DataTable GetDataTable(byte[] file)
        {
            DataTable data = new DataTable();
            using (MemoryStream stream = new MemoryStream(file))
            using (ExcelPackage excelPackage = new ExcelPackage(stream))
            {
                using (ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets[1])
                {
                    for (int j = worksheet.Dimension.Start.Column; j <= worksheet.Dimension.End.Column; j++)
                        data.Columns.Add(j.ToString(), typeof(string));

                    for (int i = worksheet.Dimension.Start.Row; i <= worksheet.Dimension.End.Row; i++)
                    {
                        DataRow row = data.NewRow();
                        for (int j = worksheet.Dimension.Start.Column; j <= worksheet.Dimension.End.Column; j++)
                            if (worksheet.Cells[i, j].Value != null)
                                row[j - 1] = worksheet.Cells[i, j].Value.ToString();
                        data.Rows.Add(row);
                    }
                }
            }
            return data;
        }

        //данный метод формирует эксель документ по заданному пути path на основе данных из dt
        public void Create(DataTable dt, string path)
        {
            var file = new FileInfo(path);

            using (var package = new ExcelPackage(file))
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Лист №1");
                for (int i = 0; i < dt.Columns.Count; i++)
                    worksheet.Cells[1, i + 1].Value = dt.Columns[i].ColumnName;

                for (int i = 0; i < dt.Rows.Count; i++)
                    for (int j = 0; j < dt.Columns.Count; j++)
                        worksheet.Cells[i + 2, j + 1].Value = dt.Rows[i][j];

                for (int i = 0; i < dt.Columns.Count; i++)
                    worksheet.Column(i + 1).AutoFit();

                package.Save();
            }
        }

        //данный метод заполняет DataTable данными из входного файла
        public DataTable ReadClocks(byte[] file)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add();
            dt.Columns.Add("Раздел", typeof(string));
            dt.Columns.Add("Семестр", typeof(string));
            dt.Columns.Add("Неделя", typeof(string));
            dt.Columns.Add("Л", typeof(string));
            dt.Columns.Add("ПС", typeof(string));
            dt.Columns.Add("Лаб", typeof(string));
            dt.Columns.Add("СРС", typeof(string));
            dt.Columns.Add("КСР", typeof(string));
            dt.Columns.Add("КР", typeof(string));
            dt.Columns.Add("КП", typeof(string));
            dt.Columns.Add("РГР", typeof(string));
            dt.Columns.Add("Под. к л/р", typeof(string));
            dt.Columns.Add("К/р", typeof(string));
            dt.Columns.Add("Форма аттестации", typeof(string));
            
            using (MemoryStream stream = new MemoryStream(file))
            using (ExcelPackage excelPackage = new ExcelPackage(stream))
            {
                foreach (ExcelWorksheet worksheet in excelPackage.Workbook.Worksheets)
                {
                    for (int i = worksheet.Dimension.Start.Row; i <= worksheet.Dimension.End.Row; i++)
                    {
                        DataRow row = dt.NewRow();
                        for (int j = worksheet.Dimension.Start.Column; j <= worksheet.Dimension.End.Column; j++)
                        {
                            if (j < 15)
                                row[j] = worksheet.Cells[i, j].Value.ToString();
                        }
                        dt.Rows.Add(row);
                    }
                }
            }
            return dt;
        }
    }    
}