﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Lockout.aspx.cs" Inherits="main_test.Account.Lockout" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <hgroup>
        <h1 class="text-success">Заявка на регистрацию успешно подана</h1>
        <h2 class="jumbotron">В данный момент учетная запись заблокирована, дождитесь подтверждения администратора</h2>
    </hgroup>
</asp:Content>
