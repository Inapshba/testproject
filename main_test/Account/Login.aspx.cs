﻿using System;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using main_test.Models;

namespace main_test.Account
{
    public partial class Login : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void LogIn(object sender, EventArgs e)
        {
            if (IsValid)
            {
                // Проверка пароля пользователя
                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var signinManager = Context.GetOwinContext().GetUserManager<ApplicationSignInManager>();

                // Сбои при входе не приводят к блокированию учетной записи
                // Чтобы ошибки при вводе пароля инициировали блокирование, замените на shouldLockout: true
                var mail = manager.FindByEmail(Email.Text).ToString();

                //var result = signinManager.PasswordSignIn(mail, Password.Text, RememberMe.Checked, shouldLockout: true);

                var rusult = manager.FindByEmail(Email.Text);
                if (rusult.EmailConfirmed == false)
                {
                    Response.Redirect("/Account/LockoutEmail");
                    HttpContext.Current.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                }
                else
                    if (rusult.LockoutEnabled)
                {
                    Response.Redirect("/Account/Lockout");
                    HttpContext.Current.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                }
                else
                       if (manager.CheckPassword(rusult, Password.Text))
                {
                    signinManager.SignIn(rusult, false, true);
                    IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
                }

            }
            FailureText.Text = "Неверно введен e-mail/пароль";
            ErrorMessage.Visible = true;
        }




        //switch (result)
        //{
        //    case SignInStatus.Success:
        //        IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
        //        break;
        //    case SignInStatus.LockedOut:
        //        Response.Redirect("/Account/Lockout");
        //        break;
        //    case SignInStatus.RequiresVerification:
        //        Response.Redirect(String.Format("/Account/TwoFactorAuthenticationSignIn?ReturnUrl={0}&RememberMe={1}",
        //                                        Request.QueryString["ReturnUrl"],
        //                                        RememberMe.Checked),
        //                          true);
        //        break;
        //    case SignInStatus.Failure:
        //    default:
        //        FailureText.Text = "Неудачная попытка входа";
        //        ErrorMessage.Visible = true;
        //        break;
    }
            }
        //}
 //  }
//}