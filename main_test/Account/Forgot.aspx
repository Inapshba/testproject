﻿<%@ Page Title="- Пароль забыт" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Forgot.aspx.cs" Inherits="main_test.Account.ForgotPassword" Async="true" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script src="../Scripts/reCAPTCHA.js"></script>
    <script>
        function onSuccess() {
            var btn = document.getElementById('<%=button_Registr.ClientID %>');
            btn.removeAttribute('disabled');
            btn.className = 'btn dark';
        }
    </script>
    <link rel="stylesheet" href="../Content/Colored.css" />
    <h2>Забыли пароль?</h2>

    <div class="row">
        <div class="col-md-8">
            <asp:PlaceHolder id="loginForm" runat="server">
                <div class="form-horizontal">
                    <h4>Восстановите его тут</h4>
                    <hr />
                    <asp:PlaceHolder runat="server" ID="ErrorMessage" Visible="false">
                        <p class="text-danger">
                            <asp:Literal runat="server" ID="FailureText" />
                        </p>
                    </asp:PlaceHolder>
                    <div class="form-group">
                        <asp:Label runat="server" AssociatedControlID="Email" CssClass="col-md-2 control-label">Логин:</asp:Label>
                        <div class="col-md-10">
                            <asp:TextBox runat="server" ID="Email" CssClass="form-control" TextMode="Email" />
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                                CssClass="text-danger" ErrorMessage="Поле логина заполнять обязательно." />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <div class="g-recaptcha" data-theme="dark" data-callback="onSuccess" data-sitekey="6Ldmq7QUAAAAAKB3tx9gyhMOf9dy-okzBYqOF3o4"></div>
                            <asp:Button runat="server" ID="button_Registr" disabled="true" style="margin-top:5px" OnClick="Forgot" Text="Отправить ссылку на почту" CssClass="btn light" OnClientClick="captchaValid()" Width="302" />
                        </div>
                    </div>
                </div>
            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" ID="DisplayEmail" Visible="false">
                <p class="text-info">
                    Проверьте электронную почту, чтобы сбросить пароль.
                </p>
            </asp:PlaceHolder>
        </div>
    </div>
</asp:Content>
