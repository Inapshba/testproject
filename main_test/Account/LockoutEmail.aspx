﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="LockoutEmail.aspx.cs" Inherits="main_test.Account.LockoutEmail" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script src="../Scripts/reCAPTCHA.js"></script>
    <script>
        function onSuccess() {
            var btn = document.getElementById('<%=GoResend_Button.ClientID %>');
            btn.removeAttribute('disabled');
            btn.className = 'btn dark';
        }
    </script>
    <link rel="stylesheet" href="../Content/Colored.css" />
    <link rel="stylesheet" href="../Content/WorkStyle.css" />
    <style type="text/css">
        .darked {
            background: rgba(102, 102, 102, 0.5);
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
            display: none;
        }
            .darked:target {
                display: block;
            }
        .open_window {
        width: 350px;
        height: 100px;
        padding: 15px;
        border: 2px solid;
        border-color: rgba(0,0,0,.075);
        border-radius: 5px;
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        margin: auto;
        background: #fff;
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(0, 0, 0, .6);
                 box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(0, 0, 0, .6);
        }
    </style>
    
    <hgroup>
        <h1 class="text-success">Заявка на регистрацию успешно подана</h1>
        <h2 class="jumbotron">В данный момент учетная запись заблокирована, для доступа к сайту подтвердите адрес электронной почты</h2>
    </hgroup>
    <p runat="server" id="Mess">Если письмо с подтверждением не пришло на указанный адрес, мы можем отправить вам еще одно. Для этого пройдите проверку "Я не робот", введите ваш email и нажмите на кнопку "Отправить повторно"</p>
    <div class="g-recaptcha" data-theme="dark" data-size="compact" data-callback="onSuccess" data-sitekey="6Ldmq7QUAAAAAKB3tx9gyhMOf9dy-okzBYqOF3o4"></div>
    <asp:TextBox runat="server" CssClass="form-control" ID="Email_TextBox" placeholder="Введите email..." TextMode="Email" />
    <asp:Button runat="server" disabled="true" ID="GoResend_Button" Text="Отправить повторно" style="margin-bottom:7px; margin-top:5px; width:196px" CssClass="btn light" OnClick="GoResend_Button_Click" OnClientClick="captchaValid()"/>
</asp:Content>
