﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System.Net.Mail;
using System.Net;

namespace main_test.Account
{
    public partial class LockoutEmail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void GoResend_Button_Click(object sender, EventArgs e)
        {
            try
            {
                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var result = manager.FindByEmail(Email_TextBox.Text);

                if (result.EmailConfirmed == true)
                {
                    Response.Write("<script>alert('Введенный адрес уже подтвержден')</script>");
                    Email_TextBox.Text = "";
                    goto end;
                }

                string code = manager.GenerateEmailConfirmationToken(result.Id);
                string callbackUrl = IdentityHelper.GetUserConfirmationRedirectUrl(code, result.Id, Request);
                MailMessage mail = new MailMessage();
                mail.IsBodyHtml = true;
                mail.Subject = "Подтверждение адреса электронной почты";
                mail.Body = "Здравствуйте! Вы запросили повторную верификацию вашей учетной записи на сайте <a href=\"https://mpuprograms.ru\">mpuprograms.ru</a> <br/>" +
                    "Для подтверждения вашего email перейдите по <a href=\"" + callbackUrl + "\">ссылке</a>." +
                    "<br/><br/>--<br/>" +
                    "Техподдержка \"мпу-програмс\" – <a href=\"mailto:support@mpuprograms.ru\">support@mpuprograms.ru</a>";
                mail.From = new MailAddress("support@mpuprograms.ru");
                mail.To.Add(result.Email);

                SmtpClient client = new SmtpClient("smtp.masterhost.ru");
                client.Credentials = new NetworkCredential("support@mpuprograms.ru", "sPB-zea-kS5-jHn");
                client.Send(mail);
                Mess.InnerHtml = "Мы отправили еще одно письмо на адрес " + result.Email;
                Email_TextBox.Text = "";
            }
            catch { Response.Write("<script>alert('Ошибка, проверьте введенный адрес')</script>"); }
        end: { }
        }
    }
}