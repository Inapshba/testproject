﻿using System;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using main_test.Models;
using System.Net.Mail;
using System.Net;

namespace main_test.Account
{
    public partial class ForgotPassword : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void Forgot(object sender, EventArgs e)
        {
            if (IsValid)
            {
                // Проверка электронного адреса пользователя
                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                ApplicationUser user = manager.FindByEmail(Email.Text);
                if (user == null || !manager.IsEmailConfirmed(user.Id))
                {
                    FailureText.Text = "Пользователь не существует или не подтвержден.";
                    ErrorMessage.Visible = true;
                    return;
                }

                // Дополнительные сведения о включении подтверждения учетной записи и сброса пароля см. на странице https://go.microsoft.com/fwlink/?LinkID=320771.
                // Отправка сообщения электронной почты с кодом и перенаправление на страницу сброса пароля

                string code = manager.GeneratePasswordResetToken(user.Id);
                string callbackUrl = IdentityHelper.GetResetPasswordRedirectUrl(code, Request);

                //отправка письма с оповещением об подтверждение учетной записи администрацией сайта и выдачей соответствующей роли 
                MailMessage mail = new MailMessage();
                mail.IsBodyHtml = true;
                mail.Subject = "Сброс пароля";
                string body = "Здравствуйте! Мы подготовили форму для сброса вашего пароля на сайте <a href=\"https://mpuprograms.ru\">mpuprograms.ru</a>. Для сброса перейдите по <a href=\"" + callbackUrl + "\">ссылке</a> " +
                    "<br/><br/>--<br/>" +
                    "Техподдержка \"мпу-програмс\" – <a href=\"mailto:support@mpuprograms.ru\">support@mpuprograms.ru</a>";
                mail.Body = body;
                mail.From = new MailAddress("support@mpuprograms.ru");
                mail.To.Add(user.Email);

                try
                {
                    SmtpClient client = new SmtpClient("smtp.masterhost.ru");
                    client.Credentials = new NetworkCredential("support@mpuprograms.ru", "sPB-zea-kS5-jHn");
                    client.Send(mail);
                }
                catch { }

                loginForm.Visible = false;
                DisplayEmail.Visible = true;
            }
        }
    }
}