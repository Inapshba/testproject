﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using main_test.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using main_test;
using System.Web.Security;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;


namespace main_test.Account
{ 
    public partial class Register : Page
    {
        protected void CreateUser_Click(object sender, EventArgs e)
        {
            //var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>());
            //var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>());
            //var role = new IdentityRole { Name = "User" };

            var roleStore = new RoleStore<IdentityRole>();
            var roleMgr = new RoleManager<IdentityRole>(roleStore);
            //IdentityResult IdRoleResult = roleMgr.Create(new IdentityRole { Name = "Test" });
            var userStore = new UserStore<IdentityUser>();
            var manager2 = new UserManager<IdentityUser>(userStore);
            

            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var signInManager = Context.GetOwinContext().Get<ApplicationSignInManager>();
            DateTime date1 = new DateTime(3000, 7, 20, 18, 30, 25);
            var user = new ApplicationUser() { UserName = Username.Text, Email = Email.Text , LockoutEndDateUtc = date1};
          

            IdentityResult result = manager.Create(user, Password.Text);
            
            //var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>());

            if (result.Succeeded)
            {
                int who;
                who = Convert.ToInt32(Request.QueryString["Admin"]);

                //отправка письма с ссылкой на подтверждение email
                string code = manager.GenerateEmailConfirmationToken(user.Id);
                string callbackUrl = IdentityHelper.GetUserConfirmationRedirectUrl(code, user.Id, Request);
                MailMessage mail = new MailMessage();
                mail.IsBodyHtml = true;
                mail.Subject = "Подтверждение адреса электронной почты";
                mail.Body = "Здравствуйте! Вы оставили заявку на регистрацию на сайте <a href=\"https://mpuprograms.ru\">mpuprograms.ru</a> <br/>" +
                    "Для подтверждения вашего email перейдите по <a href=\"" + callbackUrl + "\">ссылке</a>. " +
                    "<br/><br/>--<br/>" +
                    "Техподдержка \"мпу-програмс\" – <a href=\"mailto:support@mpuprograms.ru\">support@mpuprograms.ru</a>";
                mail.From = new MailAddress("support@mpuprograms.ru");
                mail.To.Add(user.Email);

                SmtpClient client = new SmtpClient("smtp.masterhost.ru");
                client.Credentials = new NetworkCredential("support@mpuprograms.ru", "sPB-zea-kS5-jHn");
                client.Send(mail);

                if (who != 1)
                {
                    signInManager.SignIn(user, isPersistent: false, rememberBrowser: false);
                    IdentityResult IdUserResult = manager.AddToRole(user.Id, "Guest");
                    //userManager.AddToRole(user.UserName, role.Name);
                    //var roleResult = userManager.AddToRole(user.Id, "User");
                    //IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
                    HttpContext.Current.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                    Response.Redirect("LockoutEmail.aspx");
                }
                else
                {
                    reg_form_title.InnerHtml = "Пользователь " + user.UserName + " ожидает подтверждения";
                }
            }
            else 
            {
                reg_form_title.InnerHtml = "Создание новой учетной записи";
                ErrorMessage.Text = result.Errors.FirstOrDefault();
            }
        }
    }
}