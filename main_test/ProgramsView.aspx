﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Title="- Просмотр программ" AutoEventWireup="true" CodeBehind="ProgramsView.aspx.cs" Inherits="main_test.ProgramsView" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent" ID="ProgramsViewRedactorContent">
    <link rel="stylesheet" href="Content/ContentFade.css" />
    <link rel="stylesheet" href="Content/CustomCheckBox.css" />
    <link rel="stylesheet" href="Content/Colored.css" />
    <script src="Scripts/ScrollTop.js"></script>
    <link rel="stylesheet" href="Content/WorkStyle.css" />
    
    <div class="body_fade">            
        <h2>Поиск программы</h2>
        <asp:Label runat="server" ID="SelectErrorMes" ForeColor="Red" Visible="false" Text="Выберите программу для редактирования" />
        <asp:Label runat="server" ID="ConnectErrorMes" Text="Подключение к БД не выполнено" ForeColor="Red" Visible="false" />
        <asp:Label runat="server" ID="ErrorMes" ForeColor="Red" Visible="false" />
        <div>
            <asp:TextBox runat="server" ID="TextBox_Search" CssClass="form-control" placeholder="Поиск РПД..." />
            <asp:Button runat="server" style="margin-bottom: 2px" CssClass="btn light" ID="Button_Search" Text="Найти" OnClick="Button_Search_Click" />
        </div>
        <div style="border: 2px solid #ddd; border-radius: 5px; height: 400px; overflow: auto;">
            <asp:RadioButtonList runat="server" ID="Programs_RadioBox" CssClass="CustomCheckBox" DataSourceID="SqlDataSourcePrograms" DataTextField="Viewer" DataValueField="id" />
            <asp:SqlDataSource ID="SqlDataSourcePrograms" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" ProviderName="System.Data.SqlClient" SelectCommand="SELECT [id], [Discipline], [Approve_Date], (Discipline+' ('+CONVERT(varchar,Approve_Date,105)+')') AS Viewer, [Status] FROM [Programs] WHERE Status='Утверждена'"></asp:SqlDataSource>
        </div>
        <asp:Button runat="server" ID="Button_NewTab" style="margin-top: 3px;" CssClass="btn dark" Text="Открыть в новой вкладке" OnClick="Button_NewTab_Click"  />
        <asp:Button runat="server" ID="Button_Report" style="margin-top: 3px;" CssClass="btn light" Text="Скачать" OnClick="Button_Report_Click" />
        <h3>Фильтры</h3>
        <div>
            <div class="row">
                <div class="col-md-2">
                    <strong>Год: </strong>
                    <asp:DropDownList runat="server" ID="FilterYear_List" CssClass="form-control" DataSourceID="SqlDataSourceYears" DataTextField="Year" DataValueField="Year" OnDataBound="FilterYear_List_DataBound" />
                    <asp:SqlDataSource ID="SqlDataSourceYears" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT DISTINCT YEAR(Approve_Date) as Year FROM [Programs]"></asp:SqlDataSource>
                </div>
                <div class="col-md-3">
                    <strong>Форма обучения: </strong>
                    <asp:DropDownList runat="server" ID="FilterTrainingForm_List" style="margin-top:3px" CssClass="form-control" DataSourceID="SqlDataSourceTraining_Forms" DataTextField="Name" DataValueField="id" OnDataBound="FilterTrainingForm_List_DataBound" />
                    <asp:SqlDataSource ID="SqlDataSourceTraining_Forms" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [Training_Forms]"></asp:SqlDataSource>
                </div>
            </div>
            <br />
            <strong>Профиль:</strong>
            <asp:DropDownList runat="server" CssClass="form-control" ID="FilterProfile_List" DataSourceID="SqlDataSourceProfiles" DataTextField="Name" DataValueField="Id" OnDataBound="FilterProfile_List_DataBound" /><br />
            <asp:SqlDataSource ID="SqlDataSourceProfiles" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [Profiles]"></asp:SqlDataSource>
            <br />
            <strong>Кафедра:</strong>
            <asp:DropDownList runat="server" CssClass="form-control" ID="FilterDepartment_List" DataSourceID="SqlDataSourceDepartments" DataTextField="Name" DataValueField="Id" OnDataBound="FilterDepartment_List_DataBound" /><br />
            <asp:SqlDataSource ID="SqlDataSourceDepartments" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [Departments]"></asp:SqlDataSource>
            <br />
            <strong>Дисциплина: </strong>
            <asp:DropDownList runat="server" ID="FilterDiscipline_List" CssClass="form-control" DataSourceID="SqlDataSourceDisciplines" DataTextField="Name" DataValueField="id" OnDataBound="FilterDiscipline_List_DataBound" /><br />
            <asp:SqlDataSource ID="SqlDataSourceDisciplines" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [Disciplines]"></asp:SqlDataSource>
            <br />

            <asp:Button runat="server" CssClass="btn dark" ID="FiltersUse_Button" Text="Применить фильтры" OnClick="FiltersUse_Button_Click" />
            <asp:Button runat="server" CssClass="btn light" ID="FiltersClear_Button" Text="Сбросить" OnClick="FiltersClear_Button_Click" />
        </div>
            
        <a href="#" class="scrollup">Наверх</a>
    </div>
</asp:Content>
