﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace main_test
{
    public partial class ProgramsView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ErrorMes.Visible = false;
        }

        //обработка списков
        #region ListTasks

        protected void FilterProfile_List_DataBound(object sender, EventArgs e)
        {
            ListItem item = new ListItem();
            item.Text = "Все профили";
            item.Value = "";
            FilterProfile_List.Items.Add(item);
            item.Selected = true;
        }

        protected void FilterDepartment_List_DataBound(object sender, EventArgs e)
        {
            ListItem item = new ListItem();
            item.Text = "Все кафедры";
            item.Value = "";
            FilterDepartment_List.Items.Add(item);
            item.Selected = true;
        }

        protected void FilterYear_List_DataBound(object sender, EventArgs e)
        {
            ListItem item = new ListItem();
            item.Text = "Все года";
            item.Value = "";
            FilterYear_List.Items.Add(item);
            item.Selected = true;
        }

        protected void FilterDiscipline_List_DataBound(object sender, EventArgs e)
        {
            ListItem item = new ListItem();
            item.Text = "Все дисциплины";
            item.Value = "";
            FilterDiscipline_List.Items.Add(item);
            item.Selected = true;
        }

        protected void FilterTrainingForm_List_DataBound(object sender, EventArgs e)
        {
            ListItem item = new ListItem();
            item.Text = "Все формы";
            item.Value = "";
            FilterTrainingForm_List.Items.Add(item);
            item.Selected = true;
        }

     

        #endregion ListTasks

        //установка фильтров
        #region FilterTasks

        private void Filter()
        {
            string commandstring = string.Format("SELECT [id], [Discipline], [Approve_Date], (Discipline+' ('+CONVERT(varchar,Approve_Date,105)+')') AS Viewer, Status FROM [Programs] WHERE Status='Утверждена'");
            SqlDataSourcePrograms.SelectCommand = commandstring;
            

            if (FilterYear_List.SelectedValue != "")
                commandstring += " AND YEAR(Approve_Date) = " + FilterYear_List.SelectedItem;

            if (FilterTrainingForm_List.SelectedValue != "")
                    commandstring += " AND Training_Form = '" + FilterTrainingForm_List.SelectedItem + "'";

            if (FilterProfile_List.SelectedValue != "")
                commandstring += " AND Profil = '" + FilterProfile_List.SelectedItem + "'";

            if (FilterDepartment_List.SelectedValue != "")
                commandstring += " AND Department = '" + FilterDepartment_List.SelectedItem + "'";

            if (FilterDiscipline_List.SelectedValue != "")
                    commandstring += " AND Discipline = '" + FilterDiscipline_List.SelectedItem + "'";
            

            SqlDataSourcePrograms.SelectCommand = commandstring;
        }

        protected void FiltersClear_Button_Click(object sender, EventArgs e)
        {
            FilterYear_List.SelectedValue = "";
            FilterDiscipline_List.SelectedValue = "";
            FilterTrainingForm_List.SelectedValue = "";
            FilterDepartment_List.SelectedValue = "";
            FilterProfile_List.SelectedValue = "";
            TextBox_Search.Text = "";
            Filter();
            Programs_RadioBox.DataBind();
        }

        #endregion FilterTasks

        protected void FiltersUse_Button_Click(object sender, EventArgs e)
        {
            Filter();
            Programs_RadioBox.DataBind();
        }

        //загрузка word-файла на устройство
        protected void Button_Report_Click(object sender, EventArgs e)
        {
            try
            {
                int programid = Convert.ToInt32(Programs_RadioBox.SelectedValue);
                string fileName = "Program_" + programid.ToString() + ".docx";
                string url = HttpContext.Current.Server.MapPath("~/Files/" + fileName);

                FileStream MyFileStream = new FileStream(url, FileMode.Open);
                int filesizeINT = 0;
                long fileSize = MyFileStream.Length;
                filesizeINT = Convert.ToInt32(fileSize);
                byte[] Buffer = new byte[fileSize];
                MyFileStream.Read(Buffer, 0, filesizeINT);
                MyFileStream.Close();
                Response.AppendHeader("content-disposition", "attachment; filename=" + fileName);
                Response.BinaryWrite(Buffer);
                Response.End();
            }
            catch
            {
                ErrorMes.Text = "Ошибка загрузки";
                ErrorMes.Visible = true;
            }
        }

        //поиск рпд посредством дополнения команлы селекта привязки данных (список рпд)
        protected void Button_Search_Click(object sender, EventArgs e)
        {
            Filter();
            SqlDataSourcePrograms.SelectCommand += " AND Discipline LIKE '%" + TextBox_Search.Text + "%'";
            Programs_RadioBox.DataBind();
        }

        //открыть word-файл в новой вкладке
        protected void Button_NewTab_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(Programs_RadioBox.SelectedItem.Value);
            try
            {
                BaseWork task = new BaseWork();
                string filePath = task.GetFileUrl(id);
                string script = string.Format("window.open('{0}');", filePath);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "newPage" + UniqueID, script, true);
            }
            catch
            {
                ErrorMes.Text = "Не удалось получить ссылку на файл";
                ErrorMes.Visible = true;
            }
        }
    }
}