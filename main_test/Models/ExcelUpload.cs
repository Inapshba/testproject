﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Configuration;
using main_test.Properties;
using System.Web.Configuration;

namespace main_test
{
    public class ExcelUpload
    {
        public string[] excelSheets;

        public List<string> count = new List<string>();
        List<string> cells = new List<string>();
        string Status = "";
        Settings settings;
        string tableName;

        public void AnalizExcel(string excelpath)
        {
            settings = new Settings();
            tableName = settings.UploadTable;

            GetSheets(excelpath);
            GetNumberOfRows(excelpath);

            //подчистить оперативную память
            string name = "excel.exe";
            System.Diagnostics.Process[] etc = System.Diagnostics.Process.GetProcesses();
            foreach (System.Diagnostics.Process anti in etc)
                if (anti.ProcessName.ToLower().Contains(name.ToLower())) anti.Kill();
        }

        public void DeleteFromTeacher()
        {
            string mySettings = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            //рабочая таблица
            string ssqltable = tableName;
            string ssqlconnectionstring = mySettings;

            //добавляем нужный семестр 
            string sclearsql = "UPDATE " + ssqltable + " SET Teacher=STUFF(Teacher,1,1,'') WHERE Period IS NULL";
            SqlConnection sqlconn = new SqlConnection(ssqlconnectionstring);
            SqlCommand sqlcmd = new SqlCommand(sclearsql, sqlconn);
            sqlconn.Open();
            sqlcmd.ExecuteNonQuery();
            sqlconn.Close();
        }

        public void SetPeriod(string period)
        {
            string mySettings = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            //рабочая таблица
            string ssqltable = tableName;
            string ssqlconnectionstring = mySettings;

            //добавляем нужный семестр 
            string sclearsql = "UPDATE " + ssqltable + " SET Period=@periodParam WHERE Period IS NULL";
            SqlConnection sqlconn = new SqlConnection(ssqlconnectionstring);
            SqlCommand sqlcmd = new SqlCommand(sclearsql, sqlconn);
            sqlcmd.Parameters.AddWithValue("@periodParam", period);
            sqlconn.Open();
            sqlcmd.ExecuteNonQuery();
            sqlconn.Close();
        }

        public void UploadIt(string excelFilePath, string b, string count, string period, string start)
        {
            OleDbConnection oledbconn;
            SqlBulkCopy BulkCopy = null;
            string mySettings = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            try
            {
                ClearTables();
                string excelconnectionstring = @"provider=microsoft.jet.oledb.4.0;data source=" + excelFilePath +
                                                 ";extended properties=" + "\"excel 8.0;hdr=yes;\"";

                string ssqlconnectionstring = mySettings;

                //открываем канал для закачки excel листов
                oledbconn = new OleDbConnection(excelconnectionstring);
                OleDbDataReader OleDbdr;
                oledbconn.Open();

                if (!(b == null) || !(b == ""))
                {
                    string myexceldataquery = "select * from [" + b + "$A" + start + ":AB" + count + "]";
                    OleDbCommand oledbcmd = new OleDbCommand(myexceldataquery, oledbconn);
                    OleDbdr = oledbcmd.ExecuteReader();
                    BulkCopy = new SqlBulkCopy(ssqlconnectionstring);
                    BulkCopy.DestinationTableName = tableName;
                    BulkCopy.WriteToServer(OleDbdr);
                }

                oledbconn.Close();

                Clean(false);
                DeleteFromTeacher();
                SetPeriod(period);

                Status = "File imported into sql server.";
            }

            catch (Exception ex)
            {
                string errorMessage = string.Empty;
                Status = "Error: " + ex.Message;
            }
        }

        private void ClearTables()
        {
            string mySettings = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            string ssqlconnectionstring = mySettings;

            settings = new Settings();
            tableName = settings.UploadTable;
            string ssqltable = tableName;
            SqlConnection sqlconn = new SqlConnection(ssqlconnectionstring);
            SqlCommand sqlcmd;

            try
            {
                sqlconn.Open();

                string sclearsql = "delete from CrossTableDetailedPlanExcelUpload";
                sqlcmd = new SqlCommand(sclearsql, sqlconn);
                sqlcmd.ExecuteNonQuery();

                sclearsql = "delete from " + ssqltable;
                sqlcmd = new SqlCommand(sclearsql, sqlconn);
                sqlcmd.ExecuteNonQuery();

                sclearsql = "DBCC CHECKIDENT ('[WorkloadExcelUploadTable]', RESEED, 0)";
                sqlcmd = new SqlCommand(sclearsql, sqlconn);
                sqlcmd.ExecuteNonQuery();

                sclearsql = "DBCC CHECKIDENT ('[CrossTableDetailedPlanExcelUpload]', RESEED, 0)";
                sqlcmd = new SqlCommand(sclearsql, sqlconn);
                sqlcmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                string errorMessage = string.Empty;
                Status = "Error: " + ex.Message;
            }
            finally
            {

                sqlconn.Close();
            }
        }

        private void GetSheets(string excelFilePath)
        {

            OleDbConnection objConn = null;
            System.Data.DataTable dt = null;
            int i = 0;

            try
            {
                String connString = "Provider=Microsoft.Jet.OLEDB.4.0;" + "Data Source=" + excelFilePath + ";Extended Properties=Excel 8.0;";

                objConn = new OleDbConnection(connString);
                objConn.Open();
                dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);


                excelSheets = new String[dt.Rows.Count];

                int j = 2;
                foreach (DataRow row in dt.Rows)
                {
                    if (j % 2 == 0)
                    {
                        excelSheets[i] = row["TABLE_NAME"].ToString();
                        i++;
                    }
                    j++;
                }
            }

            finally
            {
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }

                if (dt != null)
                {
                    dt.Dispose();
                }
            }
        }

        public void Clean(bool ClnAll)
        {
            string mySettings = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            string ssqlconnectionstring = mySettings;

            settings = new Settings();
            tableName = settings.UploadTable;
            string ssqltable = tableName;

            //чистим фантомные строки с NULL значениями
            string sclearsql = "DELETE FROM " + ssqltable;

            if (!ClnAll)
            {
                sclearsql += " WHERE Nomer1 IS NULL";

                SqlConnection sqlconn = new SqlConnection(ssqlconnectionstring);
                SqlCommand sqlcmd = new SqlCommand(sclearsql, sqlconn);
                sqlconn.Open();
                sqlcmd.ExecuteNonQuery();
                sqlconn.Close();
            }
        }

        private void GetNumberOfRows(string filename)
        {
            string connectionString = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            if (filename.EndsWith(".xlsx"))
            {
                connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filename + ";Mode=ReadWrite;Extended Properties=\"Excel 12.0;HDR=NO\"";
            }
            else if (filename.EndsWith(".xls"))
            {
                connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" + "Data Source=" + filename + ";Extended Properties=Excel 8.0;";
            }

            using (OleDbConnection conn = new OleDbConnection(connectionString))
            {
                try
                {
                    conn.Open();

                    foreach (string b in excelSheets)
                    {
                        if (b == null)
                        {
                            break;
                        }

                        string SQL = "SELECT COUNT (*) FROM [" + b.Replace("'", "") + "]";
                        using (OleDbCommand cmd = new OleDbCommand(SQL, conn))
                        {
                            using (OleDbDataReader reader = cmd.ExecuteReader())
                            {
                                reader.Read();
                                count.Add(reader[0].ToString());
                            }
                        }
                    }
                    conn.Close();
                }
                catch (Exception ex)
                {
                    string errorMessage = string.Empty;
                    Status = "Error: " + ex.Message;
                }
            }
        }

    }
}