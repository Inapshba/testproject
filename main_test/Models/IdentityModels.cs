﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using main_test.Models;
using System.Data.Entity;
using main_test;

namespace RolesIdentityApp.Models
{

    public class AppDbInitializer : DropCreateDatabaseAlways<ApplicationDbContext>
    {

        protected override void Seed(ApplicationDbContext context)
        {
                    var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
                    var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                    
                    if (!roleManager.RoleExists("Admin"))
                    {
                        var role = new IdentityRole { Name = "Admin" };
                        roleManager.Create(role);

                        var user = new ApplicationUser
                        {
                            UserName = "admin@test.ru",
                            Email = "admin@test.ru"
                        };

                        var userPwd = "414800";
                        var createResult = userManager.Create(user, userPwd);

                        if (createResult.Succeeded)
                        {
                            var result = userManager.AddToRole(user.Id, "Admin");
                        }
                    }

                    if (!roleManager.RoleExists("Moderator"))
                    {
                        var role = new IdentityRole { Name = "Moderator" };
                        roleManager.Create(role);

                        var user = new ApplicationUser
                        {
                            UserName = "moderator@test.ru",
                            Email = "moderator@test.ru"
                        };

                        var userPwd = "414800";
                        var createResult = userManager.Create(user, userPwd);

                        if (createResult.Succeeded)
                        {
                            var result = userManager.AddToRole(user.Id, "Moderator");
                        }
                    }

                    if (!roleManager.RoleExists("Guest"))
                    {
                        var role = new IdentityRole { Name = "Guest" };
                        roleManager.Create(role);

                        var user = new ApplicationUser
                        {
                            UserName = "guest@test.ru",
                            Email = "guest@test.ru"
                        };

                        var userPwd = "414800";
                        var createResult = userManager.Create(user, userPwd);

                        if (createResult.Succeeded)
                        {
                            var result = userManager.AddToRole(user.Id, "Guest");
                        }
                    }

                    if (!roleManager.RoleExists("User"))
                    {
                        var role = new IdentityRole { Name = "User" };
                        roleManager.Create(role);

                        var user = new ApplicationUser
                        {
                            UserName = "user@test.ru",
                            Email = "user@test.ru"
                        };

                        var userPwd = "414800";
                        var createResult = userManager.Create(user, userPwd);

                        if (createResult.Succeeded)
                        {
                            var result = userManager.AddToRole(user.Id, "User");
                        
                        }
                    }

                    if (!roleManager.RoleExists("head"))
                    {
                        var role = new IdentityRole { Name = "Head" };
                        roleManager.Create(role);

                        var user = new ApplicationUser
                        {
                            UserName = "head@test.ru",
                            Email = "head@test.ru"
                        };

                        var userPwd = "414800";
                        var createResult = userManager.Create(user, userPwd);

                        if (createResult.Succeeded)
                        {
                            var result = userManager.AddToRole(user.Id, "User");

                        }
                    }




            //var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));

            //var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            //// создаем две роли
            //var role1 = new IdentityRole { Name = "admin" };
            //var role2 = new IdentityRole { Name = "moderator" };
            //var role3 = new IdentityRole { Name = "user_role" };
            //var role4 = new IdentityRole { Name = "guest" };


            //// добавляем роли в бд
            //roleManager.Create(role1);
            //roleManager.Create(role2);
            //roleManager.Create(role3);
            //roleManager.Create(role4);
            //// создаем пользователей
            //var admin = new ApplicationUser { Email = "admin@admin.ru", UserName = "admin@test.ru" };
            //var moderator = new ApplicationUser { Email = "moderator@test.ru", UserName = "moderator@test.ru" };
            //var user_role = new ApplicationUser { Email = "user@test.ru", UserName = "user@test.ru" };
            //var guest = new ApplicationUser { Email = "guest@test.ru", UserName = "guest@test.ru" };

            //string password = "414800";
            //var result = userManager.Create(admin, password);
            //var result2 = userManager.Create(moderator, password);
            //var result3 = userManager.Create(user_role, password);
            //var result4 = userManager.Create(guest, password);

            //// если создание пользователя прошло успешно
            //if (result.Succeeded)
            //{
            //    // добавляем для пользователя роль
            //    userManager.AddToRole(admin.Id, role1.Name);
            //    userManager.AddToRole(moderator.Id, role2.Name);
            //    userManager.AddToRole(user_role.Id, role3.Name);
            //    userManager.AddToRole(guest.Id, role4.Name);
            //}

            //base.Seed(context);
        }
    }
}
namespace main_test.Models
{
    // Можно добавить данные о пользователе, указав больше свойств для класса User. Подробности см. на странице https://go.microsoft.com/fwlink/?LinkID=317594.
    public class ApplicationUser : IdentityUser
    {
        public ClaimsIdentity GenerateUserIdentity(ApplicationUserManager manager)
        {
            // Обратите внимание, что authenticationType должен совпадать с типом, определенным в CookieAuthenticationOptions.AuthenticationType
            var userIdentity = manager.CreateIdentity(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Здесь добавьте утверждения пользователя
            return userIdentity;
        }

        public Task<ClaimsIdentity> GenerateUserIdentityAsync(ApplicationUserManager manager)
        {
            return Task.FromResult(GenerateUserIdentity(manager));
        }

    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}

#region Вспомогательные приложения
namespace main_test
{
    public static class IdentityHelper
    {
        // Используется для обработки XSRF при связывании внешних имен входа
        public const string XsrfKey = "XsrfId";

        public const string ProviderNameKey = "providerName";
        public static string GetProviderNameFromRequest(HttpRequest request)
        {
            return request.QueryString[ProviderNameKey];
        }

        public const string CodeKey = "code";
        public static string GetCodeFromRequest(HttpRequest request)
        {
            return request.QueryString[CodeKey];
        }

        public const string UserIdKey = "userId";
        public static string GetUserIdFromRequest(HttpRequest request)
        {
            return HttpUtility.UrlDecode(request.QueryString[UserIdKey]);
        }

        public static string GetResetPasswordRedirectUrl(string code, HttpRequest request)
        {
            var absoluteUri = "/Account/ResetPassword?" + CodeKey + "=" + HttpUtility.UrlEncode(code);
            return new Uri(request.Url, absoluteUri).AbsoluteUri.ToString();
        }

        public static string GetUserConfirmationRedirectUrl(string code, string userId, HttpRequest request)
        {
            var absoluteUri = "/Account/Confirm?" + CodeKey + "=" + HttpUtility.UrlEncode(code) + "&" + UserIdKey + "=" + HttpUtility.UrlEncode(userId);
            return new Uri(request.Url, absoluteUri).AbsoluteUri.ToString();
        }

        private static bool IsLocalUrl(string url)
        {
            return !string.IsNullOrEmpty(url) && ((url[0] == '/' && (url.Length == 1 || (url[1] != '/' && url[1] != '\\'))) || (url.Length > 1 && url[0] == '~' && url[1] == '/'));
        }

        public static void RedirectToReturnUrl(string returnUrl, HttpResponse response)
        {
            if (!String.IsNullOrEmpty(returnUrl) && IsLocalUrl(returnUrl))
            {
                response.Redirect(returnUrl);
            }
            else
            {
                response.Redirect("~/");
            }
        }
       
            
        
    }
}
#endregion
