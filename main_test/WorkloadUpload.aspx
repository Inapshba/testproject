﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WorkloadUpload.aspx.cs" Inherits="main_test.WorkloadUpload" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>

    <link rel="stylesheet" type="text/css" href="../CSS/IndexMenu.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/PopupStyle.css" />
    <link href="../CSS/UploadPopup.css" rel="stylesheet" />
    <link href="../CSS/ReviewPopup.css" rel="stylesheet" />

    <script type="text/javascript">
        function Check_Table(table) {
            var str = "";
            var bindex = 9;
            var eindex = table.id.length;

            var S = table.id.replace(table.id.substring(bindex, eindex), "");

            if (S == "GridViewN") {
                str = "JSLabel2";
            }
            else {
                str = "JSLabel4";
            }

            Check_Click(table, str);
        }

        function Check_Click(objRef, str) {
            //Get the Row based on checkbox
            var row = objRef.parentNode.parentNode;
            if (objRef.checked) {
                //If checked change color to Aqua
                row.style.backgroundColor = "lightblue";
                GetSelectedRowSum(objRef, str);
            }
            else {
                row.style.backgroundColor = "white";
                GetSelectedRowDif(objRef, str);
            }
        }

        function GetSelectedRowSum(lnk, str) {
            var row = lnk.parentNode.parentNode;
            var sum = document.getElementById(str).innerHTML;
            var city = parseInt(row.cells[3].innerText) + parseInt(sum);
            document.getElementById(str).innerHTML = city;
            return false;
        }

        function GetSelectedRowDif(lnk, str) {
            var row = lnk.parentNode.parentNode;
            var sum = document.getElementById(str).innerHTML;
            var city = parseInt(sum) - parseInt(row.cells[3].innerText);
            document.getElementById(str).innerHTML = city;
            return false;
        }
    </script>
</head>

<body>
    
    <form id="form1" runat="server">
        <asp:FileUpload ID="FileUpload1" runat="server" />
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button" />
        <br />
        <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
        <br />
        <br />
        <asp:DropDownList ID="DropDownList2" runat="server">
            <asp:ListItem>2019/2020</asp:ListItem>
            <asp:ListItem>2019-2020</asp:ListItem>
        </asp:DropDownList>
        <br />
        <br />
        <br />
        <asp:Xml ID="Xml1" runat="server" DocumentSource="~/Uploads/09_04_022_2019-2020_plm.xml"></asp:Xml>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
    </form>
    
</body>
</html>


