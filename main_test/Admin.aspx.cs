﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using main_test.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Data;
using System.Data.Entity;
using System.Net.Mail;
using System.Net;


namespace main_test
{
    public partial class Admin : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ErrorMes.Visible = false;
            var db = new ApplicationDbContext();
            var Users = db.Users.Include(u => u.Roles);
            //Label1.Text = "UPDATE AspNetUserRoles SET RoleId = '" + DropDownList2.SelectedValue + "' FROM AspNetUserRoles INNER JOIN AspNetUsers ON AspNetUserRoles.UserId = AspNetUsers.Id WHERE (AspNetUsers.Id = '" + DropDownList1.SelectedValue + "')";
            //SqlDataSource5.UpdateCommand = Label1.Text;
        }

        //обновление таблиц
        private void BindData()
        {
            GridView1.DataBind();
            GridView2.DataBind();
            //GridView3.DataBind();
        }

        private void ListsBind()
        {
            DropDownList1.DataBind();
            DropDown_Sign.DataBind();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                string role = DropDownList2.SelectedItem.Value.ToString();
                string userid = DropDownList1.SelectedItem.Value.ToString();

                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var user = manager.FindById(DropDownList1.SelectedItem.Value.ToString());
                string roleRu;
                switch (DropDownList2.SelectedItem.Text.ToString())
                {
                    case "User":
                        roleRu = "пользователя";
                        break;
                    case "Admin":
                        roleRu = "администратора";
                        break;
                    case "Moderator":
                        roleRu = "модератора";
                        break;
                    case "head":
                        roleRu = "заведующего кафедрой";
                        break;
                    default:
                        roleRu = "";
                        break;
                }
                BaseWork task = new BaseWork();
                task.UpdateUserRole(userid, role);
                DataBind();

                //отправка письма с оповещением об подтверждение учетной записи администрацией сайта и выдачей соответствующей роли 
                MailMessage mail = new MailMessage();
                mail.IsBodyHtml = true;
                mail.Subject = "Подтверждение учетной записи";
                string body = "Здравствуйте! Ваша учетная запись на сайте <a href=\"https://mpuprograms.ru\">mpuprograms.ru</a> подтверждена администрацией сайта.";
                if (roleRu != "") body += " Вам выдана роль " + roleRu + ".";
                body += "<br/><br/>--<br/>" +
                    "Техподдержка \"мпу-програмс\" – <a href=\"mailto:support@mpuprograms.ru\">support@mpuprograms.ru</a>";
                mail.Body = body;
                mail.From = new MailAddress("support@mpuprograms.ru");
                mail.To.Add(user.Email);

                try
                {
                    SmtpClient client = new SmtpClient("smtp.masterhost.ru");
                    client.Credentials = new NetworkCredential("support@mpuprograms.ru", "sPB-zea-kS5-jHn");
                    client.Send(mail);
                }
                catch { }
            }
            catch
            {
                ErrorMes.Text = "Ошибка";
                ErrorMes.Visible = true;
            }
        }

        private void ViewSign()
        {
            try
            {
                string userid = DropDown_Sign.SelectedItem.Value.ToString();
                BaseWork task = new BaseWork();
                byte[] img = task.GetUserSign(userid);
                string base64 = Convert.ToBase64String(img);
                string src = string.Format("data:image/png;base64,{0}", base64);
                Image_Sign.Style.Add("display", "block");
                Image_Sign.Src = src;
                fileviewer.InnerHtml = "Привязанная подпись";
            }
            catch
            {
                Image_Sign.Style.Add("display", "none");
            }
        }

        protected void Button_SetSign_Click(object sender, EventArgs e)
        {
            if (uploader.HasFile)
            {
                try
                {
                    BaseWork task = new BaseWork();
                    string userid = DropDown_Sign.SelectedItem.Value.ToString();
                    byte[] img = uploader.FileBytes;
                    task.UpdateUserSign(img, userid);
                }
                catch
                {
                    Response.Write("<script>alert('Ошибка в отправке изображения в базу')</script>");
                }
                ViewSign();
            }
        }

        protected void DropDown_Sign_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewSign();
        }

        protected void GridView1_DataBound(object sender, EventArgs e)
        {
            ListsBind();
        }

        protected void Button_NewUser_Click(object sender, EventArgs e)
        {
            Response.Redirect("Account/Register.aspx?Admin=1");
        }

        protected void GridView2_RowDeleted(object sender, GridViewDeletedEventArgs e)
        {
            ListsBind();
        }

        private void SendMailAboutDelete(string email)
        {
            //отправка письма с оповещением об удалении учетной записи
            MailMessage mail = new MailMessage();
            mail.IsBodyHtml = true;
            mail.Subject = "Удаление учетной записи";
            mail.Body = "Здравствуйте! Ваша учетная запись на сайте <a href=\"https://mpuprograms.ru\">mpuprograms.ru</a> была удалена. " +
                "<br/><br/>--<br/>" +
                    "Техподдержка \"мпу-програмс\" – <a href=\"mailto:support@mpuprograms.ru\">support@mpuprograms.ru</a>";
            mail.From = new MailAddress("support@mpuprograms.ru");
            mail.To.Add(email);

            try
            {
                SmtpClient client = new SmtpClient("smtp.masterhost.ru");
                client.Credentials = new NetworkCredential("support@mpuprograms.ru", "sPB-zea-kS5-jHn");
                client.Send(mail);
            }
            catch { }
        }

        protected void GridView2_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            string email = GridView2.Rows[e.RowIndex].Cells[2].Text;
            SendMailAboutDelete(email);
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            string email = GridView1.Rows[e.RowIndex].Cells[2].Text;
            SendMailAboutDelete(email);
        }
    }
}