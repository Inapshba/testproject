﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace main_test
{
    public class Questions_Referat
    {
        private string questions;
        private string developer;
        private DateTime date;

        public string Questions { set { questions = value; } get { return questions; } }
        public string Developer { set { developer = value; } get { return developer; } }
        public DateTime Date { set { date = value; } get { return date; } }
    }
}