﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Title="- Редактор рабочих программ" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeBehind="ProgramsRedactor.aspx.cs" Inherits="main_test.ProgramsRedactor" validateRequest="false"%>

<asp:Content runat="server" ContentPlaceHolderID="MainContent" ID="ProgramsRedactorContent">
    <script src="Scripts/SetActiveTab.js"></script>
    <script src="Scripts/ScrollTop.js"></script>
    <script src="ckeditor/ckeditor.js"></script>
    <script src="ckeditor/adapters/jquery.js"></script>
    <script src="Scripts/ScreenCheck.js"></script>
    <link rel="stylesheet" href="Content/Colored.css" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    <link rel="stylesheet" href="Content/fileUploader.css" />
    <link rel="stylesheet" href="Content/CkeditorStyle.css" />
    <link rel="stylesheet" href="Content/WorkStyle.css" />
    <link rel="stylesheet" href="Content/fileUploader.css" />
    <link rel="stylesheet" href="Content/Preloader.css" />
    <script type="text/javascript">
        function OnApproveConfirm() {
            if (confirm('Все данные верны и программу можно публиковать?'))
                return true;
            else
                return false;
        }
        function onlyNumbers(input) {
            input.value = input.value.replace(/[^\d\.]/g, "");
            if(input.value.match(/\./g).length > 1) {
            input.value = input.value.substr(0, input.value.lastIndexOf("."));
            }
        }
        function showFileName() {
            var input = document.getElementById('<%=uploader.ClientID %>');
            document.getElementById('<%=fileviewer.ClientID %>').innerHTML = input.files[0].name;
            return false;
        }
    </script>

    <script>
        //отоброжение прелоедера перед готовностью ckeditor к работе
        CKEDITOR.on("instanceReady", function () {
            document.getElementById('globalForm').style.display = 'block';
            document.getElementById('preloader').style.display = 'none';
            $("#globalForm").animate({opacity: "1"}, 500);
        });
    </script>

    <div id="preloader" class="body_fade" style="display:block; height:79vh;">
        <div class="cssload-loader">
            <div class="cssload-inner cssload-one"></div>
            <div class="cssload-inner cssload-two"></div>
            <div class="cssload-inner cssload-three"></div>
        </div>
    </div>

    <div id="globalForm" style="display:none">
    <h2 class="unselecteble">Редактор учебных программ</h2>
    <asp:Label runat="server" ID="ConnectErrorMes" ForeColor="Red" Visible="false" Text="Подключение к БД не выполнено" />
    <br />
    <asp:Label runat="server" ID="ErrorMes" ForeColor="Red" Visible="false" /><br />
    <asp:LinkButton CssClass="link" runat="server" ID="ProgramReport" Text="" OnClick="ProgramReport_Click" />
    <br />
    <h4 class="unselecteble">Вы можете загрузить оформленный документ</h4>
            <asp:Button runat="server" ID="ImportDoc_Button" Text="Обновить файл программы" CssClass="btn light" OnClick="ImportDoc_Button_Click" />
            <label id="fileviewer" runat="server">Файл не выбран</label><strong>:</strong>
                <br />
                <div class="upload" style="margin-top:5px">
                    <label class="label">
                        <i class="material-icons">attach_file</i>
                        <asp:FileUpload runat="server" ID="uploader" ToolTip="Выбрать файл" CssClass="hide" onchange="showFileName();" accept=".docx, .doc"/>
                    </label>
                </div>
    <asp:Label runat="server" ID="DownLoadErrorMes" Text="Ошибка загрузки программы" Visible="false" ForeColor="Red" />
        <br />

     <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" class="tabs-dark" href="#titul">Титул</a></li>
        <li><a data-toggle="tab" class="tabs-dark" href="#purposes">Цели и место</a></li>
        <li><a data-toggle="tab" class="tabs-dark" href="#discipline_structure">Структура и содержание</a></li>
        <li class="dropdown tabs-dark">
            <a data-toggle="dropdown" class="dropdown-toggle tabs-dark" href="#">
            Инструменты оценки 
            <b class="caret"></b>
            </a>
            <ul class="dropdown-menu">
                <li><a data-toggle="tab" href="#fond_evaluation_tools">Фонд оценочных средств</a></li>
                <li><a data-toggle="tab" href="#competences">Описание компетенций</a></li>
            </ul>
        </li>
        <li class="dropdown tabs-dark">
            <a data-toggle="dropdown" class="dropdown-toggle tabs-dark" href="#">
                Элементы контроля
                <b class="caret"></b>
            </a>
            <ul class="dropdown-menu">
                <li><a data-toggle="tab" class="tabs-dark" href="#zachet">Зачёт</a></li>
                <li><a data-toggle="tab" class="tabs-dark" href="#exam">Экзамен</a></li>
                <li><a data-toggle="tab" class="tabs-dark" href="#referat">Реферат</a></li>
                <li><a data-toggle="tab" class="tabs-dark" href="#kt">КТ</a></li>
                <li><a data-toggle="tab" class="tabs-dark" href="#prakrika">Практика</a></li>
                <li><a data-toggle="tab" class="tabs-dark" href="#test">Тест</a></li>
                <li><a data-toggle="tab" class="tabs-dark" href="#curs">Курсовые работы</a></li>
            </ul>
        </li>
        <li><a data-toggle="tab" class="tabs-dark" href="#discipline_provide">Обеспечение</a></li>
        <li><a data-toggle="tab" class="tabs-dark" href="#persons">Составители</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" ID="titul">
            <h3 class="unselecteble">Титул</h3><br />
            <strong>Институт:</strong><br/>
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" id="Institute" TextMode="MultiLine"  />
            </div>
            <br />
            <strong>Должность утвердителя: </strong><br />
            <asp:TextBox runat="server"  id="Approver_Post" CssClass="form-control" placeholder="Введите должность..."/><br/>
            <br />
            <strong>Утвердитель: </strong><br />
            <asp:DropDownList runat="server" ID="Approver_Name" CssClass="form-control" DataSourceID="SqlDataSourceUsers" DataTextField="UserName" DataValueField="UserId" /><br />
            <asp:SqlDataSource ID="SqlDataSourceUsers" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" 
                SelectCommand="SELECT * FROM AspNetUsers INNER JOIN AspNetUserRoles ON AspNetUsers.Id=AspNetUserRoles.UserId INNER JOIN AspNetRoles ON AspNetRoles.Id = AspNetUserRoles.RoleId WHERE AspNetRoles.Name!='Guest' ORDER BY UserName"></asp:SqlDataSource>
            <br />
            <strong>Дата: </strong><br />
            <asp:TextBox runat="server" id="Approve_Date" CssClass="form-control" TextMode="Date"/><br/>
            <br />
            <strong>Рабочая программа дисциплины: </strong><br/>
            <asp:DropDownList runat="server" ID="Discipline" CssClass="form-control" DataSourceID="SqlDataSourceDisciplines" DataTextField="Name" DataValueField="Name"  /><br />
            <asp:SqlDataSource ID="SqlDataSourceDisciplines" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [Disciplines]"></asp:SqlDataSource>
            <br />
            <strong>Направление подготовки:</strong><br/>
            <asp:DropDownList runat="server" ID="Training_Direction" CssClass="form-control" DataSourceID="SqlDataSourceTrainingDirections" DataTextField="Viewer" DataValueField="Viewer" /><br />
            <asp:SqlDataSource ID="SqlDataSourceTrainingDirections" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT *, (Designation+' ('+ Code+')') AS Viewer FROM [Training_Directions]"></asp:SqlDataSource> 
            <br/>
            <strong>Профиль:</strong><br/>
            <asp:DropDownList runat="server" ID="DropDown_Profil" CssClass="form-control" DataSourceID="SqlDataSourceProfiles" DataTextField="Name" DataValueField="Id" />
            <asp:SqlDataSource runat="server" ID="SqlDataSourceProfiles" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [Profiles]"  />
            <br />
            <br />
            <strong>Квалификация (степень) выпусника:</strong><br/>
            <asp:DropDownList runat="server" ID="Graduate_Qualification" CssClass="form-control" DataSourceID="SqlDataSourceQualifications" DataTextField="Name" DataValueField="Name" /><br />
            <asp:SqlDataSource ID="SqlDataSourceQualifications" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [Qualifications]"></asp:SqlDataSource>
            <br />
            <strong>Форма обучения:</strong><br/>
            <asp:DropDownList runat="server" CssClass="form-control" id="Training_Form" >
                <asp:ListItem>Очная</asp:ListItem>
                <asp:ListItem>Очно-заочная</asp:ListItem>
                <asp:ListItem>Заочная</asp:ListItem>
                <asp:ListItem>Экстернат</asp:ListItem>
            </asp:DropDownList><br/>
            <br />
            <strong>Нормативный срок обучения</strong><br />
            <asp:TextBox runat="server" ID="StudyPeriod" CssClass="form-control" onkeyup="return onlyNumbers(this);" onchange="return onlyNumbers(this);" /><br />
            <br />
            <strong>Город</strong><br/>
            <asp:TextBox runat="server" id="City" CssClass="form-control" placeholder="Название города..." /><br /><hr />
            <asp:Button runat="server" ID="UpDateFirstBlock_Button" CssClass="btn light" Text="Редактировать блок" OnClick="UpDateFirstBlock_Button_Click" />
        </div>

        <div class="tab-pane fade" ID="purposes">
            <h3 class="unselecteble">Цели</h3><br />
            <h4 class="unselecteble">Цели освоения дисциплины:</h4>
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" id="ObjectivesOfDiscipline" TextMode="MultiLine" />            
            </div>
            <br />
            <h4 class="unselecteble">Место дисциплины в структуре ООП аспирантуры:</h4>
            <div class="check_screen">
                <asp:TextBox runat="server" id="DisciplineInOOP" CssClass="ckeditor" TextMode="MultiLine" />
            </div>
            <br />
            <%--<strong>Перечень планируемых результатов обучения по дисциплине (модулю),<br />
                соотнесенные с планируемыми результатами освоения образовательной программы:</strong><br />
            <div class="check_screen">
                <asp:TextBox runat="server" id="PlanDescription" CssClass="ckeditor" TextMode="MultiLine" />
            </div>--%>
            <div>
                <asp:Table ID="PlanTable" CssClass="table table-bordered" runat="server" BorderWidth="1" GridLines="Both">
                    <asp:TableHeaderRow>
                          <asp:TableHeaderCell  Width="10%" style="text-align:center"><strong>Код компетенции</strong></asp:TableHeaderCell>
                          <asp:TableHeaderCell  Width="45%" style="text-align:center"><strong>В результате освоения образовательной программы обучающийся должен обладать</strong></asp:TableHeaderCell>
                          <asp:TableHeaderCell  Width="45%" style="text-align:center"><strong>Перечень планируемых результатов обучения по дисциплине</strong></asp:TableHeaderCell>
                    </asp:TableHeaderRow>
                </asp:Table>
                <br />
                <asp:HiddenField runat="server" ID="ptrowscount" Value="0" />
                

                <asp:Table ID="PlanTable_control" CssClass="table table-bordered" runat="server" BorderWidth="1" GridLines="Both">
                    <asp:TableHeaderRow>
                          <asp:TableHeaderCell style="text-align: center" Text="Код..." ForeColor="Gray" Width="10%" />
                          <asp:TableHeaderCell style="text-align: center" Text="Результаты..." ForeColor="Gray" width="45%"/>
                    </asp:TableHeaderRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:DropDownList runat="server" CssClass="form-control" ID="Сompetence_PlanTable" DataSourceID="SqlDataSourceCompetences" DataTextField="Code" DataValueField="Description"  />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="ckeditor" TextMode="MultiLine" id="Enumeration_PlanTable"/>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:SqlDataSource ID="SqlDataSourceCompetences" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [Competences]"></asp:SqlDataSource>
                <asp:Button runat="server" CssClass="btn light" style="margin-bottom: 2px" ID="CreateRowPlanTable" Text="Добавить" OnClick="CreateRowPlanTable_Click"  />
                <asp:Button runat="server" CssClass="btn light" style="margin-bottom: 2px" ID="DeleteRowPlanTable" Text="Удалить строку № " OnClick="DeleteRowPlanTable_Click" />
                <asp:TextBox runat="server" CssClass="form-control" ID="RowNumberPlanTable" TextMode="Number" />
                <br /><hr />
            </div>
            <asp:Button runat="server" CssClass="btn light" ID="UpDateSecondBlock_Button" Text="Редактировать блок" OnClick="UpDateSecondBlock_Button_Click" /> 
        </div>

        <div class="tab-pane fade" ID="discipline_structure">
            <h3 class="unselecteble">Структура</h3><br />
            <%--<asp:Table runat="server" CssClass="table table-bordered" ID="DisciplineStructureTable" style="display:none;" contenteditable="true" BorderWidth="1" GridLines="Both" >
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell text="<strong>n/n</strong>" RowSpan="2" />
                    <asp:TableHeaderCell text="<strong>Раздел</strong>" RowSpan="2"/>
                    <asp:TableHeaderCell text="<strong>Семестр</strong>" RowSpan="2"/>
                    <asp:TableHeaderCell text="<strong>Неделя семестра</strong>" RowSpan="2"/>
                    <asp:TableHeaderCell text="<strong>Виды учебной работы, включая самостоятельную работу студентов, и трудоемкость в часах</strong>" ColumnSpan="5"/>
                    <asp:TableHeaderCell text="<strong>Виды самостоятельной работы студентов</strong>" ColumnSpan="6"/>
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell text="<strong>Л</strong>"/>
                    <asp:TableHeaderCell text="<strong>П/С</strong>"/>
                    <asp:TableHeaderCell text="<strong>Лаб</strong>"/>
                    <asp:TableHeaderCell text="<strong>СРС</strong>"/>
                    <asp:TableHeaderCell text="<strong>КСР</strong>"/>
                    <asp:TableHeaderCell text="<strong>К.Р</strong>"/>
                    <asp:TableHeaderCell text="<strong>К.П</strong>"/>
                    <asp:TableHeaderCell text="<strong>РГР</strong>"/>
                    <asp:TableHeaderCell text="<strong>Под. к л/р</strong>"/>
                    <asp:TableHeaderCell text="<strong>К/р</strong>"/>
                    <asp:TableHeaderCell text="<strong>Форма аттестации</strong>"/>
                </asp:TableHeaderRow>
            </asp:Table>
            <asp:HiddenField runat="server" ID="DSTrowscount" Value="0" />--%>
            
            <asp:GridView runat="server" CssClass="table table-bordered" OnRowUpdating="DisciplineStructureTable_GridView_RowUpdating" OnRowDeleted="DisciplineStructureTable_GridView_RowDeleted" 
                ID="DisciplineStructureTable_GridView" DataKeyNames="id" AutoGenerateColumns="False" DataSourceID="SqlDataSourceDisciplineStructure" >
            <Columns>
                <asp:BoundField DataField="id" HeaderText="ID" SortExpression="id" Visible="false" />
                <asp:CommandField ShowEditButton="true" />
                <asp:CommandField ShowDeleteButton="true" />
                <asp:BoundField DataField="Тема" HeaderText="Тема" SortExpression="Тема" />
                <asp:TemplateField HeaderText="Семестр" SortExpression="Семестр">
                    <EditItemTemplate>
                        <asp:TextBox runat="server" ID="SemestrEditInDSTGridView" Width="100%" TextMode="Number" Text=<%# Bind("Семестр") %> />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="SemestrViewInDSTGridView" Text=<%# Bind("Семестр") %> />
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="Неделя" SortExpression="Неделя">
                    <EditItemTemplate>
                        <asp:TextBox runat="server" ID="WeekEditInDSTGridView" Width="100%" TextMode="Number" Text=<%# Bind("Неделя") %> />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="WeekViewInDSTGridView" Text=<%# Bind("Неделя") %> />
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="Л" SortExpression="Л">
                    <EditItemTemplate>
                        <asp:TextBox runat="server" ID="LEditInDSTGridView" Width="100%" TextMode="Number" Text=<%# Bind("Л") %> />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="LViewInDSTGridView" Text=<%# Bind("Л") %> />
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="ПС" SortExpression="ПС">
                    <EditItemTemplate>
                        <asp:TextBox runat="server" ID="PSEditInDSTGridView" Width="100%" TextMode="Number" Text=<%# Bind("ПС") %> />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="PSViewInDSTGridView" Text=<%# Bind("ПС") %> />
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="Лаб" SortExpression="Лаб">
                    <EditItemTemplate>
                        <asp:TextBox runat="server" ID="LabEditInDSTGridView" Width="100%" TextMode="Number" Text=<%# Bind("Лаб") %> />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="LabViewInDSTGridView" Text=<%# Bind("Лаб") %> />
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="СРС" SortExpression="СРС">
                    <EditItemTemplate>
                        <asp:TextBox runat="server" ID="SRSEditInDSTGridView" Width="100%" TextMode="Number" Text=<%# Bind("СРС") %> />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="SRSViewInDSTGridView" Text=<%# Bind("СРС") %> />
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="КСР" SortExpression="КСР">
                    <EditItemTemplate>
                        <asp:TextBox runat="server" ID="KSREditInDSTGridView" Width="100%" TextMode="Number" Text=<%# Bind("КСР") %> />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="KSRViewInDSTGridView" Text=<%# Bind("КСР") %> />
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="КР" SortExpression="КР">
                    <EditItemTemplate>
                        <asp:TextBox runat="server" ID="KREditInDSTGridView" Width="100%" TextMode="Number" Text=<%# Bind("КР") %> />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="KRViewInDSTGridView" Text=<%# Bind("КР") %> />
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="КП" SortExpression="КП">
                    <EditItemTemplate>
                        <asp:TextBox runat="server" ID="KPeditInDSTGridView" Width="100%" TextMode="Number" Text=<%# Bind("КП") %> />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="KPViewInDSTGridView" Text=<%# Bind("КП") %> />
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="РГР" SortExpression="РГР">
                    <EditItemTemplate>
                        <asp:TextBox runat="server" ID="RGREditInDSTGridView" Width="100%" TextMode="Number" Text=<%# Bind("РГР") %> />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="RGRViewInDSTGridView" Text=<%# Bind("РГР") %> />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Подготовка_к_лр" HeaderText="Подготовка_к_лр" SortExpression="Подготовка_к_лр" />
                 <asp:TemplateField HeaderText="К_р" SortExpression="К_р">
                    <EditItemTemplate>
                        <asp:TextBox runat="server" ID="K_rEditInDSTGridView" Width="100%" TextMode="Number" Text=<%# Bind("К_р") %> />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="K_rViewInDSTGridView" Text=<%# Bind("К_р") %> />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Форма аттестации" SortExpression="Форма_аттестации">
                    <EditItemTemplate>
                        <asp:DropDownList runat="server" ID="list" Text=<%# Bind("Форма_аттестации") %> >
                            <asp:ListItem Text="" />
                            <asp:ListItem Text="Зачет" />
                            <asp:ListItem Text="Экзамен" />
                        </asp:DropDownList>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="AttestatFormInGridView" Text=<%# Bind("Форма_аттестации") %>></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            </asp:GridView>

            <asp:SqlDataSource ID="SqlDataSourceDisciplineStructure" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" 
              SelectCommand="SELECT * FROM [DisciplineStructurs] WHERE ([ProgramId] = @ProgramId)"
              UpdateCommand="UPDATE [DisciplineStructurs] SET [Тема]=@Тема, [Семестр]=@Семестр, [Неделя]=@Неделя, [Л]=@Л, [Лаб]=@Лаб, [СРС]=@СРС, [КСР]=@КСР, [КР]=@КР, [КП]=@КП, [РГР]=@РГР, [Подготовка_к_лр]=@Подготовка_к_лр, [К_р]=@К_р, [Форма_аттестации]=@Форма_аттестации WHERE [id]=@id AND [Тема]!='Часы за семестр' AND [Тема]!='Часы за весь период'" 
              DeleteCommand="DELETE FROM [DisciplineStructurs] WHERE [id]=@id AND [Тема]!='Часы за семестр' AND [Тема]!='Часы за весь период'" 
              InsertCommand="INSERT INTO ([Тема], [Семестр], [Неделя], [Л], [Лаб], [СРС], [КСР], [КР], [КП], [РГР], [Подготовка_к_лр], [К_р], [Форма_аттестации]) VALUES(@Тема, @Семестр, @Неделя, @Л, @Лаб, @СРС, @КСР, @КР, @КП, @РГР, @Подготовка_к_лр, @К_р, @Форма_аттестации)">
              <SelectParameters>
                  <asp:QueryStringParameter DefaultValue="0" Name="ProgramId" QueryStringField="ProgramId" Type="Int32" />
              </SelectParameters>
            </asp:SqlDataSource>

            <br />
            <h4 class="unselecteble">Содержание дисциплины:</h4>
            <div class="check_screen">
                <asp:TextBox runat="server" id="DisciplineStructureContent" CssClass="ckeditor" TextMode="MultiLine" />
            </div>
            <br />
            <h4 class="unselecteble">Содержание разделов дисциплины:</h4>
            <div class="check_screen">
                <asp:TextBox runat="server" ID="SemesteresContent" CssClass="ckeditor" TextMode="MultiLine"  /> 
            </div>
            <br /> 
            <h4 class="unselecteble">Образовательные технологии:</h4>
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" id="EducationalTechnology" TextMode="MultiLine" />
            </div>
            <br />
            <h4 class="unselecteble">Оценочные средства для текущего контроля успеваемости:</h4>
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" ID="MonitoringTools_Description" TextMode="MultiLine" />
            </div>
            <br /><hr />
            <asp:Button runat="server" CssClass="btn light" ID="UpDateThirdBlock_Button" Text="Редактировать блок" OnClick="UpDateThirdBlock_Button_Click" />
        </div>

        <div class="tab-pane fade" ID="fond_evaluation_tools">
            <h3 class="unselecteble">Фонд оценочных средств</h3><br />
            <h4 class="unselecteble">Перечень оценочных средств</h4>
            <asp:Table runat="server" CssClass="table table-bordered" ID="PrilAssessTable" BorderWidth="1" GridLines="Both">
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="№ ОС" Width="5%"/>
                    <asp:TableHeaderCell Text="Наименование оценочного средства" Width="30%" />
                    <asp:TableHeaderCell Text="Краткая характеристика оценочного средства" Width="30%" />
                    <asp:TableHeaderCell Text="Представление оценочного средства в ФОС" Width="30%" />
                </asp:TableHeaderRow>
            </asp:Table>
            <asp:HiddenField runat="server" ID="PATrowscount" Value="0" />
            <div style="border: 1px solid #ddd; border-radius: 5px">
                <asp:Table runat="server" CssClass="table table-bordered" BorderWidth="1" GridLines="Both">
                    <asp:TableRow>
                        <asp:TableCell Width="35%">
                            <asp:TextBox id="PATName" CssClass="form-control" Width="100%" runat="server" placeholder="Оценочное средство..." TextMode="MultiLine" />
                        </asp:TableCell>
                        <asp:TableCell Width="30%">
                            <asp:TextBox id="PATSpec" runat="server" CssClass="form-control" Width="100%" placeholder="Краткая характеристика..." TextMode="MultiLine" />
                        </asp:TableCell>
                        <asp:TableCell Width="30%">
                            <asp:TextBox id="PATinFOS" runat="server" CssClass="form-control" Width="100%" placeholder="Представление в ФОС..." TextMode="MultiLine" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            
                <asp:Button runat="server" CssClass="btn light" ID="button_addRowInPAT" Text="Добавить" OnClick="button_addRowInPAT_Click" />
                <asp:Button runat="server" CssClass="btn light" ID="button_deleteRowInPAT" Text="Удалить строку № " OnClick="button_deleteRowInPAT_Click" />
                <asp:TextBox runat="server" CssClass="form-control" ID="RowNumberPAT" TextMode="Number" />
            </div>
            <br />
            <em>Перечень компетенций с указанием этапов их формирования в процессе освоения образовательной программы.<br />
                В результате освоения дисциплины (модуля) формируются следующие компетенции:</em><br />
              <asp:Table ID="MonitoringTools_FondTable" CssClass="table table-bordered" runat="server" BorderWidth="1" GridLines="Both">
                <asp:TableRow>
                    <asp:TableHeaderCell Width="5%" Text="<strong>Код компетенции</strong>"  />
                    <asp:TableHeaderCell width="90%" Text="<strong>В результате освоения образовательной программы обучающийся должен обладать</strong>" />
                </asp:TableRow>
            </asp:Table>
            <asp:HiddenField runat="server" ID="ftrowscount" Value="0" />
            <asp:DropDownList runat="server" CssClass="form-control" ID="Competence_FondTable" DataSourceID="SqlDataSourceCompetences" DataTextField="Code" DataValueField="Id"  />
            <br />
            <asp:Button runat="server" CssClass="btn light" style="margin-bottom: 2px" ID="CreateRowFondTable" Text="Добавить" OnClick="CreateRowFondTable_Click" />
            <asp:Button runat="server" CssClass="btn light" style="margin-bottom: 2px" ID="DeleteRowFondTable" Text="Удалить строку №" OnClick="DeleteRowFondTable_Click" />
            <asp:TextBox runat="server" ID="RowNumberFondTable" CssClass="form-control" TextMode="Number" />
            
            <br /><br />
            <strong>Описание показателей и критериев оценивания компетенций, <br />
                формируемых по итогам освоения дисциплины (модуля), описание шкал оценивания</strong><br />
            <em>Показателем оценивания компетенций на различных этапах их формирования<br />
                является достижение обучающимися планируемых результатов обучения по дисциплине (модулю).</em><br />

             <asp:Table ID="CriterionsTable" CssClass="table table-bordered" runat="server" BorderWidth="1" GridLines="Both">
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell  RowSpan="2" text="<strong>Описание компетенции</strong>" width="200px"/>
                    <asp:TableHeaderCell  RowSpan="2" text="<strong>Показатель</strong>" width="200px"/>
                    <asp:TableHeaderCell ColumnSpan="4" text="<strong>Критерий оценивания</strong>" Width="600px"/>
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>2</strong>" />
                    <asp:TableHeaderCell Text="<strong>3</strong>" />
                    <asp:TableHeaderCell Text="<strong>4</strong>" />
                    <asp:TableHeaderCell Text="<strong>5</strong>" />
                </asp:TableHeaderRow>
            </asp:Table>
            <asp:DropDownList runat="server" CssClass="form-control" style="margin-bottom:5px; margin-left:15px;" ID="Competence_CriterionsTable" DataSourceID="SqlDataSourceCompetences" DataTextField="Code" DataValueField="Description" /><br />
            <asp:HiddenField runat="server" ID="CTrowscount" Value="1" />

            <div class="container">
                <div class="row">
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control" Width="100%" style="max-width: 100%" id="IndicatorCriterionsTable_Know" runat="server"   placeholder="Знать..."  /></div>
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control" Width="100%" style="max-width: 100%" id="CriterionCriterionsTable_Know_2" runat="server" placeholder="На 2..." /></div>
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control" Width="100%" style="max-width: 100%" id="CriterionCriterionsTable_Know_3" runat="server" placeholder="На 3..." /></div>
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control" Width="100%" style="max-width: 100%" id="CriterionCriterionsTable_Know_4" runat="server" placeholder="На 4..." /></div>
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control" Width="100%" style="max-width: 100%" id="CriterionCriterionsTable_Know_5" runat="server" placeholder="На 5..." /></div>
                </div>
                <div class="row">
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control" Width="100%" style="max-width: 100%" id="IndicatorCriterionsTable_Can" runat="server"    placeholder="Уметь..."/></div>
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control" Width="100%" style="max-width: 100%" id="CriterionCriterionsTable_Can_2" runat="server"  placeholder="На 2..."/></div>
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control" Width="100%" style="max-width: 100%" id="CriterionCriterionsTable_Can_3" runat="server"  placeholder="На 3..."/></div>
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control" Width="100%" style="max-width: 100%" id="CriterionCriterionsTable_Can_4" runat="server"  placeholder="На 4..."/></div>
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control" Width="100%" style="max-width: 100%" id="CriterionCriterionsTable_Can_5" runat="server"  placeholder="На 5..."/></div>
                </div>
                <div class="row">
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control col-md-2" Width="100%" style="max-width: 100%" id="IndicatorCriterionsTable_Wield" runat="server"  placeholder="Владеть..."/> </div>
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control col-md-2" Width="100%" style="max-width: 100%" id="CriterionCriterionsTable_Wield_2" runat="server" placeholder="На 2..." /></div>
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control col-md-2" Width="100%" style="max-width: 100%" id="CriterionCriterionsTable_Wield_3" runat="server" placeholder="На 3..." /></div>
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control col-md-2" Width="100%" style="max-width: 100%" id="CriterionCriterionsTable_Wield_4" runat="server" placeholder="На 4..." /></div>
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control col-md-2" Width="100%" style="max-width: 100%" id="CriterionCriterionsTable_Wield_5" runat="server" placeholder="На 5..." /></div>
                </div>
            </div>
            <asp:Button runat="server" CssClass="btn light"  style="margin-left:15px; margin-bottom: 2px;" ID="CreateRowsCriterionsTable" Text="Добавить" OnClick="CreateRowsCriterionsTable_Click" />                
            <asp:Button runat="server" CssClass="btn light"  style="margin-bottom:2px" ID="DeleteRowsCriterionsTable" Text="Удалить №" OnClick="DeleteRowsCriterionsTable_Click" />    
            <asp:TextBox runat="server" ID="RowNumberCriterionsTable" CssClass="form-control" style="margin-top:5px" TextMode="Number" />
            
            <br /><br />

            <h4 class="unselecteble">Форма промежуточной аттестации: зачет</h4>
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" id="ZachetDescription" TextMode="MultiLine" />
            </div>
            <br />
            <em class="unselecteble">Зачтено:</em><br />
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" ID="Zachteno" TextMode="MultiLine"/>
            </div>
            <br />
            <em class="unselecteble">Не зачтено:</em><br />
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" id="NeZachteno" TextMode="MultiLine"/>
            </div>

            <br />
            
            <h4 class="unselecteble">Форма промежуточной аттестации: экзамен</h4>
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" id="ExamDescription" TextMode="MultiLine"/>
            </div>
            <br />
            <em class="unselecteble">Отлично:</em><br />
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" ID="AttestatGreat" TextMode="MultiLine"/>
            </div>
            <br />
            <em class="unselecteble">Хорошо:</em><br />
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" id="AttestatGood" TextMode="MultiLine"/>
            </div>
            <br />
            <em class="unselecteble">Удовлетворительно:</em><br />
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" id="AttestatSatisfactorily" TextMode="MultiLine"/>
            </div>
            <br />
            <em class="unselecteble">Неудовлетворительно:</em><br />
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" id="AttestatNotSatisfactorily" TextMode="MultiLine"/>
            </div>
            <hr />
            <asp:Button runat="server" CssClass="btn light" ID="UpDateFifthBlock_Button" Text="Редактировать блок" OnClick="UpDateFifthBlock_Button_Click1" />
        </div>

        <div class="tab-pane fade" ID="discipline_provide">
            <h3 class="unselecteble">Учебно-методическое и информационное обеспечение дисциплины</h3><br />
            <h4 class="unselecteble">Основная литература:</h4>
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" id="BasicLiterature" TextMode="MultiLine" />
            </div>
            <br />
            <h4 class="unselecteble">Дополнительная литература:</h4>
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" id="AdditionalLiterature" TextMode="MultiLine" />
            </div>
            <br />
            <h4 class="unselecteble">Программное обеспечение и интернет-ресурсы:</h4>
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" id="ITResources" TextMode="MultiLine" />
            </div>
            <br />
            <h4 class="unselecteble">Материально-техническое обеспечение дисциплины:</h4>
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" ID="MaterialTechnicalSupport" TextMode="MultiLine"/>
            </div>
            <br />
            <h4 class="unselecteble">Методические рекомендации для студентов:</h4>
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" ID="StudentRecommendations" TextMode="MultiLine"/>
            </div>
            <br />
            <h4 class="unselecteble">Методические рекомендации для преподавателя:</h4>
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" ID="TeacherRecommendations" TextMode="MultiLine"/>
            </div>
            <hr />
            <asp:Button runat="server" CssClass="btn light" ID="UpDateSixthBlock_Button" Text="Редактировать блок" OnClick="UpDateSixthBlock_Button_Click" />
        </div>

        <div class="tab-pane fade" ID="persons">
            <h3 class="unselecteble">Составители</h3><br />
            <div runat="server" id="drafters_html_block">
                <strong>Программу составил: </strong><br />
                <asp:TextBox runat="server" CssClass="form-control" placeholder="Академ. статус" ID="DrafterStatus" width="150px" />
                <asp:DropDownList runat="server" CssClass="form-control" ID="DrafterName" DataSourceID="SqlDataSourceUsers" DataTextField="UserName" DataValueField="UserId" />
                <asp:HiddenField runat="server" ID="AdditionalDraftersCount" Value="0" />
            </div>
            <asp:Button runat="server" style="margin-top:2px" ID="Button_AddDrafter" CssClass="btn light" Text="Добавить составителя" OnClick="Button_AddDrafter_Click" />
            <asp:Button runat="server" style="margin-top:2px" ID="Button_DeleteDrafter" CssClass="btn light" Text="Удалить составителя" OnClick="Button_DeleteDrafter_Click" /><br />
            <br />
            <strong>Кафедра:</strong><br />
            <asp:DropDownList runat="server" ID="DropDown_Department" CssClass="form-control" DataSourceID="SqlDataSourceDepartments" DataTextField="Name" DataValueField="Id" />
            <asp:SqlDataSource runat="server" ID="SqlDataSourceDepartments" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [Departments]" />
            <asp:TextBox runat="server" CssClass="form-control" ID="DraftDate" width="150px" TextMode="Date"/>
            <asp:TextBox runat="server" CssClass="form-control" placeholder="Протокол" ID="Protocol" width="150px" /><br />
            <br />
            <strong>Заведующий кафедрой: </strong><br />
            <asp:TextBox runat="server" CssClass="form-control" placeholder="Академ. статус" ID="DepartmentHeadStatus" width="150px" />
            <asp:DropDownList runat="server" CssClass="form-control" ID="DepartmentHeadName" DataSourceID="SqlDataSourceUsers" DataTextField="UserName" DataValueField="UserId" /><br />
            <br />
            <strong>Программа согласована: </strong><br />
            <asp:TextBox runat="server" CssClass="form-control" placeholder="Должность" ID="AgreementDepartmentHeadStatus" width="150px" />
            <asp:DropDownList runat="server" CssClass="form-control" ID="AgreementDepartmentHeadName" DataSourceID="SqlDataSourceUsers" DataTextField="UserName" DataValueField="UserId" /><br />
            <asp:TextBox runat="server" CssClass="form-control" placeholder="Должность" ID="AgreementDirectorStatus" width="150px" />
            <asp:DropDownList runat="server" CssClass="form-control" ID="AgreementDirectorName" DataSourceID="SqlDataSourceUsers" DataTextField="UserName" DataValueField="UserId" />
            <br /><hr />
            <asp:Button runat="server" CssClass="btn light" ID="UpDateSeventhBlock_Button" Text="Редактировать блок" OnClick="UpDateSeventhBlock_Button_Click" /><br />
            <hr />
            <asp:Button runat="server" ID="Button_CreateFile" CssClass="btn btn-lg yellow" Text="Переформировать Word-файл" OnClick="Button_CreateFile_Click" />
            <asp:Button runat="server" CssClass="btn btn-lg dark" ID="ApproveProgram_Button" Text="Утвердить программу" OnClick="ApproveProgram_Button_Click" OnClientClick="if (!OnApproveConfirm()) return false;" />
            <asp:Label runat="server" ID="ApproveError" ForeColor="Red" Visible="false" Text="Ошибка при утверждении" />
        </div>

        <div class="tab-pane fade" ID="competences">
            <h3 class="unselecteble">Описание компетенций</h3><br />
            <asp:Table runat="server" CssClass="table table-bordered" ID="PrilFondTable_1" BorderWidth="1" GridLines="Both">
                <asp:TableRow>
                    <asp:TableCell Text="В процессе освоения данной дисциплины студент формирует и демонстрирует следующие <strong>универсальные</strong> компетенции:" ColumnSpan="6" />
                </asp:TableRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Компетенции</strong>" ColumnSpan="2"/>
                    <asp:TableHeaderCell Text="<strong>Перечень компонентов</strong>" RowSpan="2"/>
                    <asp:TableHeaderCell Text="<strong>Технология формирования компетенции</strong>" RowSpan="2" />
                    <asp:TableHeaderCell Text="<strong>Форма оценочного средства</strong>" RowSpan="2" />
                    <asp:TableHeaderCell Text="<strong>Степени уровней освоения компетенций</strong>" RowSpan="2" />
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Индекс</strong>" />
                    <asp:TableHeaderCell Text="<strong>Формулировка</strong>" />
                </asp:TableHeaderRow>
            </asp:Table>
            <asp:HiddenField runat="server" ID="PFT_1rowscount" Value="0" />
            <asp:Table runat="server" CssClass="table table-bordered" ID="PrilFondTable_2" BorderWidth="1" GridLines="Both">
                <asp:TableRow>
                    <asp:TableCell Text="В процессе освоения данной дисциплины студент формирует и демонстрирует следующие <strong>общепрофессиональные</strong> компетенции:" ColumnSpan="6" />
                </asp:TableRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Компетенции</strong>" ColumnSpan="2"/>
                    <asp:TableHeaderCell Text="<strong>Перечень компонентов</strong>" RowSpan="2"/>
                    <asp:TableHeaderCell Text="<strong>Технология формирования компетенции</strong>" RowSpan="2" />
                    <asp:TableHeaderCell Text="<strong>Форма оценочного средства</strong>" RowSpan="2" />
                    <asp:TableHeaderCell Text="<strong>Степени уровней освоения компетенций</strong>" RowSpan="2" />
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Индекс</strong>" />
                    <asp:TableHeaderCell Text="<strong>Формулировка</strong>" />
                </asp:TableHeaderRow>
            </asp:Table>
            <asp:HiddenField runat="server" ID="PFT_2rowscount" Value="0" />
            <asp:Table runat="server" CssClass="table table-bordered" ID="PrilFondTable_3" BorderWidth="1" GridLines="Both">
                <asp:TableRow>
                    <asp:TableCell Text="В процессе освоения данной дисциплины студент формирует и демонстрирует следующие <strong>профессиональные</strong> компетенции:" ColumnSpan="6" />
                </asp:TableRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Компетенции</strong>" ColumnSpan="2"/>
                    <asp:TableHeaderCell Text="<strong>Перечень компонентов</strong>" RowSpan="2"/>
                    <asp:TableHeaderCell Text="<strong>Технология формирования компетенции</strong>" RowSpan="2" />
                    <asp:TableHeaderCell Text="<strong>Форма оценочного средства</strong>" RowSpan="2" />
                    <asp:TableHeaderCell Text="<strong>Степени уровней освоения компетенций</strong>" RowSpan="2" />
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Индекс</strong>" />
                    <asp:TableHeaderCell Text="<strong>Формулировка</strong>" />
                </asp:TableHeaderRow>
            </asp:Table>
            <asp:HiddenField runat="server" ID="PFT_3rowscount" value="0" />

            <div style="border: 1px solid #ddd; border-radius: 5px">
                <asp:DropDownList runat="server" CssClass="form-control" ID="PFTCom" DataSourceID="SqlDataSourceCompetences" DataTextField="Code" DataValueField="Description" />
                <asp:Table runat="server" CssClass="table table-bordered" ID="DataRowInPrilFondTables" BorderWidth="1" GridLines="Both">
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" Text="Перечень компонентов..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" Text="Технология формирования компетенции..."/>
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" Text="Форма оценочного средства..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" Text="Степени уровней освоения компетенций..." />
                    </asp:TableHeaderRow>
                    <asp:TableRow>
                       <asp:TableCell>
                            <asp:TextBox id="PFTKomponentsList" CssClass="form-control" Width="100%" TextMode="MultiLine" runat="server"/>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox id="PFTCompetenceTechnology" CssClass="form-control" Width="100%" TextMode="MultiLine" runat="server" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox id="PFTAssessForm" runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox id="PFTSteps" runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>

                <asp:Button runat="server" CssClass="btn light" Width="25%" ID="button_createRowInPFT_1" Text="Добавить в универсальные" OnClick="button_createRowInPFT_1_Click" />
                <asp:Button runat="server" CssClass="btn light" Width="25%" ID="button_createRowInPFT_2" Text="Добавить в общепрофессиональные" OnClick="button_createRowInPFT_2_Click" />
                <asp:Button runat="server" CssClass="btn light" Width="25%" ID="button_createRowInPFT_3" Text="Добавить в профессиональные" OnClick="button_createRowInPFT_3_Click" />
                <br />
                <asp:Button runat="server" CssClass="btn light" Width="25%" ID="button_deleteRowInPFT_1" Text="Удалить из универсальных" OnClick="button_deleteRowInPFT_1_Click" />
                <asp:Button runat="server" CssClass="btn light" Width="25%" ID="button_deleteRowInPFT_2" Text="Удалить из общепрофессиональных" OnClick="button_deleteRowInPFT_2_Click" />
                <asp:Button runat="server" CssClass="btn light" Width="25%" ID="button_deleteRowInPFT_3" Text="Удалить из профессиональных" OnClick="button_deleteRowInPFT_3_Click" />
                <asp:TextBox runat="server" CssClass="form-control" ID="RowNumberPFT" TextMode="Number" />
            </div>
        </div>
        
        
        <div class="tab-pane fade" id="zachet">
            <h3 class="unselecteble">Элементы контроля</h3>
            <h4 class="unselecteble">Зачёт:</h4>
            <asp:Table runat="server" CssClass="table table-bordered" ID="PrilCompetenceTable_Zachet" BorderWidth="1" GridLines="Both">
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Описание</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Контролируемый результат обучения</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Контролируемые темы (разделы) дисциплины</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Недифференцированный зачет</strong>" ColumnSpan="2" />
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Критерии оценивания</strong>" ColumnSpan="2"/>
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Зачтено</strong>" />
                    <asp:TableHeaderCell Text="<strong>Не зачтено</strong>" />
                </asp:TableHeaderRow>
            </asp:Table>
            <asp:HiddenField runat="server" ID="PCT_zachetrowscount" Value="0" />
            <div style="border: 1px solid #ddd; border-radius: 5px">
                <asp:Table runat="server" CssClass="table table-bordered" BorderWidth="1" GridLines="Both">
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Описание..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Результат..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Темы..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Зачтено при..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Не зачтено при..." />
                    </asp:TableHeaderRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:DropDownList runat="server" ID="DropDown_PCTZ" CssClass="form-control" DataSourceID="SqlDataSourceCompetences" DataTextField="Code" DataValueField="Description" />
                            <%--<asp:TextBox runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" id="PCTZDiscription"/>--%>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" id="PCTZRezult" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" id="PCTZTopics" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" id="PCTZZachet" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" id="PCTZNeZachet" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Button runat="server" CssClass="btn light" ID="addRowInPCTzachet" Text="Добавить" OnClick="addRowInPCTzachet_Click" />
                <asp:Button runat="server" CssClass="btn light" ID="deleteRowInPCTzachet" Text="Удалить строку №" OnClick="deleteRowInPCTzachet_Click" />
                <asp:TextBox runat="server" CssClass="form-control" ID="RowNumberPCTZ" TextMode="Number" />
            </div>
            <br />
            <strong>Вопросы к зачёту:</strong><br />
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" ID="ZachetQuestions" TextMode="MultiLine" />
            </div>
            <strong>Составитель:</strong> 
            <asp:DropDownList runat="server" CssClass="form-control" ID="DeveloperZachet" DataSourceID="SqlDataSourceUsers" DataTextField="UserName" DataValueField="UserId" />
            <strong>Дата:</strong> 
            <asp:TextBox runat="server" CssClass="form-control" ID="ZachetQuestionsDate" TextMode="Date" /><br />
            <br />
            <asp:Button runat="server" CssClass="btn light" ID="UpDateZachet_Button" Text="Редактировать блок" OnClick="UpDateZachet_Button_Click" />
        </div>
            
        <div class="tab-pane fade" id="exam">
            <h3 class="unselecteble">Элементы контроля</h3>
            <h4 class="unselecteble">Экзамен:</h4>
            <asp:Table runat="server" CssClass="table table-bordered" ID="PrilCompetenceTable_Exam" BorderWidth="1" GridLines="Both">
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Описание</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Контролируемый результат обучения</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Контролируемые темы (разделы) дисциплины</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Экзамен</strong>" ColumnSpan="4" />
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Критерии оценивания</strong>" ColumnSpan="4"/>
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>2</strong>" />
                    <asp:TableHeaderCell Text="<strong>3</strong>" />
                    <asp:TableHeaderCell Text="<strong>4</strong>" />
                    <asp:TableHeaderCell Text="<strong>5</strong>" />
                </asp:TableHeaderRow>
            </asp:Table>
            <asp:HiddenField runat="server" ID="PCT_examrowscount" Value="0" />
            <div style="border: 1px solid #ddd; border-radius: 5px">
                <asp:Table runat="server" CssClass="table table-bordered" BorderWidth="1" GridLines="Both">
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Описание..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Результат..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Темы..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 2..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 3..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 4..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 5..." />
                    </asp:TableHeaderRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:DropDownList runat="server" ID="DropDown_PCTE" CssClass="form-control" DataSourceID="SqlDataSourceCompetences" DataTextField="Code" DataValueField="Description" />
                            <%--<asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTEDiscription" />--%>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTERezult" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTETopics" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTE2" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTE3" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTE4" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTE5" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>

                <asp:Button runat="server" CssClass="btn light" ID="addRowInPCTExamTable" Text="Добавить" OnClick="addRowInPCTExamTable_Click" />
                <asp:Button runat="server" CssClass="btn light" ID="deleteRowInPCTExamTable" Text="Удалить строку №" OnClick="deleteRowInPCTExamTable_Click" />
                <asp:TextBox runat="server" CssClass="form-control" ID="RowNumberPCTE" TextMode="Number" />
            </div>
            <br />
            <strong>Вопросы к экзамену:</strong><br />
            <div class="check_screen">
                <asp:TextBox runat="server" ID="ExamQuestions" CssClass="ckeditor" TextMode="MultiLine" />
            </div>
            <strong>Составитель:</strong> <asp:DropDownList runat="server" CssClass="form-control" ID="DeveloperExam" DataSourceID="SqlDataSourceUsers" DataTextField="UserName" DataValueField="UserId" />
            <strong>Дата:</strong> <asp:TextBox runat="server" CssClass="form-control" ID="ExamQuestionsDate" TextMode="Date" /><br />
            <br />
            <asp:Button runat="server" CssClass="btn light" ID="UpDateExam_Button" Text="Редактировать блок" OnClick="UpDateExam_Button_Click" />
        </div>
            
        <div class="tab-pane fade" id="referat">
            <h3 class="unselecteble">Элементы контроля</h3>
            <h4 class="unselecteble">Реферат:</h4>
            <asp:Table runat="server" CssClass="table table-bordered" ID="PrilCompetenceTable_Referat" BorderWidth="1" GridLines="Both">
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Описание</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Контролируемый результат обучения</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Контролируемые темы (разделы) дисциплины</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Реферат</strong>" ColumnSpan="4" />
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Критерии оценивания</strong>" ColumnSpan="4"/>
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>2</strong>" />
                    <asp:TableHeaderCell Text="<strong>3</strong>" />
                    <asp:TableHeaderCell Text="<strong>4</strong>" />
                    <asp:TableHeaderCell Text="<strong>5</strong>" />
                </asp:TableHeaderRow>
            </asp:Table>
            <asp:HiddenField runat="server" ID="PCT_referatrowscount" Value="0" />
            <div style="border: 1px solid #ddd; border-radius: 5px">
                <asp:Table runat="server" CssClass="table table-bordered" BorderWidth="1" GridLines="Both">
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Описание..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Результат..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Темы..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 2..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 3..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 4..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 5..." />
                    </asp:TableHeaderRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:DropDownList runat="server" ID="DropDown_PCTR" CssClass="form-control" DataSourceID="SqlDataSourceCompetences" DataTextField="Code" DataValueField="Description" />
                            <%--<asp:TextBox runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" id="PCTRDiscription"/>--%>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" id="PCTRRezult" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" id="PCTRTopics" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" id="PCTR2" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" id="PCTR3" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" id="PCTR4" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" id="PCTR5" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Button runat="server" CssClass="btn light" ID="addRowInPCTReferatTable" Text="Добавить" OnClick="addRowInPCTReferatTable_Click" />
                <asp:Button runat="server" CssClass="btn light" ID="deleteRowInPCTReferatTable" Text="Удалить строку №" OnClick="deleteRowInPCTReferatTable_Click" />
                <asp:TextBox runat="server" CssClass="form-control" ID="RowNumberPCTR" TextMode="Number" />
            </div>
            <br />
            <strong>Темы рефератов:</strong><br />
            <div class="check_screen">
                <asp:TextBox runat="server" ID="ReferatTopics" CssClass="ckeditor" TextMode="MultiLine" />
            </div>
            <strong>Составитель:</strong> <asp:DropDownList runat="server" CssClass="form-control" ID="DeveloperReferat"  DataSourceID="SqlDataSourceUsers" DataTextField="UserName" DataValueField="UserId" />
            <strong>Дата:</strong> <asp:TextBox runat="server" ID="ReferatTopicsDate" CssClass="form-control" TextMode="Date" /><br />
            <br />
            <asp:Button runat="server" CssClass="btn light" ID="UpDateReferat_Button" Text="Редактировать блок" OnClick="UpDateReferat_Button_Click" />
        </div>
            
        <div class="tab-pane fade" id="kt">
            <h3 class="unselecteble">Элементы контроля</h3>
            <h4 class="unselecteble">Контрольные работы:</h4>
            <asp:Table runat="server" CssClass="table table-bordered" ID="PrilCompetenceTable_KT" BorderWidth="1" GridLines="Both">
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Описание</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Контролируемый результат обучения</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Контролируемые темы (разделы) дисциплины</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Контрольная работа</strong>" ColumnSpan="4" />
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Критерии оценивания</strong>" ColumnSpan="4"/>
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>2</strong>" />
                    <asp:TableHeaderCell Text="<strong>3</strong>" />
                    <asp:TableHeaderCell Text="<strong>4</strong>" />
                    <asp:TableHeaderCell Text="<strong>5</strong>" />
                </asp:TableHeaderRow>
            </asp:Table>
            <asp:HiddenField runat="server" ID="PCT_ktrowscount" Value="0" />
            <div>
                <asp:Table runat="server" CssClass="table table-bordered" BorderWidth="1" GridLines="Both">
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Описание..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Результат..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Темы..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 2..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 3..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 4..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 5..." />
                    </asp:TableHeaderRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:DropDownList runat="server" ID="DropDown_PCTKT" CssClass="form-control" DataSourceID="SqlDataSourceCompetences" DataTextField="Code" DataValueField="Description" />
                            <%--<asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTKTDiscription"/>--%>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTKTRezult" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTKTTopics" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTKT2" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTKT3" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTKT4" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTKT5" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Button runat="server" CssClass="btn light" ID="addRowInPCTKTTable" Text="Добавить" OnClick="addRowInPCTKTTable_Click" />
                <asp:Button runat="server" CssClass="btn light" ID="deleteRowInPCTKTTable" Text="Удалить" OnClick="deleteRowInPCTKTTable_Click" />
                <asp:TextBox runat="server" CssClass="form-control" ID="RowNumberPCTKt" TextMode="Number" />
            </div>
            <br />
            <strong>Темы контрольных работ:</strong><br />
            <div class="check_screen">
                <asp:TextBox runat="server" ID="KTTopics" CssClass="ckeditor" TextMode="MultiLine" />
            </div>
            <strong>Составитель:</strong> <asp:DropDownList runat="server" CssClass="form-control" ID="DeveloperKT" DataSourceID="SqlDataSourceUsers" DataTextField="UserName" DataValueField="UserId" />
            <strong>Дата:</strong> <asp:TextBox runat="server" ID="KTDate" CssClass="form-control" TextMode="Date" /><br />
            <br />
            <asp:Button runat="server" ID="UpDateKT_Button" CssClass="btn light" Text="Редактировать блок" OnClick="UpDateKT_Button_Click" />
        </div>
            
        <div class="tab-pane fade" id="prakrika">
            <h3 class="unselecteble">Элементы контроля</h3>
            <h4 class="unselecteble">Практические занятия:</h4>
            <asp:Table runat="server" CssClass="table table-bordered" ID="PrilCompetenceTable_Praktika" BorderWidth="1" GridLines="Both">
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Описание</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Контролируемый результат обучения</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Контролируемые темы (разделы) дисциплины</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Практическое занятие</strong>" ColumnSpan="4" />
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Критерии оценивания</strong>" ColumnSpan="4"/>
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>2</strong>" />
                    <asp:TableHeaderCell Text="<strong>3</strong>" />
                    <asp:TableHeaderCell Text="<strong>4</strong>" />
                    <asp:TableHeaderCell Text="<strong>5</strong>" />
                </asp:TableHeaderRow>
            </asp:Table>
            <asp:HiddenField runat="server" ID="PCT_praktikarowscount" Value="0" />
            <div>
                <asp:Table runat="server" CssClass="table table-bordered"  BorderWidth="1" GridLines="Both">
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Описание..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Результат..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Темы..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 2..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 3..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 4..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 5..." />
                    </asp:TableHeaderRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:DropDownList runat="server" ID="DropDownPCTPR" CssClass="form-control" DataSourceID="SqlDataSourceCompetences" DataTextField="Code" DataValueField="Description" />
                            <%--<asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTPDiscription"/>--%>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTPRezult" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTPTopics" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTP2" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTP3" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTP4" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTP5" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Button runat="server" CssClass="btn light" ID="addRowInPCTPraktikaTable" Text="Добавить" OnClick="addRowInPCTPraktikaTable_Click" />
                <asp:Button runat="server" CssClass="btn light" ID="deleteRowInPCTPraktikaTable" Text="Удалить" OnClick="deleteRowInPCTPraktikaTable_Click" />
                <asp:TextBox runat="server" CssClass="form-control" ID="RowNumberPCTP" TextMode="Number" />
            </div>
            <br />
            <strong>Темы практических занятий:</strong><br />
            <div class="check_screen">
                <asp:TextBox runat="server" ID="PraktikTopics" CssClass="ckeditor" TextMode="MultiLine"/>
            </div>
            <strong>Составитель:</strong> <asp:DropDownList runat="server" CssClass="form-control" ID="DeveloperPraktika" DataSourceID="SqlDataSourceUsers" DataTextField="UserName" DataValueField="UserId"  />
            <strong>Дата:</strong> <asp:TextBox runat="server" ID="PraktikaTopicsDate" CssClass="form-control" TextMode="Date" /><br />
            <br />
            <asp:Button runat="server" CssClass="btn light" ID="UpDatePraktika_Button" Text="Редактировать блок" OnClick="UpDatePraktika_Button_Click" />
        </div>
            
        <div class="tab-pane fade" id="test">
            <h3 class="unselecteble">Элементы контроля</h3>
            <h4 class="unselecteble">Тесты:</h4>
            <asp:Table runat="server" CssClass="table table-bordered" ID="PrilCompetenceTable_Test" BorderWidth="1" GridLines="Both">
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Описание</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Контролируемый результат обучения</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Контролируемые темы (разделы) дисциплины</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Тест</strong>" ColumnSpan="4" />
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Критерии оценивания</strong>" ColumnSpan="4"/>
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>2</strong>" />
                    <asp:TableHeaderCell Text="<strong>3</strong>" />
                    <asp:TableHeaderCell Text="<strong>4</strong>" />
                    <asp:TableHeaderCell Text="<strong>5</strong>" />
                </asp:TableHeaderRow>
            </asp:Table>
            <asp:HiddenField runat="server" ID="PCT_testrowscount" Value="0" />
            <div>
                <asp:Table runat="server" CssClass="table table-bordered" BorderWidth="1" GridLines="Both">
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Описание..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Результат..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Темы..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 2..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 3..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 4..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 5..." />
                    </asp:TableHeaderRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:DropDownList runat="server" ID="DropDown_PCTT" CssClass="form-control" DataSourceID="SqlDataSourceCompetences" DataTextField="Code" DataValueField="Description" />
                            <%--<asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTTDiscription"/>--%>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTTRezult" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTTTopics" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTT2" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTT3" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTT4" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTT5" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Button runat="server" CssClass="btn light" ID="addRowInPCTtestTable" Text="Добавить" OnClick="addRowInPCTtestTable_Click" />
                <asp:Button runat="server" CssClass="btn light" ID="deleteRowInPCTtestTable" Text="Удалить" OnClick="deleteRowInPCTtestTable_Click" />      
                <asp:TextBox runat="server" CssClass="form-control" ID="RowNumberPCTT" TextMode="Number" />
            </div>
            <br />
            <strong>Фонд тестовых заданий:</strong><br />
            <div class="check_screen">
                <asp:TextBox runat="server" ID="TestsFond" TextMode="MultiLine" CssClass="ckeditor"/>
            </div>
            <strong>Составитель:</strong> <asp:DropDownList runat="server" ID="DeveloperTest" CssClass="form-control" DataSourceID="SqlDataSourceUsers" DataTextField="UserName" DataValueField="UserId" />
            <strong>Дата:</strong> <asp:TextBox runat="server" ID="TestsFondDate" CssClass="form-control" TextMode="Date" /><br />
            <br />
            <asp:Button runat="server" CssClass="btn light" ID="UpDateTests_Button" Text="Редактировать блок" OnClick="UpDateTests_Button_Click" />
        </div>

        <div class="tab-pane fade" id="curs">
            <h3 class="unselecteble">Элементы контроля</h3>
            <h4 class="unselecteble">Курсовые работы:</h4>
            <asp:Table runat="server" CssClass="table table-bordered" ID="PrilCompetenceTable_Curs" BorderWidth="1" GridLines="Both">
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Описание</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Контролируемый результат обучения</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Контролируемые темы (разделы) дисциплины</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Тест</strong>" ColumnSpan="4" />
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Критерии оценивания</strong>" ColumnSpan="4"/>
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>2</strong>" />
                    <asp:TableHeaderCell Text="<strong>3</strong>" />
                    <asp:TableHeaderCell Text="<strong>4</strong>" />
                    <asp:TableHeaderCell Text="<strong>5</strong>" />
                </asp:TableHeaderRow>
            </asp:Table>
            <asp:HiddenField runat="server" ID="PCT_cursrowscount" Value="0" />
            <div>
                <asp:Table runat="server" CssClass="table table-bordered" BorderWidth="1" GridLines="Both">
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Код" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Результат" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Темы" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 2" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 3" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 4" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 5" />
                    </asp:TableHeaderRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:DropDownList runat="server" ID="DropDown_PCTC" CssClass="form-control" DataSourceID="SqlDataSourceCompetences" DataTextField="Code" DataValueField="Description" />
                            <%--<asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTTDiscription"/>--%>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTCRezult" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTCTopics" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTC2" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTC3" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTC4" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTC5" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Button runat="server" CssClass="btn light" ID="addRowPCTcursTable" Text="Добавить" OnClick="addRowPCTcursTable_Click"/>
                <asp:Button runat="server" CssClass="btn light" ID="deleteRowPCTcursTable" Text="Удалить" OnClick="deleteRowPCTcursTable_Click" />      
                <asp:TextBox runat="server" CssClass="form-control" ID="PCTCursRowsNumber" TextMode="Number" />    
            </div>
            <br />
            <strong>Темы курсовых работ:</strong><br />
            <div class="check_screen">
                <asp:TextBox runat="server" ID="CursesFond" Width="100%" Height="150px" TextMode="MultiLine" CssClass="ckeditor" />
            </div>
            <strong>Составитель:</strong> <asp:DropDownList runat="server" style="margin-top:5px" ID="DeveloperCurs" CssClass="form-control" DataSourceID="SqlDataSourceUsers" DataTextField="UserName" DataValueField="UserId" />
            <strong>Дата:</strong> <asp:TextBox runat="server" ID="CursDate" style="margin-top:5px" CssClass="form-control" TextMode="Date" /><br />
            <br />
            <asp:Button runat="server" CssClass="btn light" ID="Update_Curs_Button" Text="Редактировать блок" OnClick="Update_Curs_Button_Click" />
        </div>
        
    </div>
    <a href="#" class="scrollup">Наверх</a>
    </div>
</asp:Content>


