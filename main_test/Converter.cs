﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Text;
using System.IO;
using System.Web.UI.WebControls;

namespace main_test
{
    public class Converter
    {
        public string ToHtmlCode (Control control)
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            control.RenderControl(htw);

            string html = sb.ToString();

            return html;
        }

        //в следующем блоке приведен алгоритм, который из объекта программы "Pro" собирает массив html-кодов
        //для последующей печати
        #region htmlFormat
        private string InCenterP(string text)
        {
            string html;
            html = "<p style=\"text-align: center; font-size: 14pt; font-family: 'Times New Roman';\">" + text + "</p>";
            return html;
        }

        private string InRightP(string text)
        {
            string html;
            html = "<p style=\"text-align: right; font-size: 14pt; font-family: 'Times New Roman';\">" + text + "</p>";
            return html;
        }

        private string InDefoultDiv(string text)
        {
            string htmlOut;
            htmlOut = "<div style=\"text-align: justify; font-size: 14pt; font-family: 'Times New Roman';\">" + text + "</div>";
            return htmlOut;
        }

        private string InLeftDiv(string text)
        {
            string htmlOut;
            htmlOut = "<div style=\"text-align: left; font-size: 14pt; font-family: 'Times New Roman';\">" + text + "</div>";
            return htmlOut;
        }

        private string InTableDiv(string text)
        {
            string htmlOut;
            text = text.Replace("px", "pt");
            htmlOut = "<div style=\"font-size: 12pt; font-family: 'Times New Roman';\">" + text + "</div>";
            return htmlOut;
        }

        private string oneline_right(string text)
        {
            string htmlOut;
            htmlOut = text;
            return htmlOut;
        }

        private string InCenterPBold(string text)
        {
            string html;
            html = "<p style=\"text-align: center; font-size: 16pt; font-family: 'Times New Roman'; font-weight: bold;\">" + text + "</p>";
            return html;
        }
        private string InCenterPBoldMini(string text)
        {
            string html;
            html = "<p style=\"text-align: center; font-size: 14pt; font-family: 'Times New Roman'; font-weight: bold;\">" + text + "</p>";
            return html;
        }
        
        public List<string> ToHtmlList(Pro pro)
        {
            List<string> htmles = new List<string>();

            BaseWork task = new BaseWork();

            //для кастомного форматирования html в теге <style>
            string alCenter = "text-align: center; ";
            string font = "font-size: 14pt; font-family: 'Times New Roman'; ";
            
            //в случае если в поле произойдет вставка с плохим форматированием
            string spanReplace = "<span style=\"font-size: 14pt; font-family: 'Times New Roman';\"";

            //для разрыва страницы
            string page_break = "<div style=\"page-break-after: always;\"></div>";

            //форматирую в html и складываю в список
            string instText = "<div style=\"" + alCenter + font + "\">" + pro.Institute.Replace("<p", "<p style=\"margin-top:0px; margin-bottom:0px;\"") + "</div>";
            htmles.Add(instText);

            string approve = "Утверждаю<br/>" + pro.Approver_Post + "<br/>" +
                pro.Approver_Name + "<br/>" +
                Convert.ToDateTime(pro.Approve_Date).ToString("D");
            approve = InRightP(approve);
            htmles.Add(approve);

            htmles.Add("<p>&nbsp;</p>");

            htmles.Add(InCenterP("<strong>РАБОЧАЯ ПРОГРАММА ДИСЦИПЛИНЫ</strong>"));

            string disText = InCenterP(pro.Discipline);
            htmles.Add(disText);

            htmles.Add("<p>&nbsp;</p>");

            htmles.Add(InCenterP("Направление подготовки"));
            htmles.Add(InCenterP("<strong>" + pro.Training_Direction + "</strong>"));
            htmles.Add(InCenterP("Профиль</p>"));
            htmles.Add(InCenterP("<strong>\"" + pro.Profil + "\"</strong>"));
            htmles.Add(InCenterP("Квалификация (степень) выпускника"));
            htmles.Add(InCenterP("<strong>\"" + pro.Graduate_Qualification+ "\"</strong>"));

            htmles.Add("<p>&nbsp;</p>");

            htmles.Add(InCenterP("Форма обучения"));
            htmles.Add(InCenterP("<strong>\"" + pro.Training_Form + "\"</strong>"));

            htmles.Add("<p>&nbsp;</p>");

            string cityText = InCenterP("<strong>" + pro.City + " " + Convert.ToDateTime(pro.Approve_Date).Year.ToString() + " г." + "</strong>");
            htmles.Add(cityText);

            htmles.Add(page_break);

            htmles.Add(InCenterP("<strong>1. Цели освоения дисциплины.</strong>"));

            string disObj = InDefoultDiv(pro.ObjectivesOfDiscipline);
            htmles.Add(disObj);

            htmles.Add(InCenterP("<strong>2. Место дисциплины в структуре ООП.</strong>"));

            string inOOP = InDefoultDiv(pro.DisciplineInOOP);
            htmles.Add(inOOP);

            htmles.Add(InCenterP("<strong>3. Перечень планируемых результатов обучения по дисциплине (модулю), соотнесенные с планируемыми результатами освоения образовательной программы.</strong>"));
            htmles.Add(InDefoultDiv("В результате освоения дисциплины (модуля) у обучающихся формируются следующие компетенции и должны быть достигнуты следующие результаты обучения как этап формирования соответствующих компетенций:"));

            if (pro.PlanTable.Count != 0)
            {
                Table planTable = new Table();
                TableRow row1 = new TableRow();
                TableCell cell1 = new TableCell();
                TableCell cell2 = new TableCell();
                TableCell cell3 = new TableCell();
                cell1.HorizontalAlign = HorizontalAlign.Center;
                cell2.HorizontalAlign = HorizontalAlign.Center;
                cell3.HorizontalAlign = HorizontalAlign.Center;
                cell1.Text = "<strong>Код компетенции</strong>";
                cell2.Text = "<strong>В результате освоения образовательной программы обучающийся должен обладать</strong>";
                cell3.Text = "<strong>Перечень планируемых результатов обучения по дисциплине</strong>";
                row1.Cells.Add(cell1);
                row1.Cells.Add(cell2);
                row1.Cells.Add(cell3);
                planTable.Rows.Add(row1);

                for (int i = 0; i < pro.PlanTable.Count; i++)
                {
                    TableRow row = new TableRow();
                    TableCell cell_1 = new TableCell();
                    TableCell cell_2 = new TableCell();
                    TableCell cell_3 = new TableCell();
                    cell_1.Text = pro.PlanTable[i].Сompetence;
                    cell_2.Text = pro.PlanTable[i].Ability;
                    cell_3.Text = pro.PlanTable[i].Enumeration;
                    row.Cells.Add(cell_1);
                    row.Cells.Add(cell_2);
                    row.Cells.Add(cell_3);
                    planTable.Rows.Add(row);
                }
                htmles.Add(InTableDiv(ToHtmlCode(planTable)));
            }

            htmles.Add(InCenterP("<strong>4. Структура и содержание дисциплины.</strong>"));

            string disStr = InDefoultDiv(pro.DisciplineStructureContent);
            htmles.Add(disStr);

            htmles.Add(InCenterP("<strong>Содержание разделов дисциплины.</strong>"));

            string semStr = InDefoultDiv(pro.SemesteresContent.Replace("<span", spanReplace));
            htmles.Add(semStr);

            htmles.Add(InCenterP("<strong>5. Образовательные технологии.</strong>"));
            string EduTechnology = InDefoultDiv(pro.EducationalTechnology); //Образовательные технологии
            htmles.Add(EduTechnology);

            htmles.Add(InCenterP("<strong>6. Оценочные средства для текущего контроля успеваемости, промежуточной аттестации по итогам освоения дисциплины и учебно-методическое обеспечение самостоятельной работы студентов.</strong>"));
            //htmles.Add(InDefoultDiv("<p></p>"));
            string MonitoringToolsTech = InDefoultDiv(pro.MonitoringTools_Description); //Оценочные средства для текущего контроля успеваемости
            htmles.Add(MonitoringToolsTech);

            if (pro.FondTable.Count != 0)
            {
                Table fondTable = new Table();
                TableRow row1 = new TableRow();
                TableCell cell1 = new TableCell();
                TableCell cell2 = new TableCell();
                cell1.HorizontalAlign = HorizontalAlign.Center;
                cell2.HorizontalAlign = HorizontalAlign.Center;
                cell1.Text = "<strong>Код компетенции</strong>";
                cell2.Text = "<strong>В результате освоения образовательной программы обучающийся должен обладать</strong>";
                row1.Cells.Add(cell1);
                row1.Cells.Add(cell2);
                fondTable.Rows.Add(row1);

                for (int i = 0; i < pro.FondTable.Count; i++)
                {
                    TableRow row = new TableRow();
                    TableCell cell_1 = new TableCell();
                    TableCell cell_2 = new TableCell();
                    cell_1.Text = pro.FondTable[i].Сompetence;
                    cell_2.Text = pro.FondTable[i].Ability;
                    row.Cells.Add(cell_1);
                    row.Cells.Add(cell_2);
                    fondTable.Rows.Add(row);
                }

                htmles.Add(InCenterP("<strong>6.1 Фонд оценочных средств для проведения промежуточной аттестации обучающихся по дисциплине (модулю).</strong>"));
                htmles.Add(InLeftDiv("6.1.1. Перечень компетенций с указанием этапов их формирования в процессе освоения образовательной программы.<br>В результате освоения дисциплины(модуля) формируются следующие компетенции:"));

                htmles.Add(InTableDiv(ToHtmlCode(fondTable)));

                htmles.Add(InLeftDiv("В процессе освоения образовательной программы данные компетенции, в том числе их отдельные компоненты, формируются поэтапно в ходе освоения обучающимися дисциплин (модулей), практик в соответствии с учебным планом и календарным графиком учебного процесса."));
            }

            if (pro.CriterionsTable.Count != 0)
            {
                Table CriterionsTable = new Table();
                TableRow row1 = new TableRow();
                TableRow row2 = new TableRow();
                TableCell cell11 = new TableCell();
                TableCell cell12 = new TableCell();
                TableCell cell13 = new TableCell();
                TableCell cell21 = new TableCell();
                TableCell cell22 = new TableCell();
                TableCell cell23 = new TableCell();
                TableCell cell24 = new TableCell();
                cell11.RowSpan = 2;
                cell12.RowSpan = 2;
                cell13.ColumnSpan = 4;
                cell11.Text = "<strong>Описание компетенции</strong>";
                cell12.Text = "<strong>Показатель</strong>";
                cell13.Text = "<strong>Критерий оценивания</strong>";
                cell21.Text = "<strong>2</strong>";
                cell22.Text = "<strong>3</strong>";
                cell23.Text = "<strong>4</strong>";
                cell24.Text = "<strong>5</strong>";

                row1.Cells.Add(cell11);
                row1.Cells.Add(cell12);
                row1.Cells.Add(cell13);
                row2.Cells.Add(cell21);
                row2.Cells.Add(cell22);
                row2.Cells.Add(cell23);
                row2.Cells.Add(cell24);

                CriterionsTable.Rows.Add(row1);
                CriterionsTable.Rows.Add(row2);

                for (int i = 0; i < pro.CriterionsTable.Count; i++)
                {
                    TableRow row = new TableRow();
                    for (int j = 0; j < 6; j++)
                    {
                        TableCell cell = new TableCell();
                        row.Cells.Add(cell);
                    }
                    row.Cells[0].Text = pro.CriterionsTable[i].Competence;
                    row.Cells[1].Text = pro.CriterionsTable[i].ToKnow_Description;
                    row.Cells[2].Text = pro.CriterionsTable[i].ToKnow[0];
                    row.Cells[3].Text = pro.CriterionsTable[i].ToKnow[1];
                    row.Cells[4].Text = pro.CriterionsTable[i].ToKnow[2];
                    row.Cells[5].Text = pro.CriterionsTable[i].ToKnow[3];
                    CriterionsTable.Rows.Add(row);

                    TableRow row_1 = new TableRow();
                    for (int j = 0; j < 6; j++)
                    {
                        TableCell cell = new TableCell();
                        row_1.Cells.Add(cell);
                    }
                    row_1.Cells[0].Text = "-//-//-";
                    row_1.Cells[1].Text = pro.CriterionsTable[i].ToDo_Description;
                    row_1.Cells[2].Text = pro.CriterionsTable[i].ToDo[0];
                    row_1.Cells[3].Text = pro.CriterionsTable[i].ToDo[1];
                    row_1.Cells[4].Text = pro.CriterionsTable[i].ToDo[2];
                    row_1.Cells[5].Text = pro.CriterionsTable[i].ToDo[3];
                    CriterionsTable.Rows.Add(row_1);

                    TableRow row_2 = new TableRow();
                    for (int j = 0; j < 6; j++)
                    {
                        TableCell cell = new TableCell();
                        row_2.Cells.Add(cell);
                    }
                    row_2.Cells[0].Text = "-//-//-";
                    row_2.Cells[1].Text = pro.CriterionsTable[i].ToMaster_Description;
                    row_2.Cells[2].Text = pro.CriterionsTable[i].ToMaster[0];
                    row_2.Cells[3].Text = pro.CriterionsTable[i].ToMaster[1];
                    row_2.Cells[4].Text = pro.CriterionsTable[i].ToMaster[2];
                    row_2.Cells[5].Text = pro.CriterionsTable[i].ToMaster[3];
                    CriterionsTable.Rows.Add(row_2);
                    
                }

                htmles.Add(InCenterP("<strong>Описание показателей и критериев оценивания компетенций, формируемых по итогам освоения дисциплины (модуля), описание шкал оценивания.</strong>"));
                htmles.Add(InDefoultDiv("<p></p>"));
                htmles.Add(InLeftDiv("Показателем оценивания компетенций на различных этапах их формирования является достижение обучающимися планируемых результатов обучения по дисциплине (модулю)."));
                htmles.Add(InTableDiv(ToHtmlCode(CriterionsTable)));
            }

            htmles.Add(InCenterP("Шкалы оценивания результатов промежуточной аттестации и их описание:"));
            htmles.Add(InLeftDiv("<strong> Форма промежуточной аттестации: зачет.</strong>"));
            string ZachDescrip = InDefoultDiv(pro.ZachetDescription);
            htmles.Add(ZachDescrip);

            string zachTable = InDefoultDiv("<table width='100%'><tr><td><strong>Шкала оценивания</strong></td><td><strong>Описание</strong></td><tr><td>Зачтено</td><td>" + pro.ZachetTable.ZachetDone + "</td></tr><tr><td>Не зачтено</td><td>" + pro.ZachetTable.ZachetUnDone + "</td></tr></table>");
            htmles.Add(zachTable);

            htmles.Add(InLeftDiv("<strong>Форма промежуточной аттестации: экзамен.</strong>"));
            string exDescrip = InDefoultDiv(pro.ExamDescription);
            htmles.Add(exDescrip);

            string exTable = InDefoultDiv("<table width='100%'><tr><td><strong>Шкала оценивания</strong></td><td><strong>Описание</strong></td></tr><tr><td>Отлично</td><td>" + pro.ExamTable.AttestatGreat + "</td></tr><tr><td>Хорошо</td><td>" + pro.ExamTable.AttestatGood + "</td></tr><tr><td>Удовлетворительно</td><td>" + pro.ExamTable.AttestatSatisfactorily + "</td></tr><tr><td>Неудовлетворительно</td><td>" + pro.ExamTable.AttestatNotSatisfactorily + "</td></tr></table>");
            htmles.Add(exTable);

            htmles.Add(InDefoultDiv("<strong>Фонды оценочных средств представлены в приложении 2 к рабочей программе.</strong>"));
            htmles.Add(InCenterP("<strong>7. Учебно-методическое и информационное обеспечение дисциплины.</strong>"));
            htmles.Add(InLeftDiv("<strong>а) Основная литература:</strong>"));
            string BasicLiter = InDefoultDiv(pro.BasicLiterature);
            htmles.Add(BasicLiter);
            htmles.Add(InLeftDiv("<strong>б) Дополнительная литература:</strong>"));
            string AddLiter = InDefoultDiv(pro.AdditionalLiterature);
            htmles.Add(AddLiter);
            htmles.Add(InLeftDiv("<strong>в) Программное обеспечение и интернет-ресурсы: </strong>"));
            string ITRes = InDefoultDiv(pro.ITResources);
            htmles.Add(ITRes);

            htmles.Add(InCenterP("<strong>8. Материально-техническое обеспечение дисциплины.</strong>"));
            string MatTechSup = InDefoultDiv(pro.MaterialTechnicalSupport);
            htmles.Add(MatTechSup);

            htmles.Add(InCenterP("<strong>9. Методические рекомендации для самостоятельной работы студентов</strong>"));
            string StudRec = InDefoultDiv(pro.StudentRecommendations);
            htmles.Add(StudRec);

            htmles.Add(InCenterP("<strong>10. Методические рекомендации для преподавателя</strong>"));
            string TecherRec = InDefoultDiv(pro.TeacherRecommendations);
            htmles.Add(TecherRec);

            htmles.Add(page_break);
            htmles.Add(InDefoultDiv("Программа составлена в соответствии с Федеральным государственным образовательным стандартом высшего образования по направлению подготовки <strong>«" + pro.Training_Direction + "»</strong>"));

            if (pro.AdditionalDrafters.Count != 0)
            {
                int count = Convert.ToInt32(pro.AdditionalDrafters.Count);
                htmles.Add(InLeftDiv("<strong>Программу составили:</strong>"));
                htmles.Add(InLeftDiv(pro.DrafterStatus + "                    						" + "/" + oneline_right(pro.DrafterName) + "/"));
                for (int i = 0; i < count; i++)
                {
                    htmles.Add(InLeftDiv(pro.AdditionalDrafters[i].Status + "   " + "/" + oneline_right(task.GetUserName(pro.AdditionalDrafters[i].UserId)) + "/"));
                }
            }
            else
            {
                htmles.Add(InLeftDiv("<strong>Программу составил/а:</strong>"));
                htmles.Add(InLeftDiv(pro.DrafterStatus + "  " + "/" + oneline_right(pro.DrafterName) + "/"));
            }



            htmles.Add(InLeftDiv("<strong>Программа утверждена на заседании кафедры «" + pro.Department + "»</strong>" + Convert.ToDateTime(pro.DraftDate).ToString("D") + ", " + pro.Protocol + "."));
            htmles.Add(InLeftDiv(pro.DepartmentHeadStatus + "                    						" + "/" + oneline_right(pro.DepartmentHeadName) + "/"));

            htmles.Add(InLeftDiv("<strong>Программа согласована:</strong>"));
            htmles.Add(InLeftDiv(pro.AgreementDepartmentHeadStatus + "  " + "/" + oneline_right(pro.AgreementDepartmentHeadName) + "/"));
            htmles.Add(InLeftDiv(pro.AgreementDirectorStatus + "    " + "/" + oneline_right(pro.AgreementDirectorName) + "/"));

            htmles.Add(page_break);

            if (pro.DSTTable.Count != 0)
            {
                Table DSCT = new Table();

                TableRow row1 = new TableRow();
                TableCell cell11 = new TableCell();
                TableCell cell12 = new TableCell();
                TableCell cell13 = new TableCell();
                TableCell cell14 = new TableCell();
                TableCell cell15 = new TableCell();
                TableCell cell16 = new TableCell();
                cell11.RowSpan = 2;
                cell12.RowSpan = 2;
                cell13.RowSpan = 2;
                cell14.RowSpan = 2;
                cell15.ColumnSpan = 5;
                cell16.ColumnSpan = 6;
                cell11.HorizontalAlign = HorizontalAlign.Center;
                cell12.HorizontalAlign = HorizontalAlign.Center;
                cell13.HorizontalAlign = HorizontalAlign.Center;
                cell14.HorizontalAlign = HorizontalAlign.Center;
                cell15.HorizontalAlign = HorizontalAlign.Center;
                cell16.HorizontalAlign = HorizontalAlign.Center;
                cell11.Text = "<strong>n/n</strong>";
                cell12.Text = "<strong>Раздел</strong>";
                cell13.Text = "<strong>Семестр</strong>";
                cell14.Text = "<strong>Неделя семестра</strong>";
                cell15.Text = "<strong>Виды учебной работы, включая самостоятельную работу студентов, и трудоемкость в часах</strong>";
                cell16.Text = "<strong>Виды самостоятельной работы студентов</strong>";
                row1.Cells.Add(cell11);
                row1.Cells.Add(cell12);
                row1.Cells.Add(cell13);
                row1.Cells.Add(cell14);
                row1.Cells.Add(cell15);
                row1.Cells.Add(cell16);

                TableRow row2 = new TableRow();
                for (int j = 0; j < 11; j++)
                {
                    TableCell cell = new TableCell();
                    cell.HorizontalAlign = HorizontalAlign.Center;
                    row2.Cells.Add(cell);
                }
                row2.Cells[0].Text = "<strong>Л</strong>";
                row2.Cells[1].Text = "<strong>П/С</strong>";
                row2.Cells[2].Text = "<strong>Лаб</strong>";
                row2.Cells[3].Text = "<strong>СРС</strong>";
                row2.Cells[4].Text = "<strong>КСР</strong>";
                row2.Cells[5].Text = "<strong>К.Р</strong>";
                row2.Cells[6].Text = "<strong>К.П</strong>";
                row2.Cells[7].Text = "<strong>РГР</strong>";
                row2.Cells[8].Text = "<strong>Под. к л/р</strong>";
                row2.Cells[9].Text = "<strong>К/р</strong>";
                row2.Cells[10].Text = "<strong>Форма аттестации</strong>";

                DSCT.Rows.Add(row1);
                DSCT.Rows.Add(row2);

                for (int i = 0; i < pro.DSTTable.Count; i++)
                {
                    TableRow row = new TableRow();
                    DSCT.Rows.Add(row);
                    for (int j = 0; j < 15; j++)
                    {
                        TableCell cell = new TableCell();
                        row.Cells.Add(cell);
                    }
                    row.Cells[0].Text = "#";
                    row.Cells[1].Text = pro.DSTTable[i].Topic;
                    row.Cells[2].Text = pro.DSTTable[i].Semestr.ToString();
                    row.Cells[3].Text = pro.DSTTable[i].Week.ToString();
                    row.Cells[4].Text = pro.DSTTable[i].L.ToString();
                    row.Cells[5].Text = pro.DSTTable[i].PS.ToString();
                    row.Cells[6].Text = pro.DSTTable[i].Lab.ToString();
                    row.Cells[7].Text = pro.DSTTable[i].SRS.ToString();
                    row.Cells[8].Text = pro.DSTTable[i].KSR.ToString();
                    row.Cells[9].Text = pro.DSTTable[i].KR.ToString();
                    row.Cells[10].Text = pro.DSTTable[i].KP.ToString();
                    row.Cells[11].Text = pro.DSTTable[i].RGR.ToString();
                    row.Cells[12].Text = pro.DSTTable[i].PrepToLR;
                    row.Cells[13].Text = pro.DSTTable[i].KRend.ToString();
                    row.Cells[14].Text = pro.DSTTable[i].AttestatForm;
                }

                int g = 0;
                for (int i = 2; i < DSCT.Rows.Count; i++)
                {
                    for (int j = 2; j < 15; j++)
                        if (DSCT.Rows[i].Cells[j].Text == "0")
                            DSCT.Rows[i].Cells[j].Text = "";

                    if ((DSCT.Rows[i].Cells[1].Text == "Часы за семестр") || (DSCT.Rows[i].Cells[1].Text == "Часы за весь период"))
                    {
                        DSCT.Rows[i].Cells[0].Text = "";
                        g++;
                        continue;
                    }

                    DSCT.Rows[i].Cells[0].Text = (i - 1 - g).ToString();
                }

                htmles.Add(InRightP("Приложение 1"));
                htmles.Add(InCenterP("<strong>Структура и содержание дисциплины «" + pro.Discipline + "» по направлению подготовки «" + pro.Training_Direction + "»<br>(" + pro.Graduate_Qualification + ")</strong>"));

                htmles.Add(InTableDiv(ToHtmlCode(DSCT)));
            }

            htmles.Add(page_break);

            htmles.Add(InRightP("Приложение 2 к рабочей программе"));
            htmles.Add(instText);
            htmles.Add(InCenterP("Направление подготовки: " + pro.Training_Direction + ""));
            htmles.Add(InCenterP("ОП (профиль): " + pro.Profil + ""));
            htmles.Add(InCenterP("Форма обучения: " + pro.Training_Form + ""));
            htmles.Add(InCenterP("Вид профессиональной деятельности: научно-исследовательская; проектно-технологическая"));
            htmles.Add(InLeftDiv("Кафедра: " + pro.Department + ""));
            htmles.Add(InCenterPBold("ФОНД ОЦЕНОЧНЫХ СРЕДСТВ"));
            htmles.Add(InCenterPBold("ПО ДИСЦИПЛИНЕ"));
            htmles.Add(InCenterPBold(pro.Discipline));
            htmles.Add(InCenterP("Состав: 1. Паспорт фонда оценочных средств"));
            htmles.Add(InCenterP("2. Описание оценочных средств"));
            htmles.Add("<p>&nbsp;</p>");

            if (pro.AdditionalDrafters.Count != 0)
            {
                int count = Convert.ToInt32(pro.AdditionalDrafters.Count);
                htmles.Add(InCenterP("<strong>Составители:</strong>"));
                htmles.Add(InCenterP("<strong>" + pro.DrafterStatus + ", " + pro.DrafterName + "</strong>"));
                for (int i = 0; i < count; i++)
                {
                    htmles.Add(InCenterP("<strong>" + pro.AdditionalDrafters[i].Status + ", " + task.GetUserName(pro.AdditionalDrafters[i].UserId) + "</strong>"));
                }
            }
            else
            {
                htmles.Add(InCenterP("<strong>Составитель:</strong>"));
                htmles.Add(InCenterP("<strong>" + pro.DrafterStatus + ", " + pro.DrafterName + "</strong>"));
            }
            htmles.Add("<p>&nbsp;</p>");
            htmles.Add(InCenterP(pro.City + ", " + Convert.ToDateTime(pro.Approve_Date).Year.ToString() + " г."));

            htmles.Add(page_break);
            
            if (pro.PFT_1.Count != 0)
            {
                Table pft_1 = new Table();
                TableRow row1 = new TableRow();
                TableCell cell11 = new TableCell();
                cell11.ColumnSpan = 6;
                cell11.Text = "В процессе освоения данной дисциплины студент формирует и демонстрирует следующие <strong>универсальные</strong> компетенции:";
                row1.Cells.Add(cell11);

                TableRow row2 = new TableRow();
                TableCell cell21 = new TableCell();
                TableCell cell22 = new TableCell();
                TableCell cell23 = new TableCell();
                TableCell cell24 = new TableCell();
                TableCell cell25 = new TableCell();
                cell21.ColumnSpan = 2;
                cell21.HorizontalAlign = HorizontalAlign.Center;
                cell22.HorizontalAlign = HorizontalAlign.Center;
                cell23.HorizontalAlign = HorizontalAlign.Center;
                cell24.HorizontalAlign = HorizontalAlign.Center;
                cell25.HorizontalAlign = HorizontalAlign.Center;
                cell21.Text = "<strong>Компетенции</strong>";
                cell22.Text = "<strong>Перечень компонентов</strong>";
                cell23.Text = "<strong>Технология формирования компетенции</strong>";
                cell24.Text = "<strong>Форма оценочного средства</strong>";
                cell25.Text = "<strong>Степени уровней освоения компетенций</strong>";
                row2.Cells.Add(cell21);
                row2.Cells.Add(cell22);
                row2.Cells.Add(cell23);
                row2.Cells.Add(cell24);
                row2.Cells.Add(cell25);
                
                pft_1.Rows.Add(row1);
                pft_1.Rows.Add(row2);

                for (int i = 0; i < pro.PFT_1.Count; i++)
                {
                    TableRow row = new TableRow();
                    for (int j = 0; j < 6; j++)
                    {
                        TableCell cell = new TableCell();
                        row.Cells.Add(cell);
                    }
                    row.Cells[0].Text = pro.PFT_1[i].Index;
                    row.Cells[1].Text = pro.PFT_1[i].Formul;
                    row.Cells[2].Text = pro.PFT_1[i].KomponentsList;
                    row.Cells[3].Text = pro.PFT_1[i].CompetenceTechnology;
                    row.Cells[4].Text = pro.PFT_1[i].AssessForm;
                    row.Cells[5].Text = pro.PFT_1[i].Steps;
                    pft_1.Rows.Add(row);
                }

                htmles.Add(InTableDiv(ToHtmlCode(pft_1)));
            }

            if (pro.PFT_2.Count != 0)
            {
                Table pft_2 = new Table();

                TableRow row1 = new TableRow();
                TableCell cell11 = new TableCell();
                cell11.ColumnSpan = 6;
                cell11.Text = "В процессе освоения данной дисциплины студент формирует и демонстрирует следующие <strong>общепрофессиональные</strong> компетенции:";
                row1.Cells.Add(cell11);

                TableRow row2 = new TableRow();
                TableCell cell21 = new TableCell();
                TableCell cell22 = new TableCell();
                TableCell cell23 = new TableCell();
                TableCell cell24 = new TableCell();
                TableCell cell25 = new TableCell();
                cell21.ColumnSpan = 2;
                cell21.HorizontalAlign = HorizontalAlign.Center;
                cell22.HorizontalAlign = HorizontalAlign.Center;
                cell23.HorizontalAlign = HorizontalAlign.Center;
                cell24.HorizontalAlign = HorizontalAlign.Center;
                cell25.HorizontalAlign = HorizontalAlign.Center;
                cell21.Text = "<strong>Компетенции</strong>";
                cell22.Text = "<strong>Перечень компонентов</strong>";
                cell23.Text = "<strong>Технология формирования компетенции</strong>";
                cell24.Text = "<strong>Форма оценочного средства</strong>";
                cell25.Text = "<strong>Степени уровней освоения компетенций</strong>";
                row2.Cells.Add(cell21);
                row2.Cells.Add(cell22);
                row2.Cells.Add(cell23);
                row2.Cells.Add(cell24);
                row2.Cells.Add(cell25);
                

                pft_2.Rows.Add(row1);
                pft_2.Rows.Add(row2);

                for (int i = 0; i < pro.PFT_2.Count; i++)
                {
                    TableRow row = new TableRow();
                    for (int j = 0; j < 6; j++)
                    {
                        TableCell cell = new TableCell();
                        row.Cells.Add(cell);
                    }
                    row.Cells[0].Text = pro.PFT_2[i].Index;
                    row.Cells[1].Text = pro.PFT_2[i].Formul;
                    row.Cells[2].Text = pro.PFT_2[i].KomponentsList;
                    row.Cells[3].Text = pro.PFT_2[i].CompetenceTechnology;
                    row.Cells[4].Text = pro.PFT_2[i].AssessForm;
                    row.Cells[5].Text = pro.PFT_2[i].Steps;
                    pft_2.Rows.Add(row);
                }

                htmles.Add(InTableDiv(ToHtmlCode(pft_2)));
            }

            if (pro.PFT_3.Count != 0)
            {
                Table pft_3 = new Table();

                TableRow row1 = new TableRow();
                TableCell cell11 = new TableCell();
                cell11.ColumnSpan = 6;
                cell11.Text = "В процессе освоения данной дисциплины студент формирует и демонстрирует следующие <strong>профессиональные</strong> компетенции:";
                row1.Cells.Add(cell11);

                TableRow row2 = new TableRow();
                TableCell cell21 = new TableCell();
                TableCell cell22 = new TableCell();
                TableCell cell23 = new TableCell();
                TableCell cell24 = new TableCell();
                TableCell cell25 = new TableCell();
                cell21.ColumnSpan = 2;
                cell21.HorizontalAlign = HorizontalAlign.Center;
                cell22.HorizontalAlign = HorizontalAlign.Center;
                cell23.HorizontalAlign = HorizontalAlign.Center;
                cell24.HorizontalAlign = HorizontalAlign.Center;
                cell25.HorizontalAlign = HorizontalAlign.Center;
                cell21.Text = "<strong>Компетенции</strong>";
                cell22.Text = "<strong>Перечень компонентов</strong>";
                cell23.Text = "<strong>Технология формирования компетенции</strong>";
                cell24.Text = "<strong>Форма оценочного средства</strong>";
                cell25.Text = "<strong>Степени уровней освоения компетенций</strong>";
                row2.Cells.Add(cell21);
                row2.Cells.Add(cell22);
                row2.Cells.Add(cell23);
                row2.Cells.Add(cell24);
                row2.Cells.Add(cell25);
                
                pft_3.Rows.Add(row1);
                pft_3.Rows.Add(row2);

                for (int i = 0; i < pro.PFT_3.Count; i++)
                {
                    TableRow row = new TableRow();
                    for (int j = 0; j < 6; j++)
                    {
                        TableCell cell = new TableCell();
                        row.Cells.Add(cell);
                    }
                    row.Cells[0].Text = pro.PFT_3[i].Index;
                    row.Cells[1].Text = pro.PFT_3[i].Formul;
                    row.Cells[2].Text = pro.PFT_3[i].KomponentsList;
                    row.Cells[3].Text = pro.PFT_3[i].CompetenceTechnology;
                    row.Cells[4].Text = pro.PFT_3[i].AssessForm;
                    row.Cells[5].Text = pro.PFT_3[i].Steps;
                    pft_3.Rows.Add(row);
                }

                htmles.Add(InTableDiv(ToHtmlCode(pft_3)));
            }

            htmles.Add(InCenterP("<strong>Перечень оценочных средств по дисциплине «" + pro.Discipline + "»</strong>"));
            if (pro.PAT.Count != 0)
            {
                Table pat = new Table();
                TableRow row1 = new TableRow();
                TableCell cell1 = new TableCell();
                TableCell cell2 = new TableCell();
                TableCell cell3 = new TableCell();
                TableCell cell4 = new TableCell();
                cell1.HorizontalAlign = HorizontalAlign.Center;
                cell2.HorizontalAlign = HorizontalAlign.Center;
                cell3.HorizontalAlign = HorizontalAlign.Center;
                cell4.HorizontalAlign = HorizontalAlign.Center;
                cell1.Text = "<strong>№ ОС</strong>";
                cell2.Text = "<strong>Наименование оценочного средства</strong>";
                cell3.Text = "<strong>Краткая характеристика оценочного средства</strong>";
                cell4.Text = "<strong>Представление оценочного средства в ФОС</strong>";
                row1.Cells.Add(cell1);
                row1.Cells.Add(cell2);
                row1.Cells.Add(cell3);
                row1.Cells.Add(cell4);
                pat.Rows.Add(row1);
                for (int i = 0; i < pro.PAT.Count; i++)
                {
                    TableRow row = new TableRow();
                    for (int j = 0; j < 4; j++)
                    {
                        TableCell cell = new TableCell();
                        row.Cells.Add(cell);
                    }
                    row.Cells[0].Text = (i + 1).ToString();
                    row.Cells[1].Text = pro.PAT[i].Name;
                    row.Cells[2].Text = pro.PAT[i].Spec;
                    row.Cells[3].Text = pro.PAT[i].InFOS;
                    pat.Rows.Add(row);
                }
                htmles.Add(InTableDiv(ToHtmlCode(pat)));
            }

            htmles.Add(page_break);

            if (pro.PCT_Zachet.Count != 0)
            {

                Table pct_z = new Table();
                TableRow row1 = new TableRow();
                TableRow row2 = new TableRow();
                TableRow row3 = new TableRow();
                TableCell cell11 = new TableCell();
                TableCell cell12 = new TableCell();
                TableCell cell13 = new TableCell();
                TableCell cell14 = new TableCell();
                cell11.RowSpan = 3;
                cell12.RowSpan = 3;
                cell13.RowSpan = 3;
                cell14.ColumnSpan = 2;
                cell11.Text = "<strong>Описание</strong>";
                cell12.Text = "<strong>Контролируемый результат обучения</strong>";
                cell13.Text = "<strong>Контролируемые темы (разделы) дисциплины</strong>";
                cell14.Text = "<strong>Недифференцированный зачет</strong>";
                row1.Cells.Add(cell11);
                row1.Cells.Add(cell12);
                row1.Cells.Add(cell13);
                row1.Cells.Add(cell14);
                pct_z.Rows.Add(row1);

                TableCell cell21 = new TableCell();
                cell21.ColumnSpan = 2;
                cell21.Text = "<strong>Критерии оценивания</strong>";
                row2.Cells.Add(cell21);
                pct_z.Rows.Add(row2);

                TableCell cell31 = new TableCell();
                TableCell cell32 = new TableCell();
                cell31.Text = "<strong>Зачтено</strong>";
                cell32.Text = "<strong>Не зачтено</strong>";
                row3.Cells.Add(cell31);
                row3.Cells.Add(cell32);
                pct_z.Rows.Add(row3);

                for (int i = 0; i < pro.PCT_Zachet.Count; i++)
                {
                    TableRow row = new TableRow();
                    for (int j = 0; j < 5; j++)
                    {
                        TableCell cell = new TableCell();
                        row.Cells.Add(cell);
                    }
                    row.Cells[0].Text = pro.PCT_Zachet[i].Discription;
                    row.Cells[1].Text = pro.PCT_Zachet[i].Rezult;
                    row.Cells[2].Text = pro.PCT_Zachet[i].Topics;
                    row.Cells[3].Text = pro.PCT_Zachet[i].Zachet;
                    row.Cells[4].Text = pro.PCT_Zachet[i].NeZachet;
                    pct_z.Rows.Add(row);
                }

                string date = "";
                try { date = Convert.ToDateTime(pro.Questions_Zachet.Date).ToString("D"); }
                catch { date = DateTime.Today.ToString("yyyy-MM-dd"); }
                htmles.Add(InLeftDiv("<strong>Зачет:</strong>"));

                string PrilFondTabZachet = InTableDiv(ToHtmlCode(pct_z));

                htmles.Add(PrilFondTabZachet);
                htmles.Add(InCenterPBoldMini("Вопросы к зачету:"));
                htmles.Add(InDefoultDiv(pro.Questions_Zachet.Questions));
                htmles.Add(InLeftDiv("Составитель ____________  " + pro.Questions_Zachet.Developer
                    + "<br><sub>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;(подпись)</sub>" +
                    "<br>" + date));
            }

            if (pro.PCT_Exam.Count != 0)
            {
                Table pct_e = new Table();
                TableRow row1 = new TableRow();
                TableRow row2 = new TableRow();
                TableRow row3 = new TableRow();

                TableCell cell11 = new TableCell();
                TableCell cell12 = new TableCell();
                TableCell cell13 = new TableCell();
                TableCell cell14 = new TableCell();
                cell11.RowSpan = 3;
                cell12.RowSpan = 3;
                cell13.RowSpan = 3;
                cell14.ColumnSpan = 4;
                cell11.Text = "<strong>Описание</strong>";
                cell12.Text = "<strong>Контролируемый результат обучения</strong>";
                cell13.Text = "<strong>Контролируемые темы (разделы) дисциплины</strong>";
                cell14.Text = "<strong>Экзамен</strong>";
                row1.Cells.Add(cell11);
                row1.Cells.Add(cell12);
                row1.Cells.Add(cell13);
                row1.Cells.Add(cell14);
                pct_e.Rows.Add(row1);

                TableCell cell21 = new TableCell();
                cell21.ColumnSpan = 4;
                cell21.Text = "<strong>Критерии оценивания</strong>";
                row2.Cells.Add(cell21);
                pct_e.Rows.Add(row2);

                TableCell cell31 = new TableCell();
                TableCell cell32 = new TableCell();
                TableCell cell33 = new TableCell();
                TableCell cell34 = new TableCell();
                cell31.Text = "<strong>2</strong>";
                cell32.Text = "<strong>3</strong>";
                cell33.Text = "<strong>4</strong>";
                cell34.Text = "<strong>5</strong>";
                row3.Cells.Add(cell31);
                row3.Cells.Add(cell32);
                row3.Cells.Add(cell33);
                row3.Cells.Add(cell34);
                pct_e.Rows.Add(row3);

                for (int i = 0; i < pro.PCT_Exam.Count; i++)
                {
                    TableRow row = new TableRow();
                    for (int j = 0; j < 7; j++)
                    {
                        TableCell cell = new TableCell();
                        row.Cells.Add(cell);
                    }
                    row.Cells[0].Text = pro.PCT_Exam[i].Discription;
                    row.Cells[1].Text = pro.PCT_Exam[i].Rezult;
                    row.Cells[2].Text = pro.PCT_Exam[i].Topics;
                    row.Cells[3].Text = pro.PCT_Exam[i].On2;
                    row.Cells[4].Text = pro.PCT_Exam[i].On3;
                    row.Cells[5].Text = pro.PCT_Exam[i].On4;
                    row.Cells[6].Text = pro.PCT_Exam[i].On5;
                    pct_e.Rows.Add(row);
                }

                string date = "";
                try { date = Convert.ToDateTime(pro.Questions_Exam.Date).ToString("D"); }
                catch { date = DateTime.Today.ToString("yyyy-MM-dd"); }
                htmles.Add(InLeftDiv("<strong>Экзамен:</strong>"));

                string PrilFondTabExam = InTableDiv(ToHtmlCode(pct_e));

                htmles.Add(PrilFondTabExam);
                htmles.Add(InCenterPBoldMini("Вопросы к экзамену:"));
                htmles.Add(InDefoultDiv(pro.Questions_Exam.Questions));
                htmles.Add(InLeftDiv("Составитель ____________  " + pro.Questions_Exam.Developer
                    + "<br><sub>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;(подпись)</sub>" +
                    "<br>" + date));
            }

            if (pro.PCT_Referat.Count != 0)
            {
                Table pct_r = new Table();
                TableRow row1 = new TableRow();
                TableRow row2 = new TableRow();
                TableRow row3 = new TableRow();

                TableCell cell11 = new TableCell();
                TableCell cell12 = new TableCell();
                TableCell cell13 = new TableCell();
                TableCell cell14 = new TableCell();
                cell11.RowSpan = 3;
                cell12.RowSpan = 3;
                cell13.RowSpan = 3;
                cell14.ColumnSpan = 4;
                cell11.Text = "<strong>Описание</strong>";
                cell12.Text = "<strong>Контролируемый результат обучения</strong>";
                cell13.Text = "<strong>Контролируемые темы (разделы) дисциплины</strong>";
                cell14.Text = "<strong>Реферат</strong>";
                row1.Cells.Add(cell11);
                row1.Cells.Add(cell12);
                row1.Cells.Add(cell13);
                row1.Cells.Add(cell14);
                pct_r.Rows.Add(row1);

                TableCell cell21 = new TableCell();
                cell21.ColumnSpan = 4;
                cell21.Text = "<strong>Критерии оценивания</strong>";
                row2.Cells.Add(cell21);
                pct_r.Rows.Add(row2);

                TableCell cell31 = new TableCell();
                TableCell cell32 = new TableCell();
                TableCell cell33 = new TableCell();
                TableCell cell34 = new TableCell();
                cell31.Text = "<strong>2</strong>";
                cell32.Text = "<strong>3</strong>";
                cell33.Text = "<strong>4</strong>";
                cell34.Text = "<strong>5</strong>";
                row3.Cells.Add(cell31);
                row3.Cells.Add(cell32);
                row3.Cells.Add(cell33);
                row3.Cells.Add(cell34);
                pct_r.Rows.Add(row3);

                for (int i = 0; i < pro.PCT_Referat.Count; i++)
                {
                    TableRow row = new TableRow();
                    for (int j = 0; j < 7; j++)
                    {
                        TableCell cell = new TableCell();
                        row.Cells.Add(cell);
                    }
                    row.Cells[0].Text = pro.PCT_Referat[i].Discription;
                    row.Cells[1].Text = pro.PCT_Referat[i].Rezult;
                    row.Cells[2].Text = pro.PCT_Referat[i].Topics;
                    row.Cells[3].Text = pro.PCT_Referat[i].On2;
                    row.Cells[4].Text = pro.PCT_Referat[i].On3;
                    row.Cells[5].Text = pro.PCT_Referat[i].On4;
                    row.Cells[6].Text = pro.PCT_Referat[i].On5;
                    pct_r.Rows.Add(row);
                }

                string date = "";
                try { date = Convert.ToDateTime(pro.Questions_Referat.Date).ToString("D"); }
                catch { date = DateTime.Today.ToString("yyyy-MM-dd"); }
                htmles.Add(InLeftDiv("<strong>Реферат:</strong>"));

                string PrilFondTabReferat = InTableDiv(ToHtmlCode(pct_r));

                htmles.Add(PrilFondTabReferat);
                htmles.Add(InCenterPBoldMini("Темы рефератов:"));
                htmles.Add(InDefoultDiv(pro.Questions_Referat.Questions));
                htmles.Add(InLeftDiv("Составитель ____________  " + pro.Questions_Referat.Developer
                    + "<br><sub>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;(подпись)</sub>" +
                    "<br>" + date));
            }

            if (pro.PCT_KT.Count != 0)
            {

                Table pct_kt = new Table();
                TableRow row1 = new TableRow();
                TableRow row2 = new TableRow();
                TableRow row3 = new TableRow();

                TableCell cell11 = new TableCell();
                TableCell cell12 = new TableCell();
                TableCell cell13 = new TableCell();
                TableCell cell14 = new TableCell();
                cell11.RowSpan = 3;
                cell12.RowSpan = 3;
                cell13.RowSpan = 3;
                cell14.ColumnSpan = 4;
                cell11.Text = "<strong>Описание</strong>";
                cell12.Text = "<strong>Контролируемый результат обучения</strong>";
                cell13.Text = "<strong>Контролируемые темы (разделы) дисциплины</strong>";
                cell14.Text = "<strong>Контрольная работа</strong>";
                row1.Cells.Add(cell11);
                row1.Cells.Add(cell12);
                row1.Cells.Add(cell13);
                row1.Cells.Add(cell14);
                pct_kt.Rows.Add(row1);

                TableCell cell21 = new TableCell();
                cell21.ColumnSpan = 4;
                cell21.Text = "<strong>Критерии оценивания</strong>";
                row2.Cells.Add(cell21);
                pct_kt.Rows.Add(row2);

                TableCell cell31 = new TableCell();
                TableCell cell32 = new TableCell();
                TableCell cell33 = new TableCell();
                TableCell cell34 = new TableCell();
                cell31.Text = "<strong>2</strong>";
                cell32.Text = "<strong>3</strong>";
                cell33.Text = "<strong>4</strong>";
                cell34.Text = "<strong>5</strong>";
                row3.Cells.Add(cell31);
                row3.Cells.Add(cell32);
                row3.Cells.Add(cell33);
                row3.Cells.Add(cell34);
                pct_kt.Rows.Add(row3);

                for (int i = 0; i < pro.PCT_KT.Count; i++)
                {
                    TableRow row = new TableRow();
                    for (int j = 0; j < 7; j++)
                    {
                        TableCell cell = new TableCell();
                        row.Cells.Add(cell);
                    }
                    row.Cells[0].Text = pro.PCT_KT[i].Discription;
                    row.Cells[1].Text = pro.PCT_KT[i].Rezult;
                    row.Cells[2].Text = pro.PCT_KT[i].Topics;
                    row.Cells[3].Text = pro.PCT_KT[i].On2;
                    row.Cells[4].Text = pro.PCT_KT[i].On3;
                    row.Cells[5].Text = pro.PCT_KT[i].On4;
                    row.Cells[6].Text = pro.PCT_KT[i].On5;
                    pct_kt.Rows.Add(row);
                }

                string date = "";
                try { date = Convert.ToDateTime(pro.Questions_KT.Date).ToString("D"); }
                catch { date = DateTime.Today.ToString("yyyy-MM-dd"); }
                htmles.Add(InLeftDiv("<strong>Контрольные работы:</strong>"));

                string PrilFondTabKT = InTableDiv(ToHtmlCode(pct_kt));

                htmles.Add(PrilFondTabKT);
                htmles.Add(InCenterPBoldMini("Темы контрольных работ:"));
                htmles.Add(InDefoultDiv(pro.Questions_KT.Questions));
                htmles.Add(InLeftDiv("Составитель ____________  " + pro.Questions_KT.Developer
                    + "<br><sub>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;(подпись)</sub>" +
                    "<br>" + date));
            }

            if (pro.PCT_Praktika.Count != 0)
            {

                Table pct_pr = new Table();
                TableRow row1 = new TableRow();
                TableRow row2 = new TableRow();
                TableRow row3 = new TableRow();

                TableCell cell11 = new TableCell();
                TableCell cell12 = new TableCell();
                TableCell cell13 = new TableCell();
                TableCell cell14 = new TableCell();
                cell11.RowSpan = 3;
                cell12.RowSpan = 3;
                cell13.RowSpan = 3;
                cell14.ColumnSpan = 4;
                cell11.Text = "<strong>Описание</strong>";
                cell12.Text = "<strong>Контролируемый результат обучения</strong>";
                cell13.Text = "<strong>Контролируемые темы (разделы) дисциплины</strong>";
                cell14.Text = "<strong>Практическое занятие</strong>";
                row1.Cells.Add(cell11);
                row1.Cells.Add(cell12);
                row1.Cells.Add(cell13);
                row1.Cells.Add(cell14);
                pct_pr.Rows.Add(row1);

                TableCell cell21 = new TableCell();
                cell21.ColumnSpan = 4;
                cell21.Text = "<strong>Критерии оценивания</strong>";
                row2.Cells.Add(cell21);
                pct_pr.Rows.Add(row2);

                TableCell cell31 = new TableCell();
                TableCell cell32 = new TableCell();
                TableCell cell33 = new TableCell();
                TableCell cell34 = new TableCell();
                cell31.Text = "<strong>2</strong>";
                cell32.Text = "<strong>3</strong>";
                cell33.Text = "<strong>4</strong>";
                cell34.Text = "<strong>5</strong>";
                row3.Cells.Add(cell31);
                row3.Cells.Add(cell32);
                row3.Cells.Add(cell33);
                row3.Cells.Add(cell34);
                pct_pr.Rows.Add(row3);

                for (int i = 0; i < pro.PCT_Praktika.Count; i++)
                {
                    TableRow row = new TableRow();
                    for (int j = 0; j < 7; j++)
                    {
                        TableCell cell = new TableCell();
                        row.Cells.Add(cell);
                    }
                    row.Cells[0].Text = pro.PCT_Praktika[i].Discription;
                    row.Cells[1].Text = pro.PCT_Praktika[i].Rezult;
                    row.Cells[2].Text = pro.PCT_Praktika[i].Topics;
                    row.Cells[3].Text = pro.PCT_Praktika[i].On2;
                    row.Cells[4].Text = pro.PCT_Praktika[i].On3;
                    row.Cells[5].Text = pro.PCT_Praktika[i].On4;
                    row.Cells[6].Text = pro.PCT_Praktika[i].On5;
                    pct_pr.Rows.Add(row);
                }

                string date = "";
                try { date = Convert.ToDateTime(pro.Questions_Praktika.Date).ToString("D"); }
                catch { date = DateTime.Today.ToString("yyyy-MM-dd"); }
                htmles.Add(InLeftDiv("<strong>Практические занятия:</strong>"));

                string PrilFondTabPractica = InTableDiv(ToHtmlCode(pct_pr));

                htmles.Add(PrilFondTabPractica);
                htmles.Add(InCenterPBoldMini("Темы практических занятий:"));
                htmles.Add(InDefoultDiv(pro.Questions_Praktika.Questions));
                htmles.Add(InLeftDiv("Составитель ____________  " + pro.Questions_Praktika.Developer
                    + "<br><sub>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;(подпись)</sub>" +
                    "<br>" + date));
            }

            if (pro.PCT_Test.Count != 0)
            {
                Table pct_t = new Table();
                TableRow row1 = new TableRow();
                TableRow row2 = new TableRow();
                TableRow row3 = new TableRow();

                TableCell cell11 = new TableCell();
                TableCell cell12 = new TableCell();
                TableCell cell13 = new TableCell();
                TableCell cell14 = new TableCell();
                cell11.RowSpan = 3;
                cell12.RowSpan = 3;
                cell13.RowSpan = 3;
                cell14.ColumnSpan = 4;
                cell11.Text = "<strong>Описание</strong>";
                cell12.Text = "<strong>Контролируемый результат обучения</strong>";
                cell13.Text = "<strong>Контролируемые темы (разделы) дисциплины</strong>";
                cell14.Text = "<strong>Тест</strong>";
                row1.Cells.Add(cell11);
                row1.Cells.Add(cell12);
                row1.Cells.Add(cell13);
                row1.Cells.Add(cell14);
                pct_t.Rows.Add(row1);

                TableCell cell21 = new TableCell();
                cell21.ColumnSpan = 4;
                cell21.Text = "<strong>Критерии оценивания</strong>";
                row2.Cells.Add(cell21);
                pct_t.Rows.Add(row2);

                TableCell cell31 = new TableCell();
                TableCell cell32 = new TableCell();
                TableCell cell33 = new TableCell();
                TableCell cell34 = new TableCell();
                cell31.Text = "<strong>2</strong>";
                cell32.Text = "<strong>3</strong>";
                cell33.Text = "<strong>4</strong>";
                cell34.Text = "<strong>5</strong>";
                row3.Cells.Add(cell31);
                row3.Cells.Add(cell32);
                row3.Cells.Add(cell33);
                row3.Cells.Add(cell34);
                pct_t.Rows.Add(row3);

                for (int i = 0; i < pro.PCT_Test.Count; i++)
                {
                    TableRow row = new TableRow();
                    for (int j = 0; j < 7; j++)
                    {
                        TableCell cell = new TableCell();
                        row.Cells.Add(cell);
                    }
                    row.Cells[0].Text = pro.PCT_Test[i].Discription;
                    row.Cells[1].Text = pro.PCT_Test[i].Rezult;
                    row.Cells[2].Text = pro.PCT_Test[i].Topics;
                    row.Cells[3].Text = pro.PCT_Test[i].On2;
                    row.Cells[4].Text = pro.PCT_Test[i].On3;
                    row.Cells[5].Text = pro.PCT_Test[i].On4;
                    row.Cells[6].Text = pro.PCT_Test[i].On5;
                    pct_t.Rows.Add(row);
                }

                string date = "";
                try { date = Convert.ToDateTime(pro.Questions_Test.Date).ToString("D"); }
                catch { date = DateTime.Today.ToString("yyyy-MM-dd"); }
                htmles.Add(InLeftDiv("<strong>Тесты:</strong>"));

                string PrilFondTabTest = InTableDiv(ToHtmlCode(pct_t));

                htmles.Add(PrilFondTabTest);
                htmles.Add(InCenterPBoldMini("Тестовые задания:"));
                htmles.Add(InDefoultDiv(pro.Questions_Test.Questions));
                htmles.Add(InLeftDiv("Составитель ____________  " + pro.Questions_Test.Developer
                    + "<br><sub>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;(подпись)</sub>" +
                    "<br>" + date));
            }

            if (pro.PCT_Curs.Count != 0)
            {
                Table pct_c = new Table();
                TableRow row1 = new TableRow();
                TableRow row2 = new TableRow();
                TableRow row3 = new TableRow();

                TableCell cell11 = new TableCell();
                TableCell cell12 = new TableCell();
                TableCell cell13 = new TableCell();
                TableCell cell14 = new TableCell();
                cell11.RowSpan = 3;
                cell12.RowSpan = 3;
                cell13.RowSpan = 3;
                cell14.ColumnSpan = 4;
                cell11.Text = "<strong>Описание</strong>";
                cell12.Text = "<strong>Контролируемый результат обучения</strong>";
                cell13.Text = "<strong>Контролируемые темы (разделы) дисциплины</strong>";
                cell14.Text = "<strong>Курсовая работа</strong>";
                row1.Cells.Add(cell11);
                row1.Cells.Add(cell12);
                row1.Cells.Add(cell13);
                row1.Cells.Add(cell14);
                pct_c.Rows.Add(row1);

                TableCell cell21 = new TableCell();
                cell21.ColumnSpan = 4;
                cell21.Text = "<strong>Критерии оценивания</strong>";
                row2.Cells.Add(cell21);
                pct_c.Rows.Add(row2);

                TableCell cell31 = new TableCell();
                TableCell cell32 = new TableCell();
                TableCell cell33 = new TableCell();
                TableCell cell34 = new TableCell();
                cell31.Text = "<strong>2</strong>";
                cell32.Text = "<strong>3</strong>";
                cell33.Text = "<strong>4</strong>";
                cell34.Text = "<strong>5</strong>";
                row3.Cells.Add(cell31);
                row3.Cells.Add(cell32);
                row3.Cells.Add(cell33);
                row3.Cells.Add(cell34);
                pct_c.Rows.Add(row3);

                for (int i = 0; i < pro.PCT_Curs.Count; i++)
                {
                    TableRow row = new TableRow();
                    for (int j = 0; j < 7; j++)
                    {
                        TableCell cell = new TableCell();
                        row.Cells.Add(cell);
                    }
                    row.Cells[0].Text = pro.PCT_Curs[i].Discription;
                    row.Cells[1].Text = pro.PCT_Curs[i].Rezult;
                    row.Cells[2].Text = pro.PCT_Curs[i].Topics;
                    row.Cells[3].Text = pro.PCT_Curs[i].On2;
                    row.Cells[4].Text = pro.PCT_Curs[i].On3;
                    row.Cells[5].Text = pro.PCT_Curs[i].On4;
                    row.Cells[6].Text = pro.PCT_Curs[i].On5;
                    pct_c.Rows.Add(row);
                }

                string date = "";
                try { date = Convert.ToDateTime(pro.Questions_Curs.Date).ToString("D"); }
                catch { date = DateTime.Today.ToString("yyyy-MM-dd"); }
                htmles.Add(InLeftDiv("<strong>Курсовые работы:</strong>"));

                string PrilFondTabCurs = InTableDiv(ToHtmlCode(pct_c));

                htmles.Add(PrilFondTabCurs);
                htmles.Add(InCenterPBoldMini("Темы курсовых работ:"));
                htmles.Add(InDefoultDiv(pro.Questions_Curs.Questions));
                htmles.Add(InLeftDiv("Составитель ____________  " + pro.Questions_Curs.Developer
                    + "<br><sub>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;(подпись)</sub>" +
                    "<br>" + date));
            }

            return htmles;
        }
        #endregion
    }
}