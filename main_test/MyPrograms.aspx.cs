﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using System.IO;

namespace main_test
{
    public partial class MyPrograms : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ErrorMes.Visible = false;
            ErrorMesRPDBlock.Visible = false;
            if (!IsPostBack)
            {
                if (SetAdittionToCommand())
                {
                    ListNotEmptyView.Visible = true;
                    ListEmptyView.Visible = false;
                }
                else
                {
                    ListEmptyView.Visible = true;
                    ListNotEmptyView.Visible = false;
                }
            }
        }
        
        //дополнение к команде селекта привязки данных (списка рпд)
        private bool SetAdittionToCommand()
        {
            string tocommand = "";
            string userid = User.Identity.GetUserId();
            tocommand = " WHERE DrafterId = '" + userid + "'";
            SqlDataSourcePrograms.SelectCommand += tocommand;
            Programs_List.DataBind();
            if (Programs_List.Items.Count == 0)
                return false;
            else return true;
        }

        //заполнение html данными рпд
        protected void Button_OpenRPD_Click(object sender, EventArgs e)
        {
            try
            {
                string userid = User.Identity.GetUserId();
                int ProgramId = Convert.ToInt32(Programs_List.SelectedItem.Value);
                BaseWork task = new BaseWork();
                Pro pro = new Pro();
                pro = task.GetProgram(ProgramId);
                string approvers = task.GetConfirmers(ProgramId);

                title_html.InnerHtml = "Рабочая программа №" + pro.Id.ToString();
                discipline.InnerHtml = "Дисциплина '" + pro.Discipline + "'";
                training_direction.InnerHtml = "Направление подготовки '" + pro.Training_Direction + "'";
                department.InnerHtml = "Кафедра '" + pro.Department + "'";
                developer.InnerHtml = "Разработчик – " + pro.DrafterName;
                developeDate.InnerHtml = "Дата разработки " + pro.DraftDate.ToString("dd-MM-yyyy");
                approvers_html.InnerHtml = approvers;
                ListNotEmptyView.Visible = false;
                ListEmptyView.Visible = false;
                RPDContentBlock.Visible = true;
        }
            catch
            {
                ErrorMes.Text = "Ошибка";
                ErrorMes.Visible = true;
            }
}

        //переход на форму истории рпд
        protected void Button_ShowHistory_Click(object sender, EventArgs e)
        {
            int ProgramId = Convert.ToInt32(Programs_List.SelectedItem.Value);
            Response.Redirect("ProgramHistory.aspx?ProgramId=" + ProgramId);
        }
        
        //поиск рпд с помощью дополнения к команде селекта привязки данных (списка рпд)
        protected void Search_Button_Click(object sender, EventArgs e)
        {
            SetAdittionToCommand();
            SqlDataSourcePrograms.SelectCommand += " AND Discipline LIKE '%" + Search_TextBox.Text + "%'";
            Programs_List.DataBind();
        }

        //открыть word-файл в браузере
        protected void LinkButton_OpenNewTab_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(Programs_List.SelectedItem.Value);
            try
            {
                BaseWork task = new BaseWork();
                string filePath = task.GetFileUrl(id);
                string script = string.Format("window.open('{0}');", filePath);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "newPage" + UniqueID, script, true);
            }
            catch
            {
                ErrorMesRPDBlock.Text = "Не удалось получить ссылку на файл";
                ErrorMesRPDBlock.Visible = true;
            }
        }

        //загрузка word-файла на устройство
        protected void LinkButton_Download_Click(object sender, EventArgs e)
        {
            try
            {
                int programid = Convert.ToInt32(Programs_List.SelectedItem.Value);
                string fileName = "Program_" + programid.ToString() + ".docx";
                string url = HttpContext.Current.Server.MapPath("~/Files/" + fileName);

                FileStream MyFileStream = new FileStream(url, FileMode.Open);
                int filesizeINT = 0;
                long fileSize = MyFileStream.Length;
                filesizeINT = Convert.ToInt32(fileSize);
                byte[] Buffer = new byte[fileSize];
                MyFileStream.Read(Buffer, 0, filesizeINT);
                MyFileStream.Close();
                Response.AppendHeader("content-disposition", "attachment; filename=" + fileName);
                Response.BinaryWrite(Buffer);
                Response.End();
            }
            catch
            {
                ErrorMesRPDBlock.Text = "Ошибка загрузки";
                ErrorMesRPDBlock.Visible = true;
            }
        }
    }
}