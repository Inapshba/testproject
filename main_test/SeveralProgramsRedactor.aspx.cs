﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using main_test.TableRows;
using main_test.QuestionsStructur;
using Microsoft.AspNet.Identity;
using System.IO;

namespace main_test
{
    public partial class SeveralProgramsRedactor : System.Web.UI.Page
    {
        List<int> IDes;
        protected void Page_Load(object sender, EventArgs e)
        {
            ErrorMes.Visible = false;
            BuildProgramLinks();
            if (!IsPostBack)
            {
                CreateDefoultDates();
                if (FillIDesFromSession() == false)
                    Message.Text = "Ошибка идентификации программ";
            }
            else
            {
                BuildDinamycControls();
            }
        }

        private void BuildProgramLinks()
        {
            if(FillIDesFromSession())
            {
                for (int i = 0; i < IDes.Count; i++)
                {
                    LinkButton link = new LinkButton();
                    link.ID = IDes[i].ToString();
                    link.Text = IDes[i].ToString();
                    link.CssClass = "link_button";
                    link.ToolTip = "Скачать программу";
                    link.Click += DownLoadProgram;
                    programs_panel.Controls.Add(link);
                }
            }
        }

        private void DownLoadProgram(object sender, EventArgs e)
        {
            try
            {
                LinkButton thisButton = (LinkButton)sender;
                int id = Convert.ToInt32(thisButton.ID);
                string fileName = "Program_" + id.ToString() + ".docx";
                string url = HttpContext.Current.Server.MapPath("~/Files/" + fileName);

                FileStream MyFileStream = new FileStream(url, FileMode.Open);
                int filesizeINT = 0;
                long fileSize = MyFileStream.Length;
                filesizeINT = Convert.ToInt32(fileSize);
                byte[] Buffer = new byte[fileSize];
                MyFileStream.Read(Buffer, 0, filesizeINT);
                MyFileStream.Close();
                Response.AppendHeader("content-disposition", "attachment; filename=" + fileName);
                Response.BinaryWrite(Buffer);
                Response.End();
            }
            catch
            {
                ErrorMes.Text = "Не удалось загрузить файл";
                ErrorMes.Visible = true;
            }
        }

        private void CreateDefoultDates()
        {
            Approve_Date.Text = DateTime.Today.ToString("yyyy-MM-dd");
            DraftDate.Text = DateTime.Today.ToString("yyyy-MM-dd");
        }

        private void BuildDinamycControls()
        {
            int drafterscount = Convert.ToInt32(AdditionalDraftersCount.Value);
            if (drafterscount != 0)
                for (int i = 0; i < drafterscount; i++)
                {
                    Panel panel = new Panel();
                    panel.ID = "DraftersPanel_" + i.ToString();
                    panel.Style["margin-top"] = "2px";
                    TextBox newTextBox = new TextBox();
                    newTextBox.CssClass = "form-control";
                    newTextBox.Attributes["placeholder"] = "Академ. статус";
                    newTextBox.Width = 150;
                    newTextBox.ID = "AdditionalDrafterStatus_" + i.ToString();

                    DropDownList newList = new DropDownList();
                    newList.CssClass = "form-control";
                    newList.DataSourceID = "SqlDataSourceDepartmentPersonal";
                    newList.Style["margin-left"] = "3px";
                    newList.DataTextField = "UserName";
                    newList.DataValueField = DrafterName.DataValueField;
                    newList.ID = "AdditionalDrafterId_" + i.ToString();

                    panel.Controls.Add(newTextBox);
                    panel.Controls.Add(newList);
                    drafters_html_block.Controls.Add(panel);
                }
        }

        private bool FillIDesFromSession()
        {
            try
            {
                IDes = new List<int>();
                IDes = (List<int>)Session["IDes"];
                if (IDes.Count == 0)
                    throw new Exception();
                
                return true;
            }
            catch
            {
                return false;
            }
            
        }

        #region TableTasks
        private void RebuildTables(int index)
        {
            switch (index)
            {
                case 0:
                    PlanTableTask(0);
                    FondTableTask(0);
                    CriterionsTableTask(0);
                    DSTTask(0);
                    PFTTask(0, 0);
                    PATTask(0);
                    PCTzachetTask(0);
                    PCTexamTask(0);
                    PCTreferatTask(0);
                    PCTktTask(0);
                    PCTpraktikaTask(0);
                    PCTtestTask(0);
                    PCTcursTask(0);
                    break;
                case 1:
                    FondTableTask(0);
                    CriterionsTableTask(0);
                    DSTTask(0);
                    PFTTask(0, 0);
                    PATTask(0);
                    PCTzachetTask(0);
                    PCTexamTask(0);
                    PCTreferatTask(0);
                    PCTktTask(0);
                    PCTpraktikaTask(0);
                    PCTtestTask(0);
                    PCTcursTask(0);
                    break;
                case 2:
                    PlanTableTask(0);
                    CriterionsTableTask(0);
                    DSTTask(0);
                    PFTTask(0, 0);
                    PATTask(0);
                    PCTzachetTask(0);
                    PCTexamTask(0);
                    PCTreferatTask(0);
                    PCTktTask(0);
                    PCTpraktikaTask(0);
                    PCTtestTask(0);
                    PCTcursTask(0);
                    break;
                case 3:
                    PlanTableTask(0);
                    FondTableTask(0);
                    DSTTask(0);
                    PFTTask(0, 0);
                    PATTask(0);
                    PCTzachetTask(0);
                    PCTexamTask(0);
                    PCTreferatTask(0);
                    PCTktTask(0);
                    PCTpraktikaTask(0);
                    PCTtestTask(0);
                    PCTcursTask(0);
                    break;
                case 4:
                    PlanTableTask(0);
                    FondTableTask(0);
                    CriterionsTableTask(0);
                    PFTTask(0, 0);
                    PATTask(0);
                    PCTzachetTask(0);
                    PCTexamTask(0);
                    PCTreferatTask(0);
                    PCTktTask(0);
                    PCTpraktikaTask(0);
                    PCTtestTask(0);
                    PCTcursTask(0);
                    break;
                case 5:
                    PlanTableTask(0);
                    FondTableTask(0);
                    CriterionsTableTask(0);
                    DSTTask(0);
                    PFTTask(0, 2);
                    PFTTask(0, 3);
                    PATTask(0);
                    PCTzachetTask(0);
                    PCTexamTask(0);
                    PCTreferatTask(0);
                    PCTktTask(0);
                    PCTpraktikaTask(0);
                    PCTtestTask(0);
                    PCTcursTask(0);
                    break;
                case 6:
                    PlanTableTask(0);
                    FondTableTask(0);
                    CriterionsTableTask(0);
                    DSTTask(0);
                    PFTTask(0, 1);
                    PFTTask(0, 3);
                    PATTask(0);
                    PCTzachetTask(0);
                    PCTexamTask(0);
                    PCTreferatTask(0);
                    PCTktTask(0);
                    PCTpraktikaTask(0);
                    PCTtestTask(0);
                    PCTcursTask(0);
                    break;
                case 7:
                    PlanTableTask(0);
                    FondTableTask(0);
                    CriterionsTableTask(0);
                    DSTTask(0);
                    PFTTask(0, 1);
                    PFTTask(0, 2);
                    PATTask(0);
                    PCTzachetTask(0);
                    PCTexamTask(0);
                    PCTreferatTask(0);
                    PCTktTask(0);
                    PCTpraktikaTask(0);
                    PCTtestTask(0);
                    PCTcursTask(0);
                    break;
                case 8:
                    PlanTableTask(0);
                    FondTableTask(0);
                    CriterionsTableTask(0);
                    DSTTask(0);
                    PFTTask(0, 0);
                    PCTzachetTask(0);
                    PCTexamTask(0);
                    PCTreferatTask(0);
                    PCTktTask(0);
                    PCTpraktikaTask(0);
                    PCTtestTask(0);
                    PCTcursTask(0);
                    break;
                case 9:
                    PlanTableTask(0);
                    FondTableTask(0);
                    CriterionsTableTask(0);
                    DSTTask(0);
                    PFTTask(0, 0);
                    PATTask(0);
                    PCTexamTask(0);
                    PCTreferatTask(0);
                    PCTktTask(0);
                    PCTpraktikaTask(0);
                    PCTtestTask(0);
                    PCTcursTask(0);
                    break;
                case 10:
                    PlanTableTask(0);
                    FondTableTask(0);
                    CriterionsTableTask(0);
                    DSTTask(0);
                    PFTTask(0, 0);
                    PATTask(0);
                    PCTzachetTask(0);
                    PCTreferatTask(0);
                    PCTktTask(0);
                    PCTpraktikaTask(0);
                    PCTtestTask(0);
                    PCTcursTask(0);
                    break;
                case 11:
                    PlanTableTask(0);
                    FondTableTask(0);
                    CriterionsTableTask(0);
                    DSTTask(0);
                    PFTTask(0, 0);
                    PATTask(0);
                    PCTzachetTask(0);
                    PCTexamTask(0);
                    PCTktTask(0);
                    PCTpraktikaTask(0);
                    PCTtestTask(0);
                    PCTcursTask(0);
                    break;
                case 12:
                    PlanTableTask(0);
                    FondTableTask(0);
                    CriterionsTableTask(0);
                    DSTTask(0);
                    PFTTask(0, 0);
                    PATTask(0);
                    PCTzachetTask(0);
                    PCTexamTask(0);
                    PCTreferatTask(0);
                    PCTpraktikaTask(0);
                    PCTtestTask(0);
                    PCTcursTask(0);
                    break;
                case 13:
                    PlanTableTask(0);
                    FondTableTask(0);
                    CriterionsTableTask(0);
                    DSTTask(0);
                    PFTTask(0, 0);
                    PATTask(0);
                    PCTzachetTask(0);
                    PCTexamTask(0);
                    PCTreferatTask(0);
                    PCTktTask(0);
                    PCTtestTask(0);
                    PCTcursTask(0);
                    break;
                case 14:
                    PlanTableTask(0);
                    FondTableTask(0);
                    CriterionsTableTask(0);
                    DSTTask(0);
                    PFTTask(0, 0);
                    PATTask(0);
                    PCTzachetTask(0);
                    PCTexamTask(0);
                    PCTreferatTask(0);
                    PCTktTask(0);
                    PCTpraktikaTask(0);
                    PCTcursTask(0);
                    break;
                case 15:
                    PlanTableTask(0);
                    FondTableTask(0);
                    CriterionsTableTask(0);
                    DSTTask(0);
                    PFTTask(0, 0);
                    PATTask(0);
                    PCTzachetTask(0);
                    PCTexamTask(0);
                    PCTreferatTask(0);
                    PCTktTask(0);
                    PCTpraktikaTask(0);
                    PCTtestTask(0);
                    break;
            }
        }

        private void PlanTableTask(int task)
        {
            int count = Convert.ToInt32(ptrowscount.Value);

            if (task == -1)
                if (count == 0) goto end;
                else count -= 1;

            if (task == 1) count += 1;
            ptrowscount.Value = count.ToString();

            int i, j;

            for (i = 1; i <= count; i++)
            {
                TableRow tRow = new TableRow();
                PlanTable.Rows.Add(tRow);
                for (j = 0; j < 3; j++)
                {
                    TableCell tCell = new TableCell();
                    tRow.Cells.Add(tCell);
                }

                if ((i == count) && (task == 1))
                {
                    BaseWork taskbase = new BaseWork();
                    tRow.Cells[0].Text = Сompetence_PlanTable.SelectedItem.Text;
                    tRow.Cells[1].Text = Сompetence_PlanTable.SelectedItem.Value;
                    tRow.Cells[2].Text = Enumeration_PlanTable.Text;
                }
            }
        end: { }

        }

        private void FondTableTask(int task)
        {
            int count = Convert.ToInt32(ftrowscount.Value);
            if (task == -1)
                if (count == 0) goto end;
                else count -= 1;

            if (task == 1) count += 1;
            ftrowscount.Value = count.ToString();

            int i, j;
            for (i = 1; i <= count; i++)
            {
                TableRow tRow = new TableRow();
                MonitoringTools_FondTable.Rows.Add(tRow);
                for (j = 0; j < 2; j++)
                {
                    TableCell tCell = new TableCell();
                    tRow.Cells.Add(tCell);
                }
                if ((i == count) && (task == 1))
                {
                    BaseWork taskbase = new BaseWork();
                    tRow.Cells[0].Text = Competence_FondTable.SelectedItem.Text;
                    tRow.Cells[1].Text = Competence_FondTable.SelectedItem.Value;
                }
            }
        end: { }

        }

        private void CriterionsTableTask(int task)
        {
            BaseWork baseWork = new BaseWork();
            int count = Convert.ToInt32(CTrowscount.Value);

            if (task == -1)
                if (count == 1) goto end;
                else count -= 3;

            if (task == 1) count += 3;
            CTrowscount.Value = count.ToString();

            int i, j;
            for (i = 2; i <= count; i++)
            {
                TableRow tRow = new TableRow();
                CriterionsTable.Rows.Add(tRow);
                for (j = 0; j <= 5; j++)
                {
                    TableCell tCell = new TableCell();
                    tRow.Cells.Add(tCell);
                }
                if ((i == count - 2) && (task == 1))
                {
                    tRow.Cells[0].Text = Competence_CriterionsTable.SelectedItem.Text + " - " + Competence_CriterionsTable.SelectedItem.Value;
                    tRow.Cells[1].Text = IndicatorCriterionsTable_Know.Text;
                    tRow.Cells[2].Text = CriterionCriterionsTable_Know_2.Text;
                    tRow.Cells[3].Text = CriterionCriterionsTable_Know_3.Text;
                    tRow.Cells[4].Text = CriterionCriterionsTable_Know_4.Text;
                    tRow.Cells[5].Text = CriterionCriterionsTable_Know_5.Text;
                }
                if ((i == count - 1) && (task == 1))
                {
                    tRow.Cells[0].Text = "-//-//-";
                    tRow.Cells[1].Text = IndicatorCriterionsTable_Can.Text;
                    tRow.Cells[2].Text = CriterionCriterionsTable_Can_2.Text;
                    tRow.Cells[3].Text = CriterionCriterionsTable_Can_3.Text;
                    tRow.Cells[4].Text = CriterionCriterionsTable_Can_4.Text;
                    tRow.Cells[5].Text = CriterionCriterionsTable_Can_5.Text;
                }
                if ((i == count) && (task == 1))
                {
                    tRow.Cells[0].Text = "-//-//-";
                    tRow.Cells[1].Text = IndicatorCriterionsTable_Wield.Text;
                    tRow.Cells[2].Text = CriterionCriterionsTable_Wield_2.Text;
                    tRow.Cells[3].Text = CriterionCriterionsTable_Wield_3.Text;
                    tRow.Cells[4].Text = CriterionCriterionsTable_Wield_4.Text;
                    tRow.Cells[5].Text = CriterionCriterionsTable_Wield_5.Text;
                }
            }
        end: { }
        }

        private void DSTTask(int task)
        {
            int count = Convert.ToInt32(DSTrowscount.Value);
            for (int i = 0; i < count; i++)
            {
                TableRow row = new TableRow();
                DisciplineStructureTable.Rows.Add(row);
                for (int j = 0; j < 15; j++)
                {
                    TableCell cell = new TableCell();
                    row.Cells.Add(cell);
                }
            }

            switch (task)
            {
                case 1:
                    try
                    {
                        if ((Convert.ToInt32(DSCT_sem.Text) <= 0) || (Convert.ToInt32(DSCT_sem.Text) > 10))
                            break;
                    }
                    catch { }

                    TableRow row = new TableRow();
                    DisciplineStructureTable.Rows.Add(row);
                    for (int j = 0; j < 15; j++)
                    {
                        TableCell cell = new TableCell();
                        row.Cells.Add(cell);
                    }
                    row.Cells[1].Text = DSCT_topic.Text;
                    row.Cells[2].Text = DSCT_sem.Text;
                    row.Cells[3].Text = DSCT_week.Text;
                    row.Cells[4].Text = DSCT_L.Text;
                    row.Cells[5].Text = DSCT_PS.Text;
                    row.Cells[6].Text = DSCT_Lab.Text;
                    row.Cells[7].Text = DSCT_SRS.Text;
                    row.Cells[8].Text = DSCT_KSR.Text;
                    row.Cells[9].Text = DSCT_KR.Text;
                    row.Cells[10].Text = DSCT_KP.Text;
                    row.Cells[11].Text = DSCT_RGR.Text;
                    row.Cells[12].Text = DSCT_preptolr.Text;
                    row.Cells[13].Text = DSCT_KRend.Text;
                    row.Cells[14].Text = DSCT_AttestatForm.Text;
                    count++;
                    DSTrowscount.Value = count.ToString();
                    break;

                case -1:
                    bool deleteble = false;
                    int rownumber = 0;
                    try { rownumber = Convert.ToInt32(rowNumberDSCT.Text) + 1; }
                    catch { }
                    if ((rownumber > 1) && (rownumber < DisciplineStructureTable.Rows.Count) && (count != 0))
                        deleteble = true;

                    if (deleteble)
                    {
                        DisciplineStructureTable.Rows.RemoveAt(rownumber);
                        count--;
                        DSTrowscount.Value = count.ToString();
                    }
                    break;

                case 2:
                    List<TableRow> delrows = new List<TableRow>();
                    for (int i = 2; i < DisciplineStructureTable.Rows.Count; i++)
                        if ((DisciplineStructureTable.Rows[i].Cells[1].Text == "Часы за семестр")
                            || (DisciplineStructureTable.Rows[i].Cells[1].Text == "Часы за весь период"))
                            delrows.Add(DisciplineStructureTable.Rows[i]);
                    for (int i = 0; i < delrows.Count; i++)
                        DisciplineStructureTable.Rows.Remove(delrows[i]);

                    Table bufTable = new Table();
                    bufTable = DisciplineStructureTable;

                    string attestat = "";
                    List<int> sems = new List<int>();
                    List<int> summs = new List<int>();
                    List<int> summsglobal = new List<int>();
                    for (int i = 0; i < 9; i++)
                        summs.Add(0);

                    for (int i = 0; i < 9; i++)
                        summsglobal.Add(0);

                    for (int i = 2; i < bufTable.Rows.Count; i++)
                    {
                        int semNumber = Convert.ToInt32(bufTable.Rows[i].Cells[2].Text);
                        sems.Add(semNumber);
                    }

                    sems = sems.Distinct().ToList();
                    sems.Sort();


                    for (int i = 0; i < sems.Count; i++)
                    {
                        for (int s = 0; s < 9; s++)
                            summs[s] = 0;
                        attestat = "";
                        List<TableRow> tablerows = new List<TableRow>();
                        for (int ti = 2; ti < bufTable.Rows.Count; ti++)
                        {
                            TableRow trow = bufTable.Rows[ti];
                            for (int c = 4; c < trow.Cells.Count; c++)
                                if (trow.Cells[c].Text == "")
                                    trow.Cells[c].Text = "0";

                            if (trow.Cells[2].Text == sems[i].ToString())
                                tablerows.Add(trow);
                        }

                        for (int tr = 0; tr < tablerows.Count; tr++)
                        {
                            attestat = tablerows[tr].Cells[14].Text;
                            tablerows[tr].Cells[14].Text = "";
                            DisciplineStructureTable.Rows.Add(tablerows[tr]);
                            summs[0] += Convert.ToInt32(tablerows[tr].Cells[4].Text);
                            summs[1] += Convert.ToInt32(tablerows[tr].Cells[5].Text);
                            summs[2] += Convert.ToInt32(tablerows[tr].Cells[6].Text);
                            summs[3] += Convert.ToInt32(tablerows[tr].Cells[7].Text);
                            summs[4] += Convert.ToInt32(tablerows[tr].Cells[8].Text);
                            summs[5] += Convert.ToInt32(tablerows[tr].Cells[9].Text);
                            summs[6] += Convert.ToInt32(tablerows[tr].Cells[10].Text);
                            summs[7] += Convert.ToInt32(tablerows[tr].Cells[11].Text);
                            summs[8] += Convert.ToInt32(tablerows[tr].Cells[13].Text);
                        }

                        TableRow clockrow = new TableRow();
                        DisciplineStructureTable.Rows.Add(clockrow);
                        for (int j = 0; j < 15; j++)
                        {
                            TableCell clockcell = new TableCell();
                            clockrow.Cells.Add(clockcell);
                        }
                        clockrow.Cells[1].Text = "Часы за семестр";
                        clockrow.Cells[2].Text = sems[i].ToString();
                        clockrow.Cells[4].Text = summs[0].ToString();
                        clockrow.Cells[5].Text = summs[1].ToString();
                        clockrow.Cells[6].Text = summs[2].ToString();
                        clockrow.Cells[7].Text = summs[3].ToString();
                        clockrow.Cells[8].Text = summs[4].ToString();
                        clockrow.Cells[9].Text = summs[5].ToString();
                        clockrow.Cells[10].Text = summs[6].ToString();
                        clockrow.Cells[11].Text = summs[7].ToString();
                        clockrow.Cells[13].Text = summs[8].ToString();
                        clockrow.Cells[14].Text = attestat;

                        summsglobal[0] += summs[0];
                        summsglobal[1] += summs[1];
                        summsglobal[2] += summs[2];
                        summsglobal[3] += summs[3];
                        summsglobal[4] += summs[4];
                        summsglobal[5] += summs[5];
                        summsglobal[6] += summs[6];
                        summsglobal[7] += summs[7];
                        summsglobal[8] += summs[8];
                    }

                    TableRow clockgrow = new TableRow();
                    DisciplineStructureTable.Rows.Add(clockgrow);
                    for (int j = 0; j < 15; j++)
                    {
                        TableCell clockcell = new TableCell();
                        clockgrow.Cells.Add(clockcell);
                    }
                    clockgrow.Cells[1].Text = "Часы за весь период";
                    clockgrow.Cells[4].Text = summsglobal[0].ToString();
                    clockgrow.Cells[5].Text = summsglobal[1].ToString();
                    clockgrow.Cells[6].Text = summsglobal[2].ToString();
                    clockgrow.Cells[7].Text = summsglobal[3].ToString();
                    clockgrow.Cells[8].Text = summsglobal[4].ToString();
                    clockgrow.Cells[9].Text = summsglobal[5].ToString();
                    clockgrow.Cells[10].Text = summsglobal[6].ToString();
                    clockgrow.Cells[11].Text = summsglobal[7].ToString();
                    clockgrow.Cells[13].Text = summsglobal[8].ToString();

                    count = DisciplineStructureTable.Rows.Count - 2;
                    DSTrowscount.Value = count.ToString();
                    break;
            }

            int g = 0;
            for (int i = 2; i < DisciplineStructureTable.Rows.Count; i++)
            {
                for (int j = 3; j < 15; j++)
                    if (DisciplineStructureTable.Rows[i].Cells[j].Text == "0")
                        DisciplineStructureTable.Rows[i].Cells[j].Text = "";

                if ((DisciplineStructureTable.Rows[i].Cells[1].Text == "Часы за семестр") || (DisciplineStructureTable.Rows[i].Cells[1].Text == "Часы за весь период"))
                {
                    g++;
                    continue;
                }
                DisciplineStructureTable.Rows[i].Cells[0].Text = (i - 1 - g).ToString();
            }
        }

        public List<DSTTableRow> DSTTable_FillTask()
        {
            List<DSTTableRow> rows = new List<DSTTableRow>();
            for (int i = 2; i < DisciplineStructureTable.Rows.Count; i++)
            {
                DSTTableRow row = new DSTTableRow();

                try { row.Topic = DisciplineStructureTable.Rows[i].Cells[1].Text; }
                catch { }

                try { row.Semestr = Convert.ToInt32(DisciplineStructureTable.Rows[i].Cells[2].Text); }
                catch { }

                try { row.Week = Convert.ToInt32(DisciplineStructureTable.Rows[i].Cells[3].Text); }
                catch { }

                try { row.L = Convert.ToInt32(DisciplineStructureTable.Rows[i].Cells[4].Text); }
                catch { }

                try { row.PS = Convert.ToInt32(DisciplineStructureTable.Rows[i].Cells[5].Text); }
                catch { }

                try { row.Lab = Convert.ToInt32(DisciplineStructureTable.Rows[i].Cells[6].Text); }
                catch { }

                try { row.SRS = Convert.ToInt32(DisciplineStructureTable.Rows[i].Cells[7].Text); }
                catch { }

                try { row.KSR = Convert.ToInt32(DisciplineStructureTable.Rows[i].Cells[8].Text); }
                catch { }

                try { row.KR = Convert.ToInt32(DisciplineStructureTable.Rows[i].Cells[9].Text); }
                catch { }

                try { row.KP = Convert.ToInt32(DisciplineStructureTable.Rows[i].Cells[10].Text); }
                catch { }

                try { row.RGR = Convert.ToInt32(DisciplineStructureTable.Rows[i].Cells[11].Text); }
                catch { }

                try { row.PrepToLR = DisciplineStructureTable.Rows[i].Cells[12].Text; }
                catch { }

                try { row.KRend = Convert.ToInt32(DisciplineStructureTable.Rows[i].Cells[13].Text); }
                catch { }

                try { row.AttestatForm = DisciplineStructureTable.Rows[i].Cells[14].Text; }
                catch { }

                rows.Add(row);
            }

            return rows;
        }

        private void PFTTask(int task, int id)
        {

            Table table = new Table();
            int count = 0;
            int i, j;

            switch (id)
            {
                case 0:
                    PFTTask(0, 1);
                    PFTTask(0, 2);
                    PFTTask(0, 3);
                    break;
                case 1:
                    table = PrilFondTable_1;
                    count = Convert.ToInt32(PFT_1rowscount.Value);
                    break;
                case 2:
                    table = PrilFondTable_2;
                    count = Convert.ToInt32(PFT_2rowscount.Value);
                    break;
                case 3:
                    table = PrilFondTable_3;
                    count = Convert.ToInt32(PFT_3rowscount.Value);
                    break;
            }

            if (task == -1)
                if (count == 0) goto end;
                else count -= 1;
            else if (task == 1) count += 1;

            for (i = 0; i < count; i++)
            {
                TableRow row = new TableRow();
                table.Rows.Add(row);
                for (j = 0; j < 6; j++)
                {
                    TableCell cell = new TableCell();
                    row.Cells.Add(cell);
                }
                if ((task == 1) && (i == count - 1))
                {
                    BaseWork taskbase = new BaseWork();
                    row.Cells[0].Text = PFTCom.SelectedItem.Text;
                    row.Cells[1].Text = PFTCom.SelectedItem.Value;
                    row.Cells[2].Text = PFTKomponentsList.Text;
                    row.Cells[3].Text = PFTCompetenceTechnology.Text;
                    row.Cells[4].Text = PFTAssessForm.Text;
                    row.Cells[5].Text = PFTSteps.Text;
                }
            }

            switch (id)
            {
                case 1:
                    PrilFondTable_1 = table;
                    PFT_1rowscount.Value = count.ToString();
                    break;
                case 2:
                    PrilFondTable_2 = table;
                    PFT_2rowscount.Value = count.ToString();
                    break;
                case 3:
                    PrilFondTable_3 = table;
                    PFT_3rowscount.Value = count.ToString();
                    break;
            }

        end: { }

        }

        private void PATTask(int task)
        {
            int count = Convert.ToInt32(PATrowscount.Value);
            int i, j;
            if (task == -1)
                if (count == 0) goto end;
                else count -= 1;
            else if (task == 1) count += 1;

            PATrowscount.Value = count.ToString();

            for (i = 0; i < count; i++)
            {
                TableRow row = new TableRow();
                PrilAssessTable.Rows.Add(row);
                for (j = 0; j < 4; j++)
                {
                    TableCell cell = new TableCell();
                    row.Cells.Add(cell);
                }
                if ((i == count - 1) && (task == 1))
                {
                    row.Cells[0].Text = count.ToString();
                    row.Cells[1].Text = PATName.Text;
                    row.Cells[2].Text = PATSpec.Text;
                    row.Cells[3].Text = PATinFOS.Text;
                }
            }
        end: { }
        }

        private void PCTzachetTask(int task)
        {
            int count = Convert.ToInt32(PCT_zachetrowscount.Value);
            int i, j;
            if (task == -1)
                if (count == 0) goto end;
                else count -= 1;
            else if (task == 1) count += 1;

            PCT_zachetrowscount.Value = count.ToString();

            for (i = 0; i < count; i++)
            {
                TableRow row = new TableRow();
                PrilCompetenceTable_Zachet.Rows.Add(row);
                for (j = 0; j < 5; j++)
                {
                    TableCell cell = new TableCell();
                    row.Cells.Add(cell);
                }
                if ((i == count - 1) && (task == 1))
                {
                    row.Cells[0].Text = DropDown_PCTZ.SelectedItem.Text + " - " + DropDown_PCTZ.SelectedItem.Value.ToString();
                    row.Cells[1].Text = PCTZRezult.Text;
                    row.Cells[2].Text = PCTZTopics.Text;
                    row.Cells[3].Text = PCTZZachet.Text;
                    row.Cells[4].Text = PCTZNeZachet.Text;
                }
            }

        end: { }
        }

        private void PCTexamTask(int task)
        {
            int count = Convert.ToInt32(PCT_examrowscount.Value);
            int i, j;
            if (task == -1)
                if (count == 0) goto end;
                else count -= 1;
            else if (task == 1) count += 1;

            PCT_examrowscount.Value = count.ToString();

            for (i = 0; i < count; i++)
            {
                TableRow row = new TableRow();
                PrilCompetenceTable_Exam.Rows.Add(row);
                for (j = 0; j < 7; j++)
                {
                    TableCell cell = new TableCell();
                    row.Cells.Add(cell);
                }
                if ((i == count - 1) && (task == 1))
                {
                    row.Cells[0].Text = DropDown_PCTE.SelectedItem.Text + " - " + DropDown_PCTE.SelectedItem.Value.ToString();
                    row.Cells[1].Text = PCTERezult.Text;
                    row.Cells[2].Text = PCTETopics.Text;
                    row.Cells[3].Text = PCTE2.Text;
                    row.Cells[4].Text = PCTE3.Text;
                    row.Cells[5].Text = PCTE4.Text;
                    row.Cells[6].Text = PCTE5.Text;
                }
            }

        end: { }
        }

        private void PCTreferatTask(int task)
        {
            int count = Convert.ToInt32(PCT_referatrowscount.Value);
            int i, j;
            if (task == -1)
                if (count == 0) goto end;
                else count -= 1;
            else if (task == 1) count += 1;

            PCT_referatrowscount.Value = count.ToString();

            for (i = 0; i < count; i++)
            {
                TableRow row = new TableRow();
                PrilCompetenceTable_Referat.Rows.Add(row);
                for (j = 0; j < 7; j++)
                {
                    TableCell cell = new TableCell();
                    row.Cells.Add(cell);
                }
                if ((i == count - 1) && (task == 1))
                {
                    row.Cells[0].Text = DropDown_PCTR.SelectedItem.Text + " - " + DropDown_PCTR.SelectedItem.Value.ToString();
                    row.Cells[1].Text = PCTRRezult.Text;
                    row.Cells[2].Text = PCTRTopics.Text;
                    row.Cells[3].Text = PCTR2.Text;
                    row.Cells[4].Text = PCTR3.Text;
                    row.Cells[5].Text = PCTR4.Text;
                    row.Cells[6].Text = PCTR5.Text;
                }
            }

        end: { }
        }

        private void PCTktTask(int task)
        {
            int count = Convert.ToInt32(PCT_ktrowscount.Value);
            int i, j;
            if (task == -1)
                if (count == 0) goto end;
                else count -= 1;
            else if (task == 1) count += 1;

            PCT_ktrowscount.Value = count.ToString();

            for (i = 0; i < count; i++)
            {
                TableRow row = new TableRow();
                PrilCompetenceTable_KT.Rows.Add(row);
                for (j = 0; j < 7; j++)
                {
                    TableCell cell = new TableCell();
                    row.Cells.Add(cell);
                }
                if ((i == count - 1) && (task == 1))
                {
                    row.Cells[0].Text = DropDown_PCTKT.SelectedItem.Text + " - " + DropDown_PCTKT.SelectedItem.Value.ToString();
                    row.Cells[1].Text = PCTKTRezult.Text;
                    row.Cells[2].Text = PCTKTTopics.Text;
                    row.Cells[3].Text = PCTKT2.Text;
                    row.Cells[4].Text = PCTKT3.Text;
                    row.Cells[5].Text = PCTKT4.Text;
                    row.Cells[6].Text = PCTKT5.Text;
                }
            }

        end: { }
        }

        private void PCTpraktikaTask(int task)
        {
            int count = Convert.ToInt32(PCT_praktikarowscount.Value);
            int i, j;
            if (task == -1)
                if (count == 0) goto end;
                else count -= 1;
            else if (task == 1) count += 1;

            PCT_praktikarowscount.Value = count.ToString();

            for (i = 0; i < count; i++)
            {
                TableRow row = new TableRow();
                PrilCompetenceTable_Praktika.Rows.Add(row);
                for (j = 0; j < 7; j++)
                {
                    TableCell cell = new TableCell();
                    row.Cells.Add(cell);
                }
                if ((i == count - 1) && (task == 1))
                {
                    row.Cells[0].Text = DropDown_PCTPR.SelectedItem.Text + " - " + DropDown_PCTPR.SelectedItem.Value.ToString();
                    row.Cells[1].Text = PCTPRezult.Text;
                    row.Cells[2].Text = PCTPTopics.Text;
                    row.Cells[3].Text = PCTP2.Text;
                    row.Cells[4].Text = PCTP3.Text;
                    row.Cells[5].Text = PCTP4.Text;
                    row.Cells[6].Text = PCTP5.Text;
                }
            }

        end: { }
        }

        private void PCTtestTask(int task)
        {
            int count = Convert.ToInt32(PCT_testrowscount.Value);
            int i, j;
            if (task == -1)
                if (count == 0) goto end;
                else count -= 1;
            else if (task == 1) count += 1;

            PCT_testrowscount.Value = count.ToString();

            for (i = 0; i < count; i++)
            {
                TableRow row = new TableRow();
                PrilCompetenceTable_Test.Rows.Add(row);
                for (j = 0; j < 7; j++)
                {
                    TableCell cell = new TableCell();
                    row.Cells.Add(cell);
                }
                if ((i == count - 1) && (task == 1))
                {
                    row.Cells[0].Text = DropDown_PCTT.SelectedItem.Text + " - " + DropDown_PCTT.SelectedItem.Value.ToString();
                    row.Cells[1].Text = PCTTRezult.Text;
                    row.Cells[2].Text = PCTTTopics.Text;
                    row.Cells[3].Text = PCTT2.Text;
                    row.Cells[4].Text = PCTT3.Text;
                    row.Cells[5].Text = PCTT4.Text;
                    row.Cells[6].Text = PCTT5.Text;
                }
            }

        end: { }
        }

        private void PCTcursTask(int task)
        {
            int count = Convert.ToInt32(PCT_cursrowscount.Value);
            int i, j;
            if (task == -1)
                if (count == 0) goto end;
                else count -= 1;
            else if (task == 1) count += 1;

            PCT_cursrowscount.Value = count.ToString();

            for (i = 0; i < count; i++)
            {
                TableRow row = new TableRow();
                PrilCompetenceTable_Curs.Rows.Add(row);
                for (j = 0; j < 7; j++)
                {
                    TableCell cell = new TableCell();
                    row.Cells.Add(cell);
                }
                if ((i == count - 1) && (task == 1))
                {
                    row.Cells[0].Text = DropDown_PCTC.SelectedItem.Text + " - " + DropDown_PCTC.SelectedItem.Value.ToString();
                    row.Cells[1].Text = PCTCRezult.Text;
                    row.Cells[2].Text = PCTCTopics.Text;
                    row.Cells[3].Text = PCTC2.Text;
                    row.Cells[4].Text = PCTC3.Text;
                    row.Cells[5].Text = PCTC4.Text;
                    row.Cells[6].Text = PCTC5.Text;
                }
            }

        end: { }
        }

        #endregion TableTasks

        #region RowsControll
        protected void CreateRowPlanTable_Click(object sender, EventArgs e)
        {
            PlanTableTask(1);
            RebuildTables(1);
        }
        protected void DeleteRowPlanTable_Click(object sender, EventArgs e)
        {
            PlanTableTask(-1);
            RebuildTables(1);
        }

        protected void CreateRowFondTable_Click(object sender, EventArgs e)
        {
            FondTableTask(1);
            RebuildTables(2);
        }
        protected void DeleteRowFondTable_Click(object sender, EventArgs e)
        {
            FondTableTask(-1);
            RebuildTables(2);
        }

        protected void CreateRowsCriterionsTable_Click(object sender, EventArgs e)
        {
            CriterionsTableTask(1);
            RebuildTables(3);
        }
        protected void DeleteRowsCriterionsTable_Click(object sender, EventArgs e)
        {
            CriterionsTableTask(-1);
            RebuildTables(3);
        }

        protected void AddRowDSCT_Button_Click(object sender, EventArgs e)
        {
            DSTTask(1);
            RebuildTables(4);
        }
        protected void DeleteRowDSCT_Button_Click(object sender, EventArgs e)
        {
            DSTTask(-1);
            RebuildTables(4);
        }

        protected void button_createRowInPFT_1_Click(object sender, EventArgs e)
        {
            PFTTask(1, 1);
            RebuildTables(5);
        }
        protected void button_createRowInPFT_2_Click(object sender, EventArgs e)
        {
            PFTTask(1, 2);
            RebuildTables(6);
        }
        protected void button_createRowInPFT_3_Click(object sender, EventArgs e)
        {
            PFTTask(1, 3);
            RebuildTables(7);
        }
        protected void button_deleteRowInPFT_1_Click(object sender, EventArgs e)
        {
            PFTTask(-1, 1);
            RebuildTables(5);
        }
        protected void button_deleteRowInPFT_2_Click(object sender, EventArgs e)
        {
            PFTTask(-1, 2);
            RebuildTables(6);
        }
        protected void button_deleteRowInPFT_3_Click(object sender, EventArgs e)
        {
            PFTTask(-1, 3);
            RebuildTables(7);
        }

        protected void button_addRowInPAT_Click(object sender, EventArgs e)
        {
            PATTask(1);
            RebuildTables(8);
            button_addRowInPAT.Focus();
        }
        protected void button_deleteRowInPAT_Click(object sender, EventArgs e)
        {
            PATTask(-1);
            RebuildTables(8);
        }

        protected void addRowInPCTzachet_Click(object sender, EventArgs e)
        {
            PCTzachetTask(1);
            RebuildTables(9);
        }
        protected void deleteRowInPCTzachet_Click(object sender, EventArgs e)
        {
            PCTzachetTask(-1);
            RebuildTables(9);
        }

        protected void addRowInPCTExamTable_Click(object sender, EventArgs e)
        {
            PCTexamTask(1);
            RebuildTables(10);
        }
        protected void deleteRowInPCTExamTable_Click(object sender, EventArgs e)
        {
            PCTexamTask(-1);
            RebuildTables(10);
        }

        protected void addRowInPCTReferatTable_Click(object sender, EventArgs e)
        {
            PCTreferatTask(1);
            RebuildTables(11);
        }
        protected void deleteRowInPCTReferatTable_Click(object sender, EventArgs e)
        {
            PCTreferatTask(-1);
            RebuildTables(11);
        }

        protected void addRowInPCTKTTable_Click(object sender, EventArgs e)
        {
            PCTktTask(1);
            RebuildTables(12);
        }
        protected void deleteRowInPCTKTTable_Click(object sender, EventArgs e)
        {
            PCTktTask(-1);
            RebuildTables(12);
        }

        protected void addRowInPCTPraktikaTable_Click(object sender, EventArgs e)
        {
            PCTpraktikaTask(1);
            RebuildTables(13);
        }
        protected void deleteRowInPCTPraktikaTable_Click(object sender, EventArgs e)
        {
            PCTpraktikaTask(-1);
            RebuildTables(13);
        }

        protected void addRowInPCTtestTable_Click(object sender, EventArgs e)
        {
            PCTtestTask(1);
            RebuildTables(14);
        }
        protected void deleteRowInPCTtestTable_Click(object sender, EventArgs e)
        {
            PCTtestTask(-1);
            RebuildTables(14);
        }

        protected void addRowInPCTcursTable_Click(object sender, EventArgs e)
        {
            PCTcursTask(1);
            RebuildTables(15);
        }
        protected void deleteRowInPCTcursTable_Click(object sender, EventArgs e)
        {
            PCTcursTask(-1);
            RebuildTables(15);
        }
        #endregion RowsControlls

        struct NumberString
        {
            public int number;
            public string text;
            public NumberString(int num, string txt)
            {
                this.number = num;
                this.text = txt;
            }
        }

        protected void SemesteresContentFill_Button_Click(object sender, EventArgs e)
        {
            SemesteresContent.Text = "";
            RebuildTables(0);
            List<NumberString> sems = new List<NumberString>();
            NumberString sem1 = new NumberString(1, "Первый");
            NumberString sem2 = new NumberString(2, "Второй");
            NumberString sem3 = new NumberString(3, "Третий");
            NumberString sem4 = new NumberString(4, "Четвертый");
            NumberString sem5 = new NumberString(5, "Пятый");
            NumberString sem6 = new NumberString(6, "Шестой");
            NumberString sem7 = new NumberString(7, "Седьмой");
            NumberString sem8 = new NumberString(8, "Восьмой");
            sems.Add(sem1);
            sems.Add(sem2);
            sems.Add(sem3);
            sems.Add(sem4);
            sems.Add(sem5);
            sems.Add(sem6);
            sems.Add(sem7);
            sems.Add(sem8);

            List<int> tsems = new List<int>();
            for (int i = 2; i < DisciplineStructureTable.Rows.Count; i++)
            {
                try
                {
                    int s = Convert.ToInt32(DisciplineStructureTable.Rows[i].Cells[2].Text);
                    tsems.Add(s);
                }
                catch { }
            }

            tsems = tsems.Distinct().ToList();

            string str = "";
            for (int i = 0; i < 8; i++)
                if (sems[i].number == Convert.ToInt32(DisciplineStructureTable.Rows[2].Cells[2].Text))
                    str = sems[i].text;

            SemesteresContent.Text += "<p style=\"margin-top:0px; margin-bottom:0px;\"><strong>" + str + " семестр.</strong></p>";
            for (int i = 2; i < DisciplineStructureTable.Rows.Count; i++)
            {
                try
                {
                    if (DisciplineStructureTable.Rows[i + 1].Cells[1].Text == "Часы за весь период") break;
                }
                catch { }

                if (DisciplineStructureTable.Rows[i].Cells[1].Text == "Часы за семестр")
                {
                    try
                    {
                        for (int n = 0; n < 8; n++)
                            if (sems[n].number == Convert.ToInt32(DisciplineStructureTable.Rows[i + 1].Cells[2].Text))
                                str = sems[n].text;

                        SemesteresContent.Text += "<p style=\"margin-top:0px; margin-bottom:0px;\"><strong>" + str + " семестр.</strong></p>";
                    }
                    catch { SemesteresContent.Text += DisciplineStructureTable.Rows[i + 1].Cells[2].Text + " семестр.<br/>"; }

                }
                else
                    SemesteresContent.Text += "<p style=\"margin-top:0px; margin-bottom:0px;\">" + DisciplineStructureTable.Rows[i].Cells[1].Text + ":" + "</p> " +
                        "<p style=\"margin-top:0px; margin-bottom:0px;\"></p>";
            }
            SemesteresContent.Focus();
        }

        protected void DisciplineStructureContentFill_Button_Click(object sender, EventArgs e)
        {
            RebuildTables(0);
            int count = DisciplineStructureTable.Rows.Count;
            int generalsumm = 0;
            int zed = 0;
            int srs = 0;
            int maxsem = 0;
            string zach;
            string gen;
            string srsstr;

            List<NumberString> sems = new List<NumberString>();
            NumberString sem1 = new NumberString(1, "В первом");
            NumberString sem2 = new NumberString(2, "Во втором ");
            NumberString sem3 = new NumberString(3, "В третьем");
            NumberString sem4 = new NumberString(4, "В четвертом");
            NumberString sem5 = new NumberString(5, "В пятом");
            NumberString sem6 = new NumberString(6, "В шестом");
            NumberString sem7 = new NumberString(7, "В седьмом");
            NumberString sem8 = new NumberString(8, "В восьмом");
            sems.Add(sem1);
            sems.Add(sem2);
            sems.Add(sem3);
            sems.Add(sem4);
            sems.Add(sem5);
            sems.Add(sem6);
            sems.Add(sem7);
            sems.Add(sem8);

            List<int> tsems = new List<int>();
            for (int i = 0; i < DisciplineStructureTable.Rows.Count; i++)
            {
                try
                {
                    int s = Convert.ToInt32(DisciplineStructureTable.Rows[i].Cells[2].Text);
                    tsems.Add(s);
                }
                catch { }

            }

            tsems = tsems.Distinct().ToList();

            Table table = DisciplineStructureTable;
            for (int i = 4; i < 15; i++)
            {
                int buf = 0;
                try
                {
                    buf = Convert.ToInt32(table.Rows[count - 1].Cells[i].Text);
                    generalsumm += buf;
                }
                catch { }
            }

            zed = Convert.ToInt32(Math.Truncate(Convert.ToDecimal(generalsumm / 36)));
            try { srs = Convert.ToInt32(table.Rows[count - 1].Cells[7].Text); }
            catch { }

            if (zed % 10 < 5) zach = " зачетные единицы";
            else zach = " зачетных единиц";
            if (zed % 10 == 1) zach = " зачетная единица";
            if ((zed > 9) && (zed < 21)) zach = " зачетных единиц";

            if (generalsumm % 10 < 5) gen = " академических часа";
            else gen = " академических часов";
            if (generalsumm % 10 == 1) gen = " академический час";
            if ((generalsumm > 9) && (generalsumm < 21)) gen = " академических часов";

            if (srs % 10 < 5) srsstr = " часа";
            else srsstr = " часов";
            if (srs % 10 == 1) srsstr = " час";
            if ((srs > 9) && (srs < 21)) srsstr = " часов";

            DisciplineStructureContent.Text = "<p style=\"margin-top:0px; margin-bottom:0px\">Общая трудоемкость дисциплины составляет <strong>" + zed.ToString() + "</strong>"
                + zach + ", или <strong>" + generalsumm.ToString() + "</strong>" + gen +
                " (из них <strong>" + srs.ToString() + "</strong>" + srsstr + " – самостоятельная работа студентов).</p>";

            for (int i = 2; i < count; i++)
            {
                try
                {
                    if (Convert.ToInt32(table.Rows[i].Cells[2].Text) > maxsem)
                        maxsem = Convert.ToInt32(table.Rows[i].Cells[2].Text);
                }
                catch { }
            }

            for (int i = 0; i < tsems.Count; i++)
            {
                if (i == 8) break;

                generalsumm = 0;
                zed = 0;
                srs = 0;
                zach = "";
                gen = "";
                srsstr = "";
                int l = 0;
                int p = 0;
                string lstr, pstr;
                string attestatform = "";
                for (int j = 2; j < count; j++)
                {
                    if ((table.Rows[j].Cells[1].Text == "Часы за семестр") &&
                        (Convert.ToInt32(table.Rows[j].Cells[2].Text) == (tsems[i])))
                    {

                        for (int k = 4; k < 15; k++)
                        {
                            int buf = 0;
                            try
                            {
                                buf = Convert.ToInt32(table.Rows[j].Cells[k].Text);
                                generalsumm += buf;
                            }
                            catch { }
                        }
                        zed = Convert.ToInt32(Math.Truncate(Convert.ToDecimal(generalsumm / 36)));
                        try { srs = Convert.ToInt32(table.Rows[j].Cells[7].Text); }
                        catch { }

                        try { l = Convert.ToInt32(table.Rows[j].Cells[4].Text); }
                        catch { }

                        try { p = Convert.ToInt32(table.Rows[j].Cells[5].Text); }
                        catch { }

                        attestatform = table.Rows[j].Cells[14].Text;
                        break;
                    }
                }

                if (zed % 10 < 5) zach = " зачетные единицы";
                else zach = " зачетных единиц";
                if (zed % 10 == 1) zach = " зачетная единица";
                if ((zed > 9) && (zed < 21)) zach = " зачетных единиц";

                if (generalsumm % 10 < 5) gen = " академических часа";
                else gen = " академических часов";
                if (generalsumm % 10 == 1) gen = " академический час";
                if ((generalsumm > 9) && (generalsumm < 21)) gen = " академических часов";

                if (srs % 10 < 5) srsstr = " часа";
                else srsstr = " часов";
                if (srs % 10 == 1) srsstr = " час";
                if ((srs > 9) && (srs < 21)) srsstr = " часов";

                if (l % 10 < 5) lstr = " часа";
                else lstr = " часов";
                if (l % 10 == 1) lstr = " час";
                if ((l > 9) && (l < 21)) lstr = " часов";

                if (p % 10 < 5) pstr = " часа";
                else pstr = " часов";
                if (p % 10 == 1) pstr = " час";
                if ((p > 9) && (p < 21)) pstr = " часов";

                string str = "";
                for (int n = 0; n < sems.Count; n++)
                    if (sems[n].number == tsems[i])
                        str = sems[n].text;

                DisciplineStructureContent.Text += "<p style=\"margin-top:0px; margin-bottom:0px\">" + "<strong>" + str + "</strong>" +
                    " семестре выделяется <strong>" + zed.ToString() + "</strong>"
                    + zach + ", или <strong>" + generalsumm.ToString() + "</strong>" + gen +
                    " (из них <strong>" + srs.ToString() + "</strong>" + srsstr + " – самостоятельная работа студентов). " +
                    "Лекции – <strong>" + l.ToString() + "</strong>" + lstr + ". " +
                    "Практические занятия – <strong>" + p.ToString() + "</strong>" + pstr + ". " +
                    "Форма аттестации – " + attestatform + ".</p>";
            }

            DisciplineStructureContent.Focus();
        }

        protected void SortDSCT_Button_Click(object sender, EventArgs e)
        {
            DSTTask(2);
            RebuildTables(4);
            AddRowDSCT_Button.Enabled = false;
            DeleteRowDSCT_Button.Enabled = false;
            SortDSCT_Button.Enabled = false;
            DisciplineStructureContent.Focus();
        }

        protected void DSTClear_Button_Click(object sender, EventArgs e)
        {
            RebuildTables(0);
            while (DisciplineStructureTable.Rows.Count > 2)
                DisciplineStructureTable.Rows.RemoveAt(2);
            AddRowDSCT_Button.Enabled = true;
            DeleteRowDSCT_Button.Enabled = true;
            SortDSCT_Button.Enabled = true;
            DSTrowscount.Value = "0";
            AddRowDSCT_Button.Focus();
        }

        #region RedactTasks

        #region FirstTab

        protected void Button_Institute_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                pro.Institute = Institute.Text;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    string userid = User.Identity.GetUserId();
                    task.UpDateInstitute(pro, userid);
                }
            }
        }

        protected void Button_ApproverPost_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                pro.Approver_Post = Approver_Post.Text;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    string userid = User.Identity.GetUserId();
                    task.UpDateApprover_Post(pro, userid);
                }
            }
        }

        protected void Button_ApproverName_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                pro.Approver_Name = Approver_Name.SelectedItem.Text;
                pro.Approver_Id = Approver_Name.SelectedItem.Value;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    string userid = User.Identity.GetUserId();
                    task.UpDateApprover_Name(pro, userid);
                }
            }
        }

        protected void Button_ApproveDate_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                try
                {
                    string dateInput = Approve_Date.Text;
                    DateTime d = DateTime.Parse(dateInput);
                    pro.Approve_Date = d;
                    for (int i = 0; i < IDes.Count; i++)
                    {
                        pro.Id = IDes[i];
                        task.UpDateApprove_Date(pro, User.Identity.GetUserId());
                    }
                }
                catch { }
            }
        }

        protected void Button_Discipline_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                pro.Discipline = Discipline.Text;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDateDiscipline(pro, User.Identity.GetUserId());
                }
            }
        }

        protected void Button_TrainingDirection_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                pro.Training_Direction = Training_Direction.Text;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDateTraining_Direction(pro, User.Identity.GetUserId());
                }
            }
        }

        protected void Button_Profil_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                pro.Profil = DropDown_Profil.SelectedItem.Text;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDateProfil(pro, User.Identity.GetUserId());
                }
            }
        }

        protected void Button_GraduateQualification_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                pro.Graduate_Qualification = Graduate_Qualification.Text;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDateGraduate_Qualification(pro, User.Identity.GetUserId());
                }
            }
        }

        protected void Button_TrainingForm_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                pro.Training_Form = Training_Form.Text;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDateTraining_Form(pro, User.Identity.GetUserId());
                }
            }
        }

        protected void Button_StudyPeriod_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                pro.StudPeriod = StudyPeriod.Text;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDateStudyPeriod(pro, User.Identity.GetUserId());
                }
            }
        }

        protected void Button_City_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                pro.City = City.Text;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDateCity(pro, User.Identity.GetUserId());
                }
            }
        }


        #endregion FirstTab

        #region SecondTab

        protected void Button_ObjectivesOfDiscipline_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                pro.ObjectivesOfDiscipline = ObjectivesOfDiscipline.Text;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDateObjectivesOfDiscipline(pro, User.Identity.GetUserId());
                }
            }
        }

        protected void Button_DisciplineInOOP_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                pro.DisciplineInOOP = DisciplineInOOP.Text;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDateDisciplineInOOP(pro, User.Identity.GetUserId());
                }
            }
        }

        //protected void Button_PlanDescription_Click(object sender, EventArgs e)
        //{
        //    if (FillIDesFromSession())
        //    {
        //        Pro pro = new Pro();
        //        BaseWork task = new BaseWork();
        //        pro.PlanDescription = PlanDescription.Text;
        //        for (int i = 0; i < IDes.Count; i++)
        //        {
        //            pro.Id = IDes[i];
        //            task.UpDatePlanDescription(pro, User.Identity.GetUserId());
        //        }
        //    }
        //}

        protected void Button_PlanTable_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                Pro pro = new Pro();
                RebuildTables(0);
                List<PlanTableRow> rows = new List<PlanTableRow>();
                for (int i = 1; i < PlanTable.Rows.Count; i++)
                {
                    PlanTableRow row = new PlanTableRow();
                    row.Сompetence = PlanTable.Rows[i].Cells[0].Text;
                    row.Ability = PlanTable.Rows[i].Cells[1].Text;
                    row.Enumeration = PlanTable.Rows[i].Cells[2].Text;
                    rows.Add(row);
                }
                pro.PlanTable = rows;

                BaseWork task = new BaseWork();
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpdatePlanTable(pro, User.Identity.GetUserId());
                }
            }
        }


        #endregion SecondTab

        #region ThirdTab
        protected void Button_DSCT_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                RebuildTables(0);
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                pro.DSTTable = DSTTable_FillTask();
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDateDSCT(pro, User.Identity.GetUserId());
                }
            }
        }

        protected void Button_DisciplineStructureContent_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                pro.DisciplineStructureContent = DisciplineStructureContent.Text;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDateDisciplineStructureContent(pro, User.Identity.GetUserId());
                }
            }
        }

        protected void Button_SemesteresContent_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                pro.SemesteresContent = SemesteresContent.Text;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDateSemesteresContent(pro, User.Identity.GetUserId());
                }
            }
        }

        protected void Button_EducationalTechnology_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                pro.EducationalTechnology = EducationalTechnology.Text;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDateEducationalTechnology(pro, User.Identity.GetUserId());
                }
            }
        }

        protected void Button_MonitoringToolsDescription_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                pro.MonitoringTools_Description = MonitoringTools_Description.Text;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDateMonitoringTools_Description(pro, User.Identity.GetUserId());
                }
            }
        }
        
        #endregion ThirdTab

        #region FourthTab

        protected void Button_PrilAssessTable_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                RebuildTables(0);
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                List<PrilAssessTableRow> rows_6 = new List<PrilAssessTableRow>();
                for (int i = 1; i < PrilAssessTable.Rows.Count; i++)
                {
                    PrilAssessTableRow row = new PrilAssessTableRow();
                    row.Name = PrilAssessTable.Rows[i].Cells[1].Text;
                    row.Spec = PrilAssessTable.Rows[i].Cells[2].Text;
                    row.InFOS = PrilAssessTable.Rows[i].Cells[3].Text;
                    rows_6.Add(row);
                }
                pro.PAT = rows_6;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDatePrilAssessTable(pro, User.Identity.GetUserId());
                }
            }
        }

        protected void Button_MonitoringTools_FondTable_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                RebuildTables(0);
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                List<FondTableRow> rows_1 = new List<FondTableRow>();
                for (int i = 1; i < MonitoringTools_FondTable.Rows.Count; i++)
                {
                    FondTableRow row_1 = new FondTableRow();
                    row_1.Сompetence = MonitoringTools_FondTable.Rows[i].Cells[0].Text;
                    row_1.Ability = MonitoringTools_FondTable.Rows[i].Cells[1].Text;
                    rows_1.Add(row_1);
                }
                pro.FondTable = rows_1;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDateFondTable(pro, User.Identity.GetUserId());
                }
            }
        }

        protected void Button_CriterionsTable_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                RebuildTables(0);
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                List<CriterionsTableRow> rows_2 = new List<CriterionsTableRow>();
                for (int i = 2; i < CriterionsTable.Rows.Count; i += 3)
                {
                    CriterionsTableRow row_2 = new CriterionsTableRow();
                    row_2.Competence = CriterionsTable.Rows[i].Cells[0].Text;

                    row_2.ToKnow_Description = CriterionsTable.Rows[i].Cells[1].Text;
                    List<string> toknow = new List<string>();
                    for (int j = 2; j <= 5; j++)
                    {
                        toknow.Add(CriterionsTable.Rows[i].Cells[j].Text);
                    }
                    row_2.ToKnow = toknow;

                    row_2.ToDo_Description = CriterionsTable.Rows[i + 1].Cells[1].Text;
                    List<string> todo = new List<string>();
                    for (int j = 2; j <= 5; j++)
                    {
                        todo.Add(CriterionsTable.Rows[i + 1].Cells[j].Text);
                    }
                    row_2.ToDo = todo;

                    row_2.ToMaster_Description = CriterionsTable.Rows[i + 2].Cells[1].Text;
                    List<string> tomaster = new List<string>();
                    for (int j = 2; j <= 5; j++)
                    {
                        tomaster.Add(CriterionsTable.Rows[i + 2].Cells[j].Text);
                    }
                    row_2.ToMaster = tomaster;
                    rows_2.Add(row_2);
                }
                pro.CriterionsTable = rows_2;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDateCriterionsTable(pro, User.Identity.GetUserId());
                }
            }
        }

        protected void Button_Zachet_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                pro.ZachetDescription = ZachetDescription.Text;
                ZachetTableRow zachet = new ZachetTableRow();
                zachet.ZachetDone = Zachteno.Text;
                zachet.ZachetUnDone = NeZachteno.Text;
                pro.ZachetTable = zachet;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDateZachetDescription(pro, User.Identity.GetUserId());
                    task.UpDateZachetTableZachetDone(pro);
                    task.UpDateZachetTableZachetUnDone(pro);
                }
            }
        }

        protected void Button_Exam_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                pro.ExamDescription = ExamDescription.Text;
                ExamTableRow exam = new ExamTableRow();
                exam.AttestatGreat = AttestatGreat.Text;
                exam.AttestatGood = AttestatGood.Text;
                exam.AttestatSatisfactorily = AttestatSatisfactorily.Text;
                exam.AttestatNotSatisfactorily = AttestatNotSatisfactorily.Text;
                pro.ExamTable = exam;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDateExamDescription(pro, User.Identity.GetUserId());
                    task.UpDateExamTableAttestatGreat(pro);
                    task.UpDateExamTableAttestatGood(pro);
                    task.UpDateExamTableAttestatSatisfactorily(pro);
                    task.UpDateExamTableAttestatNotSatisfactorily(pro);
                }
            }
        }
        
        #endregion FourthTab

        #region FifthTab

        protected void Button_PFT1_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                RebuildTables(0);
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                List<PrilFondTableRow> pft1 = new List<PrilFondTableRow>();
                for (int i = 3; i < PrilFondTable_1.Rows.Count; i++)
                {
                    PrilFondTableRow row = new PrilFondTableRow();
                    row.Index = PrilFondTable_1.Rows[i].Cells[0].Text;
                    row.Formul = PrilFondTable_1.Rows[i].Cells[1].Text;
                    row.KomponentsList = PrilFondTable_1.Rows[i].Cells[2].Text;
                    row.CompetenceTechnology = PrilFondTable_1.Rows[i].Cells[3].Text;
                    row.AssessForm = PrilFondTable_1.Rows[i].Cells[4].Text;
                    row.Steps = PrilFondTable_1.Rows[i].Cells[5].Text;
                    pft1.Add(row);
                }
                pro.PFT_1 = pft1;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDatePFTUniversal(pro, User.Identity.GetUserId());
                }
            }
        }

        protected void Button_PFT2_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                RebuildTables(0);
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                List<PrilFondTableRow> pft2 = new List<PrilFondTableRow>();
                for (int i = 3; i < PrilFondTable_2.Rows.Count; i++)
                {
                    PrilFondTableRow row = new PrilFondTableRow();
                    row.Index = PrilFondTable_2.Rows[i].Cells[0].Text;
                    row.Formul = PrilFondTable_2.Rows[i].Cells[1].Text;
                    row.KomponentsList = PrilFondTable_2.Rows[i].Cells[2].Text;
                    row.CompetenceTechnology = PrilFondTable_2.Rows[i].Cells[3].Text;
                    row.AssessForm = PrilFondTable_2.Rows[i].Cells[4].Text;
                    row.Steps = PrilFondTable_2.Rows[i].Cells[5].Text;
                    pft2.Add(row);
                }
                pro.PFT_2 = pft2;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDatePFTGeneral(pro, User.Identity.GetUserId());
                }
            }
        }

        protected void Button_PFT3_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                RebuildTables(0);
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                List<PrilFondTableRow> pft3 = new List<PrilFondTableRow>();
                for (int i = 3; i < PrilFondTable_3.Rows.Count; i++)
                {
                    PrilFondTableRow row = new PrilFondTableRow();
                    row.Index = PrilFondTable_3.Rows[i].Cells[0].Text;
                    row.Formul = PrilFondTable_3.Rows[i].Cells[1].Text;
                    row.KomponentsList = PrilFondTable_3.Rows[i].Cells[2].Text;
                    row.CompetenceTechnology = PrilFondTable_3.Rows[i].Cells[3].Text;
                    row.AssessForm = PrilFondTable_3.Rows[i].Cells[4].Text;
                    row.Steps = PrilFondTable_3.Rows[i].Cells[5].Text;
                    pft3.Add(row);
                }
                pro.PFT_3 = pft3;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpdatePFTProfessional(pro, User.Identity.GetUserId());
                }
            }
        }

        #endregion FifthTab

        #region SixthTab

        protected void Button_ZachetTab_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                RebuildTables(0);
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                List<PCTZachetTableRow> rows_7 = new List<PCTZachetTableRow>();
                for (int i = 3; i < PrilCompetenceTable_Zachet.Rows.Count; i++)
                {
                    PCTZachetTableRow row = new PCTZachetTableRow();
                    row.Discription = PrilCompetenceTable_Zachet.Rows[i].Cells[0].Text;
                    row.Rezult = PrilCompetenceTable_Zachet.Rows[i].Cells[1].Text;
                    row.Topics = PrilCompetenceTable_Zachet.Rows[i].Cells[2].Text;
                    row.Zachet = PrilCompetenceTable_Zachet.Rows[i].Cells[3].Text;
                    row.NeZachet = PrilCompetenceTable_Zachet.Rows[i].Cells[4].Text;
                    rows_7.Add(row);
                }
                pro.PCT_Zachet = rows_7;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDatePCTZachet(pro, User.Identity.GetUserId());
                }
            }
        }

        protected void Button_ZachetQ_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                RebuildTables(0);
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                Questions_Zachet questions_zachet = new Questions_Zachet();
                try
                {
                    questions_zachet.Questions = ZachetQuestions.Text;
                    questions_zachet.Developer = DeveloperZachet.SelectedItem.Text;
                    string dateInput = ZachetQuestionsDate.Text;
                    DateTime d = DateTime.Parse(dateInput);
                    questions_zachet.Date = d;
                    pro.Questions_Zachet = questions_zachet;
                }
                catch { }
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDateQuestionsZachet(pro, User.Identity.GetUserId());
                }
            }
        }

        protected void Button_ExamTab_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                RebuildTables(0);
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                List<PCTExamTableRow> rows_8 = new List<PCTExamTableRow>();
                for (int i = 3; i < PrilCompetenceTable_Exam.Rows.Count; i++)
                {
                    PCTExamTableRow row = new PCTExamTableRow();
                    row.Discription = PrilCompetenceTable_Exam.Rows[i].Cells[0].Text;
                    row.Rezult = PrilCompetenceTable_Exam.Rows[i].Cells[1].Text;
                    row.Topics = PrilCompetenceTable_Exam.Rows[i].Cells[2].Text;
                    row.On2 = PrilCompetenceTable_Exam.Rows[i].Cells[3].Text;
                    row.On3 = PrilCompetenceTable_Exam.Rows[i].Cells[4].Text;
                    row.On4 = PrilCompetenceTable_Exam.Rows[i].Cells[5].Text;
                    row.On5 = PrilCompetenceTable_Exam.Rows[i].Cells[6].Text;
                    rows_8.Add(row);
                }
                pro.PCT_Exam = rows_8;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDatePCTExam(pro, User.Identity.GetUserId());
                }
            }
        }

        protected void Button_ExamQ_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                RebuildTables(0);
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                Questions_Exam questions_exam = new Questions_Exam();
                try
                {
                    questions_exam.Questions = ExamQuestions.Text;
                    questions_exam.Developer = DeveloperExam.SelectedItem.Text;
                    string date = ExamQuestionsDate.Text;
                    DateTime d = DateTime.Parse(date);
                    questions_exam.Date = d;
                    pro.Questions_Exam = questions_exam;
                }
                catch { }
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDateQuestionsExam(pro, User.Identity.GetUserId());
                }
            }
        }

        protected void Button_ReferatTab_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                RebuildTables(0);
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                List<PCTReferatTableRow> rows_9 = new List<PCTReferatTableRow>();
                for (int i = 3; i < PrilCompetenceTable_Referat.Rows.Count; i++)
                {
                    PCTReferatTableRow row = new PCTReferatTableRow();
                    row.Discription = PrilCompetenceTable_Referat.Rows[i].Cells[0].Text;
                    row.Rezult = PrilCompetenceTable_Referat.Rows[i].Cells[1].Text;
                    row.Topics = PrilCompetenceTable_Referat.Rows[i].Cells[2].Text;
                    row.On2 = PrilCompetenceTable_Referat.Rows[i].Cells[3].Text;
                    row.On3 = PrilCompetenceTable_Referat.Rows[i].Cells[4].Text;
                    row.On4 = PrilCompetenceTable_Referat.Rows[i].Cells[5].Text;
                    row.On5 = PrilCompetenceTable_Referat.Rows[i].Cells[6].Text;
                    rows_9.Add(row);
                }
                pro.PCT_Referat = rows_9;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDatePCTReferat(pro, User.Identity.GetUserId());
                }
            }
        }

        protected void Button_ReferatQ_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                RebuildTables(0);
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                Questions_Referat questions_referat = new Questions_Referat();
                try
                {
                    questions_referat.Questions = ReferatTopics.Text;
                    questions_referat.Developer = DeveloperReferat.SelectedItem.Text;
                    string date = ReferatTopicsDate.Text;
                    DateTime d = DateTime.Parse(date);
                    questions_referat.Date = d;
                    pro.Questions_Referat = questions_referat;
                }
                catch { }
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDateQuestionsReferat(pro, User.Identity.GetUserId());
                }
            }
        }

        protected void Button_KTTab_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                RebuildTables(0);
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                List<PCTKtTableRow> rows_10 = new List<PCTKtTableRow>();
                for (int i = 3; i < PrilCompetenceTable_KT.Rows.Count; i++)
                {
                    PCTKtTableRow row = new PCTKtTableRow();
                    row.Discription = PrilCompetenceTable_KT.Rows[i].Cells[0].Text;
                    row.Rezult = PrilCompetenceTable_KT.Rows[i].Cells[1].Text;
                    row.Topics = PrilCompetenceTable_KT.Rows[i].Cells[2].Text;
                    row.On2 = PrilCompetenceTable_KT.Rows[i].Cells[3].Text;
                    row.On3 = PrilCompetenceTable_KT.Rows[i].Cells[4].Text;
                    row.On4 = PrilCompetenceTable_KT.Rows[i].Cells[5].Text;
                    row.On5 = PrilCompetenceTable_KT.Rows[i].Cells[6].Text;
                    rows_10.Add(row);
                }
                pro.PCT_KT = rows_10;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDatePCTKT(pro, User.Identity.GetUserId());
                }
            }
        }

        protected void Button_KTQ_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                RebuildTables(0);
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                Questions_KT questions_kt = new Questions_KT();
                try
                {
                    questions_kt.Questions = KTTopics.Text;
                    questions_kt.Developer = DeveloperKT.SelectedItem.Text;
                    string date = KTDate.Text;
                    DateTime d = DateTime.Parse(date);
                    questions_kt.Date = d;
                    pro.Questions_KT = questions_kt;
                }
                catch { }
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDateQuestionsKT(pro, User.Identity.GetUserId());
                }
            }
        }

        protected void Button_PraktikaTab_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                RebuildTables(0);
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                List<PCTPraktikaTableRow> rows_11 = new List<PCTPraktikaTableRow>();
                for (int i = 3; i < PrilCompetenceTable_Praktika.Rows.Count; i++)
                {
                    PCTPraktikaTableRow row = new PCTPraktikaTableRow();
                    row.Discription = PrilCompetenceTable_Praktika.Rows[i].Cells[0].Text;
                    row.Rezult = PrilCompetenceTable_Praktika.Rows[i].Cells[1].Text;
                    row.Topics = PrilCompetenceTable_Praktika.Rows[i].Cells[2].Text;
                    row.On2 = PrilCompetenceTable_Praktika.Rows[i].Cells[3].Text;
                    row.On3 = PrilCompetenceTable_Praktika.Rows[i].Cells[4].Text;
                    row.On4 = PrilCompetenceTable_Praktika.Rows[i].Cells[5].Text;
                    row.On5 = PrilCompetenceTable_Praktika.Rows[i].Cells[6].Text;
                    rows_11.Add(row);
                }
                pro.PCT_Praktika = rows_11;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDatePCTPraktika(pro, User.Identity.GetUserId());
                }
            }
        }

        protected void Button_PraktikaQ_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                RebuildTables(0);
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                Questions_Praktika questions_praktika = new Questions_Praktika();
                try
                {
                    questions_praktika.Questions = PraktikTopics.Text;
                    questions_praktika.Developer = DeveloperPraktika.SelectedItem.Text;
                    string date = PraktikaTopicsDate.Text;
                    DateTime d = DateTime.Parse(date);
                    questions_praktika.Date = d;
                    pro.Questions_Praktika = questions_praktika;
                }
                catch { }
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDateQuestionsPraktika(pro, User.Identity.GetUserId());
                }
            }
        }

        protected void Button_TestTab_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                RebuildTables(0);
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                List<PCTTestTableRow> rows_12 = new List<PCTTestTableRow>();
                for (int i = 3; i < PrilCompetenceTable_Test.Rows.Count; i++)
                {
                    PCTTestTableRow row = new PCTTestTableRow();
                    row.Discription = PrilCompetenceTable_Test.Rows[i].Cells[0].Text;
                    row.Rezult = PrilCompetenceTable_Test.Rows[i].Cells[1].Text;
                    row.Topics = PrilCompetenceTable_Test.Rows[i].Cells[2].Text;
                    row.On2 = PrilCompetenceTable_Test.Rows[i].Cells[3].Text;
                    row.On3 = PrilCompetenceTable_Test.Rows[i].Cells[4].Text;
                    row.On4 = PrilCompetenceTable_Test.Rows[i].Cells[5].Text;
                    row.On5 = PrilCompetenceTable_Test.Rows[i].Cells[6].Text;
                    rows_12.Add(row);
                }
                pro.PCT_Test = rows_12;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDatePCTTest(pro, User.Identity.GetUserId());
                }
            }
        }

        protected void Button_TestQ_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                RebuildTables(0);
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                Questions_Test questions_test = new Questions_Test();
                try
                {
                    questions_test.Questions = TestsFond.Text;
                    questions_test.Developer = DeveloperTest.SelectedItem.Text;
                    string date = TestsFondDate.Text;
                    DateTime d = DateTime.Parse(date);
                    questions_test.Date = d;
                    pro.Questions_Test = questions_test;
                }
                catch { }
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDateQuestionsTestsFond(pro, User.Identity.GetUserId());
                }
            }
        }

        protected void Button_CursTab_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                RebuildTables(0);
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                List<PCTCursTableRow> rows_12 = new List<PCTCursTableRow>();
                for (int i = 3; i < PrilCompetenceTable_Curs.Rows.Count; i++)
                {
                    PCTCursTableRow row = new PCTCursTableRow();
                    row.Discription = PrilCompetenceTable_Curs.Rows[i].Cells[0].Text;
                    row.Rezult = PrilCompetenceTable_Curs.Rows[i].Cells[1].Text;
                    row.Topics = PrilCompetenceTable_Curs.Rows[i].Cells[2].Text;
                    row.On2 = PrilCompetenceTable_Curs.Rows[i].Cells[3].Text;
                    row.On3 = PrilCompetenceTable_Curs.Rows[i].Cells[4].Text;
                    row.On4 = PrilCompetenceTable_Curs.Rows[i].Cells[5].Text;
                    row.On5 = PrilCompetenceTable_Curs.Rows[i].Cells[6].Text;
                    rows_12.Add(row);
                }
                pro.PCT_Curs = rows_12;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDatePCTCurs(pro, User.Identity.GetUserId());
                }
            }
        }

        protected void Button_CursQ_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                RebuildTables(0);
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                Questions_Curs questions_curs = new Questions_Curs();
                try
                {
                    questions_curs.Questions = CursesFond.Text;
                    questions_curs.Developer = DeveloperCurs.SelectedItem.Text;
                    string date = CursDate.Text;
                    DateTime d = DateTime.Parse(date);
                    questions_curs.Date = d;
                    pro.Questions_Curs = questions_curs;
                }
                catch { }
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDateQuestionsCursesFond(pro, User.Identity.GetUserId());
                }
            }
        }

        #endregion SixthTab

        #region SeventhTab

        protected void Button_BasicLiterature_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                pro.BasicLiterature = BasicLiterature.Text;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDateBasicLiterature(pro, User.Identity.GetUserId());
                }
            }
        }

        protected void Button_AdditionalLiterature_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                pro.AdditionalLiterature = AdditionalLiterature.Text;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDateAdditionalLiterature(pro, User.Identity.GetUserId());
                }
            }
        }

        protected void Button_ITResources_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                pro.ITResources = ITResources.Text;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDateITResources(pro, User.Identity.GetUserId());
                }
            }
        }

        protected void Button_MaterialTechnicalSupport_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                pro.MaterialTechnicalSupport = MaterialTechnicalSupport.Text;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDateMaterialTechnicalSupport(pro, User.Identity.GetUserId());
                }
            }
        }

        protected void Button_StudentRecommendations_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                pro.StudentRecommendations = StudentRecommendations.Text;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDateStudentRecommendations(pro, User.Identity.GetUserId());
                }
            }
        }

        protected void Button_TeacherRecommendations_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                pro.TeacherRecommendations = TeacherRecommendations.Text;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDateTeacherRecommendations(pro, User.Identity.GetUserId());
                }
            }
        }
        
        #endregion SeventhTab

        #region EigthTab

        protected void Button_Developer_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                Pro pro = new Pro();
                List<AdditionalDrafter> drafters = new List<AdditionalDrafter>();
                int count = Convert.ToInt32(AdditionalDraftersCount.Value);
                for (int i = 0; i < count; i++)
                {
                    AdditionalDrafter drafter = new AdditionalDrafter();
                    Panel Bufpanel = new Panel();
                    Bufpanel = (Panel)drafters_html_block.FindControl("DraftersPanel_" + i.ToString());
                    TextBox textbox = new TextBox();
                    DropDownList list = new DropDownList();
                    textbox = (TextBox)Bufpanel.FindControl("AdditionalDrafterStatus_" + i.ToString());
                    list = (DropDownList)Bufpanel.FindControl("AdditionalDrafterId_" + i.ToString());
                    drafter.Status = textbox.Text;
                    drafter.UserId = list.SelectedItem.Value;
                    drafters.Add(drafter);
                }
                pro.AdditionalDrafters = drafters;

                BaseWork task = new BaseWork();
                pro.DrafterStatus = DrafterStatus.Text;
                pro.DrafterName = DrafterName.SelectedItem.Text;
                pro.DrafterId = DrafterName.SelectedItem.Value;
                pro.Department = DropDown_Department.SelectedItem.Text;
                try
                {
                    string dateInput = DraftDate.Text;
                    DateTime d = DateTime.Parse(dateInput);
                    pro.DraftDate = d;
                }
                catch { pro.DraftDate = DateTime.Today; }
                pro.Protocol = Protocol.Text;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpdateDrafters(pro);
                    task.UpDateDrafterStatus(pro, User.Identity.GetUserId());
                    task.UpDateDrafterName(pro, User.Identity.GetUserId());
                    task.UpDateDraftDate(pro, User.Identity.GetUserId());
                    task.UpDateDepartment(pro, User.Identity.GetUserId());
                    task.UpDateProtocol(pro, User.Identity.GetUserId());
                }
            }
        }

        protected void Button_DepartmentHead_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                pro.DepartmentHeadStatus = DepartmentHeadStatus.Text;
                pro.DepartmentHeadName = DepartmentHeadName.SelectedItem.Text;
                pro.DepartmentHeadId = DepartmentHeadName.SelectedItem.Value;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDateDepartmentHeadStatus(pro, User.Identity.GetUserId());
                    task.UpDateDepartmentHeadName(pro, User.Identity.GetUserId());
                }
            }
        }

        protected void Button_Agreemers_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                pro.AgreementDepartmentHeadStatus = AgreementDepartmentHeadStatus.Text;
                pro.AgreementDepartmentHeadName = AgreementDepartmentHeadName.SelectedItem.Text;
                pro.AgreementDepartmentHeadId = AgreementDepartmentHeadName.SelectedItem.Value;
                pro.AgreementDirectorStatus = AgreementDirectorStatus.Text;
                pro.AgreementDirectorName = AgreementDirectorName.SelectedItem.Text;
                pro.AgreementDirectorId = AgreementDirectorName.SelectedItem.Value;
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.UpDateAgreementDepartmentHeadStatus(pro, User.Identity.GetUserId());
                    task.UpDateAgreementDepartmentHeadName(pro, User.Identity.GetUserId());
                    task.UpDateAgreementDirectorStatus(pro, User.Identity.GetUserId());
                    task.UpDateAgreementDirectorName(pro, User.Identity.GetUserId());
                }
            }
        }


        #endregion EigthTab

        #endregion RedactTasks

        protected void Button_ApproveProgram_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                Pro pro = new Pro();
                BaseWork task = new BaseWork();
                for (int i = 0; i < IDes.Count; i++)
                {
                    pro.Id = IDes[i];
                    task.ApproveProgram(pro.Id);
                }
            }
        }
        
        protected void Button_Report_Click(object sender, EventArgs e)
        {
            if (FillIDesFromSession())
            {
                try
                {
                    if (FillIDesFromSession())
                    {
                        for (int i = 0; i < IDes.Count; i++)
                        {
                            HtmlToOpenXMLBuilder builder = new HtmlToOpenXMLBuilder();
                            Converter converter = new Converter();
                            List<string> htmles = new List<string>();
                            Pro pro = new Pro();
                            BaseWork task = new BaseWork();
                            pro = task.GetProgram(IDes[i]);
                            htmles = converter.ToHtmlList(pro);
                            builder.Build(htmles, IDes[i]);
                        }
                    }
                }
                catch
                {
                    ErrorMes.Text = "Произошел сбой в процессе формирования файлов";
                    ErrorMes.Visible = true;
                }
            }
        }
        
        #region Draftercontrols
        protected void Button_AddDrafter_Click(object sender, EventArgs e)
        {
            int count = Convert.ToInt32(AdditionalDraftersCount.Value);
            Panel newPanel = new Panel();
            newPanel.Style["margin-top"] = "2px";
            newPanel.ID = "DraftersPanel_" + count.ToString(); ;
            TextBox newTextBox = new TextBox();
            newTextBox.CssClass = "form-control";
            newTextBox.Attributes["placeholder"] = "Академ. статус";
            newTextBox.Width = 150;
            newTextBox.ID = "AdditionalDrafterStatus_" + count.ToString();

            DropDownList newList = new DropDownList();
            newList.CssClass = "form-control";
            newList.Style["margin-left"] = "3px";
            newList.DataSourceID = "SqlDataSourceDepartmentPersonal";
            newList.DataTextField = "UserName";
            newList.DataValueField = DrafterName.DataValueField;
            newList.ID = "AdditionalDrafterId_" + count.ToString();

            newPanel.Controls.Add(newTextBox);
            newPanel.Controls.Add(newList);
            drafters_html_block.Controls.Add(newPanel);
            count++;
            AdditionalDraftersCount.Value = count.ToString();
        }

        protected void Button_DeleteDrafter_Click(object sender, EventArgs e)
        {
            int count = Convert.ToInt32(AdditionalDraftersCount.Value);
            if (count != 0)
            {
                string panelid = "DraftersPanel_" + (count - 1).ToString();
                drafters_html_block.Controls.Remove(drafters_html_block.FindControl(panelid));
                count--;
                AdditionalDraftersCount.Value = count.ToString();
            }
        }


        #endregion
        
    }
}