﻿using main_test.TemplateBuildaers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;

namespace main_test
{
    public class Report
    {
        public void CreateFile(int programid)
        {
            DataSet data = new DataSet();
            string connetionString = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            using (SqlConnection connection = new SqlConnection(connetionString))
            using (SqlDataAdapter adapter = new SqlDataAdapter())
            using (SqlCommand command = connection.CreateCommand())
            {
                connection.Open();
                command.CommandText = @"
                     SELECT * FROM [Programs] Where id = " + programid +
                    "SELECT * FROM [Plan] Where ProgramId = " + programid +
                    "SELECT * FROM [Fond] Where ProgramId = " + programid +
                    "SELECT * FROM [Criterions] Where ProgramId = " + programid +
                    "SELECT * FROM Criterions INNER JOIN Competences ON Criterions.id = Competences.id WHERE Criterions.Competence = Competences.Description AND Criterions.ProgramId = " + programid +
                    "SELECT * FROM [DisciplineStructurs] Where ProgramId = " + programid +
                    "SELECT * FROM [PrilFondUniversal] Where ProgramId = " + programid +
                    "SELECT * FROM [PrilFondGeneral] Where ProgramId = " + programid +
                    "SELECT * FROM [PrilFondProfessionally] Where ProgramId = " + programid +
                    "SELECT * FROM [PrilAssess] Where ProgramId = " + programid +
                    "SELECT * FROM [PrilCompetence_Zachet] Where ProgramId = " + programid +
                    "SELECT * FROM [Questions_Zachet] Where ProgramId = " + programid +
                    "SELECT * FROM [PrilCompetence_Exam] Where ProgramId = " + programid +
                    "SELECT * FROM [Questions_Exam] Where ProgramId = " + programid +
                    "SELECT * FROM [PrilCompetence_Referat] Where ProgramId = " + programid +
                    "SELECT * FROM [Questions_Referat] Where ProgramId = " + programid +
                    "SELECT * FROM [PrilCompetence_KT] Where ProgramId = " + programid +
                    "SELECT * FROM [Questions_KT] Where ProgramId = " + programid +
                    "SELECT * FROM [PrilCompetence_Praktika] Where ProgramId = " + programid +
                    "SELECT * FROM [Questions_Praktika] Where ProgramId = " + programid +
                    "SELECT * FROM [PrilCompetence_Test] Where ProgramId = " + programid +
                    "SELECT * FROM [Questions_Test] Where ProgramId = " + programid + ";";
                adapter.SelectCommand = command;
                adapter.Fill(data);
                connection.Close();
            }
            
            TemplateModel model = new TemplateModel()
            {
                ProgramData = new ProgramData(),
                Points = new List<PointsRow>(),
                Fonds = new List<FondsRow>(),
                Criterions = new List<CriterionsRow>(),
                Structurs = new List<StructursRow>(),
                PrilFoundUniv = new List<PrilFoundUnivRow>(),
                PrilFondGeneral = new List<PrilFondGeneralRow>(),
                PrilFondProfessionally = new List<PrilFondProfessionallyRow>(),
                PrilAssess = new List<PrilAssessRow>(),
                PrilCompetenceZachet = new List<PrilCompetenceZachetRow>(),
                PrilCompetenceExam = new List<PrilCompetenceRow>(),
                PrilCompetenceReferat = new List<PrilCompetenceReferatRow>(),
                PrilCompetencePractika = new List<PrilCompetencePractikaRow>(),
                PrilCompetenceKT = new List<PrilCompetenceKTRow>(),
                PrilCompetenceTest = new List<PrilCompetenceTestRow>()
            };
            try { model.ProgramData.ProgramId = (int)data.Tables[0].Rows[0]["Id"];} catch { }
            try { model.ProgramData.Institute = data.Tables[0].Rows[0]["Institute"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.Approver_Post = data.Tables[0].Rows[0]["Approver_Post"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.Approver_Name = data.Tables[0].Rows[0]["Approver_Name"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.Approve_Date = Convert.ToDateTime(data.Tables[0].Rows[0]["Approve_Date"]).ToString("D");} catch { }
            try { model.ProgramData.YearApprove_Date = Convert.ToDateTime(data.Tables[0].Rows[0]["Approve_Date"]).Year.ToString();} catch { }
            try { model.ProgramData.Discipline = data.Tables[0].Rows[0]["Discipline"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.Training_Direction = data.Tables[0].Rows[0]["Training_Direction"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.Profil = data.Tables[0].Rows[0]["Profil"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.Graduate_Qualification = data.Tables[0].Rows[0]["Graduate_Qualification"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.Training_Form = data.Tables[0].Rows[0]["Training_Form"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.City = data.Tables[0].Rows[0]["City"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.ObjectivesOfDiscipline = data.Tables[0].Rows[0]["ObjectivesOfDiscipline"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.DisciplineInOOP = data.Tables[0].Rows[0]["DisciplineInOOP"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.PlanDescription = data.Tables[0].Rows[0]["PlanDescription"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.DisciplineStructureContent = data.Tables[0].Rows[0]["DisciplineStructureContent"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.SemesteresContent = data.Tables[0].Rows[0]["SemesteresContent"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.EducationalTechnology = data.Tables[0].Rows[0]["EducationalTechnology"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.MonitoringTools_Description = data.Tables[0].Rows[0]["MonitoringTools_Description"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.ZachetDescription = data.Tables[0].Rows[0]["ZachetDescription"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.ZachetDone = data.Tables[0].Rows[0]["ZachetDone"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.ZachetUnDone = data.Tables[0].Rows[0]["ZachetUnDone"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.ExamDescription = data.Tables[0].Rows[0]["ExamDescription"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.AttestatGreat = data.Tables[0].Rows[0]["AttestatGreat"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.AttestatGood = data.Tables[0].Rows[0]["AttestatGood"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.AttestatSatisfactorily = data.Tables[0].Rows[0]["AttestatSatisfactorily"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.AttestatNotSatisfactorily = data.Tables[0].Rows[0]["AttestatNotSatisfactorily"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.BasicLiterature = data.Tables[0].Rows[0]["BasicLiterature"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.AdditionalLiterature = data.Tables[0].Rows[0]["AdditionalLiterature"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.ITResources = data.Tables[0].Rows[0]["ITResources"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.MaterialTechnicalSupport = data.Tables[0].Rows[0]["MaterialTechnicalSupport"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.StudentRecommendations = data.Tables[0].Rows[0]["StudentRecommendations"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.TeacherRecommendations = data.Tables[0].Rows[0]["TeacherRecommendations"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.DrafterStatus = data.Tables[0].Rows[0]["DrafterStatus"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.DrafterName = data.Tables[0].Rows[0]["DrafterName"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.Department = data.Tables[0].Rows[0]["Department"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.DraftDate = Convert.ToDateTime(data.Tables[0].Rows[0]["DraftDate"]).ToString("D");} catch { }
            try { model.ProgramData.Protocol = data.Tables[0].Rows[0]["Protocol"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.DepartmentHeadStatus = data.Tables[0].Rows[0]["DepartmentHeadStatus"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.DepartmentHeadName = data.Tables[0].Rows[0]["DepartmentHeadName"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.AgreementDepartmentHeadStatus = data.Tables[0].Rows[0]["AgreementDepartmentHeadStatus"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.AgreementDepartmentHeadName = data.Tables[0].Rows[0]["AgreementDepartmentHeadName"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.AgreementDirectorStatus = data.Tables[0].Rows[0]["AgreementDirectorStatus"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.AgreementDirectorName = data.Tables[0].Rows[0]["AgreementDirectorName"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.Status = data.Tables[0].Rows[0]["Status"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.Competence = data.Tables[3].Rows[0]["Competence"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.Discription = data.Tables[10].Rows[0]["Discription"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.Questions = data.Tables[11].Rows[0]["Questions"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.DeveloperZachet = data.Tables[11].Rows[0]["Developer"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.DateZachet = Convert.ToDateTime(data.Tables[11].Rows[0]["Date"]).ToString("D");} catch { }
            try { model.ProgramData.DescriptionExam = data.Tables[12].Rows[0]["Discription"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.DescriptionReferat = data.Tables[14].Rows[0]["Discription"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.DescriptionPractika = data.Tables[18].Rows[0]["Discription"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.DescriptionKT = data.Tables[16].Rows[0]["Discription"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.DescriptionTest = data.Tables[20].Rows[0]["Discription"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.DateExam = Convert.ToDateTime(data.Tables[13].Rows[0]["Date"]).ToString("D");} catch { }
            try { model.ProgramData.DeveloperExam = data.Tables[13].Rows[0]["Developer"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.QuestionsExam = data.Tables[13].Rows[0]["Questions"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.DevReferat = data.Tables[15].Rows[0]["Developer"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.DateReferat = Convert.ToDateTime(data.Tables[15].Rows[0]["Date"]).ToString("D");} catch { }
            try { model.ProgramData.QuestionsReferat = data.Tables[15].Rows[0]["Questions"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.QuestionsKT = data.Tables[17].Rows[0]["Questions"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.DateKT = Convert.ToDateTime(data.Tables[17].Rows[0]["Date"]).ToString("D");} catch { }
            try { model.ProgramData.DevKT = data.Tables[17].Rows[0]["Developer"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.DevPractika = data.Tables[19].Rows[0]["Developer"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.DatePractika = Convert.ToDateTime(data.Tables[19].Rows[0]["Date"]).ToString("D");} catch { }
            try { model.ProgramData.QuestionsPraktika = data.Tables[19].Rows[0]["Questions"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.QuestionsTest = data.Tables[21].Rows[0]["Questions"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.DevTest = data.Tables[21].Rows[0]["Developer"].ToString().Replace("<br/>", "<w:br/>").Replace("\n", "</w:t></w:r></w:p><w:p w:rsidR='00D11242'><w:r><w:t>"); } catch { }
            try { model.ProgramData.DateTest = Convert.ToDateTime(data.Tables[21].Rows[0]["Date"]).ToString("D"); } catch { }
            //model.ProgramData.Code = data.Tables[5].Rows[0]["Code"].ToString();


            foreach (DataRow row in data.Tables[1].Rows)
            {
                model.Points.Add(new PointsRow()
                {
                    Ability = row["Ability"].ToString().Replace("<br/>", "<w:br/>"),
                    Competence = row["Competence"].ToString().Replace("<br/>", "<w:br/>"),
                    Enumeration = row["Enumeration"].ToString().Replace("<br/>", "<w:br/>")
                });
            }

            foreach (DataRow row in data.Tables[2].Rows)
            {
                model.Fonds.Add(new FondsRow()
                {
                    Ability = row["Ability"].ToString().Replace("<br/>", "<w:br/>"),
                    Competence = row["Competence"].ToString().Replace("<br/>", "<w:br/>"),
                });
            }

            foreach (DataRow row in data.Tables[3].Rows)
            {
                model.Criterions.Add(new CriterionsRow()
                {
                    Competence = row["Competence"].ToString().Replace("<br/>", "<w:br/>"),
                    ToKnow = row["ToKnow"].ToString().Replace("<br/>", "<w:br/>"),
                    ToKnow_2 = row["ToKnow_2"].ToString().Replace("<br/>", "<w:br/>"),
                    ToKnow_3 = row["ToKnow_3"].ToString().Replace("<br/>", "<w:br/>"),
                    ToKnow_4 = row["ToKnow_4"].ToString().Replace("<br/>", "<w:br/>"),
                    ToKnow_5 = row["ToKnow_5"].ToString().Replace("<br/>", "<w:br/>"),
                    ToDo = row["ToDo"].ToString().Replace("<br/>", "<w:br/>"),
                    ToDo_2 = row["ToDo_2"].ToString().Replace("<br/>", "<w:br/>"),
                    ToDo_3 = row["ToDo_3"].ToString().Replace("<br/>", "<w:br/>"),
                    ToDo_4 = row["ToDo_4"].ToString().Replace("<br/>", "<w:br/>"),
                    ToDo_5 = row["ToDo_5"].ToString().Replace("<br/>", "<w:br/>"),
                    ToMaster = row["ToMaster"].ToString().Replace("<br/>", "<w:br/>"),
                    ToMaster_2 = row["ToMaster_2"].ToString().Replace("<br/>", "<w:br/>"),
                    ToMaster_3 = row["ToMaster_3"].ToString().Replace("<br/>", "<w:br/>"),
                    ToMaster_4 = row["ToMaster_4"].ToString().Replace("<br/>", "<w:br/>"),
                    ToMaster_5 = row["ToMaster_5"].ToString().Replace("<br/>", "<w:br/>")
                });
            }

            foreach (DataRow row in data.Tables[5].Rows)
            {
                model.Structurs.Add(new StructursRow()
                {
                    Тема = row["Тема"].ToString().Replace("<br/>", "<w:br/>"),
                    Семестр = row["Семестр"].ToString().Replace("<br/>", "<w:br/>"),
                    Неделя = row["Неделя"].ToString().Replace("<br/>", "<w:br/>"),
                    Л = row["Л"].ToString().Replace("<br/>", "<w:br/>"),
                    ПС = row["ПС"].ToString().Replace("<br/>", "<w:br/>"),
                    Лаб = row["Лаб"].ToString().Replace("<br/>", "<w:br/>"),
                    СРС = row["СРС"].ToString().Replace("<br/>", "<w:br/>"),
                    КСР = row["КСР"].ToString().Replace("<br/>", "<w:br/>"),
                    КР = row["КР"].ToString().Replace("<br/>", "<w:br/>"),
                    КП = row["КП"].ToString().Replace("<br/>", "<w:br/>"),
                    РГР = row["РГР"].ToString().Replace("<br/>", "<w:br/>"),
                    Подготовка = row["Подготовка_к_лр"].ToString().Replace("<br/>", "<w:br/>"),
                    К_р = row["К_р"].ToString().Replace("<br/>", "<w:br/>"),
                    Форма_аттестации = row["Форма_аттестации"].ToString().Replace("<br/>", "<w:br/>"),
                });
            }

            for (int i = 0; i < model.Structurs.Count; i++)
            {
                if (model.Structurs[i].Семестр == "0")
                    model.Structurs[i].Семестр = "";
                if (model.Structurs[i].Неделя == "0")
                    model.Structurs[i].Неделя = "";
                if (model.Structurs[i].Л == "0")
                    model.Structurs[i].Л= "";
                if (model.Structurs[i].ПС== "0")
                    model.Structurs[i].ПС= "";
                if (model.Structurs[i].Лаб== "0")
                    model.Structurs[i].Лаб= "";
                if (model.Structurs[i].СРС== "0")
                    model.Structurs[i].СРС= "";
                if (model.Structurs[i].КСР== "0")
                    model.Structurs[i].КСР= "";
                if (model.Structurs[i].КР== "0")
                    model.Structurs[i].КР= "";
                if (model.Structurs[i].К_р== "0")
                    model.Structurs[i].К_р= "";
                if (model.Structurs[i].РГР== "0")
                    model.Structurs[i].РГР= "";
                if (model.Structurs[i].КП== "0")
                    model.Structurs[i].КП= "";
            }

            foreach (DataRow row in data.Tables[6].Rows)
            {
                model.PrilFoundUniv.Add(new PrilFoundUnivRow()
                {
                    Code = row["Code"].ToString().Replace("<br/>", "<w:br/>"),
                    Wording = row["Wording"].ToString().Replace("<br/>", "<w:br/>"),
                    Components = row["Components"].ToString().Replace("<br/>", "<w:br/>"),
                    WordingTechnology = row["WordingTechnology"].ToString().Replace("<br/>", "<w:br/>"),
                    AssessForm = row["AssessForm"].ToString().Replace("<br/>", "<w:br/>"),
                    Steps = row["Steps"].ToString().Replace("<br/>", "<w:br/>"),
                });
            }
            foreach (DataRow row in data.Tables[7].Rows)
            {
                model.PrilFondGeneral.Add(new PrilFondGeneralRow()
                {
                    Code = row["Code"].ToString().Replace("<br/>", "<w:br/>"),
                    Wording = row["Wording"].ToString().Replace("<br/>", "<w:br/>"),
                    Components = row["Components"].ToString().Replace("<br/>", "<w:br/>"),
                    WordingTechnology = row["WordingTechnology"].ToString().Replace("<br/>", "<w:br/>"),
                    AssessForm = row["AssessForm"].ToString().Replace("<br/>", "<w:br/>"),
                    Steps = row["Steps"].ToString().Replace("<br/>", "<w:br/>"),
                });
            }
            foreach (DataRow row in data.Tables[8].Rows)
            {
                model.PrilFondProfessionally.Add(new PrilFondProfessionallyRow()
                {
                    Code = row["Code"].ToString().Replace("<br/>", "<w:br/>"),
                    Wording = row["Wording"].ToString().Replace("<br/>", "<w:br/>"),
                    Components = row["Components"].ToString().Replace("<br/>", "<w:br/>"),
                    WordingTechnology = row["WordingTechnology"].ToString().Replace("<br/>", "<w:br/>"),
                    AssessForm = row["AssessForm"].ToString().Replace("<br/>", "<w:br/>"),
                    Steps = row["Steps"].ToString().Replace("<br/>", "<w:br/>"),
                });
            }
            foreach (DataRow row in data.Tables[9].Rows)
            {
                model.PrilAssess.Add(new PrilAssessRow()
                {
                    Name = row["Name"].ToString().Replace("<br/>", "<w:br/>"),
                    Spec = row["Spec"].ToString().Replace("<br/>", "<w:br/>"),
                    InFOS = row["InFOS"].ToString().Replace("<br/>", "<w:br/>"),
                });
            }
            foreach (DataRow row in data.Tables[10].Rows)
            {
                model.PrilCompetenceZachet.Add(new PrilCompetenceZachetRow()
                {
                    Rezult = row["Rezult"].ToString().Replace("<br/>", "<w:br/>"),
                    Topics = row["Topics"].ToString().Replace("<br/>", "<w:br/>"),
                    Zachet = row["Zachet"].ToString().Replace("<br/>", "<w:br/>"),
                    NeZachet = row["NeZachet"].ToString().Replace("<br/>", "<w:br/>"),
                });
            }
            foreach (DataRow row in data.Tables[12].Rows)
            {
                model.PrilCompetenceExam.Add(new PrilCompetenceRow()
                {
                    Rezult = row["Rezult"].ToString().Replace("<br/>", "<w:br/>"),
                    Topics = row["Topics"].ToString().Replace("<br/>", "<w:br/>"),
                    On2 = row["On2"].ToString().Replace("<br/>", "<w:br/>"),
                    On3 = row["On3"].ToString().Replace("<br/>", "<w:br/>"),
                    On4 = row["On4"].ToString().Replace("<br/>", "<w:br/>"),
                    On5 = row["On5"].ToString().Replace("<br/>", "<w:br/>"),
                });
            }
            foreach (DataRow row in data.Tables[14].Rows)
            {
                model.PrilCompetenceReferat.Add(new PrilCompetenceReferatRow()
                {
                    Rezult = row["Rezult"].ToString().Replace("<br/>", "<w:br/>"),
                    Topics = row["Topics"].ToString().Replace("<br/>", "<w:br/>"),
                    On2 = row["On2"].ToString().Replace("<br/>", "<w:br/>"),
                    On3 = row["On3"].ToString().Replace("<br/>", "<w:br/>"),
                    On4 = row["On4"].ToString().Replace("<br/>", "<w:br/>"),
                    On5 = row["On5"].ToString().Replace("<br/>", "<w:br/>"),
                });
            }
            foreach (DataRow row in data.Tables[16].Rows)
            {
                model.PrilCompetenceKT.Add(new PrilCompetenceKTRow()
                {
                    Rezult = row["Rezult"].ToString().Replace("<br/>", "<w:br/>"),
                    Topics = row["Topics"].ToString().Replace("<br/>", "<w:br/>"),
                    On2 = row["On2"].ToString().Replace("<br/>", "<w:br/>"),
                    On3 = row["On3"].ToString().Replace("<br/>", "<w:br/>"),
                    On4 = row["On4"].ToString().Replace("<br/>", "<w:br/>"),
                    On5 = row["On5"].ToString().Replace("<br/>", "<w:br/>"),
                });
            }
            foreach (DataRow row in data.Tables[18].Rows)
            {
                model.PrilCompetencePractika.Add(new PrilCompetencePractikaRow()
                {
                    Rezult = row["Rezult"].ToString().Replace("<br/>", "<w:br/>"),
                    Topics = row["Topics"].ToString().Replace("<br/>", "<w:br/>"),
                    On2 = row["On2"].ToString().Replace("<br/>", "<w:br/>"),
                    On3 = row["On3"].ToString().Replace("<br/>", "<w:br/>"),
                    On4 = row["On4"].ToString().Replace("<br/>", "<w:br/>"),
                    On5 = row["On5"].ToString().Replace("<br/>", "<w:br/>"),
                });
            }
            foreach (DataRow row in data.Tables[20].Rows)
            {
                model.PrilCompetenceTest.Add(new PrilCompetenceTestRow()
                {
                    Rezult = row["Rezult"].ToString().Replace("<br/>", "<w:br/>"),
                    Topics = row["Topics"].ToString().Replace("<br/>", "<w:br/>"),
                    On2 = row["On2"].ToString().Replace("<br/>", "<w:br/>"),
                    On3 = row["On3"].ToString().Replace("<br/>", "<w:br/>"),
                    On4 = row["On4"].ToString().Replace("<br/>", "<w:br/>"),
                    On5 = row["On5"].ToString().Replace("<br/>", "<w:br/>"),
                });
            }

            new DocumentTempateBuilder().BuildTemplateOfSomeDocument(model);
        }


        public class TemplateModel
        {
            public ProgramData ProgramData { get; set; }
            public List<PointsRow> Points { get; set; }
            public List<FondsRow> Fonds { get; set; }
            public List<CriterionsRow> Criterions { get; set; }
            public List<StructursRow> Structurs { get; set; }
            public List<PrilFoundUnivRow> PrilFoundUniv { get; set; }
            public List<PrilFondGeneralRow> PrilFondGeneral { get; set; }
            public List<PrilFondProfessionallyRow> PrilFondProfessionally { get; set; }
            public List<PrilAssessRow> PrilAssess { get; set; }
            public List<PrilCompetenceZachetRow> PrilCompetenceZachet { get; set; }
            public List<PrilCompetenceRow> PrilCompetenceExam { get; set; }
            public List<PrilCompetenceReferatRow> PrilCompetenceReferat { get; set; }
            public List<PrilCompetenceKTRow> PrilCompetenceKT { get; set; }
            public List<PrilCompetencePractikaRow> PrilCompetencePractika { get; set; }
            public List<PrilCompetenceTestRow> PrilCompetenceTest { get; set; }

        }

        public class PrilCompetenceKTRow
        {
            public string Rezult { get; set; }
            public string Topics { get; set; }
            public string On2 { get; set; }
            public string On3 { get; set; }
            public string On4 { get; set; }
            public string On5 { get; set; }

        }
        public class PrilCompetenceTestRow
        {
            public string Rezult { get; set; }
            public string Topics { get; set; }
            public string On2 { get; set; }
            public string On3 { get; set; }
            public string On4 { get; set; }
            public string On5 { get; set; }

        }
        public class PrilCompetencePractikaRow
        {
            public string Rezult { get; set; }
            public string Topics { get; set; }
            public string On2 { get; set; }
            public string On3 { get; set; }
            public string On4 { get; set; }
            public string On5 { get; set; }

        }

        public class PrilFoundUnivRow
        {
            public string Code { get; set; }
            public string Wording { get; set; }
            public string Components { get; set; }
            public string WordingTechnology { get; set; }
            public string AssessForm { get; set; }
            public string Steps { get; set; }

        }
        public class PrilCompetenceReferatRow
        {
            public string Rezult { get; set; }
            public string Topics { get; set; }
            public string On2 { get; set; }
            public string On3 { get; set; }
            public string On4 { get; set; }
            public string On5 { get; set; }

        }
        public class PrilCompetenceRow
        {
            public string Rezult { get; set; }
            public string Topics { get; set; }
            public string On2 { get; set; }
            public string On3 { get; set; }
            public string On4 { get; set; }
            public string On5 { get; set; }

        }
        public class PrilCompetenceZachetRow
        {
            public string Rezult { get; set; }
            public string Topics { get; set; }
            public string Zachet { get; set; }
            public string NeZachet { get; set; }

        }
        public class PrilAssessRow
        {
            public string Name { get; set; }
            public string Spec { get; set; }
            public string InFOS { get; set; }


        }
        public class PrilFondGeneralRow
        {
            public string Code { get; set; }
            public string Wording { get; set; }
            public string Components { get; set; }
            public string WordingTechnology { get; set; }
            public string AssessForm { get; set; }
            public string Steps { get; set; }

        }
        public class PrilFondProfessionallyRow
        {
            public string Code { get; set; }
            public string Wording { get; set; }
            public string Components { get; set; }
            public string WordingTechnology { get; set; }
            public string AssessForm { get; set; }
            public string Steps { get; set; }

        }
        public class PointsRow
        {
            public string Competence { get; set; }
            public string Ability { get; set; }
            public string Enumeration { get; set; }
        }

        public class FondsRow
        {
            public string Competence { get; set; }
            public string Ability { get; set; }
        }
        public class CriterionsRow
        {
            public string Competence { get; set; }
            public string ToKnow { get; set; }
            public string ToKnow_2 { get; set; }
            public string ToKnow_3 { get; set; }
            public string ToKnow_4 { get; set; }
            public string ToKnow_5 { get; set; }
            public string ToDo { get; set; }
            public string ToDo_2 { get; set; }
            public string ToDo_3 { get; set; }
            public string ToDo_4 { get; set; }
            public string ToDo_5 { get; set; }
            public string ToMaster { get; set; }
            public string ToMaster_2 { get; set; }
            public string ToMaster_3 { get; set; }
            public string ToMaster_4 { get; set; }
            public string ToMaster_5 { get; set; }
        }
        public class StructursRow
        {
            public string Тема { get; set; }
            public string Семестр { get; set; }
            public string Неделя { get; set; }
            public string Л { get; set; }
            public string ПС { get; set; }
            public string Лаб { get; set; }
            public string СРС { get; set; }
            public string КСР { get; set; }
            public string КР { get; set; }
            public string КП { get; set; }
            public string РГР { get; set; }
            public string Подготовка { get; set; }
            public string К_р { get; set; }
            public string Форма_аттестации { get; set; }
        }
        public class ProgramData
        {
            public string QuestionsTest { get; set; }
            public string DateTest { get; set; }
            public string DevTest { get; set; }
            public string DescriptionTest { get; set; }
            public string DescriptionKT { get; set; }
            public string DevPractika { get; set; }
            public string DatePractika { get; set; }
            public string QuestionsPraktika { get; set; }

            public string DescriptionPractika { get; set; }
            public string QuestionsKT { get; set; }
            public string DevKT { get; set; }

            public string DateKT { get; set; }
            public string QuestionsReferat { get; set; }
            public string DevReferat { get; set; }

            public string DateReferat { get; set; }
            public string DescriptionExam { get; set; }
            public string DescriptionReferat { get; set; }

            public string DateExam { get; set; }
            public string QuestionsExam { get; set; }
            public string DeveloperExam { get; set; }

            public string DateZachet { get; set; }
            public string Questions { get; set; }
            public string DeveloperZachet { get; set; }
            public int ProgramId { get; set; }
            public string Institute { get; set; }
            public string Approver_Post { get; set; }
            public string Approver_Name { get; set; }
            public string Approve_Date { get; set; }
            public string YearApprove_Date { get; set; }
            public string Discipline { get; set; }
            public string Training_Direction { get; set; }
            public string Profil { get; set; }
            public string Graduate_Qualification { get; set; }
            public string Training_Form { get; set; }
            public int StudyPeriod { get; set; }
            public string City { get; set; }
            public string ObjectivesOfDiscipline { get; set; }
            public string DisciplineInOOP { get; set; }
            public string PlanDescription { get; set; }
            public string DisciplineStructureContent { get; set; }
            public string SemesteresContent { get; set; }
            public string EducationalTechnology { get; set; }
            public string MonitoringTools_Description { get; set; }
            public string ZachetDescription { get; set; }
            public string ZachetDone { get; set; }
            public string ZachetUnDone { get; set; }
            public string ExamDescription { get; set; }
            public string AttestatGreat { get; set; }
            public string AttestatGood { get; set; }
            public string AttestatSatisfactorily { get; set; }
            public string AttestatNotSatisfactorily { get; set; }
            public string BasicLiterature { get; set; }
            public string AdditionalLiterature { get; set; }
            public string ITResources { get; set; }
            public string StudentRecommendations { get; set; }
            public string MaterialTechnicalSupport { get; set; }
            public string TeacherRecommendations { get; set; }
            public string DrafterStatus { get; set; }
            public string DrafterName { get; set; }
            public string Department { get; set; }
            public string DraftDate { get; set; }
            public string Protocol { get; set; }
            public string DepartmentHeadStatus { get; set; }
            public string DepartmentHeadName { get; set; }
            public string AgreementDepartmentHeadStatus { get; set; }
            public string AgreementDepartmentHeadName { get; set; }
            public string Discription { get; set; }
            public string AgreementDirectorStatus { get; set; }
            public string AgreementDirectorName { get; set; }
            public string Status { get; set; }
            public string Competence { get; set; }
            public string Code { get; set; }
        }
    }
}

