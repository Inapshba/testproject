﻿<%@ Page Language="C#" Title="- РПД на подпись" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RPDConfirm.aspx.cs" Inherits="main_test.RPDConfirm" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent" ID="RPDConfirmContent">
    <link rel="stylesheet" href="Content/Colored.css" />
    <link rel="stylesheet" href="Content/ContentFade.css" />
    <link rel="stylesheet" href="Content/CustomCheckBox.css" />
    <link rel="stylesheet" href="Content/WorkStyle.css" />
    <script src="Scripts/ScrollTop.js"></script>
    <style type="text/css">
        .darked {
            background: rgba(102, 102, 102, 0.5);
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
            display: none;
        }
            .darked:target {
                display: block;
            }
        .open_window {
        width: 500px;
        height: 300px;
        text-align: right;
        padding: 15px;
        border: 2px solid;
        border-color: rgba(0,0,0,.075);
        border-radius: 5px;
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        margin: auto;
        background: #fff;
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(0, 0, 0, .6);
                 box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(0, 0, 0, .6);
        }
    </style>
    
    
    <div class="body_fade">
        <h2 runat="server" id="listEmptyMes" visible="false" class="body_fade">
        Ни в одной из программ не указано ваше имя :(</h2>
        <div runat="server" id="MainContentBlock">
            <h2>Поиск программы</h2>
            <asp:Label runat="server" ID="ErrorMes" Visible="false" ForeColor="Red" />
            <div>
                <asp:TextBox runat="server" ID="Search_TextBox" CssClass="form-control" placeholder="Поиск РПД..." />
                <asp:Button runat="server" ID="Search_Button" CssClass="btn light" style="margin-bottom: 2px" Text="Найти" OnClick="Search_Button_Click" />
            </div>
            <div style="border: 2px solid #ddd; border-radius: 5px; height: 400px; overflow: auto;">
                <asp:RadioButtonList runat="server" ID="Programs_List" CssClass="CustomCheckBox" DataSourceID="SqlDataSourcePrograms" DataTextField="Viewer" DataValueField="id" />
                <asp:SqlDataSource ID="SqlDataSourcePrograms" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT [id], [Discipline], [Approve_Date], (Discipline+' ('+CONVERT(varchar,Approve_Date,105)+')'+' ('+Status+')') AS Viewer, [Status] FROM [Programs]"></asp:SqlDataSource> 
            </div>
            <asp:Button runat="server" ID="Button_OpenRPD" CssClass="btn btn-lg dark" style="margin-top: 2px" Text="Открыть программу" OnClick="Button_OpenRPD_Click" />
        </div>
        <div runat="server" id="RPDContentBlock" visible="false" class="body_fade">
            <asp:Label runat="server" ID="ErrorMesRPDBlock" Visible="false" ForeColor="Red" /><br />
            <h3 runat="server" id="title_html">Рабочая программа №</h3>
            <%--<asp:LinkButton runat="server" class="link" OnClick="discipline_Click" Font-Size="24px" id="discipline" Text="Рабочая программа дисциплины"/>--%>
            <h4 runat="server" id="discipline">Дисциплина</h4>
            <h4 runat="server" id="training_direction">Направление подготовки</h4>
            <h4 runat="server" id="department">Кафедра</h4>
            <h4 runat="server" id="developer">Разработчик – </h4>
            <h4 runat="server" id="developeDate">Дата разработки</h4>
            <h4 runat="server" id="status_html"></h4>
            <asp:LinkButton runat="server" ID="LinkButton_OpenNewTab" CssClass="link" Text="Открыть в новой вкладке" OnClick="LinkButton_OpenNewTab_Click" />
            &nbsp;&nbsp;
            <asp:LinkButton runat="server" ID="LinkButton_Download" CssClass="link" Text="Скачать" OnClick="LinkButton_Download_Click" />
            <hr />
            <asp:Button runat="server" ID="Approve_Button" CssClass="btn btn-lg dark" Text="Подписать" OnClick="Approve_Button_Click" />
            <a href="#dialog" class="btn btn-lg red">Отклонить</a>
            <div class="darked" id="dialog">
                <div class="open_window" id="commentWindow">
                    <asp:TextBox runat="server" CssClass="form-control" ID="Comment_TextBox" Width="100%" Height="80%" placeholder="Введите комментарий..." TextMode="MultiLine" />
                    <asp:Button runat="server" ID="GoRefuse_Button" Text="Отклонить" CssClass="btn red" OnClick="GoRefuse_Button_Click" />
                    <a href="#" class="btn light">Отменить</a>
                </div>
            </div>
        </div>
       
        <a runat="server" id="GoBack_Button" style="margin-top: 30vh; margin-bottom: 30vh" class="btn btn-lg light" href="javascript:history.go(-1);">&larr; Вернуться</a>
    </div>
    <a class="scrollup">Наверх</a>
</asp:Content>