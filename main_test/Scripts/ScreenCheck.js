﻿var isMobile = {
    Android: function () {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function () {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function () {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function () {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function () {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function () {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

window.onload = function () {
    var divs_editor;
    var i;
    var width;
    if (isMobile.any())
        width = '100%';
    else 
        width = '50%';
    try {
        divs_editor = document.querySelectorAll('.check_screen');
        for (i = 0; i < divs_editor.length; i++) {
            divs_editor[i].style.width = width;
        }
    }
    catch{ }
};