﻿<%@ Page Language="C#" Title="- Мои программы" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MyPrograms.aspx.cs" Inherits="main_test.MyPrograms" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent" ID="MyProgramsContent">
    <link rel="stylesheet" href="Content/Colored.css" />
    <link rel="stylesheet" href="Content/ContentFade.css" />
    <link rel="stylesheet" href="Content/CustomCheckBox.css" />
    <link rel="stylesheet" href="Content/WorkStyle.css" />
    <script src="Scripts/ScrollTop.js"></script>

    <div class="body_fade">
        <div id="ListNotEmptyView" runat="server" visible="false">
            <h2>Поиск программы</h2>
            <asp:Label runat="server" ID="ErrorMes" Visible="false" ForeColor="Red" />
            <div>
                <asp:TextBox runat="server" ID="Search_TextBox" CssClass="form-control" placeholder="Поиск РПД..." />
                <asp:Button runat="server" ID="Search_Button" CssClass="btn light" style="margin-bottom: 2px" Text="Найти" OnClick="Search_Button_Click" />
            </div>
            <div style="border: 2px solid #ddd; border-radius: 5px; height: 400px; overflow: auto;">
                <asp:RadioButtonList runat="server" ID="Programs_List" CssClass="CustomCheckBox" DataSourceID="SqlDataSourcePrograms" DataTextField="Viewer" DataValueField="id" />
                <asp:SqlDataSource ID="SqlDataSourcePrograms" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT [id], [Discipline], [Approve_Date], (Discipline+' ('+CONVERT(varchar,Approve_Date,105)+')'+' ('+Status+')') AS Viewer, [Status] FROM [Programs]"></asp:SqlDataSource> 
            </div>
            <asp:Button runat="server" ID="Button_OpenRPD" CssClass="btn btn-lg dark" style="margin-top: 2px" Text="Открыть программу" OnClick="Button_OpenRPD_Click" />
        </div>
        
        <div runat="server" id="RPDContentBlock" visible="false" class="body_fade">
            <asp:Label runat="server" ID="ErrorMesRPDBlock" Visible="false" ForeColor="Red" /><br />
            <h3 runat="server" id="title_html">Рабочая программа №</h3>
            <%--<asp:LinkButton runat="server" class="link" OnClick="discipline_Click" Font-Size="24px" id="discipline" Text="Рабочая программа дисциплины"/>--%>
            <h4 runat="server" id="discipline">Дисциплина</h4>
            <h4 runat="server" id="training_direction">Направление подготовки</h4>
            <h4 runat="server" id="department">Кафедра</h4>
            <h4 runat="server" id="developer">Разработчик – </h4>
            <h4 runat="server" id="developeDate">Дата разработки</h4>
            <asp:LinkButton runat="server" ID="LinkButton_OpenNewTab" Text="Открыть в новой вкладке" CssClass="link" OnClick="LinkButton_OpenNewTab_Click" />
            &nbsp;&nbsp;
            <asp:LinkButton runat="server" ID="LinkButton_Download" Text="Скачать" CssClass="link" OnClick="LinkButton_Download_Click" />
            <hr />
            <h4 aria-multiline="true" runat="server" id="approvers_html"></h4>
            <asp:Button runat="server" ID="Button_ShowHistory" CssClass="btn dark" Text="Показать подробную историю" OnClick="Button_ShowHistory_Click" />
        </div>

        <div id="ListEmptyView" runat="server" visible="false">
            <h2>Ни в одной из программ не указано ваше имя :(</h2>
            <a style="margin-top: 30vh; margin-bottom: 30vh" class="btn btn-lg light" href="Designer.aspx">Открыть консруктор</a>
        </div>
    </div>
</asp:Content>