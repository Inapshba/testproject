﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using System.IO;

namespace main_test
{
    public partial class RPDConfirm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ErrorMes.Visible = false;
            ErrorMesRPDBlock.Visible = false;
            if (!IsPostBack)
                try
                {
                    if (SetAdittionToCommand())
                    {
                        listEmptyMes.Visible = false;
                        Search_Button.Enabled = true;
                        MainContentBlock.Visible = true;
                        GoBack_Button.Visible = false;
                    }
                    else
                    {
                        listEmptyMes.Visible = true;
                        Search_Button.Enabled = false;
                        MainContentBlock.Visible = false;
                        GoBack_Button.Visible = true;
                    }
                }
                catch
                {
                    ErrorMes.Text = "Ошибка фильтрации программ";
                    ErrorMes.Visible = true;
                }
            else
            {

            }
        }

        //дополнение команды селекта привязки
        private bool SetAdittionToCommand()
        {
            string toCommand = "";
            List<int> IDes = new List<int>();
            BaseWork task = new BaseWork();
            IDes = task.GetProgramsIDesToUserConfirm(User.Identity.GetUserId());
            if (IDes.Count != 0)
            {
                for (int i = 0; i < IDes.Count; i++)
                {
                    if (i == 0)
                        toCommand += "(" + IDes[i].ToString() + ", ";
                    else if (i == IDes.Count - 1)
                        toCommand += IDes[i].ToString() + ")";
                    else toCommand += IDes[i] + ", ";
                }
                if (IDes.Count == 1)
                    toCommand = "(" + IDes[0].ToString() + ")";
                SqlDataSourcePrograms.SelectCommand += " WHERE id IN " + toCommand;
                Programs_List.DataBind();
                return true;
            }
            else
                return false;
        }

        //поиск рпд
        protected void Search_Button_Click(object sender, EventArgs e)
        {
            SetAdittionToCommand();
            SqlDataSourcePrograms.SelectCommand += " AND Discipline LIKE '%" + Search_TextBox.Text + "%'";
            Programs_List.DataBind();
        }
        
        //заполнение html данными рпд
        protected void Button_OpenRPD_Click(object sender, EventArgs e)
        {
            try
            {
                string userid = User.Identity.GetUserId();
                string status = "[]";
                int ProgramId = Convert.ToInt32(Programs_List.SelectedItem.Value);
                BaseWork task = new BaseWork();
                Pro pro = new Pro();
                pro = task.GetProgram(ProgramId);

                int statusInt = task.GetConfirmStatus(userid, ProgramId);
                if (statusInt == 1)
                    status = "[Подписана вами]";
                else if (statusInt == 0)
                    status = "[Ожидает подписи]";
                else if (statusInt == -1)
                    status = "[Отклонена вами]";

                title_html.InnerHtml = "Рабочая программа №" + pro.Id.ToString();
                discipline.InnerHtml = "Дисциплина '" + pro.Discipline + "'";
                training_direction.InnerHtml = "Направление подготовки '" + pro.Training_Direction + "'";
                department.InnerHtml = "Кафедра '" + pro.Department + "'";
                developer.InnerHtml = "Разработчик – " + pro.DrafterName;
                developeDate.InnerHtml = "Дата разработки " + pro.DraftDate.ToString("dd-MM-yyyy");
                status_html.InnerHtml = status;
                MainContentBlock.Visible = false;
                RPDContentBlock.Visible = true;
            }
            catch
            {
                ErrorMes.Text = "Ошибка";
                ErrorMes.Visible = true;
            }

        }
        
        //пописание рпд
        protected void Approve_Button_Click(object sender, EventArgs e)
        {
            try
            {
                int programid = Convert.ToInt32(Programs_List.SelectedItem.Value);
                BaseWork task = new BaseWork();
                string userid = User.Identity.GetUserId();
                task.SetConfirmProgram(userid, programid, 1, "");
                ErrorMesRPDBlock.Visible = false;
                status_html.InnerHtml = "[Подписана вами]";
            }
            catch
            {
                ErrorMesRPDBlock.Text = "Ошибка в процессе подтверждения";
                ErrorMesRPDBlock.Visible = true;
            }
        }

        //отклонение рпд
        protected void GoRefuse_Button_Click(object sender, EventArgs e)
        {
            try
            {
                int programid = Convert.ToInt32(Programs_List.SelectedItem.Value);
                BaseWork task = new BaseWork();
                string userid = User.Identity.GetUserId();
                task.SetConfirmProgram(userid, programid, -1, Comment_TextBox.Text);
                ErrorMesRPDBlock.Visible = false;
                status_html.InnerHtml = "[Отклонена вами]";
            }
            catch
            {
                ErrorMesRPDBlock.Text = "Ошибка в процессе отклонения";
                ErrorMesRPDBlock.Visible = true;
            }
        }

        //открытие word-файла в браузере
        protected void LinkButton_OpenNewTab_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(Programs_List.SelectedItem.Value);
            try
            {
                BaseWork task = new BaseWork();
                string filePath = task.GetFileUrl(id);
                string script = string.Format("window.open('{0}');", filePath);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "newPage" + UniqueID, script, true);
            }
            catch
            {
                ErrorMesRPDBlock.Text = "Не удалось получить ссылку на файл";
                ErrorMesRPDBlock.Visible = true;
            }
        }

        //загрузка word-файла на устройство
        protected void LinkButton_Download_Click(object sender, EventArgs e)
        {
            try
            {
                int programid = Convert.ToInt32(Programs_List.SelectedItem.Value);
                string fileName = "Program_" + programid.ToString() + ".docx";
                string url = HttpContext.Current.Server.MapPath("~/Files/" + fileName);

                FileStream MyFileStream = new FileStream(url, FileMode.Open);
                int filesizeINT = 0;
                long fileSize = MyFileStream.Length;
                filesizeINT = Convert.ToInt32(fileSize);
                byte[] Buffer = new byte[fileSize];
                MyFileStream.Read(Buffer, 0, filesizeINT);
                MyFileStream.Close();
                Response.AppendHeader("content-disposition", "attachment; filename=" + fileName);
                Response.BinaryWrite(Buffer);
                Response.End();
            }
            catch
            {
                ErrorMesRPDBlock.Text = "Ошибка загрузки";
                ErrorMesRPDBlock.Visible = true;
            }
        }
    }
}