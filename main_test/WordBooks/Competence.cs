﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace main_test
{
    public class Competence
    {
        private int id;
        private int fgosid;
        private string code;
        private string description;

        public int Id { set { id = value; } get { return id; } }
        public int FGOSId { set { fgosid = value; } get { return fgosid; } }
        public string Code { set { code = value; } get { return code; } }
        public string Description { set { description = value; } get { return description; } }
    }
}