﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace main_test
{
    public class TrainingPlan
    {
        private int id;
        private string name;
        private string year;
        private List<int> directionides;

        public int Id { set { id = value; } get { return id; } }
        public string Name { set { name = value; } get { return name; } }
        public string Year { set { year = value; } get { return year; } }
        public List<int> DirectionIDes { set { directionides = value; } get { return directionides; } }
    }
}