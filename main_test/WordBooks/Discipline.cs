﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace main_test.WordBooks
{
    public class Discipline
    {
        private string name;
        private int id;
        private List<int> disciplines_before;
        private List<int> disciplines_after;

        public string Name { set { name = value; } get { return name; } }
        public int Id { set { id = value; } get { return id; } }
        public List<int> Disciplines_Before { set { disciplines_before = value; } get { return disciplines_before; } }
        public List<int> Disciplines_After { set { disciplines_after = value; } get { return disciplines_after; } }
    }
}