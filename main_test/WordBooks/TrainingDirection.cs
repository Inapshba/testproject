﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace main_test
{
    public class TrainingDirection
    {
        private int id;
        private string disignation;
        private string code;
        private List<int> profilesides;

        public int Id { set { id = value; } get { return id; } }
        public string Designation { set { disignation = value; } get { return disignation; } }
        public string Code { set { code = value; } get { return code; } }
        public List<int> ProfilesIDes { set { profilesides = value; } get { return profilesides; } }
    }
}