﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace main_test.WordBooks
{
    public class FGOS
    {
        private int id;
        private string name;
        private string designation;
        private List<int> trainingplans;

        public int Id { set { id = value; } get { return id; } }
        public string Name { set { name = value; } get { return name; } }
        public string Designation { set { designation = value; } get { return designation; } }
        public List<int> TrainingPlansIDes { set { trainingplans = value; } get { return trainingplans; } }
    }
}