﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace main_test
{
    public class Institute
    {
        private int id;
        private string name;
        private List<int> departmentsides;

        public int Id { set { id = value; } get { return id; } }
        public string Name { set { name = value; } get { return name; } }
        public List<int> DepartmentsIDes { set { departmentsides = value; } get { return departmentsides; } }
    }
}