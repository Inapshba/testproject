﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace main_test
{
    public class Profile
    {
        private int id;
        private string name;
        private List<int> programsides;

        public int Id { set { id = value; } get { return id; } }
        public string Name { set { name = value; } get { return name; } }
        public List<int> ProgramsIDes { set { programsides = value; } get { return programsides; } }
    }
}