﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace main_test
{
    public class Department
    {
        private int id;
        private string name;
        private List<string> personalides;

        public int Id { set { id = value; } get { return id; } }
        public string Name { set { name = value; } get { return name; } }
        public List<string> PersonalIDes { set { personalides = value; } get { return personalides; } }
    }
}