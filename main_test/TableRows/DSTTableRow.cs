﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace main_test.TableRows
{
    public class DSTTableRow
    {
        private string topic, preptolr;
        private int semestr;
        private int week;
        private int l, ps, lab, srs, ksr, kr, kp, rgr, krend;
        private string attestatform;

        public string Topic { set {  topic = value; } get { return topic; } }
        public int Semestr { set { semestr = value; } get { return semestr; } }
        public int Week { set { week = value; } get { return week; } }
        public int L { set { l = value; } get { return l; } }
        public int PS { set { ps = value; } get { return ps; } }
        public int Lab { set { lab = value; } get { return lab; } }
        public int SRS { set { srs = value; } get { return srs; } }
        public int KSR { set { ksr = value; } get { return ksr; } }
        public int KR { set { kr = value; } get { return kr; } }
        public int KP { set { kp = value; } get { return kp; } }
        public int RGR { set { rgr = value; } get { return rgr; } }
        public string PrepToLR { set { preptolr = value; } get { return preptolr; } }
        public int KRend { set { krend = value; } get { return krend; } }
        public string AttestatForm { set { attestatform = value; } get { return attestatform; } }
    }
}