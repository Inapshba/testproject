﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace main_test.TableRows
{
    public class PrilAssessTableRow
    {
        private string name;
        private string spec;
        private string infos;

        public string Name { set { name = value; } get { return name; } }
        public string Spec { set { spec = value; } get { return spec; } }
        public string InFOS { set { infos = value; } get { return infos; } }
    }
}