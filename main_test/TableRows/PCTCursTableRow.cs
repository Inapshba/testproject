﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace main_test.TableRows
{
    public class PCTCursTableRow
    {
        private string discription;
        private string rezult;
        private string topics;
        private string on2;
        private string on3;
        private string on4;
        private string on5;

        public string Discription { set { discription = value; } get { return discription; } }
        public string Rezult { set { rezult = value; } get { return rezult; } }
        public string Topics { set { topics = value; } get { return topics; } }
        public string On2 { set { on2 = value; } get { return on2; } }
        public string On3 { set { on3 = value; } get { return on3; } }
        public string On4 { set { on4 = value; } get { return on4; } }
        public string On5 { set { on5 = value; } get { return on5; } }
    }
}