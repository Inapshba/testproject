﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace main_test
{
    public class CriterionsTableRow
    {
        private string competence;
        private string toknow_description;
        private string todo_description;
        private string tomaster_description;
        private List<string> toknow;
        private List<string> todo;
        private List<string> tomaster;

        public string Competence { set { competence = value; } get { return competence; } }
        public string ToKnow_Description { set { toknow_description = value; } get { return toknow_description; } }
        public string ToDo_Description { set { todo_description = value; } get { return todo_description; } }
        public string ToMaster_Description { set { tomaster_description = value; } get { return tomaster_description; } }
        public List<string> ToKnow { set { toknow = value; } get { return toknow; } }
        public List<string> ToDo { set { todo = value; } get { return todo; } }
        public List<string> ToMaster { set { tomaster = value; } get { return tomaster; } }
    }
}