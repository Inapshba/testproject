﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace main_test
{
    public class PlanTableRow
    {
        private string competence;
        private string ability;
        private string enumeration;

        public string Сompetence { set { competence = value; } get { return competence; } }
        public string Ability { set { ability = value; } get { return ability; } }
        public string Enumeration { set { enumeration = value; } get { return enumeration; } }

    }
}