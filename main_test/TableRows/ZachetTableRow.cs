﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace main_test
{
    public class ZachetTableRow
    {
        private string zachetdone;
        private string zachetundone;

        public string ZachetDone { set { zachetdone = value; } get { return zachetdone; } }
        public string ZachetUnDone { set { zachetundone = value; } get { return zachetundone; } }
    }
}