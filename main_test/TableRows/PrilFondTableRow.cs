﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace main_test.TableRows
{
    public class PrilFondTableRow
    {
        private string index;
        private string formul;
        private string komponentslist;
        private string competencetechnology;
        private string assessform;
        private string steps;

        public string Index { set { index = value; } get { return index; }  }
        public string KomponentsList { set { komponentslist = value; } get { return komponentslist; } }
        public string CompetenceTechnology { set { competencetechnology= value; } get { return competencetechnology; } }
        public string AssessForm { set { assessform = value; } get { return assessform; } }
        public string Formul { set { formul = value; } get { return formul; } }
        public string Steps { set { steps = value; } get { return steps; } }
    }
}