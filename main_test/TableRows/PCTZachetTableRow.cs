﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace main_test.TableRows
{
    public class PCTZachetTableRow
    {
        private string discription;
        private string rezult;
        private string topics;
        private string zachet;
        private string nezachet;

        public string Discription { set { discription = value; } get { return discription; } }
        public string Rezult { set { rezult = value; } get { return rezult; } }
        public string Topics { set { topics = value; } get { return topics; } }
        public string Zachet{ set { zachet = value; } get { return zachet; } }
        public string NeZachet { set { nezachet = value; } get { return nezachet; } }
    }
}