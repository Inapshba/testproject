﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace main_test.TableRows
{
    public class AdditionalDrafter
    {
        private string status;
        private string userid;

        public string Status { set { status = value; } get { return status; } }
        public string UserId { set { userid = value; } get { return userid; } }
    }
}