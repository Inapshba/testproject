﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace main_test
{
    public class ExamTableRow
    {
        private string attestatgreat;
        private string attestatgood;
        private string attestatsatisfactorily;
        private string attestatnotsatisfactorily;

        public string AttestatGreat { set { attestatgreat = value; } get { return attestatgreat; } }
        public string AttestatGood { set { attestatgood = value; } get { return attestatgood; } }
        public string AttestatSatisfactorily { set { attestatsatisfactorily = value; } get { return attestatsatisfactorily; } }
        public string AttestatNotSatisfactorily { set { attestatnotsatisfactorily = value; } get { return attestatnotsatisfactorily; } }

    }
}