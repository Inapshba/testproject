﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using main_test.TableRows;
using main_test.QuestionsStructur;

namespace main_test
{
    public class Pro
    {
        private int id;
        private string institute;
        private string approver_post;
        private string approver_name;
        private string approver_id;
        private DateTime approve_date;
        private string discipline;
        private string training_direction;
        private string profil;
        private string graduate_qualification;
        private string training_form;
        private string studperiod;
        private string city;

        private string objectivesofdiscipline;
        private string disciplineinoop;

        private string plandescription;
        private List<PlanTableRow> plantable;

        private string disciplinestructurecontent;
        private string semesterescontent;
        private string educationaltechnology;
        private string monitoringtools_description;
        private List<FondTableRow> fondtable;

        private List<CriterionsTableRow> criterionstable;

        private string zachetdescription;
        private ZachetTableRow zachettable;
        private string examdescription;
        private ExamTableRow examtable;
        private string basicliterature;
        private string additionalliterature;
        private string itresources;
        private string materialtechnicalsupport;
        private string studentrecommendations;
        private string teacherrecommendations;

        private string drafterstatus;
        private string draftername;
        private string drafterid;
        private string department;
        private DateTime draftdate;
        private string protocol;
        private string departmentheadstatus;
        private string departmentheadname;
        private string departmentheadid;
        private string agreementdepartmentheadstatus;
        private string agreementdepartmentheadname;
        private string agreementdepartmentheadid;
        private string agreementdirectorstatus;
        private string agreementdirectorname;
        private string agreementdirectorid;
        private string status;
        private List<DSTTableRow> dsttable;

        private List<PrilFondTableRow> pft1;
        private List<PrilFondTableRow> pft2;
        private List<PrilFondTableRow> pft3;

        private List<PrilAssessTableRow> pat;
        private List<PCTZachetTableRow> pctzachet;

        private Questions_Zachet questions_zachet;

        private List<PCTExamTableRow> pctexam;
        private Questions_Exam questions_exam;

        private List<PCTReferatTableRow> pctreferat;
        private Questions_Referat questions_referat;

        private List<PCTKtTableRow> pctkt;
        private Questions_KT questions_kt;

        private List<PCTPraktikaTableRow> pctpraktika;
        private Questions_Praktika questions_praktika;

        private List<PCTTestTableRow> pcttest;
        private Questions_Test questions_test;
        
        private List<PCTCursTableRow> pctcurs;
        private Questions_Curs questions_curs;

        private List<AdditionalDrafter> additionaldrafters;

        public int Id { set { id = value; } get { return id; } }
        public string Institute { set { institute = value; } get { return institute; } }
        public string Approver_Post { set { approver_post = value; } get { return approver_post; } }
        public string Approver_Name { set { approver_name = value; } get { return approver_name; } }
        public string Approver_Id { set { approver_id = value; } get { return approver_id; } }
        public DateTime Approve_Date { set { approve_date = value; } get { return approve_date; } }
        public string Discipline { set { discipline = value; } get { return discipline; } }
        public string Training_Direction { set { training_direction = value; } get { return training_direction; } }
        public string Profil { set { profil = value; } get { return profil; } }
        public string Graduate_Qualification { set { graduate_qualification = value; } get { return graduate_qualification; } }
        public string Training_Form { set { training_form = value; } get { return training_form; } }
        public string StudPeriod { set { studperiod = value; } get { return studperiod; } }
        public string City { set { city = value; } get { return city; } }

        public string ObjectivesOfDiscipline { set { objectivesofdiscipline = value; } get { return objectivesofdiscipline; } }
        public string DisciplineInOOP { set { disciplineinoop = value; } get { return disciplineinoop; } }

        public string PlanDescription { set { plandescription = value; } get { return plandescription; } }
        public List<PlanTableRow> PlanTable { set { plantable = value; } get { return plantable; } }

        public string DisciplineStructureContent { set { disciplinestructurecontent = value;} get { return disciplinestructurecontent; } }
        public string SemesteresContent { set { semesterescontent = value; } get { return semesterescontent; } }
        public string EducationalTechnology { set { educationaltechnology = value; } get { return educationaltechnology; } }
        public string MonitoringTools_Description { set { monitoringtools_description = value;} get { return monitoringtools_description; } }
        public List<FondTableRow> FondTable { set { fondtable = value; } get { return fondtable; } }

        public List<CriterionsTableRow> CriterionsTable { set { criterionstable = value; } get { return criterionstable; } }

        public string ZachetDescription { set { zachetdescription = value; } get { return zachetdescription; } }
        public ZachetTableRow ZachetTable { set { zachettable = value; } get { return zachettable; } }
        public string ExamDescription { set { examdescription = value; } get { return examdescription; } }
        public ExamTableRow ExamTable { set { examtable = value; } get { return examtable; } }
        public string BasicLiterature { set { basicliterature = value; } get { return basicliterature; } }
        public string AdditionalLiterature { set { additionalliterature = value; } get { return additionalliterature; } }
        public string ITResources { set { itresources = value; } get { return itresources; } }
        public string MaterialTechnicalSupport { set { materialtechnicalsupport = value; } get { return materialtechnicalsupport; } }
        public string StudentRecommendations { set { studentrecommendations = value; } get { return studentrecommendations; } }
        public string TeacherRecommendations { set { teacherrecommendations = value; } get { return teacherrecommendations; } }

        public string DrafterStatus { set { drafterstatus = value; } get { return drafterstatus; } }
        public string DrafterName { set { draftername = value; } get { return draftername; } }
        public string DrafterId { set { drafterid = value; } get { return drafterid; } }
        public string Department { set { department = value; } get { return department; } }
        public DateTime DraftDate { set { draftdate = value; } get { return draftdate; } }
        public string Protocol { set { protocol = value; } get { return protocol; } }
        public string DepartmentHeadStatus { set { departmentheadstatus = value; } get { return departmentheadstatus; } }
        public string DepartmentHeadName { set { departmentheadname = value; } get { return departmentheadname; } }
        public string DepartmentHeadId { set { departmentheadid = value; } get { return departmentheadid; } }
        public string AgreementDepartmentHeadStatus { set { agreementdepartmentheadstatus = value; } get { return agreementdepartmentheadstatus; } }
        public string AgreementDepartmentHeadName { set { agreementdepartmentheadname = value; } get { return agreementdepartmentheadname; } }
        public string AgreementDepartmentHeadId { set { agreementdepartmentheadid = value; } get { return agreementdepartmentheadid; } }
        public string AgreementDirectorStatus { set { agreementdirectorstatus = value; } get { return agreementdirectorstatus; } }
        public string AgreementDirectorName { set { agreementdirectorname = value; } get { return agreementdirectorname; } }
        public string AgreementDirectorId { set { agreementdirectorid = value; } get { return agreementdirectorid; } }
        public string Status { set { status = value; } get { return status; } }

        public List<DSTTableRow> DSTTable { set { dsttable = value; } get { return dsttable; } }

        public List<PrilFondTableRow> PFT_1 { set { pft1 = value; } get { return pft1; } }
        public List<PrilFondTableRow> PFT_2 { set { pft2 = value; } get { return pft2; } }
        public List<PrilFondTableRow> PFT_3 { set { pft3 = value; } get { return pft3; } }

        public List<PrilAssessTableRow> PAT { set { pat = value; } get { return pat; } }

        public List<PCTZachetTableRow> PCT_Zachet { set { pctzachet = value; } get { return pctzachet; } }
        public Questions_Zachet Questions_Zachet { set { questions_zachet = value; } get { return questions_zachet; } }

        public List<PCTExamTableRow> PCT_Exam { set { pctexam = value; } get { return pctexam; } }
        public Questions_Exam Questions_Exam { set { questions_exam = value; } get { return questions_exam; } }

        public List<PCTReferatTableRow> PCT_Referat { set { pctreferat = value; } get { return pctreferat; } }
        public Questions_Referat Questions_Referat { set { questions_referat = value; } get { return questions_referat; } }
        
        public List<PCTKtTableRow> PCT_KT { set { pctkt = value; } get { return pctkt; } }
        public Questions_KT Questions_KT { set { questions_kt = value; } get { return questions_kt; } }

        public List<PCTPraktikaTableRow> PCT_Praktika { set { pctpraktika = value; } get { return pctpraktika; } }
        public Questions_Praktika Questions_Praktika { set { questions_praktika = value; }  get { return questions_praktika; } }

        public List<PCTTestTableRow> PCT_Test { set { pcttest = value; } get { return pcttest; } }
        public Questions_Test Questions_Test { set { questions_test = value; } get { return questions_test; } }

        public List<PCTCursTableRow> PCT_Curs { set { pctcurs = value; } get { return pctcurs; } }
        public Questions_Curs Questions_Curs { set { questions_curs = value; } get { return questions_curs; } }

        public List<AdditionalDrafter> AdditionalDrafters { set { additionaldrafters = value; } get { return additionaldrafters;} }
    }
}