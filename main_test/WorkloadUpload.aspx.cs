﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using main_test.Models;
using main_test.Properties;

namespace main_test
{
    public partial class WorkloadUpload : System.Web.UI.Page
    {
        AjaxControlToolkit.ModalPopupExtender modalPopUpload;
        AjaxControlToolkit.ModalPopupExtender ModalPopupExtender1;
        //bool filtercheck = false;
        //bool filtercheckDis = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            modalPopUpload = new AjaxControlToolkit.ModalPopupExtender();

            modalPopUpload.ID = "popUpUpload";
            modalPopUpload.PopupControlID = "ReconcilePanel2";
            modalPopUpload.TargetControlID = "ContButton";
            modalPopUpload.BackgroundCssClass = "modalBackground";

          

            ModalPopupExtender1 = new AjaxControlToolkit.ModalPopupExtender();
            ModalPopupExtender1.ID = "ModalPopupExtender1";
            ModalPopupExtender1.TargetControlID = "UploadButton";
            ModalPopupExtender1.PopupControlID = "ReconcilePanel";
            ModalPopupExtender1.BackgroundCssClass = "modalBackground";

            

            /*----------------------------*/


        }

        protected void Page_Prerender()
        {
        }



        protected void Button1_Click(object sender, EventArgs e)
        {
            if (FileUpload1.HasFile)
            {
                try
                {
                    string filename = Path.GetFileName(FileUpload1.FileName);
                    string filePath = Server.MapPath("\\Uploads\\") + filename;
                    FileUpload1.SaveAs(filePath);

                    _ExcelUpload = new ExcelUpload();
                    Session["MyExcel"] = _ExcelUpload;
                    string path = Path.Combine(Server.MapPath("~/Uploads/"), Directory.GetFiles(Server.MapPath("~/Uploads/"))[0]);
                    Session["ExcelPath"] = path;
                    _ExcelUpload.AnalizExcel(path);
                    _ExcelUpload.DeleteFromTeacher();
                    string permstr;

                    foreach (var str in _ExcelUpload.excelSheets.Where(x => !string.IsNullOrEmpty(x)).ToArray())
                    {
                        permstr = str.Remove(0, 1).Remove(str.Length - 3, 2);

                    }

                    _ExcelUpload = (ExcelUpload)(Session["MyExcel"]);
                    string ExcelPath = (string)(Session["ExcelPath"]);
                    string NagruzkaPeriod = DropDownList2.SelectedValue;
                    string raz = "3";
                    string dva = "4";
                    _ExcelUpload.UploadIt(ExcelPath, DropDownList2.SelectedItem.ToString(), raz, NagruzkaPeriod, dva);


                    Settings mySettings = new Settings();
                    string ssqlconnectionstring = ConfigurationManager.ConnectionStrings["WorkloadConnectionString"].ConnectionString;
                    string ssqltable = mySettings.UploadTable;
                    SqlConnection sqlconn = new SqlConnection(ssqlconnectionstring);
                }
                catch (Exception ex)
                {
                    Label1.Text = "Ошибка: {0}" + ex.Message;
                    Label1.Visible = true;
                }
                finally
                {
                    ClearButton_Click(sender, e);
                }
            }
            else
            {
                ModalPopupExtender1.Show();
                Label1.Text = "Пожалуйста, выберите файл";
                Label1.Visible = true;
            }
        }

        ExcelUpload _ExcelUpload;



        protected void ClearButton_Click(object sender, EventArgs e)
        {
            ExcelUpload _ExcelUpload = new ExcelUpload();
            _ExcelUpload.Clean(true);
        }


    }
}