﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using main_test.TableRows;
using System.IO;
using System.Web.Configuration;
using main_test.WordBooks;
using main_test.QuestionsStructur;

namespace main_test
{
    public class BaseWork
    {
        string connection_string = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        public void TryConnect()
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(connection_string, connect);
            command.CommandText = "SELECT 1";
            command.ExecuteNonQuery();
            connect.Close();
        }

        private DataSet GetData(string query, SqlConnection connect)
        {
            DataSet dataset = new DataSet();
            SqlCommand command = new SqlCommand(query, connect);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;
            adapter.Fill(dataset);

            return dataset;
        }

        #region Files

        public void InsertFileURL(int programid, string url)
        {
            string queryDelete = string.Format("DELETE FROM [ADocs] WHERE ProgramId=" + programid);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand commandDelete = new SqlCommand(queryDelete, connect);
            commandDelete.ExecuteNonQuery();

            string queryInsert = string.Format("INSERT INTO [ADocs] " +
                "(ProgramId, FileURL)" +
                "VALUES(@ProgramId, @FileURL)");
            SqlCommand commandInsert = new SqlCommand(queryInsert, connect);
            commandInsert.Parameters.AddWithValue("@ProgramId", programid);
            commandInsert.Parameters.AddWithValue("@FileURL", url);
            commandInsert.ExecuteNonQuery();
            connect.Close();
        }

        public string GetFileUrl(int programid)
        {
            string urlStr = "";
            string query = string.Format("SELECT FileURL FROM [ADocs] WHERE ProgramId=" + programid);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            urlStr = command.ExecuteScalar().ToString();
            connect.Close();
            return urlStr;
        }
        #endregion

        #region WordBooks

        public FGOS GetFGOS(int id)
        {
            FGOS fgos = new FGOS();

            string query = string.Format("SELECT * FROM [FGOS] WHERE id = " + id);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            DataRow row = GetData(query, connect).Tables[0].Rows[0];
            fgos.Id = id;
            fgos.Name = row["Name"].ToString();
            fgos.Designation = row["Designation"].ToString();

            string query_IDes = string.Format("SELECT * FROM [Cross_FGOS_TrainingPlans] " +
                "WHERE FGOSId = " + id);
            List<int> IDes = new List<int>();
            foreach (DataRow row_ides in GetData(query_IDes, connect).Tables[0].Rows)
            {
                int added = (int)row_ides["TrainingPlanId"];
                IDes.Add(added);
            }

            fgos.TrainingPlansIDes = IDes;
            connect.Close();
            return fgos;
        }

        public void CreateFGOS_WordBook(FGOS fgos)
        {
            string query = string.Format("INSERT INTO [FGOS] (Name, Designation) VALUES(@Name, @Designation)");
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.Parameters.AddWithValue("@Name", fgos.Name);
            command.Parameters.AddWithValue("@Designation", fgos.Designation);
            command.ExecuteNonQuery();

            string query_selectID = "SELECT MAX(id) FROM[FGOS]";
            SqlCommand command_selectID = new SqlCommand(query_selectID, connect);
            fgos.Id = Convert.ToInt32(command_selectID.ExecuteScalar());

            string queryCross = string.Format("INSERT INTO [Cross_FGOS_TrainingPlans] " +
                "(FGOSId, TrainingPlanId) " +
                "VALUES (@FGOSId, @TrainingPlanId)");
            if (fgos.TrainingPlansIDes.Count != 0)
            {
                for (int i = 0; i < fgos.TrainingPlansIDes.Count; i++)
                {
                    SqlCommand command_cross = new SqlCommand(queryCross, connect);
                    command_cross.Parameters.AddWithValue("@FGOSId", fgos.Id);
                    command_cross.Parameters.AddWithValue("@TrainingPlanId", fgos.TrainingPlansIDes[i]);
                    command_cross.ExecuteNonQuery();
                }
            }
            connect.Close();
        }

        public void UpdateFGOS_WordBook(FGOS fgos)
        {
            string query = string.Format("UPDATE [FGOS] SET Name = '" + fgos.Name + "', " +
                "Designation = '" + fgos.Designation + "' WHERE id = " + fgos.Id);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command_up = new SqlCommand(query, connect);
            command_up.ExecuteNonQuery();

            string query_del = string.Format("DELETE FROM [Cross_FGOS_TrainingPlans] " +
                "WHERE FGOSId = " + fgos.Id);
            SqlCommand command_del = new SqlCommand(query_del, connect);
            command_del.ExecuteNonQuery();

            string queryCross = string.Format("INSERT INTO [Cross_FGOS_TrainingPlans] " +
               "(FGOSId, TrainingPlanId) " +
               "VALUES (@FGOSId, @TrainingPlanId)");
            if (fgos.TrainingPlansIDes.Count != 0)
            {
                for (int i = 0; i < fgos.TrainingPlansIDes.Count; i++)
                {
                    SqlCommand command_cross = new SqlCommand(queryCross, connect);
                    command_cross.Parameters.AddWithValue("@FGOSId", fgos.Id);
                    command_cross.Parameters.AddWithValue("@TrainingPlanId", fgos.TrainingPlansIDes[i]);
                    command_cross.ExecuteNonQuery();
                }
            }
            connect.Close();
        }

        public void DeleteFGOS_WordBook(int id)
        {
            string query = string.Format("DELETE FROM [FGOS] WHERE id = " + id);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();

            string query_cross = string.Format("DELETE FROM [Cross_FGOS_TrainingPlans] WHERE FGOSId = " + id);
            SqlCommand command_cross = new SqlCommand(query_cross, connect);
            command_cross.ExecuteNonQuery();
            command.ExecuteNonQuery();

            connect.Close();
        }

        public TrainingPlan GetTrainingPlan(int id)
        {
            TrainingPlan plan = new TrainingPlan();

            string query = string.Format("SELECT * FROM [Training_Plans] WHERE id = " + id);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            DataRow row = GetData(query, connect).Tables[0].Rows[0];
            plan.Id = id;
            plan.Name = row["Name"].ToString();
            plan.Year = row["Year"].ToString();

            string query_IDes = string.Format("SELECT * FROM [Cross_TrainingPlans_TrainingDirections] " +
                "WHERE TrainingPlanId = " + id);
            List<int> IDes = new List<int>();
            foreach (DataRow row_ides in GetData(query_IDes, connect).Tables[0].Rows)
            {
                int added = (int)row_ides["TrainingDirectionId"];
                IDes.Add(added);
            }

            plan.DirectionIDes = IDes;
            connect.Close();
            return plan;
        }

        public void CreateTrainingPlan_WordBook(TrainingPlan plan)
        {
            string query = string.Format("INSERT INTO [Training_Plans] (Name, Year) VALUES(@Name, @Year)");
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.Parameters.AddWithValue("@Name", plan.Name);
            command.Parameters.AddWithValue("@Year", plan.Year);
            command.ExecuteNonQuery();

            string query_selectID = "SELECT MAX(id) FROM[Training_Plans]";
            SqlCommand command_selectID = new SqlCommand(query_selectID, connect);
            plan.Id = Convert.ToInt32(command_selectID.ExecuteScalar());

            string queryCross = string.Format("INSERT INTO [Cross_TrainingPlans_TrainingDirections] " +
                "(TrainingPlanId, TrainingDirectionId) " +
                "VALUES (@TrainingPlanId, @TrainingDirectionId)");
            if (plan.DirectionIDes.Count != 0)
            {
                for (int i = 0; i < plan.DirectionIDes.Count; i++)
                {
                    SqlCommand command_cross = new SqlCommand(queryCross, connect);
                    command_cross.Parameters.AddWithValue("@TrainingPlanId", plan.Id);
                    command_cross.Parameters.AddWithValue("@TrainingDirectionId", plan.DirectionIDes[i]);
                    command_cross.ExecuteNonQuery();
                }
            }
            connect.Close();
        }

        public void UpdateTrainingPlan_WordBook(TrainingPlan plan)
        {
            string query = string.Format("UPDATE [Training_Plans] SET Name = '" + plan.Name + "', " +
                "Year = '" + plan.Year + "' WHERE id = " + plan.Id);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command_up = new SqlCommand(query, connect);
            command_up.ExecuteNonQuery();

            string query_del = string.Format("DELETE FROM [Cross_TrainingPlans_TrainingDirections] " +
                "WHERE TrainingPlanId = " + plan.Id);
            SqlCommand command_del = new SqlCommand(query_del, connect);
            command_del.ExecuteNonQuery();

            string queryCross = string.Format("INSERT INTO [Cross_TrainingPlans_TrainingDirections] " +
               "(TrainingPlanId, TrainingDirectionId) " +
               "VALUES (@TrainingPlanId, @TrainingDirectionId)");
            if (plan.DirectionIDes.Count != 0)
            {
                for (int i = 0; i < plan.DirectionIDes.Count; i++)
                {
                    SqlCommand command_cross = new SqlCommand(queryCross, connect);
                    command_cross.Parameters.AddWithValue("@TrainingPlanId", plan.Id);
                    command_cross.Parameters.AddWithValue("@TrainingDirectionId", plan.DirectionIDes[i]);
                    command_cross.ExecuteNonQuery();
                }
            }
            connect.Close();
        }

        public void DeleteTrainingPlan_WordBook(int id)
        {
            string query = string.Format("DELETE FROM [Training_Plans] WHERE id = " + id);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();

            string query_cross = string.Format("DELETE FROM [Cross_TrainingPlans_TrainingDirections] WHERE TrainingPlanId = " + id);
            SqlCommand command_cross = new SqlCommand(query_cross, connect);
            command_cross.ExecuteNonQuery();
            command.ExecuteNonQuery();

            connect.Close();
        }

        public Institute GetInstitute_WordBook(int id)
        {
            Institute inst = new Institute();

            string query = string.Format("SELECT * FROM [Institutes] WHERE id = " + id);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            DataRow row = GetData(query, connect).Tables[0].Rows[0];
            inst.Id = id;
            inst.Name = row["Name"].ToString();

            string query_IDes = string.Format("SELECT * FROM [Cross_Institutes_Departments] " +
                "WHERE InstituteId = " + id);
            List<int> IDes = new List<int>();
            foreach (DataRow row_ides in GetData(query_IDes, connect).Tables[0].Rows)
            {
                int added = (int)row_ides["DepartmentId"];
                IDes.Add(added);
            }

            inst.DepartmentsIDes = IDes;
            connect.Close();
            return inst;
        }

        public void CreateInstitute_WordBook(Institute inst)
        {
            string query = string.Format("INSERT INTO [Institutes] (Name) VALUES(@Name)");
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.Parameters.AddWithValue("@Name", inst.Name);
            command.ExecuteNonQuery();

            string query_selectID = "SELECT MAX(id) FROM[Institutes]";
            SqlCommand command_selectID = new SqlCommand(query_selectID, connect);
            inst.Id = Convert.ToInt32(command_selectID.ExecuteScalar());

            string queryCross = string.Format("INSERT INTO [Cross_Institutes_Departments] " +
                "(InstituteId, DepartmentId) " +
                "VALUES (@InstituteId, @DepartmentId)");
            if (inst.DepartmentsIDes.Count != 0)
            {
                for (int i = 0; i < inst.DepartmentsIDes.Count; i++)
                {
                    SqlCommand command_cross = new SqlCommand(queryCross, connect);
                    command_cross.Parameters.AddWithValue("@InstituteId", inst.Id);
                    command_cross.Parameters.AddWithValue("@DepartmentId", inst.DepartmentsIDes[i]);
                    command_cross.ExecuteNonQuery();
                }
            }
            connect.Close();
        }

        public void UpdateInstitute_WordBook(Institute inst)
        {
            string query = string.Format("UPDATE [Institutes] SET Name = '" + inst.Name + "' WHERE id = " + inst.Id);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();

            string query_del = string.Format("DELETE FROM [Cross_Institutes_Departments] " +
                "WHERE InstituteId = " + inst.Id);
            SqlCommand command_del = new SqlCommand(query_del, connect);
            command_del.ExecuteNonQuery();

            string queryCross = string.Format("INSERT INTO [Cross_Institutes_Departments] " +
               "(InstituteId, DepartmentId) " +
               "VALUES (@InstituteId, @DepartmentId)");
            if (inst.DepartmentsIDes.Count != 0)
            {
                for (int i = 0; i < inst.DepartmentsIDes.Count; i++)
                {
                    SqlCommand command_cross = new SqlCommand(queryCross, connect);
                    command_cross.Parameters.AddWithValue("@InstituteId", inst.Id);
                    command_cross.Parameters.AddWithValue("@DepartmentId", inst.DepartmentsIDes[i]);
                    command_cross.ExecuteNonQuery();
                }
            }

            connect.Close();
        }

        public void DeleteInstitute_WordBook(int id)
        {
            string query = string.Format("DELETE FROM [Institutes] WHERE id = " + id);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();
            string query_del = string.Format("DELETE FROM [Cross_Institutes_Departments] " +
               "WHERE InstituteId = " + id);
            SqlCommand command_del = new SqlCommand(query_del, connect);
            command_del.ExecuteNonQuery();
            connect.Close();
        }

        public Department GetDepartment_WordBook(int id)
        {
            Department dep = new Department();

            string query = string.Format("SELECT * FROM [Departments] WHERE id = " + id);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            DataRow row = GetData(query, connect).Tables[0].Rows[0];
            dep.Id = id;
            dep.Name = row["Name"].ToString();

            string query_IDes = string.Format("SELECT * FROM [Cross_Departments_Personal] " +
                "WHERE DepartmentId = " + id);
            List<string> IDes = new List<string>();
            foreach (DataRow row_ides in GetData(query_IDes, connect).Tables[0].Rows)
            {
                string added = row_ides["PersonalId"].ToString();
                IDes.Add(added);
            }

            dep.PersonalIDes = IDes;
            connect.Close();
            return dep;
        }

        public void CreateDepartment_WordBook(Department dep)
        {
            string query = string.Format("INSERT INTO [Departments] (Name) VALUES(@Name)");
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.Parameters.AddWithValue("@Name", dep.Name);
            command.ExecuteNonQuery();

            string query_selectID = "SELECT MAX(id) FROM[Departments]";
            SqlCommand command_selectID = new SqlCommand(query_selectID, connect);
            dep.Id = Convert.ToInt32(command_selectID.ExecuteScalar());

            string queryCross = string.Format("INSERT INTO [Cross_Departments_Personal] " +
                "(DepartmentId, PersonalId) " +
                "VALUES (@DepartmentId, @PersonalId)");
            if (dep.PersonalIDes.Count != 0)
            {
                for (int i = 0; i < dep.PersonalIDes.Count; i++)
                {
                    SqlCommand command_cross = new SqlCommand(queryCross, connect);
                    command_cross.Parameters.AddWithValue("@DepartmentId", dep.Id);
                    command_cross.Parameters.AddWithValue("@PersonalId", dep.PersonalIDes[i]);
                    command_cross.ExecuteNonQuery();
                }
            }
            connect.Close();
        }

        public void UpdateDepartment_WordBook(Department dep)
        {
            string query = string.Format("UPDATE [Departments] SET Name = '" + dep.Name + "' WHERE id = " + dep.Id);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();

            string query_del = string.Format("DELETE FROM [Cross_Departments_Personal] " +
                "WHERE DepartmentId = " + dep.Id);
            SqlCommand command_del = new SqlCommand(query_del, connect);
            command_del.ExecuteNonQuery();

            string queryCross = string.Format("INSERT INTO [Cross_Departments_Personal] " +
               "(DepartmentId, PersonalId) " +
               "VALUES (@DepartmentId, @PersonalId)");
            if (dep.PersonalIDes.Count != 0)
            {
                for (int i = 0; i < dep.PersonalIDes.Count; i++)
                {
                    SqlCommand command_cross = new SqlCommand(queryCross, connect);
                    command_cross.Parameters.AddWithValue("@DepartmentId", dep.Id);
                    command_cross.Parameters.AddWithValue("@PersonalId", dep.PersonalIDes[i]);
                    command_cross.ExecuteNonQuery();
                }
            }

            connect.Close();
        }

        public void DeleteDepartment_WordBook(int id)
        {
            string query = string.Format("DELETE FROM [Departments] WHERE id = " + id);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();

            string query_del = string.Format("DELETE FROM [Cross_Departments_Personal] " +
               "WHERE DepartmentId = " + id);
            SqlCommand command_del = new SqlCommand(query_del, connect);
            command_del.ExecuteNonQuery();

            connect.Close();
        }

        public Profile GetProfile_WordBook(int id)
        {
            Profile profile = new Profile();

            string query = string.Format("SELECT * FROM [Profiles] WHERE id = " + id);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            DataRow row = GetData(query, connect).Tables[0].Rows[0];
            profile.Id = id;
            profile.Name = row["Name"].ToString();

            string query_IDes = string.Format("SELECT * FROM [Cross_Profiles_Programs] " +
                "WHERE ProfileId = " + id);
            List<int> IDes = new List<int>();
            foreach (DataRow row_ides in GetData(query_IDes, connect).Tables[0].Rows)
            {
                int added = (int)row_ides["ProgramId"];
                IDes.Add(added);
            }

            profile.ProgramsIDes = IDes;
            connect.Close();
            return profile;
        }

        public void CreateProfile_WordBook(Profile profile)
        {
            string query = string.Format("INSERT INTO [Profiles] (Name) VALUES(@Name)");
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.Parameters.AddWithValue("@Name", profile);
            command.ExecuteNonQuery();

            string query_selectID = "SELECT MAX(id) FROM[Profiles]";
            SqlCommand command_selectID = new SqlCommand(query_selectID, connect);
            profile.Id = Convert.ToInt32(command_selectID.ExecuteScalar());

            string queryCross = string.Format("INSERT INTO [Cross_Profiles_Programs] " +
                "(ProfileId, ProgramId) " +
                "VALUES (@ProfileId, @ProgramId)");
            if (profile.ProgramsIDes.Count != 0)
            {
                for (int i = 0; i < profile.ProgramsIDes.Count; i++)
                {
                    SqlCommand command_cross = new SqlCommand(queryCross, connect);
                    command_cross.Parameters.AddWithValue("@ProfileId", profile.Id);
                    command_cross.Parameters.AddWithValue("@ProgramId", profile.ProgramsIDes[i]);
                    command_cross.ExecuteNonQuery();
                }
            }

            connect.Close();
        }

        public void UpdateProfile_WordBook(Profile profile)
        {
            string query = string.Format("UPDATE [Profiles] SET Name = '" + profile.Name + "' WHERE id = " + profile.Id);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();

            string query_del = string.Format("DELETE FROM [Cross_Profiles_Programs] " +
               "WHERE ProfileId = " + profile.Id);
            SqlCommand command_del = new SqlCommand(query_del, connect);
            command_del.ExecuteNonQuery();

            string queryCross = string.Format("INSERT INTO [Cross_Profiles_Programs] " +
                "(ProfileId, ProgramId) " +
                "VALUES (@ProfileId, @ProgramId)");
            if (profile.ProgramsIDes.Count != 0)
            {
                for (int i = 0; i < profile.ProgramsIDes.Count; i++)
                {
                    SqlCommand command_cross = new SqlCommand(queryCross, connect);
                    command_cross.Parameters.AddWithValue("@ProfileId", profile.Id);
                    command_cross.Parameters.AddWithValue("@ProgramId", profile.ProgramsIDes[i]);
                    command_cross.ExecuteNonQuery();
                }
            }
            connect.Close();
        }

        public void DeleteProfile_WordBook(int id)
        {
            string query = string.Format("DELETE FROM [Profiles] WHERE id = " + id);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();

            string query_del = string.Format("DELETE FROM [Cross_Profiles_Programs] " +
               "WHERE ProfileId = " + id);
            SqlCommand command_del = new SqlCommand(query_del, connect);
            command_del.ExecuteNonQuery();

            connect.Close();
        }

        public Discipline GetDiscipline(int disciplineid)
        {
            Discipline dis = new Discipline();
            SqlConnection connect = new SqlConnection(connection_string);
            List<int> beforeIDes, afterIDes;
            connect.Open();
            string query = string.Format("SELECT * FROM [Disciplines] WHERE id = " + disciplineid);
            DataRow row_ = GetData(query, connect).Tables[0].Rows[0];
            dis.Id = disciplineid;
            dis.Name = row_["Name"].ToString();

            query = string.Format("SELECT * FROM [Cross_Disciplines_Before] WHERE DisciplineId = " + disciplineid);
            beforeIDes = new List<int>();
            foreach (DataRow row in GetData(query, connect).Tables[0].Rows)
                beforeIDes.Add((int)row["DisciplineId_Before"]);
            dis.Disciplines_Before = beforeIDes;

            query = string.Format("SELECT * FROM [Cross_Disciplines_After] WHERE DisciplineId = " + disciplineid);
            afterIDes = new List<int>();
            foreach (DataRow row in GetData(query, connect).Tables[0].Rows)
                afterIDes.Add((int)row["DisciplineId_After"]);
            dis.Disciplines_After = afterIDes;

            connect.Close();
            return dis;
        }

        public int GetDisciplineIdByName(string name)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string query = string.Format("SELECT id FROM [Disciplines] WHERE Name = '" + name + "'");
            SqlCommand command = new SqlCommand(query, connect);
            int id = 0;
            if (command.ExecuteScalar() != null)
                id = (int)command.ExecuteScalar();
            connect.Close();
            return id;
        }

        public bool CreateDiscipline_WordBook(string name)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            bool answer = false;
            string query = string.Format("SELECT COUNT(*) FROM [Disciplines] WHERE Name = '" + name + "'");
            SqlCommand command = new SqlCommand(query, connect);
            if ((int)command.ExecuteScalar() == 0 && name != null && name.Replace(" ", "") != "")
            {
                query = string.Format("INSERT INTO [Disciplines] (Name) VALUES(@Name)");
                command = new SqlCommand(query, connect);
                command.Parameters.AddWithValue("@Name", name);
                command.ExecuteNonQuery();
                answer = true;
            }
            connect.Close();
            return answer;
        }

        public void UpDateDiscipline_WordBook(int id, string newname)
        {
            string query = string.Format("UPDATE [Disciplines] SET Name = '" + newname + "' WHERE id = " + id);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();
            connect.Close();
        }

        public void DeleteDiscipline_WordBook(int id)
        {
            string query = string.Format("DELETE FROM [Disciplines] WHERE id = " + id);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();
            query = string.Format("DELETE FROM [Cross_Disciplines_Before] " +
                "WHERE DisciplineId = " + id + " OR DisciplineId_Before = " + id);
            command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();
            query = string.Format("DELETE FROM [Cross_Disciplines_After] " +
                "WHERE DisciplineId = " + id + " OR DisciplineId_After = " + id);
            command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();
            connect.Close();
        }

        public void CreateCrossDisciplineBefore_WordBook(int disciplineid, int disciplineid_before)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string condition_query = string.Format("SELECT COUNT(*) FROM [Cross_Disciplines_Before] " +
                "WHERE DisciplineId = " + disciplineid + " AND DisciplineId_Before = " + disciplineid_before);
            SqlCommand condition_command = new SqlCommand(condition_query, connect);
            if ((int)condition_command.ExecuteScalar() == 0)
            {
                string query = string.Format("INSERT INTO [Cross_Disciplines_Before] " +
                "(DisciplineId, DisciplineId_Before)" +
                "VALUES(@DisciplineId, @DisciplineId_Before)");
                SqlCommand command = new SqlCommand(query, connect);
                command.Parameters.AddWithValue("@DisciplineId", disciplineid);
                command.Parameters.AddWithValue("@DisciplineId_Before", disciplineid_before);
                command.ExecuteNonQuery();
            }
            connect.Close();
        }

        public void DeleteCrossDisciplineBefore_WordBook(int disciplineid, int disciplineid_before)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string query = string.Format("DELETE FROM [Cross_Disciplines_Before] " +
                "WHERE DisciplineId = " + disciplineid + " AND DisciplineId_Before = " + disciplineid_before);
            SqlCommand command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();
            connect.Close();
        }

        public void CreateCrossDisciplineAfter_WordBook(int disciplineid, int disciplineid_after)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string condition_query = string.Format("SELECT COUNT(*) FROM [Cross_Disciplines_After] " +
                "WHERE DisciplineId = " + disciplineid + " AND DisciplineId_After = " + disciplineid_after);
            SqlCommand condition_command = new SqlCommand(condition_query, connect);
            if ((int)condition_command.ExecuteScalar() == 0)
            {
                string query = string.Format("INSERT INTO [Cross_Disciplines_After] " +
                "(DisciplineId, DisciplineId_After)" +
                "VALUES(@DisciplineId, @DisciplineId_After)");
                SqlCommand command = new SqlCommand(query, connect);
                command.Parameters.AddWithValue("@DisciplineId", disciplineid);
                command.Parameters.AddWithValue("@DisciplineId_After", disciplineid_after);
                command.ExecuteNonQuery();
            }
            connect.Close();
        }

        public void DeleteCrossDisciplineAfter_WordBook(int disciplineid, int disciplineid_after)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string query = string.Format("DELETE FROM [Cross_Disciplines_After] " +
                "WHERE DisciplineId = " + disciplineid + " AND DisciplineId_After = " + disciplineid_after);
            SqlCommand command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();
            connect.Close();
        }

        public void CreateQalifications_WordBook(string name)
        {
            string query = string.Format("INSERT INTO [Qualifications] (Name) VALUES(@Name)");
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.Parameters.AddWithValue("@Name", name);
            command.ExecuteNonQuery();
            connect.Close();
        }

        public void UpDateQalifications_WordBook(int id, string newname)
        {
            string query = string.Format("UPDATE [Qualifications] SET Name = '" + newname + "' WHERE id = " + id);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();
            connect.Close();
        }

        public void DeleteQalifications_WordBook(int id)
        {
            string query = string.Format("DELETE FROM [Qu" +
                "alifications] WHERE id = " + id);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();
            connect.Close();
        }

        public TrainingDirection GetTrainingDirection_WordBook(int id)
        {
            TrainingDirection direction = new TrainingDirection();
            string query = string.Format("SELECT * FROM [Training_Directions] WHERE id = " + id);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            DataRow cells = GetData(query, connect).Tables[0].Rows[0];
            direction.Id = id;
            direction.Designation = cells["Designation"].ToString();
            direction.Code = cells["Code"].ToString();

            string query_ides = string.Format("SELECT * FROM [Cross_TrainingDirections_Profiles] " +
                "WHERE TrainingDirectionId = " + id);
            List<int> ides = new List<int>();
            foreach (DataRow row in GetData(query_ides, connect).Tables[0].Rows)
                ides.Add((int)row["ProfileId"]);

            direction.ProfilesIDes = ides;

            connect.Close();
            return direction;
        }

        public void CreateTrainingDirection_WordBook(TrainingDirection direction)
        {
            string query = string.Format("INSERT INTO [Training_Directions] " +
                "(Designation, Code) VALUES(@Designation, @Code)");
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.Parameters.AddWithValue("@Designation", direction.Designation);
            command.Parameters.AddWithValue("@Code", direction.Code);
            command.ExecuteNonQuery();

            string query_selectID = "SELECT MAX(id) FROM[Training_Directions]";
            SqlCommand command_selectID = new SqlCommand(query_selectID, connect);
            direction.Id = Convert.ToInt32(command_selectID.ExecuteScalar());

            if (direction.ProfilesIDes.Count != 0)
            {
                query = string.Format("INSERT INTO [Cross_TrainingDirections_Profiles] " +
                    "(TrainingDirectionId, ProfileId) VALUES(@TrainingDirectionId, @ProfileId)");
                for (int i = 0; i < direction.ProfilesIDes.Count; i++)
                {
                    SqlCommand cross_command = new SqlCommand(query, connect);
                    cross_command.Parameters.AddWithValue("@TrainingDirectionId", direction.Id);
                    cross_command.Parameters.AddWithValue("@ProfileId", direction.ProfilesIDes[i]);
                    cross_command.ExecuteNonQuery();
                }
            }
            connect.Close();
        }

        public void UpDateTrainingDirection_WordBook(TrainingDirection direction)
        {
            string query = string.Format("UPDATE [Training_Directions] SET " +
                "Designation = '" + direction.Designation + "', " +
                "Code = '" + direction.Code + "' WHERE id = " + direction.Id);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();

            query = string.Format("DELETE FROM [Cross_TrainingDirections_Profiles] WHERE TrainingDirectionId = " + direction.Id);
            SqlCommand del_command = new SqlCommand(query, connect);
            del_command.ExecuteNonQuery();

            if (direction.ProfilesIDes.Count != 0)
            {
                query = string.Format("INSERT INTO [Cross_TrainingDirections_Profiles] " +
                    "(TrainingDirectionId, ProfileId) VALUES(@TrainingDirectionId, @ProfileId)");
                for (int i = 0; i < direction.ProfilesIDes.Count; i++)
                {
                    SqlCommand cross_command = new SqlCommand(query, connect);
                    cross_command.Parameters.AddWithValue("@TrainingDirectionId", direction.Id);
                    cross_command.Parameters.AddWithValue("@ProfileId", direction.ProfilesIDes[i]);
                    cross_command.ExecuteNonQuery();
                }
            }
            connect.Close();
        }

        public void DeleteTrainingDirection_WordBook(int id)
        {
            string query = string.Format("DELETE FROM [Training_Directions] WHERE id = " + id);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();
            query = string.Format("DELETE FROM [Cross_TrainingDirections_Profiles] WHERE TrainingDirectionId = " + id);
            SqlCommand del_command = new SqlCommand(query, connect);
            del_command.ExecuteNonQuery();
            connect.Close();
        }

        public Competence GetCompetence(int id)
        {
            Competence com = new Competence();
            string query = string.Format("SELECT * FROM [Competences] WHERE Id = " + id);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            DataRow row = GetData(query, connect).Tables[0].Rows[0];
            com.Id = (int)row["Id"];
            com.Code = row["Code"].ToString();
            com.Description = row["Description"].ToString();

            string query_ides = string.Format("SELECT FGOSId FROM [Cross_Competences_FGOS] " +
                "WHERE CompetenceId = " + id);
            SqlCommand command = new SqlCommand(query_ides, connect);
            int fgosid = 0;
            if (command.ExecuteScalar() != null)
                fgosid = (int)command.ExecuteScalar();

            com.FGOSId = fgosid;

            connect.Close();
            return com;
        }

        public bool CreateCompetence_WordBook(Competence com)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            bool accessToCreate = true;
            string filter_query = string.Format("SELECT COUNT(*) FROM [Competences] WHERE " +
                "Code = '" + com.Code + "' AND Description = '" + com.Description + "'");
            SqlCommand filterCommand = new SqlCommand(filter_query, connect);
            if ((int)filterCommand.ExecuteScalar() != 0)
            {
                filter_query = string.Format("SELECT id FROM [Competences] WHERE " +
                "Code = '" + com.Code + "' AND Description = '" + com.Description + "'");
                filterCommand = new SqlCommand(filter_query, connect);
                int id = (int)filterCommand.ExecuteScalar();

                filter_query = string.Format("SELECT COUNT(*) FROM [Cross_Competences_FGOS] " +
                    "WHERE CompetenceId = " + id);
                filterCommand = new SqlCommand(filter_query, connect);

                if ((int)filterCommand.ExecuteScalar() == 0)
                {
                    if(com.FGOSId == 0)
                        accessToCreate = false;
                }
                else
                {
                    filter_query = string.Format("SELECT FGOSId FROM [Cross_Competences_FGOS] " +
                    "WHERE CompetenceId = " + id);
                    filterCommand = new SqlCommand(filter_query, connect);
                    if ((int)filterCommand.ExecuteScalar() == com.FGOSId)
                        accessToCreate = false;
                }
            }

            if (accessToCreate)
            {
                string query = string.Format("INSERT INTO [Competences] " +
                    "(Code, Description) " +
                    "VALUES(@Code, @Description)");
                SqlCommand command = new SqlCommand(query, connect);
                command.Parameters.AddWithValue("@Code", com.Code);
                command.Parameters.AddWithValue("@Description", com.Description);
                command.ExecuteNonQuery();
                
                if (com.FGOSId != 0)
                {
                    query = string.Format("SELECT MAX(id) FROM [Competences]");
                    command = new SqlCommand(query, connect);
                    int id = (int)command.ExecuteScalar();
                    string insert_cross_query = "INSERT INTO [Cross_Competences_FGOS] " +
                        "(CompetenceId, FGOSId) VALUES(@CompetenceId, @FGOSId)";
                    SqlCommand commandcross = new SqlCommand(insert_cross_query, connect);
                    commandcross.Parameters.AddWithValue("@CompetenceId", id);
                    commandcross.Parameters.AddWithValue("@FGOSId", com.FGOSId);
                    commandcross.ExecuteNonQuery();
                }
            }
            
            connect.Close();
            return accessToCreate;
        }

        public void UpDateCompetence_WordBook(Competence com)
        {
            string query = string.Format("UPDATE [Competences] SET " +
                "Code = '" + com.Code + "', " +
                "Description = '" + com.Description + "' " +
                "WHERE id = " + com.Id);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();

            string del_query = "DELETE FROM [Cross_Competences_FGOS] WHERE CompetenceId = " + com.Id;
            SqlCommand delcommand = new SqlCommand(del_query, connect);
            delcommand.ExecuteNonQuery();

            if (com.FGOSId != 0)
            {
                string insert_cross_query = "INSERT INTO [Cross_Competences_FGOS] " +
                "(CompetenceId, FGOSId) VALUES(@CompetenceId, @FGOSId)";
                SqlCommand commandcross = new SqlCommand(insert_cross_query, connect);
                commandcross.Parameters.AddWithValue("@CompetenceId", com.Id);
                commandcross.Parameters.AddWithValue("@FGOSId", com.FGOSId);
                commandcross.ExecuteNonQuery();
            }

            connect.Close();
        }

        public void DeleteCompetence_WordBook(int id)
        {
            string query = string.Format("DELETE FROM [Competences] WHERE id = " + id);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();

            string del_query = "DELETE FROM [Cross_Competences_FGOS] WHERE CompetenceId = " + id;
            SqlCommand delcommand = new SqlCommand(del_query, connect);
            delcommand.ExecuteNonQuery();

            connect.Close();
        }
        #endregion

        #region Users

        public string GetUserName(string userid)
        {
            string name = "";
            string query = string.Format("SELECT UserName FROM [AspNetUsers] WHERE Id = '" + userid + "'");
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            name = command.ExecuteScalar().ToString();
            connect.Close();
            return name;
        }
        
        public void UpdateUserSign(byte[] img, string userid)
        {
            string query = string.Format("UPDATE [AspNetUsers] SET UserSign = @UserSign WHERE Id = '" + userid + "'");
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.Parameters.AddWithValue("@UserSign", img);
            command.ExecuteNonQuery();
            connect.Close();
        }

        public void UpdateUserRole(string userid, string roleid)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string query = string.Format("SELECT * FROM [AspNetUserRoles] WHERE UserId = '" + userid + "'");
            SqlCommand sel_com = new SqlCommand(query, connect);
            try
            {
                string id = sel_com.ExecuteScalar().ToString();
                query = string.Format("UPDATE [AspNetUserRoles] SET RoleId = '" + roleid + "' WHERE UserId = '" + userid + "'");
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();
            }
            catch
            {
                query = string.Format("INSERT INTO [AspNetUserRoles] (UserId, RoleId) " +
                    "VALUES(@UserId, @RoleId)");
                SqlCommand command = new SqlCommand(query, connect);
                command.Parameters.AddWithValue("@UserId", userid);
                command.Parameters.AddWithValue("@RoleId", roleid);
                command.ExecuteNonQuery();
            }


            query = string.Format("UPDATE [AspNetUsers] SET LockoutEnabled = 0 WHERE Id = '" + userid + "'");
            SqlCommand commandUp = new SqlCommand(query, connect);
            commandUp.ExecuteNonQuery();

            connect.Close();
        }

        public byte[] GetUserSign(string userid)
        {
            byte[] img;
            string query = string.Format("SELECT UserSign FROM [AspNetUsers] WHERE Id = '" + userid + "'");
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            img = (byte[])command.ExecuteScalar();
            connect.Close();
            return img;
        }
        
        #endregion

        #region CrossUserPrograms
        public void UpdateDrafters(Pro pro)
        {
            if (pro.AdditionalDrafters != null)
            {
                SqlConnection connect = new SqlConnection(connection_string);
                connect.Open();

                string querydel = string.Format("DELETE FROM [AdditionalDrafters] WHERE ProgramId = " + pro.Id);
                SqlCommand commanddel = new SqlCommand(querydel, connect);
                commanddel.ExecuteNonQuery();

                string query_drafters = string.Format("INSERT INTO [AdditionalDrafters] " +
                    "(AcademStatus, UserId, ProgramId) " +
                    "VALUES(@AcademStatus, @UserId, @ProgramId)");
                for (int i = 0; i < pro.AdditionalDrafters.Count; i++)
                {
                    SqlCommand command_drafters = new SqlCommand(query_drafters, connect);
                    command_drafters.Parameters.AddWithValue("@AcademStatus", pro.AdditionalDrafters[i].Status);
                    command_drafters.Parameters.AddWithValue("@UserId", pro.AdditionalDrafters[i].UserId);
                    command_drafters.Parameters.AddWithValue("@ProgramId", pro.Id);
                    command_drafters.ExecuteNonQuery();
                }
                connect.Close();
            }
        }

        public List<int> GetProgramsIDesToUserConfirm(string UserId)
        {
            List<int> IDes = new List<int>();

            string query = string.Format("SELECT * FROM [ProgramsConfirmers] WHERE UserId = '" + UserId + "'");
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            foreach (DataRow row in GetData(query, connect).Tables[0].Rows)
                IDes.Add((int)row[1]);
            connect.Close();
            return IDes;
        }

        public void SetConfirmProgram(string userid, int programid, int programstatus, string comment)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            string query = string.Format("UPDATE [ProgramsConfirmers] SET Status = " + programstatus + ", " +
                "Comment = '" + comment + "' WHERE ProgramId = " + programid + " AND UserId = '" + userid + "'");
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();

            bool programapproved = true;
            string selectquery = string.Format("SELECT Status FROM [ProgramsConfirmers] WHERE ProgramId = " + programid);
            DataSet data = GetData(selectquery, connect);
            foreach (DataRow row in data.Tables[0].Rows)
            {
                int cell = (int)row[0];
                if (cell != 1)
                {
                    programapproved = false;
                    break;
                }
            }

            if (programapproved)
            {
                string approve_query = string.Format("UPDATE [Programs] SET Status = 'Утверждена' WHERE id = " + programid);
                SqlCommand command_approve = new SqlCommand(approve_query, connect);
                command_approve.ExecuteNonQuery();
            }

            string status = "";
            switch (programstatus)
            {
                case 1:
                    status = "Подписана";
                    SetHistory(userid, programid, status);
                    break;
                case -1:
                    status = "Отклонена. Комментарий - '" + comment + "'";
                    SetHistory(userid, programid, status);
                    break;
            }
            connect.Close();
        }

        public int GetConfirmStatus(string userid, int programid)
        {
            int status;
            string query = "SELECT Status FROM [ProgramsConfirmers] WHERE UserId = '" + userid + "' " +
                "AND ProgramId = " + programid;
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            status = (int)command.ExecuteScalar();
            connect.Close();
            return status;
        }

        private void SetZeroConfirmProgram(int id)
        {
            string query_in_pro = string.Format("UPDATE [Programs] SET Status = 'Ожидает утверждения' " +
                "WHERE id = " + id);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command_to_pro = new SqlCommand(query_in_pro, connect);
            command_to_pro.ExecuteNonQuery();

            string query_in_confirmers = string.Format("UPDATE [ProgramsConfirmers] SET " +
                "Status = 0, Comment = 'В РПД внесена правка' WHERE ProgramId = " + id);
            SqlCommand command_to_confirmers = new SqlCommand(query_in_confirmers, connect);
            command_to_confirmers.ExecuteNonQuery();

            connect.Close();
        }

        public string GetConfirmers(int programid)
        {
            string answer = "";
            string query = string.Format("SELECT * FROM [ProgramsConfirmers] WHERE ProgramId = " + programid);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            foreach (DataRow row in GetData(query, connect).Tables[0].Rows)
            {
                string username = "";
                string status = "";
                string query_search = "SELECT UserName FROM [AspNetUsers] WHERE Id = '" + row["UserId"].ToString() + "'";
                SqlCommand command = new SqlCommand(query_search, connect);
                username = command.ExecuteScalar().ToString();
                switch ((int)row["Status"])
                {
                    case 0:
                        status = "ожидается решение";
                        break;
                    case 1:
                        status = "подписал";
                        break;
                    case -1:
                        status = "отклонил. Комментарий - '" + row["comment"].ToString() + "'";
                        break;
                }
                answer += username + " – " + status + ".<br /><br />";
            }
            connect.Close();
            return answer;
        }
        #endregion

        #region Programs
        public int CreateNewProgram(Pro pro)
        {
            int programid;
            string InsertInProTable_string = string.Format("INSERT INTO [Programs] " +
            "(Institute, Approver_Post, Approver_Name, Approver_Id, Approve_Date, Discipline, Training_Direction, Profil, Graduate_Qualification, Training_Form, StudyPeriod, City, ObjectivesOfDiscipline, DisciplineInOOP, PlanDescription, DisciplineStructureContent, SemesteresContent, EducationalTechnology, MonitoringTools_Description, ZachetDescription, ZachetDone, ZachetUnDone, ExamDescription, AttestatGreat, AttestatGood, AttestatSatisfactorily, AttestatNotSatisfactorily, BasicLiterature, AdditionalLiterature, ITResources, MaterialTechnicalSupport, StudentRecommendations, TeacherRecommendations, DrafterStatus, DrafterName, DrafterId, Department, DraftDate, Protocol, DepartmentHeadStatus, DepartmentHeadName, DepartmentHeadId, AgreementDepartmentHeadStatus, AgreementDepartmentHeadName, AgreementDepartmentHeadId, AgreementDirectorStatus, AgreementDirectorName, AgreementDirectorId, Status) " +
            "Values(@Institute, @Approver_Post, @Approver_Name, @Approver_Id, @Approve_Date, @Discipline, @Training_Direction, @Profil, @Graduate_Qualification, @Training_Form, @StudyPeriod, @City, @ObjectivesOfDiscipline, @DisciplineInOOP, @PlanDescription, @DisciplineStructureContent, @SemesteresContent, @EducationalTechnology, @MonitoringTools_Description, @ZachetDescription, @ZachetDone, @ZachetUnDone, @ExamDescription, @AttestatGreat, @AttestatGood, @AttestatSatisfactorily, @AttestatNotSatisfactorily, @BasicLiterature, @AdditionalLiterature, @ITResources, @MaterialTechnicalSupport, @StudentRecommendations, @TeacherRecommendations, @DrafterStatus, @DrafterName, @DrafterId, @Department, @DraftDate, @Protocol, @DepartmentHeadStatus, @DepartmentHeadName, @DepartmentHeadId, @AgreementDepartmentHeadStatus, @AgreementDepartmentHeadName, @AgreementDepartmentHeadId, @AgreementDirectorStatus, @AgreementDirectorName, @AgreementDirectorId, @Status)");
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(InsertInProTable_string, connect);
            command.Parameters.AddWithValue("@Institute", pro.Institute);
            command.Parameters.AddWithValue("@Approver_Post", pro.Approver_Post);
            command.Parameters.AddWithValue("@Approver_Name", pro.Approver_Name);
            command.Parameters.AddWithValue("@Approver_Id", pro.Approver_Id);
            command.Parameters.AddWithValue("@Approve_Date", pro.Approve_Date);
            command.Parameters.AddWithValue("@Discipline", pro.Discipline);
            command.Parameters.AddWithValue("@Training_Direction", pro.Training_Direction);
            command.Parameters.AddWithValue("@Profil", pro.Profil);
            command.Parameters.AddWithValue("@Graduate_Qualification", pro.Graduate_Qualification);
            command.Parameters.AddWithValue("@Training_Form", pro.Training_Form);
            command.Parameters.AddWithValue("@StudyPeriod", pro.StudPeriod);
            command.Parameters.AddWithValue("@City", pro.City);
            command.Parameters.AddWithValue("@ObjectivesOfDiscipline", pro.ObjectivesOfDiscipline);
            command.Parameters.AddWithValue("@DisciplineInOOP", pro.DisciplineInOOP);
            command.Parameters.AddWithValue("@PlanDescription", pro.PlanDescription);
            command.Parameters.AddWithValue("@DisciplineStructureContent", pro.DisciplineStructureContent);
            command.Parameters.AddWithValue("@SemesteresContent", pro.SemesteresContent);
            command.Parameters.AddWithValue("@EducationalTechnology", pro.EducationalTechnology);
            command.Parameters.AddWithValue("@MonitoringTools_Description", pro.MonitoringTools_Description);

            command.Parameters.AddWithValue("@ZachetDescription", pro.ZachetDescription);
            command.Parameters.AddWithValue("@ZachetDone", pro.ZachetTable.ZachetDone);
            command.Parameters.AddWithValue("@ZachetUnDone", pro.ZachetTable.ZachetUnDone);
            command.Parameters.AddWithValue("@ExamDescription", pro.ExamDescription);
            command.Parameters.AddWithValue("@AttestatGreat", pro.ExamTable.AttestatGreat);
            command.Parameters.AddWithValue("@AttestatGood", pro.ExamTable.AttestatGood);
            command.Parameters.AddWithValue("@AttestatSatisfactorily", pro.ExamTable.AttestatSatisfactorily);
            command.Parameters.AddWithValue("@AttestatNotSatisfactorily", pro.ExamTable.AttestatNotSatisfactorily);
            command.Parameters.AddWithValue("@BasicLiterature", pro.BasicLiterature);
            command.Parameters.AddWithValue("@AdditionalLiterature", pro.AdditionalLiterature);
            command.Parameters.AddWithValue("@ITResources", pro.ITResources);
            command.Parameters.AddWithValue("@MaterialTechnicalSupport", pro.MaterialTechnicalSupport);
            command.Parameters.AddWithValue("@StudentRecommendations", pro.StudentRecommendations);
            command.Parameters.AddWithValue("@TeacherRecommendations", pro.TeacherRecommendations);
            command.Parameters.AddWithValue("@DrafterStatus", pro.DrafterStatus);
            command.Parameters.AddWithValue("@DrafterName", pro.DrafterName);
            command.Parameters.AddWithValue("@DrafterId", pro.DrafterId);
            command.Parameters.AddWithValue("@Department", pro.Department);
            command.Parameters.AddWithValue("@DraftDate", pro.DraftDate);
            command.Parameters.AddWithValue("@Protocol", pro.Protocol);
            command.Parameters.AddWithValue("@DepartmentHeadStatus", pro.DepartmentHeadStatus);
            command.Parameters.AddWithValue("@DepartmentHeadName", pro.DepartmentHeadName);
            command.Parameters.AddWithValue("@DepartmentHeadId", pro.DepartmentHeadId);
            command.Parameters.AddWithValue("@AgreementDepartmentHeadStatus", pro.AgreementDepartmentHeadStatus);
            command.Parameters.AddWithValue("@AgreementDepartmentHeadName", pro.AgreementDepartmentHeadName);
            command.Parameters.AddWithValue("@AgreementDepartmentHeadId", pro.AgreementDepartmentHeadId);
            command.Parameters.AddWithValue("@AgreementDirectorStatus", pro.AgreementDirectorStatus);
            command.Parameters.AddWithValue("@AgreementDirectorName", pro.AgreementDirectorName);
            command.Parameters.AddWithValue("@AgreementDirectorId", pro.AgreementDirectorId);
            command.Parameters.AddWithValue("@Status", pro.Status);

            command.ExecuteNonQuery();

            string query_selectID = "SELECT MAX(id) FROM[Programs]";
            SqlCommand command_selectID = new SqlCommand(query_selectID, connect);
            programid = Convert.ToInt32(command_selectID.ExecuteScalar());

            if (pro.AdditionalDrafters != null)
            {
                string query_drafters = string.Format("INSERT INTO [AdditionalDrafters] " +
                    "(AcademStatus, UserId, ProgramId) " +
                    "VALUES(@AcademStatus, @UserId, @ProgramId)");
                for (int i = 0; i < pro.AdditionalDrafters.Count; i++)
                {
                    SqlCommand command_drafters = new SqlCommand(query_drafters, connect);
                    command_drafters.Parameters.AddWithValue("@AcademStatus", pro.AdditionalDrafters[i].Status);
                    command_drafters.Parameters.AddWithValue("@UserId", pro.AdditionalDrafters[i].UserId);
                    command_drafters.Parameters.AddWithValue("@ProgramId", programid);
                    command_drafters.ExecuteNonQuery();
                }
            }

            string query_confirmers = string.Format("INSERT INTO [ProgramsConfirmers] " +
                "(UserId, ProgramId, Status)" +
                " VALUES(@UserId, @ProgramId, @Status)");

            List<string> IDes = new List<string>();
            IDes.Add(pro.Approver_Id);
            IDes.Add(pro.DrafterId);
            IDes.Add(pro.DepartmentHeadId);
            IDes.Add(pro.AgreementDepartmentHeadId);
            IDes.Add(pro.AgreementDirectorId);

            List<string> UniIDes = new List<string>();
            UniIDes = IDes.Distinct().ToList();
            for (int i = 0; i < UniIDes.Count; i++)
            {
                SqlCommand commandConfirmers = new SqlCommand(query_confirmers, connect);
                commandConfirmers.Parameters.AddWithValue("@UserId", UniIDes[i]);
                commandConfirmers.Parameters.AddWithValue("@ProgramId", programid);
                commandConfirmers.Parameters.AddWithValue("@Status", 0);
                commandConfirmers.ExecuteNonQuery();
            }



            if (pro.DSTTable != null)
            {
                string query_insertDS = string.Format("INSERT INTO [DisciplineStructurs] " +
                    "(ProgramId, Тема, Семестр, Неделя, Л, ПС, Лаб, СРС, КСР, КР, КП, РГР, Подготовка_к_лр, К_р, Форма_аттестации)" +
                    " VALUES (@ProgramId, @Тема, @Семестр, @Неделя, @Л, @ПС, @Лаб, @СРС, @КСР, @КР, @КП, @РГР, @Подготовка_к_лр, @К_р, @Форма_аттестации)");
                for (int i = 0; i < pro.DSTTable.Count; i++)
                {
                    SqlCommand command_insertDS = new SqlCommand(query_insertDS, connect);
                    command_insertDS.Parameters.AddWithValue("@ProgramId", programid);
                    command_insertDS.Parameters.AddWithValue("@Тема", pro.DSTTable[i].Topic);
                    command_insertDS.Parameters.AddWithValue("@Семестр", pro.DSTTable[i].Semestr);
                    command_insertDS.Parameters.AddWithValue("@Неделя", pro.DSTTable[i].Week);
                    command_insertDS.Parameters.AddWithValue("@Л", pro.DSTTable[i].L);
                    command_insertDS.Parameters.AddWithValue("@ПС", pro.DSTTable[i].PS);
                    command_insertDS.Parameters.AddWithValue("@Лаб", pro.DSTTable[i].Lab);
                    command_insertDS.Parameters.AddWithValue("@СРС", pro.DSTTable[i].SRS);
                    command_insertDS.Parameters.AddWithValue("@КСР", pro.DSTTable[i].KSR);
                    command_insertDS.Parameters.AddWithValue("@КР", pro.DSTTable[i].KR);
                    command_insertDS.Parameters.AddWithValue("@КП", pro.DSTTable[i].KP);
                    command_insertDS.Parameters.AddWithValue("@РГР", pro.DSTTable[i].RGR);
                    command_insertDS.Parameters.AddWithValue("@Подготовка_к_лр", pro.DSTTable[i].PrepToLR);
                    command_insertDS.Parameters.AddWithValue("@К_р", pro.DSTTable[i].KRend);
                    command_insertDS.Parameters.AddWithValue("@Форма_аттестации", pro.DSTTable[i].AttestatForm);

                    command_insertDS.ExecuteNonQuery();
                }

            }

            if (pro.PlanTable.Count != 0)
            {
                string query_InsertPlan = string.Format("Insert Into [Plan] (ProgramId, Competence, Ability, Enumeration) " +
                    "Values(@ProgramId, @Competence, @Ability, @Enumeration)");
                for (int i = 0; i < pro.PlanTable.Count; i++)
                {
                    SqlCommand command_insertPLAN = new SqlCommand(query_InsertPlan, connect);
                    command_insertPLAN.Parameters.AddWithValue("@ProgramId", programid);
                    command_insertPLAN.Parameters.AddWithValue("@Competence", pro.PlanTable[i].Сompetence);
                    command_insertPLAN.Parameters.AddWithValue("@Ability", pro.PlanTable[i].Ability);
                    command_insertPLAN.Parameters.AddWithValue("@Enumeration", pro.PlanTable[i].Enumeration);
                    command_insertPLAN.ExecuteNonQuery();
                }
            }


            if (pro.FondTable.Count != 0)
            {
                string query_InsertFond = string.Format("INSERT INTO [Fond]" +
                   "(ProgramId, Competence, Ability) " +
                   "VALUES (@ProgramId, @Competence, @Ability)");
                for (int i = 0; i < pro.FondTable.Count; i++)
                {
                    SqlCommand command_insertFOND = new SqlCommand(query_InsertFond, connect);
                    command_insertFOND.Parameters.AddWithValue("@ProgramId", programid);
                    command_insertFOND.Parameters.AddWithValue("@Competence", pro.FondTable[i].Сompetence);
                    command_insertFOND.Parameters.AddWithValue("@Ability", pro.FondTable[i].Ability);
                    command_insertFOND.ExecuteNonQuery();
                }
            }

            if (pro.CriterionsTable.Count != 0)
            {
                string query_InsertCriterions = string.Format("INSERT INTO [Criterions]" +
                   "(ProgramId, Competence, ToKnow, ToKnow_2, ToKnow_3, ToKnow_4, ToKnow_5, ToDo, ToDo_2, ToDo_3, ToDo_4, ToDo_5, ToMaster, ToMaster_2, ToMaster_3, ToMaster_4, ToMaster_5) " +
                   "VALUES (@ProgramId, @Competence, @ToKnow, @ToKnow_2, @ToKnow_3, @ToKnow_4, @ToKnow_5, @ToDo, @ToDo_2, @ToDo_3, @ToDo_4, @ToDo_5, @ToMaster, @ToMaster_2, @ToMaster_3, @ToMaster_4, @ToMaster_5)");
                for (int i = 0; i < pro.CriterionsTable.Count; i++)
                {
                    SqlCommand command_insertCRITERIONS = new SqlCommand(query_InsertCriterions, connect);
                    command_insertCRITERIONS.Parameters.AddWithValue("@ProgramId", programid);
                    command_insertCRITERIONS.Parameters.AddWithValue("@Competence", pro.CriterionsTable[i].Competence);
                    command_insertCRITERIONS.Parameters.AddWithValue("@ToKnow", pro.CriterionsTable[i].ToKnow_Description);
                    command_insertCRITERIONS.Parameters.AddWithValue("@ToKnow_2", pro.CriterionsTable[i].ToKnow[0]);
                    command_insertCRITERIONS.Parameters.AddWithValue("@ToKnow_3", pro.CriterionsTable[i].ToKnow[1]);
                    command_insertCRITERIONS.Parameters.AddWithValue("@ToKnow_4", pro.CriterionsTable[i].ToKnow[2]);
                    command_insertCRITERIONS.Parameters.AddWithValue("@ToKnow_5", pro.CriterionsTable[i].ToKnow[3]);
                    command_insertCRITERIONS.Parameters.AddWithValue("@ToDo", pro.CriterionsTable[i].ToDo_Description);
                    command_insertCRITERIONS.Parameters.AddWithValue("@ToDo_2", pro.CriterionsTable[i].ToDo[0]);
                    command_insertCRITERIONS.Parameters.AddWithValue("@ToDo_3", pro.CriterionsTable[i].ToDo[1]);
                    command_insertCRITERIONS.Parameters.AddWithValue("@ToDo_4", pro.CriterionsTable[i].ToDo[2]);
                    command_insertCRITERIONS.Parameters.AddWithValue("@ToDo_5", pro.CriterionsTable[i].ToDo[3]);
                    command_insertCRITERIONS.Parameters.AddWithValue("@ToMaster", pro.CriterionsTable[i].ToMaster_Description);
                    command_insertCRITERIONS.Parameters.AddWithValue("@ToMaster_2", pro.CriterionsTable[i].ToMaster[0]);
                    command_insertCRITERIONS.Parameters.AddWithValue("@ToMaster_3", pro.CriterionsTable[i].ToMaster[1]);
                    command_insertCRITERIONS.Parameters.AddWithValue("@ToMaster_4", pro.CriterionsTable[i].ToMaster[2]);
                    command_insertCRITERIONS.Parameters.AddWithValue("@ToMaster_5", pro.CriterionsTable[i].ToMaster[3]);
                    command_insertCRITERIONS.ExecuteNonQuery();
                }
            }

            if (pro.PFT_1 != null)
            {
                string query_InsertPrilFondUniversal = string.Format("INSERT INTO [PrilFondUniversal]" +
                    "(ProgramId, Code, Wording, Components, WordingTechnology, AssessForm, Steps)" +
                    "VALUES (@ProgramId, @Code, @Wording, @Components, @WordingTechnology, @AssessForm, @Steps)");
                for (int i = 0; i < pro.PFT_1.Count; i++)
                {
                    SqlCommand command_insertPrilFondUniversal = new SqlCommand(query_InsertPrilFondUniversal, connect);
                    command_insertPrilFondUniversal.Parameters.AddWithValue("@ProgramId", programid);
                    command_insertPrilFondUniversal.Parameters.AddWithValue("@Code", pro.PFT_1[i].Index);
                    command_insertPrilFondUniversal.Parameters.AddWithValue("@Wording", pro.PFT_1[i].Formul);
                    command_insertPrilFondUniversal.Parameters.AddWithValue("@Components", pro.PFT_1[i].KomponentsList);
                    command_insertPrilFondUniversal.Parameters.AddWithValue("@WordingTechnology", pro.PFT_1[i].CompetenceTechnology);
                    command_insertPrilFondUniversal.Parameters.AddWithValue("@AssessForm", pro.PFT_1[i].AssessForm);
                    command_insertPrilFondUniversal.Parameters.AddWithValue("@Steps", pro.PFT_1[i].Steps);
                    command_insertPrilFondUniversal.ExecuteNonQuery();
                }
            }

            if (pro.PFT_2 != null)
            {
                string query_InsertPrilFondGeneral = string.Format("INSERT INTO [PrilFondGeneral]" +
                   "(ProgramId, Code, Wording, Components, WordingTechnology, AssessForm, Steps)" +
                   "VALUES (@ProgramId, @Code, @Wording, @Components, @WordingTechnology, @AssessForm, @Steps)");
                for (int i = 0; i < pro.PFT_2.Count; i++)
                {
                    SqlCommand command_insertPrilFondGeneral = new SqlCommand(query_InsertPrilFondGeneral, connect);
                    command_insertPrilFondGeneral.Parameters.AddWithValue("@ProgramId", programid);
                    command_insertPrilFondGeneral.Parameters.AddWithValue("@Code", pro.PFT_2[i].Index);
                    command_insertPrilFondGeneral.Parameters.AddWithValue("@Wording", pro.PFT_2[i].Formul);
                    command_insertPrilFondGeneral.Parameters.AddWithValue("@Components", pro.PFT_2[i].KomponentsList);
                    command_insertPrilFondGeneral.Parameters.AddWithValue("@WordingTechnology", pro.PFT_2[i].CompetenceTechnology);
                    command_insertPrilFondGeneral.Parameters.AddWithValue("@AssessForm", pro.PFT_2[i].AssessForm);
                    command_insertPrilFondGeneral.Parameters.AddWithValue("@Steps", pro.PFT_2[i].Steps);
                    command_insertPrilFondGeneral.ExecuteNonQuery();
                }
            }

            if (pro.PFT_3 != null)
            {
                string query_InsertPrilFondProfessionally = string.Format("INSERT INTO [PrilFondProfessionally]" +
                  "(ProgramId, Code, Wording, Components, WordingTechnology, AssessForm, Steps)" +
                  "VALUES (@ProgramId, @Code, @Wording, @Components, @WordingTechnology, @AssessForm, @Steps)");
                for (int i = 0; i < pro.PFT_3.Count; i++)
                {
                    SqlCommand command_insertPrilFondProfessionally = new SqlCommand(query_InsertPrilFondProfessionally, connect);
                    command_insertPrilFondProfessionally.Parameters.AddWithValue("@ProgramId", programid);
                    command_insertPrilFondProfessionally.Parameters.AddWithValue("@Code", pro.PFT_3[i].Index);
                    command_insertPrilFondProfessionally.Parameters.AddWithValue("@Wording", pro.PFT_3[i].Formul);
                    command_insertPrilFondProfessionally.Parameters.AddWithValue("@Components", pro.PFT_3[i].KomponentsList);
                    command_insertPrilFondProfessionally.Parameters.AddWithValue("@WordingTechnology", pro.PFT_3[i].CompetenceTechnology);
                    command_insertPrilFondProfessionally.Parameters.AddWithValue("@AssessForm", pro.PFT_3[i].AssessForm);
                    command_insertPrilFondProfessionally.Parameters.AddWithValue("@Steps", pro.PFT_3[i].Steps);
                    command_insertPrilFondProfessionally.ExecuteNonQuery();
                }
            }

            if (pro.PAT != null)
            {
                string query_InsertPrilAssess = string.Format("INSERT INTO [PrilAssess]" +
                    "(ProgramId, Name, Spec, InFOS)" +
                    "VALUES (@ProgramId, @Name, @Spec, @InFOS)");
                for (int i = 0; i < pro.PAT.Count; i++)
                {
                    SqlCommand command_insertPrilAssess = new SqlCommand(query_InsertPrilAssess, connect);
                    command_insertPrilAssess.Parameters.AddWithValue("@ProgramId", programid);
                    command_insertPrilAssess.Parameters.AddWithValue("@Name", pro.PAT[i].Name);
                    command_insertPrilAssess.Parameters.AddWithValue("@Spec", pro.PAT[i].Spec);
                    command_insertPrilAssess.Parameters.AddWithValue("@InFOS", pro.PAT[i].InFOS);
                    command_insertPrilAssess.ExecuteNonQuery();
                }
            }

            if (pro.PCT_Zachet != null)
            {
                string query_InsertPrilCompetenceZachet = string.Format("INSERT INTO [PrilCompetence_Zachet]" +
                    "(ProgramId, Discription, Rezult, Topics, Zachet, NeZachet)" +
                    "VALUES (@ProgramId, @Discription, @Rezult, @Topics, @Zachet, @NeZachet)");
                for (int i = 0; i < pro.PCT_Zachet.Count; i++)
                {
                    SqlCommand command_insertPCTZ = new SqlCommand(query_InsertPrilCompetenceZachet, connect);
                    command_insertPCTZ.Parameters.AddWithValue("@ProgramId", programid);
                    command_insertPCTZ.Parameters.AddWithValue("@Discription", pro.PCT_Zachet[i].Discription);
                    command_insertPCTZ.Parameters.AddWithValue("@Rezult", pro.PCT_Zachet[i].Rezult);
                    command_insertPCTZ.Parameters.AddWithValue("@Topics", pro.PCT_Zachet[i].Topics);
                    command_insertPCTZ.Parameters.AddWithValue("@Zachet", pro.PCT_Zachet[i].Zachet);
                    command_insertPCTZ.Parameters.AddWithValue("@NeZachet", pro.PCT_Zachet[i].NeZachet);
                    command_insertPCTZ.ExecuteNonQuery();
                }
            }

            if (pro.Questions_Zachet != null)
            {
                string query = string.Format("INSERT INTO [Questions_Zachet]" +
                    "(ProgramId, Questions, Developer, Date)" +
                    "VALUES (@ProgramId, @Questions, @Developer, @Date)");
                SqlCommand command_insertQZ = new SqlCommand(query, connect);
                command_insertQZ.Parameters.AddWithValue("@ProgramId", programid);
                command_insertQZ.Parameters.AddWithValue("@Questions", pro.Questions_Zachet.Questions);
                command_insertQZ.Parameters.AddWithValue("@Developer", pro.Questions_Zachet.Developer);
                command_insertQZ.Parameters.AddWithValue("@Date", pro.Questions_Zachet.Date);
                command_insertQZ.ExecuteNonQuery();
            }

            if (pro.PCT_Exam != null)
            {
                string query = string.Format("INSERT INTO [PrilCompetence_Exam]" +
                    "(ProgramId, Discription, Rezult, Topics, On2, On3, On4, On5)" +
                    "VALUES (@ProgramId, @Discription, @Rezult, @Topics, @On2, @On3, @On4, @On5)");
                for (int i = 0; i < pro.PCT_Exam.Count; i++)
                {
                    SqlCommand command_insertPCTE = new SqlCommand(query, connect);
                    command_insertPCTE.Parameters.AddWithValue("@ProgramId", programid);
                    command_insertPCTE.Parameters.AddWithValue("@Discription", pro.PCT_Exam[i].Discription);
                    command_insertPCTE.Parameters.AddWithValue("@Rezult", pro.PCT_Exam[i].Rezult);
                    command_insertPCTE.Parameters.AddWithValue("@Topics", pro.PCT_Exam[i].Topics);
                    command_insertPCTE.Parameters.AddWithValue("@On2", pro.PCT_Exam[i].On2);
                    command_insertPCTE.Parameters.AddWithValue("@On3", pro.PCT_Exam[i].On3);
                    command_insertPCTE.Parameters.AddWithValue("@On4", pro.PCT_Exam[i].On4);
                    command_insertPCTE.Parameters.AddWithValue("@On5", pro.PCT_Exam[i].On5);
                    command_insertPCTE.ExecuteNonQuery();
                }

            }

            if (pro.Questions_Exam != null)
            {
                string query = string.Format("INSERT INTO [Questions_Exam]" +
                    "(ProgramId, Questions, Developer, Date)" +
                    "VALUES (@ProgramId, @Questions, @Developer, @Date)");
                SqlCommand command_insertEQ = new SqlCommand(query, connect);
                command_insertEQ.Parameters.AddWithValue("@ProgramId", programid);
                command_insertEQ.Parameters.AddWithValue("@Questions", pro.Questions_Exam.Questions);
                command_insertEQ.Parameters.AddWithValue("@Developer", pro.Questions_Exam.Developer);
                command_insertEQ.Parameters.AddWithValue("@Date", pro.Questions_Exam.Date);
                command_insertEQ.ExecuteNonQuery();
            }

            if (pro.PCT_Referat != null)
            {
                string query = string.Format("INSERT INTO [PrilCompetence_Referat]" +
                    "(ProgramId, Discription, Rezult, Topics, On2, On3, On4, On5)" +
                    "VALUES (@ProgramId, @Discription, @Rezult, @Topics, @On2, @On3, @On4, @On5)");
                for (int i = 0; i < pro.PCT_Referat.Count; i++)
                {
                    SqlCommand command_insertPCTR = new SqlCommand(query, connect);
                    command_insertPCTR.Parameters.AddWithValue("@ProgramId", programid);
                    command_insertPCTR.Parameters.AddWithValue("@Discription", pro.PCT_Referat[i].Discription);
                    command_insertPCTR.Parameters.AddWithValue("@Rezult", pro.PCT_Referat[i].Rezult);
                    command_insertPCTR.Parameters.AddWithValue("@Topics", pro.PCT_Referat[i].Topics);
                    command_insertPCTR.Parameters.AddWithValue("@On2", pro.PCT_Referat[i].On2);
                    command_insertPCTR.Parameters.AddWithValue("@On3", pro.PCT_Referat[i].On3);
                    command_insertPCTR.Parameters.AddWithValue("@On4", pro.PCT_Referat[i].On4);
                    command_insertPCTR.Parameters.AddWithValue("@On5", pro.PCT_Referat[i].On5);
                    command_insertPCTR.ExecuteNonQuery();
                }

            }

            if (pro.Questions_Referat != null)
            {
                string query = string.Format("INSERT INTO [Questions_Referat]" +
                    "(ProgramId, Questions, Developer, Date)" +
                    "VALUES (@ProgramId, @Questions, @Developer, @Date)");
                SqlCommand command_insertRQ = new SqlCommand(query, connect);
                command_insertRQ.Parameters.AddWithValue("@ProgramId", programid);
                command_insertRQ.Parameters.AddWithValue("@Questions", pro.Questions_Referat.Questions);
                command_insertRQ.Parameters.AddWithValue("@Developer", pro.Questions_Referat.Developer);
                command_insertRQ.Parameters.AddWithValue("@Date", pro.Questions_Referat.Date);
                command_insertRQ.ExecuteNonQuery();
            }

            if (pro.PCT_KT != null)
            {
                string query = string.Format("INSERT INTO [PrilCompetence_KT]" +
                    "(ProgramId, Discription, Rezult, Topics, On2, On3, On4, On5)" +
                    "VALUES (@ProgramId, @Discription, @Rezult, @Topics, @On2, @On3, @On4, @On5)");
                for (int i = 0; i < pro.PCT_KT.Count; i++)
                {
                    SqlCommand command_insertPCTKT = new SqlCommand(query, connect);
                    command_insertPCTKT.Parameters.AddWithValue("@ProgramId", programid);
                    command_insertPCTKT.Parameters.AddWithValue("@Discription", pro.PCT_KT[i].Discription);
                    command_insertPCTKT.Parameters.AddWithValue("@Rezult", pro.PCT_KT[i].Rezult);
                    command_insertPCTKT.Parameters.AddWithValue("@Topics", pro.PCT_KT[i].Topics);
                    command_insertPCTKT.Parameters.AddWithValue("@On2", pro.PCT_KT[i].On2);
                    command_insertPCTKT.Parameters.AddWithValue("@On3", pro.PCT_KT[i].On3);
                    command_insertPCTKT.Parameters.AddWithValue("@On4", pro.PCT_KT[i].On4);
                    command_insertPCTKT.Parameters.AddWithValue("@On5", pro.PCT_KT[i].On5);
                    command_insertPCTKT.ExecuteNonQuery();
                }

            }

            if (pro.Questions_KT != null)
            {
                string query = string.Format("INSERT INTO [Questions_KT]" +
                    "(ProgramId, Questions, Developer, Date)" +
                    "VALUES (@ProgramId, @Questions, @Developer, @Date)");
                SqlCommand command_insertKtQ = new SqlCommand(query, connect);
                command_insertKtQ.Parameters.AddWithValue("@ProgramId", programid);
                command_insertKtQ.Parameters.AddWithValue("@Questions", pro.Questions_KT.Questions);
                command_insertKtQ.Parameters.AddWithValue("@Developer", pro.Questions_KT.Developer);
                command_insertKtQ.Parameters.AddWithValue("@Date", pro.Questions_KT.Date);
                command_insertKtQ.ExecuteNonQuery();
            }

            if (pro.PCT_Praktika != null)
            {
                string query = string.Format("INSERT INTO [PrilCompetence_Praktika]" +
                    "(ProgramId, Discription, Rezult, Topics, On2, On3, On4, On5)" +
                    "VALUES (@ProgramId, @Discription, @Rezult, @Topics, @On2, @On3, @On4, @On5)");
                for (int i = 0; i < pro.PCT_Praktika.Count; i++)
                {
                    SqlCommand command_insertPCTPraktika = new SqlCommand(query, connect);
                    command_insertPCTPraktika.Parameters.AddWithValue("@ProgramId", programid);
                    command_insertPCTPraktika.Parameters.AddWithValue("@Discription", pro.PCT_Praktika[i].Discription);
                    command_insertPCTPraktika.Parameters.AddWithValue("@Rezult", pro.PCT_Praktika[i].Rezult);
                    command_insertPCTPraktika.Parameters.AddWithValue("@Topics", pro.PCT_Praktika[i].Topics);
                    command_insertPCTPraktika.Parameters.AddWithValue("@On2", pro.PCT_Praktika[i].On2);
                    command_insertPCTPraktika.Parameters.AddWithValue("@On3", pro.PCT_Praktika[i].On3);
                    command_insertPCTPraktika.Parameters.AddWithValue("@On4", pro.PCT_Praktika[i].On4);
                    command_insertPCTPraktika.Parameters.AddWithValue("@On5", pro.PCT_Praktika[i].On5);
                    command_insertPCTPraktika.ExecuteNonQuery();
                }

            }

            if (pro.Questions_Praktika != null)
            {
                string query = string.Format("INSERT INTO [Questions_Praktika]" +
                    "(ProgramId, Questions, Developer, Date)" +
                    "VALUES (@ProgramId, @Questions, @Developer, @Date)");
                SqlCommand command_insertPrQ = new SqlCommand(query, connect);
                command_insertPrQ.Parameters.AddWithValue("@ProgramId", programid);
                command_insertPrQ.Parameters.AddWithValue("@Questions", pro.Questions_Praktika.Questions);
                command_insertPrQ.Parameters.AddWithValue("@Developer", pro.Questions_Praktika.Developer);
                command_insertPrQ.Parameters.AddWithValue("@Date", pro.Questions_Praktika.Date);
                command_insertPrQ.ExecuteNonQuery();
            }

            if (pro.PCT_Test != null)
            {
                string query = string.Format("INSERT INTO [PrilCompetence_Test]" +
                    "(ProgramId, Discription, Rezult, Topics, On2, On3, On4, On5)" +
                    "VALUES (@ProgramId, @Discription, @Rezult, @Topics, @On2, @On3, @On4, @On5)");
                for (int i = 0; i < pro.PCT_Test.Count; i++)
                {
                    SqlCommand command_insertPCTTest = new SqlCommand(query, connect);
                    command_insertPCTTest.Parameters.AddWithValue("@ProgramId", programid);
                    command_insertPCTTest.Parameters.AddWithValue("@Discription", pro.PCT_Test[i].Discription);
                    command_insertPCTTest.Parameters.AddWithValue("@Rezult", pro.PCT_Test[i].Rezult);
                    command_insertPCTTest.Parameters.AddWithValue("@Topics", pro.PCT_Test[i].Topics);
                    command_insertPCTTest.Parameters.AddWithValue("@On2", pro.PCT_Test[i].On2);
                    command_insertPCTTest.Parameters.AddWithValue("@On3", pro.PCT_Test[i].On3);
                    command_insertPCTTest.Parameters.AddWithValue("@On4", pro.PCT_Test[i].On4);
                    command_insertPCTTest.Parameters.AddWithValue("@On5", pro.PCT_Test[i].On5);
                    command_insertPCTTest.ExecuteNonQuery();
                }

            }

            if (pro.Questions_Test != null)
            {
                string query = string.Format("INSERT INTO [Questions_Test]" +
                    "(ProgramId, Questions, Developer, Date)" +
                    "VALUES (@ProgramId, @Questions, @Developer, @Date)");
                SqlCommand command_insertTQ = new SqlCommand(query, connect);
                command_insertTQ.Parameters.AddWithValue("@ProgramId", programid);
                command_insertTQ.Parameters.AddWithValue("@Questions", pro.Questions_Test.Questions);
                command_insertTQ.Parameters.AddWithValue("@Developer", pro.Questions_Test.Developer);
                command_insertTQ.Parameters.AddWithValue("@Date", pro.Questions_Test.Date);
                command_insertTQ.ExecuteNonQuery();
            }

            if (pro.PCT_Curs != null)
            {
                string query = string.Format("INSERT INTO [PrilCompetence_Curs]" +
                    "(ProgramId, Discription, Rezult, Topics, On2, On3, On4, On5)" +
                    "VALUES (@ProgramId, @Discription, @Rezult, @Topics, @On2, @On3, @On4, @On5)");
                for (int i = 0; i < pro.PCT_Curs.Count; i++)
                {
                    SqlCommand command_insertPCTCurs = new SqlCommand(query, connect);
                    command_insertPCTCurs.Parameters.AddWithValue("@ProgramId", programid);
                    command_insertPCTCurs.Parameters.AddWithValue("@Discription", pro.PCT_Curs[i].Discription);
                    command_insertPCTCurs.Parameters.AddWithValue("@Rezult", pro.PCT_Curs[i].Rezult);
                    command_insertPCTCurs.Parameters.AddWithValue("@Topics", pro.PCT_Curs[i].Topics);
                    command_insertPCTCurs.Parameters.AddWithValue("@On2", pro.PCT_Curs[i].On2);
                    command_insertPCTCurs.Parameters.AddWithValue("@On3", pro.PCT_Curs[i].On3);
                    command_insertPCTCurs.Parameters.AddWithValue("@On4", pro.PCT_Curs[i].On4);
                    command_insertPCTCurs.Parameters.AddWithValue("@On5", pro.PCT_Curs[i].On5);
                    command_insertPCTCurs.ExecuteNonQuery();
                }
            }

            if (pro.Questions_Curs != null)
            {
                string query = string.Format("INSERT INTO [Questions_Curs]" +
                    "(ProgramId, Questions, Developer, Date)" +
                    "VALUES (@ProgramId, @Questions, @Developer, @Date)");
                SqlCommand command_insertCQ = new SqlCommand(query, connect);
                command_insertCQ.Parameters.AddWithValue("@ProgramId", programid);
                command_insertCQ.Parameters.AddWithValue("@Questions", pro.Questions_Curs.Questions);
                command_insertCQ.Parameters.AddWithValue("@Developer", pro.Questions_Curs.Developer);
                command_insertCQ.Parameters.AddWithValue("@Date", pro.Questions_Curs.Date);
                command_insertCQ.ExecuteNonQuery();
            }

            connect.Close();
            return programid;
        }

        public void ApproveProgram(int programid)
        {
            string query = string.Format("UPDATE [Programs] SET Status='Утверждена' WHERE id=" + programid);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();
            connect.Close();
        }

        public void ProgramCopy(List<int> IDes, string userid)
        {
            for (int i = 0; i < IDes.Count; i++)
            {
                Pro pro = GetProgram(IDes[i]);
                pro.Approve_Date = DateTime.Today;
                pro.DraftDate = DateTime.Today;
                pro.Status = "Ожидает утверждения";
                int newId = CreateNewProgram(pro);
                SetHistory(userid, newId, "Скопирована с программы №" + pro.Id.ToString());
            }
        }

        public void RecoverProgram(int programid)
        {
            string query = string.Format("UPDATE [Programs] SET Status='Ожидает утверждения' WHERE id=" + programid);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();
            connect.Close();
        }

        public Pro GetProgram(int programid)
        {
            Pro program = new Pro();
            string query;
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();

            query = string.Format("SELECT * FROM [Programs] WHERE id = " + programid.ToString());

            var cells = GetData(query, connect).Tables[0].Rows[0].ItemArray;

            program.Id = (int)cells[0];
            program.Institute = cells[1].ToString();
            program.Approver_Post = cells[2].ToString();
            program.Approver_Name = cells[3].ToString();
            program.Approver_Id = cells[4].ToString();
            program.Approve_Date = Convert.ToDateTime(cells[5]);
            program.Discipline = cells[6].ToString();
            program.Training_Direction = cells[7].ToString();
            program.Profil = cells[8].ToString();
            program.Graduate_Qualification = cells[9].ToString();
            program.Training_Form = cells[10].ToString();
            program.StudPeriod = cells[11].ToString();
            program.City = cells[12].ToString();

            program.ObjectivesOfDiscipline = cells[13].ToString();
            program.DisciplineInOOP = cells[14].ToString();
            program.PlanDescription = cells[15].ToString();
            program.DisciplineStructureContent = cells[16].ToString();
            program.SemesteresContent = cells[17].ToString();
            program.EducationalTechnology = cells[18].ToString();
            program.MonitoringTools_Description = cells[19].ToString();
            program.ZachetDescription = cells[20].ToString();

            ZachetTableRow zachetTable = new ZachetTableRow();
            zachetTable.ZachetDone = cells[21].ToString();
            zachetTable.ZachetUnDone = cells[22].ToString();

            program.ZachetTable = zachetTable;

            program.ExamDescription = cells[23].ToString();
            ExamTableRow examTable = new ExamTableRow();
            examTable.AttestatGreat = cells[24].ToString();
            examTable.AttestatGood = cells[25].ToString();
            examTable.AttestatSatisfactorily = cells[26].ToString();
            examTable.AttestatNotSatisfactorily = cells[27].ToString();

            program.ExamTable = examTable;

            program.BasicLiterature = cells[28].ToString();
            program.AdditionalLiterature = cells[29].ToString();
            program.ITResources = cells[30].ToString();
            program.MaterialTechnicalSupport = cells[31].ToString();

            program.StudentRecommendations = cells[32].ToString();
            program.TeacherRecommendations = cells[33].ToString();
            program.DrafterStatus = cells[34].ToString();
            program.DrafterName = cells[35].ToString();
            program.DrafterId = cells[36].ToString();
            program.Department = cells[37].ToString();
            program.DraftDate = Convert.ToDateTime(cells[38]);
            program.Protocol = cells[39].ToString();
            program.DepartmentHeadStatus = cells[40].ToString();
            program.DepartmentHeadName = cells[41].ToString();
            program.DepartmentHeadId = cells[42].ToString();
            program.AgreementDepartmentHeadStatus = cells[43].ToString();
            program.AgreementDepartmentHeadName = cells[44].ToString();
            program.AgreementDepartmentHeadId = cells[45].ToString();
            program.AgreementDirectorStatus = cells[46].ToString();
            program.AgreementDirectorName = cells[47].ToString();
            program.AgreementDirectorId = cells[48].ToString();

            query = string.Format("SELECT * FROM [AdditionalDrafters] WHERE ProgramId = " + programid);

            List<AdditionalDrafter> drafters = new List<AdditionalDrafter>();
            foreach (DataRow row in GetData(query, connect).Tables[0].Rows)
            {
                AdditionalDrafter drafter = new AdditionalDrafter();
                drafter.Status = row["AcademStatus"].ToString();
                drafter.UserId = row["UserId"].ToString();
                drafters.Add(drafter);
            }
            program.AdditionalDrafters = drafters;

            query = string.Format("SELECT * FROM [Plan] WHERE ProgramId = " + programid.ToString());

            List<PlanTableRow> plan = new List<PlanTableRow>();
            foreach (DataRow row in GetData(query, connect).Tables[0].Rows)
            {
                PlanTableRow planrow = new PlanTableRow();
                var cells_1 = row.ItemArray;
                planrow.Сompetence = cells_1[2].ToString();
                planrow.Ability = cells_1[3].ToString();
                planrow.Enumeration = cells_1[4].ToString();
                plan.Add(planrow);
            }
            program.PlanTable = plan;

            query = string.Format("SELECT * FROM [Fond] WHERE ProgramId = " + programid.ToString());

            List<FondTableRow> fond = new List<FondTableRow>();
            foreach (DataRow row in GetData(query, connect).Tables[0].Rows)
            {
                FondTableRow fondrow = new FondTableRow();
                var cells_2 = row.ItemArray;
                fondrow.Сompetence = cells_2[2].ToString();
                fondrow.Ability = cells_2[3].ToString();
                fond.Add(fondrow);
            }
            program.FondTable = fond;

            query = string.Format("SELECT * FROM [Criterions] WHERE ProgramId = " + programid);

            List<CriterionsTableRow> criterions = new List<CriterionsTableRow>();
            foreach (DataRow row in GetData(query, connect).Tables[0].Rows)
            {
                CriterionsTableRow criterionrow = new CriterionsTableRow();
                List<string> toknow = new List<string>();
                List<string> todo = new List<string>();
                List<string> tomaster = new List<string>();
                var cells_3 = row.ItemArray;
                criterionrow.Competence = cells_3[2].ToString();
                criterionrow.ToKnow_Description = cells_3[3].ToString();
                toknow.Add(cells_3[4].ToString());
                toknow.Add(cells_3[5].ToString());
                toknow.Add(cells_3[6].ToString());
                toknow.Add(cells_3[7].ToString());
                criterionrow.ToKnow = toknow;
                criterionrow.ToDo_Description = cells_3[8].ToString();
                todo.Add(cells_3[9].ToString());
                todo.Add(cells_3[10].ToString());
                todo.Add(cells_3[11].ToString());
                todo.Add(cells_3[12].ToString());
                criterionrow.ToDo = todo;
                criterionrow.ToMaster_Description = cells_3[13].ToString();
                tomaster.Add(cells_3[14].ToString());
                tomaster.Add(cells_3[15].ToString());
                tomaster.Add(cells_3[16].ToString());
                tomaster.Add(cells_3[17].ToString());
                criterionrow.ToMaster = tomaster;
                criterions.Add(criterionrow);
            }
            program.CriterionsTable = criterions;

            query = string.Format("SELECT * FROM [DisciplineStructurs] WHERE ProgramId = " + programid);

            List<DSTTableRow> dstlist = new List<DSTTableRow>();
            foreach (DataRow row in GetData(query, connect).Tables[0].Rows)
            {

                DSTTableRow dstrow = new DSTTableRow();
                var cells_4 = row.ItemArray;
                dstrow.Topic = cells_4[2].ToString();
                dstrow.Semestr = (int)cells_4[3];
                dstrow.Week = (int)cells_4[4];
                dstrow.L = (int)cells_4[5];
                dstrow.PS = (int)cells_4[6];
                dstrow.Lab = (int)cells_4[7];
                dstrow.SRS = (int)cells_4[8];
                dstrow.KSR = (int)cells_4[9];
                dstrow.KR = (int)cells_4[10];
                dstrow.KP = (int)cells_4[11];
                dstrow.RGR = (int)cells_4[12];
                dstrow.PrepToLR = cells_4[13].ToString();
                dstrow.KRend = (int)cells_4[14];
                dstrow.AttestatForm = cells_4[15].ToString();
                dstlist.Add(dstrow);
            }
            program.DSTTable = dstlist;

            query = string.Format("SELECT * FROM [PrilFondUniversal] WHERE ProgramId = " + programid.ToString());

            List<PrilFondTableRow> pft_1 = new List<PrilFondTableRow>();
            foreach (DataRow row in GetData(query, connect).Tables[0].Rows)
            {
                PrilFondTableRow prilrow = new PrilFondTableRow();
                var cells_5 = row.ItemArray;
                prilrow.Index = cells_5[2].ToString();
                prilrow.Formul = cells_5[3].ToString();
                prilrow.KomponentsList = cells_5[4].ToString();
                prilrow.CompetenceTechnology = cells_5[5].ToString();
                prilrow.AssessForm = cells_5[6].ToString();
                prilrow.Steps = cells_5[7].ToString();
                pft_1.Add(prilrow);
            }
            program.PFT_1 = pft_1;

            query = string.Format("SELECT * FROM [PrilFondGeneral] WHERE ProgramId = " + programid.ToString());

            List<PrilFondTableRow> pft_2 = new List<PrilFondTableRow>();
            foreach (DataRow row in GetData(query, connect).Tables[0].Rows)
            {
                PrilFondTableRow prilrow = new PrilFondTableRow();
                var cells_6 = row.ItemArray;
                prilrow.Index = cells_6[2].ToString();
                prilrow.Formul = cells_6[3].ToString();
                prilrow.KomponentsList = cells_6[4].ToString();
                prilrow.CompetenceTechnology = cells_6[5].ToString();
                prilrow.AssessForm = cells_6[6].ToString();
                prilrow.Steps = cells_6[7].ToString();
                pft_2.Add(prilrow);
            }
            program.PFT_2 = pft_2;

            query = string.Format("SELECT * FROM [PrilFondProfessionally] WHERE ProgramId = " + programid.ToString());

            List<PrilFondTableRow> pft_3 = new List<PrilFondTableRow>();
            foreach (DataRow row in GetData(query, connect).Tables[0].Rows)
            {
                PrilFondTableRow prilrow = new PrilFondTableRow();
                var cells_7 = row.ItemArray;
                prilrow.Index = cells_7[2].ToString();
                prilrow.Formul = cells_7[3].ToString();
                prilrow.KomponentsList = cells_7[4].ToString();
                prilrow.CompetenceTechnology = cells_7[5].ToString();
                prilrow.AssessForm = cells_7[6].ToString();
                prilrow.Steps = cells_7[7].ToString();
                pft_3.Add(prilrow);
            }
            program.PFT_3 = pft_3;


            query = string.Format("SELECT * FROM [PrilAssess] WHERE ProgramId = " + programid);

            List<PrilAssessTableRow> pat = new List<PrilAssessTableRow>();
            foreach (DataRow row in GetData(query, connect).Tables[0].Rows)
            {
                PrilAssessTableRow prilrow = new PrilAssessTableRow();
                var cells_8 = row.ItemArray;
                prilrow.Name = cells_8[2].ToString();
                prilrow.Spec = cells_8[3].ToString();
                prilrow.InFOS = cells_8[4].ToString();
                pat.Add(prilrow);
            }
            program.PAT = pat;

            query = string.Format("SELECT * FROM [PrilCompetence_Zachet] WHERE ProgramId = " + programid.ToString());

            List<PCTZachetTableRow> pct_z = new List<PCTZachetTableRow>();
            foreach (DataRow row in GetData(query, connect).Tables[0].Rows)
            {
                PCTZachetTableRow prow = new PCTZachetTableRow();
                var cells_9 = row.ItemArray;
                prow.Discription = cells_9[2].ToString();
                prow.Rezult = cells_9[3].ToString();
                prow.Topics = cells_9[4].ToString();
                prow.Zachet = cells_9[5].ToString();
                prow.NeZachet = cells_9[6].ToString();
                pct_z.Add(prow);
            }
            program.PCT_Zachet = pct_z;

            query = string.Format("SELECT * FROM [PrilCompetence_Exam] WHERE ProgramId = " + programid.ToString());

            List<PCTExamTableRow> pct_e = new List<PCTExamTableRow>();
            foreach (DataRow row in GetData(query, connect).Tables[0].Rows)
            {
                PCTExamTableRow prow = new PCTExamTableRow();
                var cells_11 = row.ItemArray;
                prow.Discription = cells_11[2].ToString();
                prow.Rezult = cells_11[3].ToString();
                prow.Topics = cells_11[4].ToString();
                prow.On2 = cells_11[5].ToString();
                prow.On3 = cells_11[6].ToString();
                prow.On4 = cells_11[7].ToString();
                prow.On5 = cells_11[8].ToString();
                pct_e.Add(prow);
            }
            program.PCT_Exam = pct_e;

            query = string.Format("SELECT * FROM [PrilCompetence_Referat] WHERE ProgramId = " + programid.ToString());

            List<PCTReferatTableRow> pct_r = new List<PCTReferatTableRow>();
            foreach (DataRow row in GetData(query, connect).Tables[0].Rows)
            {
                PCTReferatTableRow prow = new PCTReferatTableRow();
                var cells_12 = row.ItemArray;
                prow.Discription = cells_12[2].ToString();
                prow.Rezult = cells_12[3].ToString();
                prow.Topics = cells_12[4].ToString();
                prow.On2 = cells_12[5].ToString();
                prow.On3 = cells_12[6].ToString();
                prow.On4 = cells_12[7].ToString();
                prow.On5 = cells_12[8].ToString();
                pct_r.Add(prow);
            }
            program.PCT_Referat = pct_r;

            query = string.Format("SELECT * FROM [PrilCompetence_KT] WHERE ProgramId = " + programid.ToString());

            List<PCTKtTableRow> pct_kt = new List<PCTKtTableRow>();
            foreach (DataRow row in GetData(query, connect).Tables[0].Rows)
            {
                PCTKtTableRow prow = new PCTKtTableRow();
                var cells_13 = row.ItemArray;
                prow.Discription = cells_13[2].ToString();
                prow.Rezult = cells_13[3].ToString();
                prow.Topics = cells_13[4].ToString();
                prow.On2 = cells_13[5].ToString();
                prow.On3 = cells_13[6].ToString();
                prow.On4 = cells_13[7].ToString();
                prow.On5 = cells_13[8].ToString();
                pct_kt.Add(prow);
            }
            program.PCT_KT = pct_kt;

            query = string.Format("SELECT * FROM [PrilCompetence_Praktika] WHERE ProgramId = " + programid.ToString());

            List<PCTPraktikaTableRow> pct_p = new List<PCTPraktikaTableRow>();
            foreach (DataRow row in GetData(query, connect).Tables[0].Rows)
            {
                PCTPraktikaTableRow prow = new PCTPraktikaTableRow();
                var cells_14 = row.ItemArray;
                prow.Discription = cells_14[2].ToString();
                prow.Rezult = cells_14[3].ToString();
                prow.Topics = cells_14[4].ToString();
                prow.On2 = cells_14[5].ToString();
                prow.On3 = cells_14[6].ToString();
                prow.On4 = cells_14[7].ToString();
                prow.On5 = cells_14[8].ToString();
                pct_p.Add(prow);
            }
            program.PCT_Praktika = pct_p;

            query = string.Format("SELECT * FROM [PrilCompetence_Test] WHERE ProgramId = " + programid.ToString());

            List<PCTTestTableRow> pct_t = new List<PCTTestTableRow>();
            foreach (DataRow row in GetData(query, connect).Tables[0].Rows)
            {
                PCTTestTableRow prow = new PCTTestTableRow();
                var cells_15 = row.ItemArray;
                prow.Discription = cells_15[2].ToString();
                prow.Rezult = cells_15[3].ToString();
                prow.Topics = cells_15[4].ToString();
                prow.On2 = cells_15[5].ToString();
                prow.On3 = cells_15[6].ToString();
                prow.On4 = cells_15[7].ToString();
                prow.On5 = cells_15[8].ToString();
                pct_t.Add(prow);
            }
            program.PCT_Test = pct_t;


            query = string.Format("SELECT * FROM [PrilCompetence_Curs] WHERE ProgramId = " + programid.ToString());
            List<PCTCursTableRow> pct_c = new List<PCTCursTableRow>();
            foreach (DataRow row in GetData(query, connect).Tables[0].Rows)
            {
                PCTCursTableRow prow = new PCTCursTableRow();
                var cells_15 = row.ItemArray;
                prow.Discription = cells_15[2].ToString();
                prow.Rezult = cells_15[3].ToString();
                prow.Topics = cells_15[4].ToString();
                prow.On2 = cells_15[5].ToString();
                prow.On3 = cells_15[6].ToString();
                prow.On4 = cells_15[7].ToString();
                prow.On5 = cells_15[8].ToString();
                pct_c.Add(prow);
            }
            program.PCT_Curs = pct_c;

            try
            {
                query = string.Format("SELECT * FROM [Questions_Zachet] WHERE ProgramId = " + programid.ToString());
                Questions_Zachet qz = new Questions_Zachet();
                var cells_16 = GetData(query, connect).Tables[0].Rows[0].ItemArray;
                qz.Questions = cells_16[2].ToString();
                qz.Developer = cells_16[3].ToString();
                qz.Date = Convert.ToDateTime(cells_16[4]);
                program.Questions_Zachet = qz;
            }
            catch { }

            try
            {
                query = string.Format("SELECT * FROM [Questions_Exam] WHERE ProgramId = " + programid.ToString());
                Questions_Exam qe = new Questions_Exam();
                var cells_17 = GetData(query, connect).Tables[0].Rows[0].ItemArray;
                qe.Questions = cells_17[2].ToString();
                qe.Developer = cells_17[3].ToString();
                qe.Date = Convert.ToDateTime(cells_17[4]);
                program.Questions_Exam = qe;
            }
            catch { }

            try
            {
                query = string.Format("SELECT * FROM [Questions_Referat] WHERE ProgramId = " + programid.ToString());
                Questions_Referat qr = new Questions_Referat();
                var cells_18 = GetData(query, connect).Tables[0].Rows[0].ItemArray;
                qr.Questions = cells_18[2].ToString();
                qr.Developer = cells_18[3].ToString();
                qr.Date = Convert.ToDateTime(cells_18[4]);
                program.Questions_Referat = qr;
            }
            catch { }

            try
            {
                query = string.Format("SELECT * FROM [Questions_KT] WHERE ProgramId = " + programid.ToString());
                Questions_KT qkt = new Questions_KT();
                var cells_19 = GetData(query, connect).Tables[0].Rows[0].ItemArray;
                qkt.Questions = cells_19[2].ToString();
                qkt.Developer = cells_19[3].ToString();
                qkt.Date = Convert.ToDateTime(cells_19[4]);
                program.Questions_KT = qkt;
            }
            catch { }

            try
            {
                query = string.Format("SELECT * FROM [dbo].[Questions_Praktika] WHERE ProgramId = " + programid.ToString());
                Questions_Praktika qp = new Questions_Praktika();
                var cells_22 = GetData(query, connect).Tables[0].Rows[0].ItemArray;
                qp.Questions = cells_22[2].ToString();
                qp.Developer = cells_22[3].ToString();
                qp.Date = Convert.ToDateTime(cells_22[4]);
                program.Questions_Praktika = qp;
            }
            catch { }

            try
            {
                query = string.Format("SELECT * FROM [Questions_Test] WHERE ProgramId = " + programid.ToString());
                Questions_Test qt = new Questions_Test();
                var cells_23 = GetData(query, connect).Tables[0].Rows[0].ItemArray;
                qt.Questions = cells_23[2].ToString();
                qt.Developer = cells_23[3].ToString();
                qt.Date = Convert.ToDateTime(cells_23[4]);
                program.Questions_Test = qt;
            }
            catch { }

            try
            {
                query = string.Format("SELECT * FROM [Questions_Curs] WHERE ProgramId = " + programid.ToString());
                Questions_Curs qc = new Questions_Curs();
                var cells_24 = GetData(query, connect).Tables[0].Rows[0].ItemArray;
                qc.Questions = cells_24[2].ToString();
                qc.Developer = cells_24[3].ToString();
                qc.Date = Convert.ToDateTime(cells_24[4]);
                program.Questions_Curs = qc;
            }
            catch { }

            connect.Close();

            return program;
        }
        
        public void TransferToBasket(int programid)
        {
            string query = string.Format("UPDATE [Programs] SET Status='Удалена' WHERE id=" + programid);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();
        }
        
        public void ClearBasket(string userid)
        {
            int id = 0;
            string deletequery;
            string selectquery = string.Format("SELECT id FROM [Programs] WHERE Status='Удалена'");
            SqlConnection connect = new SqlConnection(connection_string);

            connect.Open();
            foreach (DataRow row in GetData(selectquery, connect).Tables[0].Rows)
            {
                var cells = row.ItemArray;
                id = Convert.ToInt32(cells[0]);
                deletequery = string.Format("DELETE FROM [Programs] WHERE id=" + id + "; " +
               "DELETE FROM [ADocs] WHERE ProgramId=" + id + "; " +
               "DELETE FROM [Criterions] WHERE ProgramId=" + id + "; " +
               "DELETE FROM [DisciplineStructurs] WHERE ProgramId=" + id + "; " +
               "DELETE FROM [Fond] WHERE ProgramId=" + id + "; " +
               "DELETE FROM [Plan] WHERE ProgramId=" + id + "; " +
               "DELETE FROM [PrilAssess] WHERE ProgramId=" + id + "; " +
               "DELETE FROM [PrilCompetence_Exam] WHERE ProgramId=" + id + "; " +
               "DELETE FROM [PrilCompetence_KT] WHERE ProgramId=" + id + "; " +
               "DELETE FROM [PrilCompetence_Praktika] WHERE ProgramId=" + id + "; " +
               "DELETE FROM [PrilCompetence_Referat] WHERE ProgramId=" + id + "; " +
               "DELETE FROM [PrilCompetence_Test] WHERE ProgramId=" + id + "; " +
               "DELETE FROM [PrilCompetence_Curs] WHERE ProgramId=" + id + "; " +
               "DELETE FROM [PrilCompetence_Zachet] WHERE ProgramId=" + id + "; " +
               "DELETE FROM [PrilFondGeneral] WHERE ProgramId=" + id + "; " +
               "DELETE FROM [PrilFondProfessionally] WHERE ProgramId=" + id + "; " +
               "DELETE FROM [PrilFondUniversal] WHERE ProgramId=" + id + "; " +
               "DELETE FROM [Questions_Exam] WHERE ProgramId=" + id + "; " +
               "DELETE FROM [Questions_KT] WHERE ProgramId=" + id + "; " +
               "DELETE FROM [Questions_Praktika] WHERE ProgramId=" + id + "; " +
               "DELETE FROM [Questions_Referat] WHERE ProgramId=" + id + "; " +
               "DELETE FROM [Questions_Test] WHERE ProgramId=" + id + "; " +
               "DELETE FROM [Questions_Zachet] WHERE ProgramId=" + id + "; " +
               "DELETE FROM [Questions_Curs] WHERE ProgramId=" + id + "; " +
               "DELETE FROM [ProgramsConfirmers] WHERE ProgramId=" + id + "; " +
               "DELETE FROM [AdditionalDrafters] WHERE ProgramId=" + id + "; " +
               "DELETE FROM [ProgramsHistory] WHERE ProgramId=" + id + " AND Task != 'Удалена'");

                SqlCommand command = new SqlCommand(deletequery, connect);
                command.ExecuteNonQuery();

                SetHistory(userid, id, "Удалена");

                string path = HttpContext.Current.Server.MapPath("~/Files/Program_" + id.ToString() + ".docx");
                if (File.Exists(path))
                    File.Delete(path);
            }
            connect.Close();
        }

        #endregion

        #region History
        public void SetHistory(string userid, int programid, string task)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string query_history = string.Format("INSERT INTO [ProgramsHistory] " +
                "(UserId, ProgramId, Task) " +
                "VALUES(@UserId, @ProgramId, @Task)");
            SqlCommand command_history = new SqlCommand(query_history, connect);
            command_history.Parameters.AddWithValue("@UserId", userid);
            command_history.Parameters.AddWithValue("@ProgramId", programid);
            command_history.Parameters.AddWithValue("@Task", task);
            command_history.ExecuteNonQuery();
            connect.Close();
        }

        public string GetFullHistory(int programid)
        {
            string history = "";
            string query = string.Format("SELECT * FROM [ProgramsHistory] WHERE ProgramId = " + programid + " ORDER BY DateTime");
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            foreach (DataRow row in GetData(query, connect).Tables[0].Rows)
            {
                string query_searchUser = string.Format("SELECT UserName FROM [AspNetUsers] " +
                    "WHERE Id = '" + row["UserId"].ToString() + "'");
                SqlCommand command_searchUser = new SqlCommand(query_searchUser, connect);
                string username = command_searchUser.ExecuteScalar().ToString();
                string task = row["Task"].ToString();
                DateTime datetime = Convert.ToDateTime(row["DateTime"]);
                string datetimestr = datetime.ToString("f");
                history += task + ". " + "Пользователь - " + username + ". " + datetimestr + "\n";
            }
            connect.Close();
            return history;
        }

        public string GetHistoryWithPeriodRange(int programid, DateTime period_1, DateTime period_2)
        {
            string history = "";
            string query_toHis = string.Format("SELECT * FROM [ProgramsHistory] WHERE ProgramId = " + programid + " ORDER BY DateTime");
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            foreach (DataRow row in GetData(query_toHis, connect).Tables[0].Rows)
            {
                string query_searchUser = string.Format("SELECT UserName FROM [AspNetUsers] " +
                    "WHERE Id = '" + row["UserId"].ToString() + "'");
                SqlCommand command_searchUser = new SqlCommand(query_searchUser, connect);
                string username = command_searchUser.ExecuteScalar().ToString();
                string task = row["Task"].ToString();
                DateTime datetime = Convert.ToDateTime(row["DateTime"]);
                string datetimestr = datetime.ToString("f");

                if ((datetime.Date >= period_1.Date) && (datetime.Date <= period_2.Date))
                    history += task + ". " + "Пользователь - " + username + ". " + datetimestr + "\n";
            }
            connect.Close();
            return history;
        }
        #endregion
        
        #region TablesUpDate

        public void UpdatePlanTable(Pro pro, string userid)
        {
            if (pro.PlanTable.Count != 0)
            {
                SetZeroConfirmProgram(pro.Id);

                string query = string.Format("DELETE FROM [Plan] WHERE ProgramId = " + pro.Id);
                SqlConnection connect = new SqlConnection(connection_string);
                connect.Open();
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();
                
                string query_InsertPlan = string.Format("Insert Into [Plan] (ProgramId, Competence, Ability, Enumeration) " +
                    "Values(@ProgramId, @Competence, @Ability, @Enumeration)");
                for (int i = 0; i < pro.PlanTable.Count; i++)
                {
                    SqlCommand command_insertPLAN = new SqlCommand(query_InsertPlan, connect);
                    command_insertPLAN.Parameters.AddWithValue("@ProgramId", pro.Id);
                    command_insertPLAN.Parameters.AddWithValue("@Competence", pro.PlanTable[i].Сompetence);
                    command_insertPLAN.Parameters.AddWithValue("@Ability", pro.PlanTable[i].Ability);
                    command_insertPLAN.Parameters.AddWithValue("@Enumeration", pro.PlanTable[i].Enumeration);
                    command_insertPLAN.ExecuteNonQuery();
                }
                SetHistory(userid, pro.Id, "Обновлена таблица о планируемых резултатах обучения");
                connect.Close();
            }

        }

        public void UpDateDSCT(Pro pro, string userid)
        {
            if (pro.DSTTable != null)
            {
                SetZeroConfirmProgram(pro.Id);

                string query = string.Format("DELETE FROM [DisciplineStructurs] WHERE ProgramId = " + pro.Id);
                SqlConnection connect = new SqlConnection(connection_string);
                connect.Open();
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();
                
                string query_insertDS = string.Format("Insert Into DisciplineStructurs " +
                    "(ProgramId, Тема, Семестр, Неделя, Л, ПС, Лаб, СРС, КСР, КР, КП, РГР, Подготовка_к_лр, К_р, Форма_аттестации)" +
                    " Values (@ProgramId, @Тема, @Семестр, @Неделя, @Л, @ПС, @Лаб, @СРС, @КСР, @КР, @КП, @РГР, @Подготовка_к_лр, @К_р, @Форма_аттестации)");
                for (int i = 0; i < pro.DSTTable.Count; i++)
                {
                    SqlCommand command_insertDS = new SqlCommand(query_insertDS, connect);
                    command_insertDS.Parameters.AddWithValue("@ProgramId", pro.Id);
                    command_insertDS.Parameters.AddWithValue("@Тема", pro.DSTTable[i].Topic);
                    command_insertDS.Parameters.AddWithValue("@Семестр", pro.DSTTable[i].Semestr);
                    command_insertDS.Parameters.AddWithValue("@Неделя", pro.DSTTable[i].Week);
                    command_insertDS.Parameters.AddWithValue("@Л", pro.DSTTable[i].L);
                    command_insertDS.Parameters.AddWithValue("@ПС", pro.DSTTable[i].PS);
                    command_insertDS.Parameters.AddWithValue("@Лаб", pro.DSTTable[i].Lab);
                    command_insertDS.Parameters.AddWithValue("@СРС", pro.DSTTable[i].SRS);
                    command_insertDS.Parameters.AddWithValue("@КСР", pro.DSTTable[i].KSR);
                    command_insertDS.Parameters.AddWithValue("@КР", pro.DSTTable[i].KR);
                    command_insertDS.Parameters.AddWithValue("@КП", pro.DSTTable[i].KP);
                    command_insertDS.Parameters.AddWithValue("@РГР", pro.DSTTable[i].RGR);
                    command_insertDS.Parameters.AddWithValue("@Подготовка_к_лр", pro.DSTTable[i].PrepToLR);
                    command_insertDS.Parameters.AddWithValue("@К_р", pro.DSTTable[i].KRend);
                    command_insertDS.Parameters.AddWithValue("@Форма_аттестации", pro.DSTTable[i].AttestatForm);

                    command_insertDS.ExecuteNonQuery();
                }

                SetHistory(userid, pro.Id, "Обновлена таблица структуры дисциплины");
                connect.Close();
            }
        }

        public void UpDatePrilAssessTable(Pro pro, string userid)
        {
            if (pro.PAT != null)
            {
                SetZeroConfirmProgram(pro.Id);

                string query = string.Format("DELETE FROM [PrilAssess] WHERE ProgramId = " + pro.Id);
                SqlConnection connect = new SqlConnection(connection_string);
                connect.Open();
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();
                
                string query_InsertPrilAssess = string.Format("INSERT INTO [PrilAssess]" +
                    "(ProgramId, Name, Spec, InFOS)" +
                    "VALUES (@ProgramId, @Name, @Spec, @InFOS)");
                for (int i = 0; i < pro.PAT.Count; i++)
                {
                    SqlCommand command_insertPrilAssess = new SqlCommand(query_InsertPrilAssess, connect);
                    command_insertPrilAssess.Parameters.AddWithValue("@ProgramId", pro.Id);
                    command_insertPrilAssess.Parameters.AddWithValue("@Name", pro.PAT[i].Name);
                    command_insertPrilAssess.Parameters.AddWithValue("@Spec", pro.PAT[i].Spec);
                    command_insertPrilAssess.Parameters.AddWithValue("@InFOS", pro.PAT[i].InFOS);
                    command_insertPrilAssess.ExecuteNonQuery();
                }
                SetHistory(userid, pro.Id, "Обновлена таблица перечня оценочных средств");
                connect.Close();
            }
        }

        public void UpDateFondTable(Pro pro, string userid)
        {
            if (pro.FondTable.Count != 0)
            {
                SetZeroConfirmProgram(pro.Id);

                string query = string.Format("DELETE FROM [Fond] WHERE ProgramId = " + pro.Id);
                SqlConnection connect = new SqlConnection(connection_string);
                connect.Open();
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();

                string query_InsertFond = string.Format("INSERT INTO [Fond]" +
                   "(ProgramId, Competence, Ability) " +
                   "VALUES (@ProgramId, @Competence, @Ability)");
                for (int i = 0; i < pro.FondTable.Count; i++)
                {
                    SqlCommand command_insertFOND = new SqlCommand(query_InsertFond, connect);
                    command_insertFOND.Parameters.AddWithValue("@ProgramId", pro.Id);
                    command_insertFOND.Parameters.AddWithValue("@Competence", pro.FondTable[i].Сompetence);
                    command_insertFOND.Parameters.AddWithValue("@Ability", pro.FondTable[i].Ability);
                    command_insertFOND.ExecuteNonQuery();
                }
                SetHistory(userid, pro.Id, "Обновлена таблица перечня формируемых компетенций");
                connect.Close();
            }
        }

        public void UpDateCriterionsTable(Pro pro, string userid)
        {
            if (pro.CriterionsTable.Count != 0)
            {
                SetZeroConfirmProgram(pro.Id);

                string query = string.Format("DELETE FROM [Criterions] WHERE ProgramId = " + pro.Id);
                SqlConnection connect = new SqlConnection(connection_string);
                connect.Open();
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();

                string query_InsertCriterions = string.Format("INSERT INTO [Criterions]" +
                   "(ProgramId, Competence, ToKnow, ToKnow_2, ToKnow_3, ToKnow_4, ToKnow_5, ToDo, ToDo_2, ToDo_3, ToDo_4, ToDo_5, ToMaster, ToMaster_2, ToMaster_3, ToMaster_4, ToMaster_5) " +
                   "VALUES (@ProgramId, @Competence, @ToKnow, @ToKnow_2, @ToKnow_3, @ToKnow_4, @ToKnow_5, @ToDo, @ToDo_2, @ToDo_3, @ToDo_4, @ToDo_5, @ToMaster, @ToMaster_2, @ToMaster_3, @ToMaster_4, @ToMaster_5)");
                for (int i = 0; i < pro.CriterionsTable.Count; i++)
                {
                    SqlCommand command_insertCRITERIONS = new SqlCommand(query_InsertCriterions, connect);
                    command_insertCRITERIONS.Parameters.AddWithValue("@ProgramId", pro.Id);
                    command_insertCRITERIONS.Parameters.AddWithValue("@Competence", pro.CriterionsTable[i].Competence);
                    command_insertCRITERIONS.Parameters.AddWithValue("@ToKnow", pro.CriterionsTable[i].ToKnow_Description);
                    command_insertCRITERIONS.Parameters.AddWithValue("@ToKnow_2", pro.CriterionsTable[i].ToKnow[0]);
                    command_insertCRITERIONS.Parameters.AddWithValue("@ToKnow_3", pro.CriterionsTable[i].ToKnow[1]);
                    command_insertCRITERIONS.Parameters.AddWithValue("@ToKnow_4", pro.CriterionsTable[i].ToKnow[2]);
                    command_insertCRITERIONS.Parameters.AddWithValue("@ToKnow_5", pro.CriterionsTable[i].ToKnow[3]);
                    command_insertCRITERIONS.Parameters.AddWithValue("@ToDo", pro.CriterionsTable[i].ToDo_Description);
                    command_insertCRITERIONS.Parameters.AddWithValue("@ToDo_2", pro.CriterionsTable[i].ToDo[0]);
                    command_insertCRITERIONS.Parameters.AddWithValue("@ToDo_3", pro.CriterionsTable[i].ToDo[1]);
                    command_insertCRITERIONS.Parameters.AddWithValue("@ToDo_4", pro.CriterionsTable[i].ToDo[2]);
                    command_insertCRITERIONS.Parameters.AddWithValue("@ToDo_5", pro.CriterionsTable[i].ToDo[3]);
                    command_insertCRITERIONS.Parameters.AddWithValue("@ToMaster", pro.CriterionsTable[i].ToMaster_Description);
                    command_insertCRITERIONS.Parameters.AddWithValue("@ToMaster_2", pro.CriterionsTable[i].ToMaster[0]);
                    command_insertCRITERIONS.Parameters.AddWithValue("@ToMaster_3", pro.CriterionsTable[i].ToMaster[1]);
                    command_insertCRITERIONS.Parameters.AddWithValue("@ToMaster_4", pro.CriterionsTable[i].ToMaster[2]);
                    command_insertCRITERIONS.Parameters.AddWithValue("@ToMaster_5", pro.CriterionsTable[i].ToMaster[3]);
                    command_insertCRITERIONS.ExecuteNonQuery();
                }
                SetHistory(userid, pro.Id, "Обновлена таблица критериев оценивания компетенций");
                connect.Close();
            }
        }

        public void UpDatePFTUniversal(Pro pro, string userid)
        {
            if (pro.PFT_1 != null)
            {
                SetZeroConfirmProgram(pro.Id);

                string query = string.Format("DELETE FROM [PrilFondUniversal] WHERE ProgramId = " + pro.Id);
                SqlConnection connect = new SqlConnection(connection_string);
                connect.Open();
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();

                string query_InsertPrilFondUniversal = string.Format("INSERT INTO [PrilFondUniversal]" +
                    "(ProgramId, Code, Wording, Components, WordingTechnology, AssessForm, Steps)" +
                    "VALUES (@ProgramId, @Code, @Wording, @Components, @WordingTechnology, @AssessForm, @Steps)");
                for (int i = 0; i < pro.PFT_1.Count; i++)
                {
                    SqlCommand command_insertPrilFondUniversal = new SqlCommand(query_InsertPrilFondUniversal, connect);
                    command_insertPrilFondUniversal.Parameters.AddWithValue("@ProgramId", pro.Id);
                    command_insertPrilFondUniversal.Parameters.AddWithValue("@Code", pro.PFT_1[i].Index);
                    command_insertPrilFondUniversal.Parameters.AddWithValue("@Wording", pro.PFT_1[i].Formul);
                    command_insertPrilFondUniversal.Parameters.AddWithValue("@Components", pro.PFT_1[i].KomponentsList);
                    command_insertPrilFondUniversal.Parameters.AddWithValue("@WordingTechnology", pro.PFT_1[i].CompetenceTechnology);
                    command_insertPrilFondUniversal.Parameters.AddWithValue("@AssessForm", pro.PFT_1[i].AssessForm);
                    command_insertPrilFondUniversal.Parameters.AddWithValue("@Steps", pro.PFT_1[i].Steps);
                    command_insertPrilFondUniversal.ExecuteNonQuery();
                }
                SetHistory(userid, pro.Id, "Обновлена таблица перечня универсальных компетенций");
                connect.Close();
            }
        }

        public void UpDatePFTGeneral(Pro pro, string userid)
        {
            if (pro.PFT_2 != null)
            {
                SetZeroConfirmProgram(pro.Id);

                string query = string.Format("DELETE FROM [PrilFondGeneral] WHERE ProgramId = " + pro.Id);
                SqlConnection connect = new SqlConnection(connection_string);
                connect.Open();
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();

                string query_InsertPrilFondGeneral = string.Format("INSERT INTO [PrilFondGeneral]" +
                   "(ProgramId, Code, Wording, Components, WordingTechnology, AssessForm, Steps)" +
                   "VALUES (@ProgramId, @Code, @Wording, @Components, @WordingTechnology, @AssessForm, @Steps)");
                for (int i = 0; i < pro.PFT_2.Count; i++)
                {
                    SqlCommand command_insertPrilFondGeneral = new SqlCommand(query_InsertPrilFondGeneral, connect);
                    command_insertPrilFondGeneral.Parameters.AddWithValue("@ProgramId", pro.Id);
                    command_insertPrilFondGeneral.Parameters.AddWithValue("@Code", pro.PFT_2[i].Index);
                    command_insertPrilFondGeneral.Parameters.AddWithValue("@Wording", pro.PFT_2[i].Formul);
                    command_insertPrilFondGeneral.Parameters.AddWithValue("@Components", pro.PFT_2[i].KomponentsList);
                    command_insertPrilFondGeneral.Parameters.AddWithValue("@WordingTechnology", pro.PFT_2[i].CompetenceTechnology);
                    command_insertPrilFondGeneral.Parameters.AddWithValue("@AssessForm", pro.PFT_2[i].AssessForm);
                    command_insertPrilFondGeneral.Parameters.AddWithValue("@Steps", pro.PFT_2[i].Steps);
                    command_insertPrilFondGeneral.ExecuteNonQuery();
                }
                SetHistory(userid, pro.Id, "Обновлена таблица общепрофессиональных компетенций");
                connect.Close();
            }
        }

        public void UpdatePFTProfessional(Pro pro, string userid)
        {
            if (pro.PFT_3 != null)
            {
                SetZeroConfirmProgram(pro.Id);

                string query = string.Format("DELETE FROM [PrilFondProfessionally] WHERE ProgramId = " + pro.Id);
                SqlConnection connect = new SqlConnection(connection_string);
                connect.Open();
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();

                string query_InsertPrilFondProfessionally = string.Format("INSERT INTO [PrilFondProfessionally]" +
                  "(ProgramId, Code, Wording, Components, WordingTechnology, AssessForm, Steps)" +
                  "VALUES (@ProgramId, @Code, @Wording, @Components, @WordingTechnology, @AssessForm, @Steps)");
                for (int i = 0; i < pro.PFT_3.Count; i++)
                {
                    SqlCommand command_insertPrilFondProfessionally = new SqlCommand(query_InsertPrilFondProfessionally, connect);
                    command_insertPrilFondProfessionally.Parameters.AddWithValue("@ProgramId", pro.Id);
                    command_insertPrilFondProfessionally.Parameters.AddWithValue("@Code", pro.PFT_3[i].Index);
                    command_insertPrilFondProfessionally.Parameters.AddWithValue("@Wording", pro.PFT_3[i].Formul);
                    command_insertPrilFondProfessionally.Parameters.AddWithValue("@Components", pro.PFT_3[i].KomponentsList);
                    command_insertPrilFondProfessionally.Parameters.AddWithValue("@WordingTechnology", pro.PFT_3[i].CompetenceTechnology);
                    command_insertPrilFondProfessionally.Parameters.AddWithValue("@AssessForm", pro.PFT_3[i].AssessForm);
                    command_insertPrilFondProfessionally.Parameters.AddWithValue("@Steps", pro.PFT_3[i].Steps);
                    command_insertPrilFondProfessionally.ExecuteNonQuery();
                }
                SetHistory(userid, pro.Id, "Обновлена таблица профессиональных компетенций");
                connect.Close();
            }

        }

        public void UpDatePCTZachet(Pro pro, string userid)
        {
            if (pro.PCT_Zachet != null)
            {
                SetZeroConfirmProgram(pro.Id);

                string query = string.Format("DELETE FROM [PrilCompetence_Zachet] WHERE ProgramId = " + pro.Id);
                SqlConnection connect = new SqlConnection(connection_string);
                connect.Open();
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();

                string query_InsertPrilCompetenceZachet = string.Format("INSERT INTO [PrilCompetence_Zachet]" +
                    "(ProgramId, Discription, Rezult, Topics, Zachet, NeZachet)" +
                    "VALUES (@ProgramId, @Discription, @Rezult, @Topics, @Zachet, @NeZachet)");
                for (int i = 0; i < pro.PCT_Zachet.Count; i++)
                {
                    SqlCommand command_insertPCTZ = new SqlCommand(query_InsertPrilCompetenceZachet, connect);
                    command_insertPCTZ.Parameters.AddWithValue("@ProgramId", pro.Id);
                    command_insertPCTZ.Parameters.AddWithValue("@Discription", pro.PCT_Zachet[i].Discription);
                    command_insertPCTZ.Parameters.AddWithValue("@Rezult", pro.PCT_Zachet[i].Rezult);
                    command_insertPCTZ.Parameters.AddWithValue("@Topics", pro.PCT_Zachet[i].Topics);
                    command_insertPCTZ.Parameters.AddWithValue("@Zachet", pro.PCT_Zachet[i].Zachet);
                    command_insertPCTZ.Parameters.AddWithValue("@NeZachet", pro.PCT_Zachet[i].NeZachet);
                    command_insertPCTZ.ExecuteNonQuery();
                }
                SetHistory(userid, pro.Id, "Обновлена таблица описания элемента контроля 'зачет'");
                connect.Close();
            }
        }

        public void UpDatePCTExam(Pro pro, string userid)
        {
            if (pro.PCT_Exam != null)
            {
                SetZeroConfirmProgram(pro.Id);

                string query = string.Format("DELETE FROM [PrilCompetence_Exam] WHERE ProgramId = " + pro.Id);
                SqlConnection connect = new SqlConnection(connection_string);
                connect.Open();
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();

                string query1 = string.Format("INSERT INTO [PrilCompetence_Exam]" +
                    "(ProgramId, Discription, Rezult, Topics, On2, On3, On4, On5)" +
                    "VALUES (@ProgramId, @Discription, @Rezult, @Topics, @On2, @On3, @On4, @On5)");
                for (int i = 0; i < pro.PCT_Exam.Count; i++)
                {
                    SqlCommand command_insertPCTE = new SqlCommand(query1, connect);
                    command_insertPCTE.Parameters.AddWithValue("@ProgramId", pro.Id);
                    command_insertPCTE.Parameters.AddWithValue("@Discription", pro.PCT_Exam[i].Discription);
                    command_insertPCTE.Parameters.AddWithValue("@Rezult", pro.PCT_Exam[i].Rezult);
                    command_insertPCTE.Parameters.AddWithValue("@Topics", pro.PCT_Exam[i].Topics);
                    command_insertPCTE.Parameters.AddWithValue("@On2", pro.PCT_Exam[i].On2);
                    command_insertPCTE.Parameters.AddWithValue("@On3", pro.PCT_Exam[i].On3);
                    command_insertPCTE.Parameters.AddWithValue("@On4", pro.PCT_Exam[i].On4);
                    command_insertPCTE.Parameters.AddWithValue("@On5", pro.PCT_Exam[i].On5);
                    command_insertPCTE.ExecuteNonQuery();
                }
                SetHistory(userid, pro.Id, "Обновлена таблица описания элемента контроля 'экзамен'");
                connect.Close();
            }
        }

        public void UpDatePCTReferat(Pro pro, string userid)
        {
            if (pro.PCT_Referat != null)
            {
                SetZeroConfirmProgram(pro.Id);

                string query = string.Format("DELETE FROM [PrilCompetence_Referat] WHERE ProgramId = " + pro.Id);
                SqlConnection connect = new SqlConnection(connection_string);
                connect.Open();
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();

                string query1 = string.Format("INSERT INTO [PrilCompetence_Referat]" +
                    "(ProgramId, Discription, Rezult, Topics, On2, On3, On4, On5)" +
                    "VALUES (@ProgramId, @Discription, @Rezult, @Topics, @On2, @On3, @On4, @On5)");
                for (int i = 0; i < pro.PCT_Referat.Count; i++)
                {
                    SqlCommand command_insertPCTR = new SqlCommand(query1, connect);
                    command_insertPCTR.Parameters.AddWithValue("@ProgramId", pro.Id);
                    command_insertPCTR.Parameters.AddWithValue("@Discription", pro.PCT_Referat[i].Discription);
                    command_insertPCTR.Parameters.AddWithValue("@Rezult", pro.PCT_Referat[i].Rezult);
                    command_insertPCTR.Parameters.AddWithValue("@Topics", pro.PCT_Referat[i].Topics);
                    command_insertPCTR.Parameters.AddWithValue("@On2", pro.PCT_Referat[i].On2);
                    command_insertPCTR.Parameters.AddWithValue("@On3", pro.PCT_Referat[i].On3);
                    command_insertPCTR.Parameters.AddWithValue("@On4", pro.PCT_Referat[i].On4);
                    command_insertPCTR.Parameters.AddWithValue("@On5", pro.PCT_Referat[i].On5);
                    command_insertPCTR.ExecuteNonQuery();
                }
                SetHistory(userid, pro.Id, "Обновлена таблица описания элемента контроля 'реферат'");
                connect.Close();
            }
        }

        public void UpDatePCTKT(Pro pro, string userid)
        {
            if (pro.PCT_KT != null)
            {
                SetZeroConfirmProgram(pro.Id);

                string query = string.Format("DELETE FROM [PrilCompetence_KT] WHERE ProgramId = " + pro.Id);
                SqlConnection connect = new SqlConnection(connection_string);
                connect.Open();
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();

                string query1 = string.Format("INSERT INTO [PrilCompetence_KT]" +
                    "(ProgramId, Discription, Rezult, Topics, On2, On3, On4, On5)" +
                    "VALUES (@ProgramId, @Discription, @Rezult, @Topics, @On2, @On3, @On4, @On5)");
                for (int i = 0; i < pro.PCT_KT.Count; i++)
                {
                    SqlCommand command_insertPCTKT = new SqlCommand(query1, connect);
                    command_insertPCTKT.Parameters.AddWithValue("@ProgramId", pro.Id);
                    command_insertPCTKT.Parameters.AddWithValue("@Discription", pro.PCT_KT[i].Discription);
                    command_insertPCTKT.Parameters.AddWithValue("@Rezult", pro.PCT_KT[i].Rezult);
                    command_insertPCTKT.Parameters.AddWithValue("@Topics", pro.PCT_KT[i].Topics);
                    command_insertPCTKT.Parameters.AddWithValue("@On2", pro.PCT_KT[i].On2);
                    command_insertPCTKT.Parameters.AddWithValue("@On3", pro.PCT_KT[i].On3);
                    command_insertPCTKT.Parameters.AddWithValue("@On4", pro.PCT_KT[i].On4);
                    command_insertPCTKT.Parameters.AddWithValue("@On5", pro.PCT_KT[i].On5);
                    command_insertPCTKT.ExecuteNonQuery();
                }
                SetHistory(userid, pro.Id, "Обновлена таблица описания элемента контроля 'контрольная точка'");
                connect.Close();
            }
        }

        public void UpDatePCTPraktika(Pro pro, string userid)
        {
            if (pro.PCT_Praktika != null)
            {
                SetZeroConfirmProgram(pro.Id);

                string query = string.Format("DELETE FROM [PrilCompetence_Praktika] WHERE ProgramId = " + pro.Id);
                SqlConnection connect = new SqlConnection(connection_string);
                connect.Open();
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();

                string query1 = string.Format("INSERT INTO [PrilCompetence_Praktika]" +
                    "(ProgramId, Discription, Rezult, Topics, On2, On3, On4, On5)" +
                    "VALUES (@ProgramId, @Discription, @Rezult, @Topics, @On2, @On3, @On4, @On5)");
                for (int i = 0; i < pro.PCT_Praktika.Count; i++)
                {
                    SqlCommand command_insertPCTPraktika = new SqlCommand(query1, connect);
                    command_insertPCTPraktika.Parameters.AddWithValue("@ProgramId", pro.Id);
                    command_insertPCTPraktika.Parameters.AddWithValue("@Discription", pro.PCT_Praktika[i].Discription);
                    command_insertPCTPraktika.Parameters.AddWithValue("@Rezult", pro.PCT_Praktika[i].Rezult);
                    command_insertPCTPraktika.Parameters.AddWithValue("@Topics", pro.PCT_Praktika[i].Topics);
                    command_insertPCTPraktika.Parameters.AddWithValue("@On2", pro.PCT_Praktika[i].On2);
                    command_insertPCTPraktika.Parameters.AddWithValue("@On3", pro.PCT_Praktika[i].On3);
                    command_insertPCTPraktika.Parameters.AddWithValue("@On4", pro.PCT_Praktika[i].On4);
                    command_insertPCTPraktika.Parameters.AddWithValue("@On5", pro.PCT_Praktika[i].On5);
                    command_insertPCTPraktika.ExecuteNonQuery();
                }
                SetHistory(userid, pro.Id, "Обновлена таблица описания элемента контроля 'практическое занятие'");
                connect.Close();
            }
        }

        public void UpDatePCTTest(Pro pro, string userid)
        {
            if (pro.PCT_Test != null)
            {
                SetZeroConfirmProgram(pro.Id);

                string query = string.Format("DELETE FROM [PrilCompetence_Test] WHERE ProgramId = " + pro.Id);
                SqlConnection connect = new SqlConnection(connection_string);
                connect.Open();
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();

                string query1 = string.Format("INSERT INTO [PrilCompetence_Test]" +
                    "(ProgramId, Discription, Rezult, Topics, On2, On3, On4, On5)" +
                    "VALUES (@ProgramId, @Discription, @Rezult, @Topics, @On2, @On3, @On4, @On5)");
                for (int i = 0; i < pro.PCT_Test.Count; i++)
                {
                    SqlCommand command_insertPCTTest = new SqlCommand(query1, connect);
                    command_insertPCTTest.Parameters.AddWithValue("@ProgramId", pro.Id);
                    command_insertPCTTest.Parameters.AddWithValue("@Discription", pro.PCT_Test[i].Discription);
                    command_insertPCTTest.Parameters.AddWithValue("@Rezult", pro.PCT_Test[i].Rezult);
                    command_insertPCTTest.Parameters.AddWithValue("@Topics", pro.PCT_Test[i].Topics);
                    command_insertPCTTest.Parameters.AddWithValue("@On2", pro.PCT_Test[i].On2);
                    command_insertPCTTest.Parameters.AddWithValue("@On3", pro.PCT_Test[i].On3);
                    command_insertPCTTest.Parameters.AddWithValue("@On4", pro.PCT_Test[i].On4);
                    command_insertPCTTest.Parameters.AddWithValue("@On5", pro.PCT_Test[i].On5);
                    command_insertPCTTest.ExecuteNonQuery();
                }
                SetHistory(userid, pro.Id, "Обновлена таблица описания элемента контроля 'тест'");
                connect.Close();
            }
        }

        public void UpDatePCTCurs(Pro pro, string userid)
        {
            if (pro.PCT_Curs != null)
            {
                SetZeroConfirmProgram(pro.Id);

                string query = string.Format("DELETE FROM [PrilCompetence_Curs] WHERE ProgramId = " + pro.Id);
                SqlConnection connect = new SqlConnection(connection_string);
                connect.Open();
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();

                string query1 = string.Format("INSERT INTO [PrilCompetence_Curs]" +
                    "(ProgramId, Discription, Rezult, Topics, On2, On3, On4, On5)" +
                    "VALUES (@ProgramId, @Discription, @Rezult, @Topics, @On2, @On3, @On4, @On5)");
                for (int i = 0; i < pro.PCT_Curs.Count; i++)
                {
                    SqlCommand command_insertPCTCurs = new SqlCommand(query1, connect);
                    command_insertPCTCurs.Parameters.AddWithValue("@ProgramId", pro.Id);
                    command_insertPCTCurs.Parameters.AddWithValue("@Discription", pro.PCT_Curs[i].Discription);
                    command_insertPCTCurs.Parameters.AddWithValue("@Rezult", pro.PCT_Curs[i].Rezult);
                    command_insertPCTCurs.Parameters.AddWithValue("@Topics", pro.PCT_Curs[i].Topics);
                    command_insertPCTCurs.Parameters.AddWithValue("@On2", pro.PCT_Curs[i].On2);
                    command_insertPCTCurs.Parameters.AddWithValue("@On3", pro.PCT_Curs[i].On3);
                    command_insertPCTCurs.Parameters.AddWithValue("@On4", pro.PCT_Curs[i].On4);
                    command_insertPCTCurs.Parameters.AddWithValue("@On5", pro.PCT_Curs[i].On5);
                    command_insertPCTCurs.ExecuteNonQuery();
                }
                SetHistory(userid, pro.Id, "Обновлена таблица описания элемента контроля 'курсовая работа'");
                connect.Close();
            }
        }

        #endregion TablesUpDate

        #region TableRedactor

        public void CreateRowInPlan(int programid, PlanTableRow row, string userid)
        {
            SetZeroConfirmProgram(programid);

            string query = string.Format("INSERT INTO [Plan] " +
                "(ProgramId, Competence, Ability, Enumeration) " +
                "VALUES (@ProgramId, @Competence, @Ability, @Enumeration)");
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.Parameters.AddWithValue("@ProgramId", programid);
            command.Parameters.AddWithValue("@Competence", row.Сompetence);
            command.Parameters.AddWithValue("@Ability", row.Ability);
            command.Parameters.AddWithValue("@Enumeration", row.Enumeration);
            command.ExecuteNonQuery();

            SetHistory(userid, programid, "Добавлена позиция в таблицу о планируемых результатах обучения");

            connect.Close();
        }

        public void DeleteRowInPlan(int programid, PlanTableRow row, string userid)
        {
            SetZeroConfirmProgram(programid);

            string query = string.Format("DELETE FROM [Plan] WHERE " +
                "ProgramId = " + programid + " AND Competence = '" + row.Сompetence + "' AND Ability = '" + row.Ability + "' AND Enumeration = '" + row.Enumeration + "'");
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();

            SetHistory(userid, programid, "Удалена позиция из таблицы о планируемых результатах обучения");

            connect.Close();
        }

        public void CreateRowInFond(int programid, FondTableRow row, string userid)
        {
            SetZeroConfirmProgram(programid);

            string query = string.Format("INSERT INTO [Fond] " +
                "(ProgramId, Competence, Ability) " +
                "VALUES (@ProgramId, @Competence, @Ability)");
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.Parameters.AddWithValue("@ProgramId", programid);
            command.Parameters.AddWithValue("@Competence", row.Сompetence);
            command.Parameters.AddWithValue("@Ability", row.Ability);
            command.ExecuteNonQuery();
            SetHistory(userid, programid, "Добавлена позиция в таблицу перечня формируемых компетенций");
            connect.Close();
        }

        public void DeleteRowInFond(int programid, FondTableRow row, string userid)
        {
            SetZeroConfirmProgram(programid);

            string query = string.Format("DELETE FROM [Fond] WHERE " +
                "ProgramId = " + programid + " AND Competence = '" + row.Сompetence + "' AND Ability = '" + row.Ability + "'");
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();
            SetHistory(userid, programid, "Удалена позиция из таблицы перечня формируемых компетенций");
            connect.Close();
        }

        public void CreateRowInCriterions(int id, CriterionsTableRow row, string userid)
        {
            SetZeroConfirmProgram(id);

            string query = string.Format("INSERT INTO [Criterions] " +
                "(ProgramId, Competence, ToKnow, ToKnow_2, ToKnow_3, ToKnow_4, ToKnow_5, ToDo, ToDo_2, ToDo_3, ToDo_4, ToDo_5, ToMaster, ToMaster_2, ToMaster_3, ToMaster_4, ToMaster_5) " +
                "VALUES (@ProgramId, @Competence, @ToKnow, @ToKnow_2, @ToKnow_3, @ToKnow_4, @ToKnow_5, @ToDo, @ToDo_2, @ToDo_3, @ToDo_4, @ToDo_5, @ToMaster, @ToMaster_2, @ToMaster_3, @ToMaster_4, @ToMaster_5)");
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.Parameters.AddWithValue("@ProgramId", id);
            command.Parameters.AddWithValue("@Competence", row.Competence);
            command.Parameters.AddWithValue("@ToKnow", row.ToKnow_Description);
            command.Parameters.AddWithValue("@ToKnow_2", row.ToKnow[0]);
            command.Parameters.AddWithValue("@ToKnow_3", row.ToKnow[1]);
            command.Parameters.AddWithValue("@ToKnow_4", row.ToKnow[2]);
            command.Parameters.AddWithValue("@ToKnow_5", row.ToKnow[3]);
            command.Parameters.AddWithValue("@ToDo", row.ToDo_Description);
            command.Parameters.AddWithValue("@ToDo_2", row.ToDo[0]);
            command.Parameters.AddWithValue("@ToDo_3", row.ToDo[1]);
            command.Parameters.AddWithValue("@ToDo_4", row.ToDo[2]);
            command.Parameters.AddWithValue("@ToDo_5", row.ToDo[3]);
            command.Parameters.AddWithValue("@ToMaster", row.ToMaster_Description);
            command.Parameters.AddWithValue("@ToMaster_2", row.ToMaster[0]);
            command.Parameters.AddWithValue("@ToMaster_3", row.ToMaster[1]);
            command.Parameters.AddWithValue("@ToMaster_4", row.ToMaster[2]);
            command.Parameters.AddWithValue("@ToMaster_5", row.ToMaster[3]);
            command.ExecuteNonQuery();
            SetHistory(userid, id, "Добавлена позиция в таблицу критериев оценивания компетенций");
            connect.Close();
        }

        public void DeleteRowInCriterions(int id, CriterionsTableRow row, string userid)
        {
            SetZeroConfirmProgram(id);

            string query = string.Format("DELETE FROM [Criterions] WHERE " +
                "ProgramId = " + id + " AND Competence = '" + row.Competence + "' AND " +
                "ToKnow = '" + row.ToKnow_Description + "' AND ToKnow_2 = '" + row.ToKnow[0] + "' AND " +
                "ToKnow_3 = '" + row.ToKnow[1] + "' AND ToKnow_4 = '" + row.ToKnow[2] + "' AND ToKnow_5 = '" + row.ToKnow[3] + "' AND " +
                "ToDo = '" + row.ToDo_Description + "' AND ToDo_2 = '" + row.ToDo[0] + "' AND " +
                "ToDo_3 = '" + row.ToDo[1] + "' AND ToDo_4 = '" + row.ToDo[2] + "' AND ToDo_5 = '" + row.ToDo[3] + "' AND " +
                "ToMaster = '" + row.ToMaster_Description + "' AND ToMaster_2 = '" + row.ToMaster[0] + "' AND " +
                "ToMaster_3 = '" + row.ToMaster[1] + "' AND ToMaster_4 = '" + row.ToMaster[2] + "' AND ToMaster_5 = '" + row.ToMaster[3] + "'");
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();
            SetHistory(userid, id, "Удалена позиция из таблицы критериев оценивания компетенций");
            connect.Close();
        }

        public void CreateRowInPrilFond(int tableindex, int programid, PrilFondTableRow row, string userid)
        {
            SetZeroConfirmProgram(programid);

            string table = "";
            string forTask = "";
            switch (tableindex)
            {
                case 0:
                    table = "[PrilFondUniversal]";
                    forTask = "универсальных";
                    break;
                case 1:
                    table = "[PrilFondGeneral]";
                    forTask = "общепрофессиональных";
                    break;
                case 2:
                    table = "[PrilFondProfessionally]";
                    forTask = "профессиональных";
                    break;
            }
            string query = string.Format("INSERT INTO " + table + " " +
                "(ProgramId, Code, Wording, Components, WordingTechnology, AssessForm, Steps) " +
                "VALUES(@ProgramId, @Code, @Wording, @Components, @WordingTechnology, @AssessForm, @Steps)");
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.Parameters.AddWithValue("@ProgramId", programid);
            command.Parameters.AddWithValue("@Code", row.Index);
            command.Parameters.AddWithValue("@Wording", row.Formul);
            command.Parameters.AddWithValue("@Components", row.KomponentsList);
            command.Parameters.AddWithValue("@WordingTechnology", row.CompetenceTechnology);
            command.Parameters.AddWithValue("@AssessForm", row.AssessForm);
            command.Parameters.AddWithValue("@Steps", row.Steps);
            command.ExecuteNonQuery();
            SetHistory(userid, programid, "Добавлена позиция в таблицу " + forTask + " компетенций");
            connect.Close();
        }

        public void DeleteRowInPrilFond(int tableindex, int programid, PrilFondTableRow row, string userid)
        {
            SetZeroConfirmProgram(programid);

            string table = "";
            string forTask = "";
            switch (tableindex)
            {
                case 0:
                    table = "[PrilFondUniversal]";
                    forTask = "универсальных";
                    break;
                case 1:
                    table = "[PrilFondGeneral]";
                    forTask = "общепрофессиональных";
                    break;
                case 2:
                    table = "[PrilFondProfessionally]";
                    forTask = "профессиональных";
                    break;
            }
            string query = string.Format("DELETE FROM " + table + " WHERE ProgramId = " + programid + " AND " +
                "Code = '" + row.Index + "' AND Wording = '" + row.Formul + "' AND Components = '" + row.KomponentsList + "' " +
                "AND WordingTechnology = '" + row.CompetenceTechnology + "' AND AssessForm = '" + row.AssessForm + "' " +
                "AND Steps = '" + row.Steps + "'");
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();
            SetHistory(userid, programid, "Удалена позиция из таблицы " + forTask + " компетенций");
            connect.Close();
        }

        public void CreateRowInPrilAssess(int programid, PrilAssessTableRow row, string userid)
        {
            SetZeroConfirmProgram(programid);

            string query = string.Format("INSERT INTO PrilAssess " +
                "(ProgramId, Name, Spec, InFOS) " +
                "VALUES(@ProgramId, @Name, @Spec, @InFOS)");
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.Parameters.AddWithValue("@ProgramId", programid);
            command.Parameters.AddWithValue("@Name", row.Name);
            command.Parameters.AddWithValue("@Spec", row.Spec);
            command.Parameters.AddWithValue("@InFOS", row.InFOS);
            command.ExecuteNonQuery();
            SetHistory(userid, programid, "Добавлена позиция в таблицу перечня оценочных средств");
            connect.Close();
        }

        public void DeleteRowInPrilAssess(int programid, PrilAssessTableRow row, string userid)
        {
            SetZeroConfirmProgram(programid);

            string query = string.Format("DELETE FROM PrilAssess WHERE ProgramId = " + programid + " AND " +
                "Name = '" + row.Name + "' AND Spec = '" + row.Spec + "' AND InFOS = '" + row.InFOS + "'");
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();
            SetHistory(userid, programid, "Удалена позиция из таблицы перечня оценочных средств");
            connect.Close();
        }

        public void CreateRowInPrilCompetence_Zachet(int programid, PCTZachetTableRow row, string userid)
        {
            SetZeroConfirmProgram(programid);

            string query = string.Format("INSERT INTO PrilCompetence_Zachet " +
                "(ProgramId, Discription, Rezult, Topics, Zachet, NeZachet) " +
                "VALUES(@ProgramId, @Discription, @Rezult, @Topics, @Zachet, @NeZachet)");
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.Parameters.AddWithValue("@ProgramId", programid);
            command.Parameters.AddWithValue("@Discription", row.Discription);
            command.Parameters.AddWithValue("@Rezult", row.Rezult);
            command.Parameters.AddWithValue("@Topics", row.Topics);
            command.Parameters.AddWithValue("@Zachet", row.Zachet);
            command.Parameters.AddWithValue("@NeZachet", row.NeZachet);
            command.ExecuteNonQuery();
            SetHistory(userid, programid, "Добавлена позиция в таблицу описания элемента котроля 'зачет'");
            connect.Close();
        }

        public void DeleteRowInPrilCompetence_Zachet(int programid, PCTZachetTableRow row, string userid)
        {
            SetZeroConfirmProgram(programid);

            string query = string.Format("DELETE FROM PrilCompetence_Zachet WHERE ProgramId = " + programid + " AND " +
                "Discription = '" + row.Discription + "' AND Rezult = '" + row.Rezult + "' AND " +
                "Topics = '" + row.Topics + "' AND Zachet = '" + row.Zachet + "' AND NeZachet = '" + row.NeZachet + "'");
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();
            SetHistory(userid, programid, "Удалена позиция из таблицы описания элемента котроля 'зачет'");
            connect.Close();
        }

        public void CreateRowInPrilCompetence(int tableindex, int programid, PCTGeneral row, string userid)
        {
            SetZeroConfirmProgram(programid);

            string table = "";
            string forTask = "";
            switch (tableindex)
            {
                case 1:
                    table = "PrilCompetence_Exam";
                    forTask = "Экзамен";
                    break;
                case 2:
                    table = "PrilCompetence_Referat";
                    forTask = "Реферат";
                    break;
                case 3:
                    table = "PrilCompetence_KT";
                    forTask = "Контрольная точка";
                    break;
                case 4:
                    table = "PrilCompetence_Praktika";
                    forTask = "Практическое занятие";
                    break;
                case 5:
                    table = "PrilCompetence_Test";
                    forTask = "Тест";
                    break;
                case 6:
                    table = "PrilCompetence_Curs";
                    forTask = "Курсовая работа";
                    break;
            }
            string query = string.Format("INSERT INTO " + table + " " +
                "(ProgramId, Discription, Rezult, Topics, On2, On3, On4, On5)" +
                "VALUES(@ProgramId, @Discription, @Rezult, @Topics, @On2, @On3, @On4, @On5)");
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.Parameters.AddWithValue("@ProgramId", programid);
            command.Parameters.AddWithValue("@Discription", row.Discription);
            command.Parameters.AddWithValue("@Rezult", row.Rezult);
            command.Parameters.AddWithValue("@Topics", row.Topics);
            command.Parameters.AddWithValue("@On2", row.On2);
            command.Parameters.AddWithValue("@On3", row.On3);
            command.Parameters.AddWithValue("@On4", row.On4);
            command.Parameters.AddWithValue("@On5", row.On5);
            command.ExecuteNonQuery();
            SetHistory(userid, programid, "Добавлена позиция в таблицу описания элемента контроля '" + forTask + "'");
            connect.Close();
        }

        public void DeleteRowInPrilCompetence(int tableindex, int programid, PCTGeneral row, string userid)
        {
            SetZeroConfirmProgram(programid);

            string table = "";
            string forTask = "";
            switch (tableindex)
            {
                case 1:
                    table = "PrilCompetence_Exam";
                    forTask = "Экзамен";
                    break;
                case 2:
                    table = "PrilCompetence_Referat";
                    forTask = "Реферат";
                    break;
                case 3:
                    table = "PrilCompetence_KT";
                    forTask = "Контрольная точка";
                    break;
                case 4:
                    table = "PrilCompetence_Praktika";
                    forTask = "Практическое занятие";
                    break;
                case 5:
                    table = "PrilCompetence_Test";
                    forTask = "Тест";
                    break;
                case 6:
                    table = "PrilCompetence_Curs";
                    forTask = "Курсовая работа";
                    break;
            }
            string query = string.Format("DELETE FROM " + table + " WHERE ProgramId = " + programid + " AND " +
                "Discription = '" + row.Discription + "' AND Rezult = '" + row.Rezult + "' AND " +
                "Topics = '" + row.Topics + "' AND On2 = '" + row.On2 + "' AND On3 = '" + row.On3 + "'" + " AND " +
                "On4 = '" + row.On4 + "'" + " AND On5 = '" + row.On5 + "'");
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();
            SetHistory(userid, programid, "Удалена позиция из таблицы описания элемента контроля '" + forTask + "'");
            connect.Close();
        }
        
        #endregion TableRedactor
        
        #region CellsRedactor

        public void UpDateInstitute(Pro pro, string userid)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string task = "";
            string before = "";
            string after = pro.Institute;
            string query_select = string.Format("SELECT Institute FROM [Programs] WHERE id = " + pro.Id);
            SqlCommand command_s = new SqlCommand(query_select, connect);
            before = command_s.ExecuteScalar().ToString();
            task = "Правка в названии ВУЗа";
            if (after != before)
            {
                string query = string.Format("UPDATE Programs SET " +
                    "Institute = '" + pro.Institute + "' WHERE id = " + pro.Id);
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();
                SetHistory(userid, pro.Id, task);
            }
            connect.Close();
        }

        public void UpDateApprover_Post(Pro pro, string userid)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();

            string task = "";
            string before = "";
            string after = pro.Approver_Post;
            string query_select = string.Format("SELECT Approver_Post FROM [Programs] WHERE id = " + pro.Id);
            SqlCommand command_s = new SqlCommand(query_select, connect);
            before = command_s.ExecuteScalar().ToString();
            task = "Правка в должности утвердителя – '" + before + "' > '" + after + "'";
            if (after != before)
            { 
                string query = string.Format("UPDATE Programs SET " +
                    "Approver_Post = '" + pro.Approver_Post + "' WHERE id = " + pro.Id);
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();
                SetHistory(userid, pro.Id, task);
            }
            connect.Close();
        }

        public void UpDateApprover_Name(Pro pro, string userid)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string task = "";
            string before = "";
            string after = pro.Approver_Name;
            string query_select = string.Format("SELECT Approver_Name FROM [Programs] WHERE id = " + pro.Id);
            SqlCommand command_s = new SqlCommand(query_select, connect);
            before = command_s.ExecuteScalar().ToString();
            task = "Правка в имени утвердителя – '" + before + "' > '" + after + "'";
            if (after != before)
            {
                string query = string.Format("UPDATE Programs SET " +
                "Approver_Name = '" + pro.Approver_Name + "', " +
                "Approver_Id = '" + pro.Approver_Id + "' WHERE id = " + pro.Id);
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();
                SetHistory(userid, pro.Id, task);
                UpDateConfirmers(pro);
            }
            connect.Close();
        }
        
        public void UpDateApprove_Date(Pro pro, string userid)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string task = "";
            string before = "";
            string after = pro.Approve_Date.ToString("yyyy-MM-dd");
            string query_select = string.Format("SELECT Approve_Date FROM [Programs] WHERE id = " + pro.Id);
            SqlCommand command_s = new SqlCommand(query_select, connect);
            before = Convert.ToDateTime(command_s.ExecuteScalar()).ToString("yyyy-MM-dd");
            task = "Правка в дате утверждения – '" + before + "' > '" + after + "'";
            if (after != before)
            {
                string query = string.Format("UPDATE Programs SET " +
                "Approve_Date = '" + pro.Approve_Date.ToString("yyyy-MM-dd") + "' WHERE id = " + pro.Id);
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();
                SetHistory(userid, pro.Id, task);
            }
            connect.Close();
        }

        public void UpDateDiscipline(Pro pro, string userid)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string task = "";
            string before = "";
            string after = pro.Discipline;
            string query_select = string.Format("SELECT Discipline FROM [Programs] WHERE id = " + pro.Id);
            SqlCommand command_s = new SqlCommand(query_select, connect);
            before = command_s.ExecuteScalar().ToString();
            task = "Правка в названии дисциплины – '" + before + "' > '" + after + "'";
            if (after != before)
            {
                string query = string.Format("UPDATE Programs SET " +
                "Discipline = '" + pro.Discipline + "' WHERE id = " + pro.Id);
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();
                SetHistory(userid, pro.Id, task);
            }
            connect.Close();
        }

        public void UpDateTraining_Direction(Pro pro, string userid)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string task = "";
            string before = "";
            string after = pro.Training_Direction;
            string query_select = string.Format("SELECT Training_Direction FROM [Programs] WHERE id = " + pro.Id);
            SqlCommand command_s = new SqlCommand(query_select, connect);
            before = command_s.ExecuteScalar().ToString();
            task = "Правка в названии направления – '" + before + "' > '" + after + "'";
            if (after != before)
            {
                string query = string.Format("UPDATE Programs SET " +
                "Training_Direction = '" + pro.Training_Direction + "' WHERE id = " + pro.Id);
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();
                SetHistory(userid, pro.Id, task);
            }
            connect.Close();
        }

        public void UpDateProfil(Pro pro, string userid)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string task = "";
            string before = "";
            string after = pro.Profil;
            string query_select = string.Format("SELECT Profil FROM [Programs] WHERE id = " + pro.Id);
            SqlCommand command_s = new SqlCommand(query_select, connect);
            before = command_s.ExecuteScalar().ToString();
            task = "Правка в названии профиля – '" + before + "' > '" + after + "'";
            if (after != before)
            {
                string query = string.Format("UPDATE Programs SET " +
                "Profil = '" + pro.Profil + "' WHERE id = " + pro.Id);
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();
                SetHistory(userid, pro.Id, task);
            }
            connect.Close();
        }

        public void UpDateGraduate_Qualification(Pro pro, string userid)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string task = "";
            string before = "";
            string after = pro.Graduate_Qualification;
            string query_select = string.Format("SELECT Graduate_Qualification FROM [Programs] WHERE id = " + pro.Id);
            SqlCommand command_s = new SqlCommand(query_select, connect);
            before = command_s.ExecuteScalar().ToString();
            task = "Правка в квалификации выпускника – '" + before + "' > '" + after + "'";
            if (after != before)
            {
                string query = string.Format("UPDATE Programs SET " +
                "Graduate_Qualification = '" + pro.Graduate_Qualification + "' WHERE id = " + pro.Id);
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();
                SetHistory(userid, pro.Id, task);
            }
            connect.Close();
        }

        public void UpDateTraining_Form(Pro pro, string userid)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string task = "";
            string before = "";
            string after = pro.Training_Form;
            string query_select = string.Format("SELECT Training_Form FROM [Programs] WHERE id = " + pro.Id);
            SqlCommand command_s = new SqlCommand(query_select, connect);
            before = command_s.ExecuteScalar().ToString();
            task = "Правка в форме обучения – '" + before + "' > '" + after + "'";
            if (after != before)
            {
                string query = string.Format("UPDATE Programs SET " +
                "Training_Form = '" + pro.Training_Form + "' WHERE id = " + pro.Id);
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();
                SetHistory(userid, pro.Id, task);
            }
            connect.Close();
        }

        public void UpDateStudyPeriod (Pro pro, string userid)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string task = "";
            string before = "";
            string after = pro.StudPeriod;
            string query_select = string.Format("SELECT StudyPeriod FROM [Programs] WHERE id = " + pro.Id);
            SqlCommand command_s = new SqlCommand(query_select, connect);
            before = command_s.ExecuteScalar().ToString();
            task = "Правка в периоде обучения – '" + before + "' > '" + after + "'";
            if (after != before)
            {
                string query = string.Format("UPDATE Programs SET " +
                "StudyPeriod = '" + pro.StudPeriod + "' WHERE id = " + pro.Id);
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();
                SetHistory(userid, pro.Id, task);
            }
            connect.Close();
        }

        public void UpDateCity(Pro pro, string userid)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string task = "";
            string before = "";
            string after = pro.City;
            string query_select = string.Format("SELECT City FROM [Programs] WHERE id = " + pro.Id);
            SqlCommand command_s = new SqlCommand(query_select, connect);
            before = command_s.ExecuteScalar().ToString();
            task = "Правка в названии города – '" + before + "' > '" + after + "'";
            if (after != before)
            {
                string query = string.Format("UPDATE Programs SET " +
                "City = '" + pro.City + "' WHERE id = " + pro.Id);
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();
                SetHistory(userid, pro.Id, task);
            }
            connect.Close();
        }

        public void UpDateObjectivesOfDiscipline(Pro pro, string userid)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string task = "";
            string before = "";
            string after = pro.ObjectivesOfDiscipline;
            string query_select = string.Format("SELECT ObjectivesOfDiscipline FROM [Programs] WHERE id = " + pro.Id);
            SqlCommand command_s = new SqlCommand(query_select, connect);
            before = command_s.ExecuteScalar().ToString();
            task = "Правка в поле о целях дисциплины";
            if (after != before)
            {
                string query = string.Format("UPDATE Programs SET " +
                "ObjectivesOfDiscipline = '" + pro.ObjectivesOfDiscipline + "' WHERE id = " + pro.Id);
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();
                SetHistory(userid, pro.Id, task);
            }
            connect.Close();
        }

        public void UpDateDisciplineInOOP(Pro pro, string userid)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string task = "";
            string before = "";
            string after = pro.DisciplineInOOP;
            string query_select = string.Format("SELECT DisciplineInOOP FROM [Programs] WHERE id = " + pro.Id);
            SqlCommand command_s = new SqlCommand(query_select, connect);
            before = command_s.ExecuteScalar().ToString();
            task = "Правка в поле о месте дисциплины в структуре ООП";
            if (after != before)
            {
                string query = string.Format("UPDATE Programs SET " +
                "DisciplineInOOP = '" + pro.DisciplineInOOP + "' WHERE id = " + pro.Id);
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();
                SetHistory(userid, pro.Id, task);
            }
            connect.Close();
        }

        public void UpDatePlanDescription(Pro pro, string userid)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string task = "";
            string before = "";
            string after = pro.PlanDescription;
            string query_select = string.Format("SELECT PlanDescription FROM [Programs] WHERE id = " + pro.Id);
            SqlCommand command_s = new SqlCommand(query_select, connect);
            before = command_s.ExecuteScalar().ToString();
            task = "Правка в поле о планируемых результатах обучения";
            if (after != before)
            {
                string query = string.Format("UPDATE Programs SET " +
                "PlanDescription = '" + pro.PlanDescription + "' WHERE id = " + pro.Id);
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();
                SetHistory(userid, pro.Id, task);
            }
            connect.Close();
        }

        public void UpDateDisciplineStructureContent(Pro pro, string userid)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string task = "";
            string before = "";
            string after = pro.DisciplineStructureContent;
            string query_select = string.Format("SELECT DisciplineStructureContent FROM [Programs] WHERE id = " + pro.Id);
            SqlCommand command_s = new SqlCommand(query_select, connect);
            before = command_s.ExecuteScalar().ToString();
            task = "Правка в поле о структуре дисциплины";
            if (after != before)
            {
                string query = string.Format("UPDATE Programs SET " +
                "DisciplineStructureContent = '" + pro.DisciplineStructureContent + "' WHERE id = " + pro.Id);
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();
                SetHistory(userid, pro.Id, task);
            }
            connect.Close();
        }

        public void UpDateSemesteresContent(Pro pro, string userid)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string task = "";
            string before = "";
            string after = pro.SemesteresContent;
            string query_select = string.Format("SELECT SemesteresContent FROM [Programs] WHERE id = " + pro.Id);
            SqlCommand command_s = new SqlCommand(query_select, connect);
            before = command_s.ExecuteScalar().ToString();
            task = "Правка в поле о содержании семестров";
            if (after != before)
            {
                string query = string.Format("UPDATE Programs SET " +
                "SemesteresContent = '" + pro.SemesteresContent + "' WHERE id = " + pro.Id);
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();
                SetHistory(userid, pro.Id, task);
            }
            connect.Close();
        }
       
        public void UpDateEducationalTechnology(Pro pro, string userid)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string task = "";
            string before = "";
            string after = pro.EducationalTechnology;
            string query_select = string.Format("SELECT EducationalTechnology FROM [Programs] WHERE id = " + pro.Id);
            SqlCommand command_s = new SqlCommand(query_select, connect);
            before = command_s.ExecuteScalar().ToString();
            task = "Правка в поле об образовательных технологиях";
            if (after != before)
            {
                string query = string.Format("UPDATE Programs SET " +
                "EducationalTechnology = '" + pro.EducationalTechnology + "' WHERE id = " + pro.Id);
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();
                SetHistory(userid, pro.Id, task);
            }
            connect.Close();
        }

        public void UpDateMonitoringTools_Description(Pro pro, string userid)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string task = "";
            string before = "";
            string after = pro.MonitoringTools_Description;
            string query_select = string.Format("SELECT MonitoringTools_Description FROM [Programs] WHERE id = " + pro.Id);
            SqlCommand command_s = new SqlCommand(query_select, connect);
            before = command_s.ExecuteScalar().ToString();
            task = "Правка в поле об оценочных средствах";
            if (after != before)
            {
                string query = string.Format("UPDATE Programs SET " +
                "MonitoringTools_Description = '" + pro.MonitoringTools_Description + "' WHERE id = " + pro.Id);
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();
                SetHistory(userid, pro.Id, task);
            }
            connect.Close();
        }

        public void UpDateZachetDescription(Pro pro, string userid)
        {
            string query = string.Format("UPDATE Programs SET " +
                "ZachetDescription = '" + pro.ZachetDescription + "' WHERE id = " + pro.Id);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();
            SetHistory(userid, pro.Id, "Обновлено описание оценивания зачета");
            connect.Close();
        }

        public void UpDateZachetTableZachetDone(Pro pro)
        {
            string query = string.Format("UPDATE Programs SET " +
                "ZachetDone = '" + pro.ZachetTable.ZachetDone + "' WHERE id = " + pro.Id);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();
            connect.Close();
        }

        public void UpDateZachetTableZachetUnDone(Pro pro)
        {
            string query = string.Format("UPDATE Programs SET " +
                "ZachetUnDone = '" + pro.ZachetTable.ZachetUnDone + "' WHERE id = " + pro.Id);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();
            connect.Close();
        }

        public void UpDateExamDescription(Pro pro, string userid)
        {
            string query = string.Format("UPDATE Programs SET " +
                "ExamDescription = '" + pro.ExamDescription + "' WHERE id = " + pro.Id);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();
            SetHistory(userid, pro.Id, "Обновлено описание оценивания экзамена");
            connect.Close();
        }

        public void UpDateExamTableAttestatGreat(Pro pro)
        {
            string query = string.Format("UPDATE Programs SET " +
                "AttestatGreat = '" + pro.ExamTable.AttestatGreat + "' WHERE id = " + pro.Id);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();
            connect.Close();
        }

        public void UpDateExamTableAttestatGood(Pro pro)
        {
            string query = string.Format("UPDATE Programs SET " +
                "AttestatGood = '" + pro.ExamTable.AttestatGood + "' WHERE id = " + pro.Id);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();
            connect.Close();
        }

        public void UpDateExamTableAttestatSatisfactorily(Pro pro)
        {
            string query = string.Format("UPDATE Programs SET " +
                "AttestatSatisfactorily = '" + pro.ExamTable.AttestatSatisfactorily + "' WHERE id = " + pro.Id);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();
            connect.Close();
        }

        public void UpDateExamTableAttestatNotSatisfactorily(Pro pro)
        {
            string query = string.Format("UPDATE Programs SET " +
                "AttestatNotSatisfactorily = '" + pro.ExamTable.AttestatNotSatisfactorily + "' WHERE id = " + pro.Id);
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            SqlCommand command = new SqlCommand(query, connect);
            command.ExecuteNonQuery();
            connect.Close();
        }

        public void UpDateBasicLiterature(Pro pro, string userid)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string task = "";
            string before = "";
            string after = pro.BasicLiterature;
            string query_select = string.Format("SELECT BasicLiterature FROM [Programs] WHERE id = " + pro.Id);
            SqlCommand command_s = new SqlCommand(query_select, connect);
            before = command_s.ExecuteScalar().ToString();
            task = "Правка в поле об основной литературе";
            if (after != before)
            {
                string query = string.Format("UPDATE Programs SET " +
                "BasicLiterature = '" + pro.BasicLiterature + "' WHERE id = " + pro.Id);
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();
                SetHistory(userid, pro.Id, task);
            }
            connect.Close();
        }

        public void UpDateAdditionalLiterature(Pro pro, string userid)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string task = "";
            string before = "";
            string after = pro.AdditionalLiterature;
            string query_select = string.Format("SELECT AdditionalLiterature FROM [Programs] WHERE id = " + pro.Id);
            SqlCommand command_s = new SqlCommand(query_select, connect);
            before = command_s.ExecuteScalar().ToString();
            task = "Правка в поле о дополнительной литературе";
            if (after != before)
            {
                string query = string.Format("UPDATE Programs SET " +
                "AdditionalLiterature = '" + pro.AdditionalLiterature + "' WHERE id = " + pro.Id);
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();
                SetHistory(userid, pro.Id, task);
            }
            connect.Close();
        }

        public void UpDateITResources(Pro pro, string userid)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string task = "";
            string before = "";
            string after = pro.ITResources;
            string query_select = string.Format("SELECT ITResources FROM [Programs] WHERE id = " + pro.Id);
            SqlCommand command_s = new SqlCommand(query_select, connect);
            before = command_s.ExecuteScalar().ToString();
            task = "Правка в поле об интернет-ресурсах";
            if (after != before)
            {
                string query = string.Format("UPDATE Programs SET " +
                "ITResources = '" + pro.ITResources + "' WHERE id = " + pro.Id);
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();
                SetHistory(userid, pro.Id, task);
            }
            connect.Close();
        }

        public void UpDateMaterialTechnicalSupport(Pro pro, string userid)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string task = "";
            string before = "";
            string after = pro.MaterialTechnicalSupport;
            string query_select = string.Format("SELECT MaterialTechnicalSupport FROM [Programs] WHERE id = " + pro.Id);
            SqlCommand command_s = new SqlCommand(query_select, connect);
            before = command_s.ExecuteScalar().ToString();
            task = "Правка в поле о материально-техническом обеспечении дисциплины";
            if (after != before)
            {
                string query = string.Format("UPDATE Programs SET " +
                "MaterialTechnicalSupport = '" + pro.MaterialTechnicalSupport + "' WHERE id = " + pro.Id);
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();
                SetHistory(userid, pro.Id, task);
            }
            connect.Close();
        }

        public void UpDateStudentRecommendations(Pro pro, string userid)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string task = "";
            string before = "";
            string after = pro.StudentRecommendations;
            string query_select = string.Format("SELECT StudentRecommendations FROM [Programs] WHERE id = " + pro.Id);
            SqlCommand command_s = new SqlCommand(query_select, connect);
            before = command_s.ExecuteScalar().ToString();
            task = "Правка в поле о рекомендациях для студента";
            if (after != before)
            {
                string query = string.Format("UPDATE Programs SET " +
                "StudentRecommendations = '" + pro.StudentRecommendations + "' WHERE id = " + pro.Id);
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();
                SetHistory(userid, pro.Id, task);
            }
            connect.Close();
        }

        public void UpDateTeacherRecommendations(Pro pro, string userid)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string task = "";
            string before = "";
            string after = pro.TeacherRecommendations;
            string query_select = string.Format("SELECT TeacherRecommendations FROM [Programs] WHERE id = " + pro.Id);
            SqlCommand command_s = new SqlCommand(query_select, connect);
            before = command_s.ExecuteScalar().ToString();
            task = "Правка в поле о рекомендациях для преподавателя";
            if (after != before)
            {
                string query = string.Format("UPDATE Programs SET " +
                "TeacherRecommendations = '" + pro.TeacherRecommendations + "' WHERE id = " + pro.Id);
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();
                SetHistory(userid, pro.Id, task);
            }
            connect.Close();
        }

        public void UpDateDrafterStatus(Pro pro, string userid)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string task = "";
            string before = "";
            string after = pro.DrafterStatus;
            string query_select = string.Format("SELECT DrafterStatus FROM [Programs] WHERE id = " + pro.Id);
            SqlCommand command_s = new SqlCommand(query_select, connect);
            before = command_s.ExecuteScalar().ToString();
            task = "Правка в должности разработчика – '" + before + "' > '" + after + "'";
            if (after != before)
            {
                string query = string.Format("UPDATE Programs SET " +
                "DrafterStatus = '" + pro.DrafterStatus + "' WHERE id = " + pro.Id);
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();
                SetHistory(userid, pro.Id, task);
            }
            connect.Close();
        }

        public void UpDateDrafterName(Pro pro, string userid)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string task = "";
            string before = "";
            string after = pro.DrafterName;
            string query_select = string.Format("SELECT DrafterName FROM [Programs] WHERE id = " + pro.Id);
            SqlCommand command_s = new SqlCommand(query_select, connect);
            before = command_s.ExecuteScalar().ToString();
            task = "Правка в имени разработчика – '" + before + "' > '" + after + "'";
            if (after != before)
            {
                string query = string.Format("UPDATE Programs SET " +
                "DrafterName = '" + pro.DrafterName + "', " +
                "DrafterId = '" + pro.DrafterId + "' WHERE id = " + pro.Id);
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();
                UpDateConfirmers(pro);
                SetHistory(userid, pro.Id, task);
            }
            connect.Close();
        }

        public void UpDateDepartment(Pro pro, string userid)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string task = "";
            string before = "";
            string after = pro.Department;
            string query_select = string.Format("SELECT Department FROM [Programs] WHERE id = " + pro.Id);
            SqlCommand command_s = new SqlCommand(query_select, connect);
            before = command_s.ExecuteScalar().ToString();
            task = "Правка в названии кафедры – '" + before + "' > '" + after + "'";
            if (after != before)
            {
                string query = string.Format("UPDATE Programs SET " +
                "Department = '" + pro.Department + "' WHERE id = " + pro.Id);
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();
                SetHistory(userid, pro.Id, task);
            }
            connect.Close();
        }

        public void UpDateDraftDate(Pro pro, string userid)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string task = "";
            string before = "";
            string after = pro.DraftDate.ToString("yyyy-MM-dd");
            string query_select = string.Format("SELECT DraftDate FROM [Programs] WHERE id = " + pro.Id);
            SqlCommand command_s = new SqlCommand(query_select, connect);
            before = Convert.ToDateTime(command_s.ExecuteScalar()).ToString("yyyy-MM-dd");
            task = "Правка в дате подписания – '" + before + "' > '" + after + "'";
            if (after != before)
            {
                string query = string.Format("UPDATE Programs SET " +
                "DraftDate = '" + pro.DraftDate.ToString("yyyy-MM-dd") + "' WHERE id = " + pro.Id);
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();
                SetHistory(userid, pro.Id, task);
            }
            connect.Close();
        }

        public void UpDateProtocol(Pro pro, string userid)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string task = "";
            string before = "";
            string after = pro.Protocol;
            string query_select = string.Format("SELECT Protocol FROM [Programs] WHERE id = " + pro.Id);
            SqlCommand command_s = new SqlCommand(query_select, connect);
            before = command_s.ExecuteScalar().ToString();
            task = "Правка в названии протокола – '" + before + "' > '" + after + "'";
            if (after != before)
            {
                string query = string.Format("UPDATE Programs SET " +
                "Protocol = '" + pro.Protocol + "' WHERE id = " + pro.Id);
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();
                SetHistory(userid, pro.Id, task);
            }
            connect.Close();
        }

        public void UpDateDepartmentHeadStatus(Pro pro, string userid)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string task = "";
            string before = "";
            string after = pro.DepartmentHeadStatus;
            string query_select = string.Format("SELECT DepartmentHeadStatus FROM [Programs] WHERE id = " + pro.Id);
            SqlCommand command_s = new SqlCommand(query_select, connect);
            before = command_s.ExecuteScalar().ToString();
            task = "Правка в статусе заведующего кафедры – '" + before + "' > '" + after + "'";
            if (after != before)
            {
                string query = string.Format("UPDATE Programs SET " +
                "DepartmentHeadStatus = '" + pro.DepartmentHeadStatus + "' WHERE id = " + pro.Id);
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();
                SetHistory(userid, pro.Id, task);
            }
            connect.Close();
        }

        public void UpDateDepartmentHeadName(Pro pro, string userid)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string task = "";
            string before = "";
            string after = pro.DepartmentHeadName;
            string query_select = string.Format("SELECT DepartmentHeadName FROM [Programs] WHERE id = " + pro.Id);
            SqlCommand command_s = new SqlCommand(query_select, connect);
            before = command_s.ExecuteScalar().ToString();
            task = "Правка в имени заведующего кафедры – '" + before + "' > '" + after + "'";
            if (after != before)
            {
                string query = string.Format("UPDATE Programs SET " +
                "DepartmentHeadName = '" + pro.DepartmentHeadName + "', " +
                "DepartmentHeadId = '" + pro.DepartmentHeadId + "' WHERE id = " + pro.Id);
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();
                UpDateConfirmers(pro);
                SetHistory(userid, pro.Id, task);
            }
            connect.Close();
        }

        public void UpDateAgreementDepartmentHeadStatus(Pro pro, string userid)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string task = "";
            string before = "";
            string after = pro.AgreementDepartmentHeadStatus;
            string query_select = string.Format("SELECT AgreementDepartmentHeadStatus FROM [Programs] WHERE id = " + pro.Id);
            SqlCommand command_s = new SqlCommand(query_select, connect);
            before = command_s.ExecuteScalar().ToString();
            task = "Правка в должности подписанта – '" + before + "' > '" + after + "'";
            if (after != before)
            {
                string query = string.Format("UPDATE Programs SET " +
                "AgreementDepartmentHeadStatus = '" + pro.AgreementDepartmentHeadStatus + "' WHERE id = " + pro.Id);
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();
                SetHistory(userid, pro.Id, task);
            }
            connect.Close();
        }

        public void UpDateAgreementDepartmentHeadName(Pro pro, string userid)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string task = "";
            string before = "";
            string after = pro.AgreementDepartmentHeadName;
            string query_select = string.Format("SELECT AgreementDepartmentHeadName FROM [Programs] WHERE id = " + pro.Id);
            SqlCommand command_s = new SqlCommand(query_select, connect);
            before = command_s.ExecuteScalar().ToString();
            task = "Правка в имени подписанта – '" + before + "' > '" + after + "'";
            if (after != before)
            {
                string query = string.Format("UPDATE Programs SET " +
                "AgreementDepartmentHeadName = '" + pro.AgreementDepartmentHeadName + "', " +
                "AgreementDepartmentHeadId = '" + pro.AgreementDepartmentHeadId + "' WHERE id = " + pro.Id);
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();
                UpDateConfirmers(pro);
                SetHistory(userid, pro.Id, task);
            }
            connect.Close();
        }

        public void UpDateAgreementDirectorStatus(Pro pro, string userid)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string task = "";
            string before = "";
            string after = pro.AgreementDirectorStatus;
            string query_select = string.Format("SELECT AgreementDirectorStatus FROM [Programs] WHERE id = " + pro.Id);
            SqlCommand command_s = new SqlCommand(query_select, connect);
            before = command_s.ExecuteScalar().ToString();
            task = "Правка в должности подписанта – '" + before + "' > '" + after + "'";
            if (after != before)
            {
                string query = string.Format("UPDATE Programs SET " +
                "AgreementDirectorStatus = '" + pro.AgreementDirectorStatus + "' WHERE id = " + pro.Id);
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();
                SetHistory(userid, pro.Id, task);
            }
            connect.Close();
        }

        public void UpDateAgreementDirectorName(Pro pro, string userid)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();
            string task = "";
            string before = "";
            string after = pro.AgreementDirectorName;
            string query_select = string.Format("SELECT AgreementDirectorName FROM [Programs] WHERE id = " + pro.Id);
            SqlCommand command_s = new SqlCommand(query_select, connect);
            before = command_s.ExecuteScalar().ToString();
            task = "Правка в имени подписанта – '" + before + "' > '" + after + "'";
            if (after != before)
            {
                string query = string.Format("UPDATE Programs SET " +
                "AgreementDirectorName = '" + pro.AgreementDirectorName + "', " +
                "AgreementDirectorId = '" + pro.AgreementDirectorId + "' WHERE id = " + pro.Id);
                SqlCommand command = new SqlCommand(query, connect);
                command.ExecuteNonQuery();
                UpDateConfirmers(pro);
                SetHistory(userid, pro.Id, task);
            }
            connect.Close();
        }

        private void UpDateConfirmers(Pro pro)
        {
            SqlConnection connect = new SqlConnection(connection_string);
            connect.Open();

            string deleteQuery = string.Format("DELETE FROM ProgramsConfirmers WHERE ProgramId = " + pro.Id);
            SqlCommand deleteCommand = new SqlCommand(deleteQuery, connect);
            deleteCommand.ExecuteNonQuery();

            string selectQuery = string.Format("SELECT Approver_Id, DrafterId, DepartmentHeadId, AgreementDepartmentHeadId," +
                " AgreementDirectorId FROM Programs WHERE id = " + pro.Id);
            var cells = GetData(selectQuery, connect).Tables[0].Rows[0].ItemArray;
            List<string> IDes = new List<string>();
            IDes.Add(cells[0].ToString());
            IDes.Add(cells[1].ToString());
            IDes.Add(cells[2].ToString());
            IDes.Add(cells[3].ToString());
            IDes.Add(cells[4].ToString());

            List<string> UniIDes = new List<string>();
            UniIDes = IDes.Distinct().ToList();

            for (int i = 0; i < UniIDes.Count; i++)
            {
                string query_confI = string.Format("INSERT INTO ProgramsConfirmers " +
                "(UserId, ProgramId, Status) " +
                "VALUES(@UserId, @ProgramId, @Status)");
                SqlCommand commandI = new SqlCommand(query_confI, connect);
                commandI.Parameters.AddWithValue("@UserId", UniIDes[i]);
                commandI.Parameters.AddWithValue("@ProgramId", pro.Id);
                commandI.Parameters.AddWithValue("@Status", 0);
                commandI.ExecuteNonQuery();
            }
            connect.Close();
        }

        #endregion CellsRedacor

        #region BlocksUpDate

        public void UpDateFirstBlock(Pro pro, string userid)
        {
            UpDateInstitute(pro, userid);
            UpDateApprover_Post(pro, userid);
            UpDateApprover_Name(pro, userid);
            UpDateApprove_Date(pro, userid);
            UpDateDiscipline(pro, userid);
            UpDateTraining_Direction(pro, userid);
            UpDateProfil(pro, userid);
            UpDateGraduate_Qualification(pro, userid);
            UpDateTraining_Form(pro, userid);
            UpDateStudyPeriod(pro, userid);
            UpDateCity(pro, userid);
            SetZeroConfirmProgram(pro.Id);
        }

        public void UpDateSecondBlock(Pro pro, string userid)
        {
            UpDateObjectivesOfDiscipline(pro, userid);
            UpDateDisciplineInOOP(pro, userid);
            UpDatePlanDescription(pro, userid);
            SetZeroConfirmProgram(pro.Id);
        }

        public void UpDateThirdBlock(Pro pro, string userid)
        {
            UpDateDisciplineStructureContent(pro, userid);
            UpDateSemesteresContent(pro, userid);
            UpDateEducationalTechnology(pro, userid);
            UpDateMonitoringTools_Description(pro, userid);
            SetZeroConfirmProgram(pro.Id);
        }

        public void UpDateFourthBlock(Pro pro, string userid)
        {
            UpDateZachetDescription(pro, userid);
            UpDateZachetTableZachetDone(pro);
            UpDateZachetTableZachetUnDone(pro);
            SetZeroConfirmProgram(pro.Id);
        }

        public void UpdateFifthBlock(Pro pro, string userid)
        {
            UpDateExamDescription(pro, userid);
            UpDateExamTableAttestatGreat(pro);
            UpDateExamTableAttestatGood(pro);
            UpDateExamTableAttestatSatisfactorily(pro);
            UpDateExamTableAttestatNotSatisfactorily(pro);
            SetZeroConfirmProgram(pro.Id);
        }

        public void UpDateSixthBlock(Pro pro, string userid)
        {
            UpDateBasicLiterature(pro, userid);
            UpDateAdditionalLiterature(pro, userid);
            UpDateITResources(pro, userid);
            UpDateMaterialTechnicalSupport(pro, userid);
            UpDateStudentRecommendations(pro, userid);
            UpDateTeacherRecommendations(pro, userid);
            SetZeroConfirmProgram(pro.Id);
        }

        public void UpDateSeventhBlock(Pro pro, string userid)
        {
            UpDateDrafterStatus(pro, userid);
            UpDateDrafterName(pro, userid);
            UpdateDrafters(pro);
            UpDateDepartment(pro, userid);
            UpDateDraftDate(pro, userid);
            UpDateProtocol(pro, userid);
            UpDateDepartmentHeadStatus(pro, userid);
            UpDateDepartmentHeadName(pro, userid);
            UpDateAgreementDepartmentHeadStatus(pro, userid);
            UpDateAgreementDepartmentHeadName(pro, userid);
            UpDateAgreementDirectorStatus(pro, userid);
            UpDateAgreementDirectorName(pro, userid);
            SetZeroConfirmProgram(pro.Id);
        }

        public void UpDateQuestionsCursesFond(Pro pro, string userid)
        {
            SetZeroConfirmProgram(pro.Id);

            SqlConnection connect = new SqlConnection(connection_string);
            string testquery = string.Format("SELECT COUNT(*) as number FROM [Questions_Curs] WHERE ProgramId=" + pro.Id);
            string updatequery = string.Format("UPDATE [Questions_Curs] SET " +
                "Questions = '" + pro.Questions_Curs.Questions + "', " +
                "Developer = '" + pro.Questions_Curs.Developer + "', " +
                "Date = '" + pro.Questions_Curs.Date.ToString("yyyy-MM-dd") + "' " +
                "WHERE ProgramId = " + pro.Id);
            string insertquery = string.Format("INSERT INTO [Questions_Curs] " +
                "(ProgramId, Questions, Developer, Date) " +
                "VALUES(@ProgramId, @Questions, @Developer, @Date)");
            connect.Open();
            SqlCommand testcommand = new SqlCommand(testquery, connect);
            int count = (int)testcommand.ExecuteScalar();
            if (count != 0)
            {
                SqlCommand command = new SqlCommand(updatequery, connect);
                command.ExecuteNonQuery();
            }
            else
            {
                SqlCommand command = new SqlCommand(insertquery, connect);
                command.Parameters.AddWithValue("@ProgramId", pro.Id);
                command.Parameters.AddWithValue("@Questions", pro.Questions_Curs.Questions);
                command.Parameters.AddWithValue("@Developer", pro.Questions_Curs.Developer);
                command.Parameters.AddWithValue("@Date", pro.Questions_Curs.Date);
                command.ExecuteNonQuery();
            }
            SetHistory(userid, pro.Id, "Обновлен список тем курсовых работ");
            connect.Close();
        }

        public void UpDateQuestionsTestsFond(Pro pro, string userid)
        {

            SetZeroConfirmProgram(pro.Id);

            SqlConnection connect = new SqlConnection(connection_string);
            string testquery = string.Format("SELECT COUNT(*) as number FROM [Questions_Test] WHERE ProgramId=" + pro.Id);
            string updatequery = string.Format("UPDATE [Questions_Test] SET " +
                "Questions = '" + pro.Questions_Test.Questions + "', " +
                "Developer = '" + pro.Questions_Test.Developer + "', " +
                "Date = '" + pro.Questions_Test.Date.ToString("yyyy-MM-dd") + "' " +
                "WHERE ProgramId = " + pro.Id);
            string insertquery = string.Format("INSERT INTO [Questions_Test] " +
                "(ProgramId, Questions, Developer, Date) " +
                "VALUES(@ProgramId, @Questions, @Developer, @Date)");
            connect.Open();
            SqlCommand testcommand = new SqlCommand(testquery, connect);
            int count = (int)testcommand.ExecuteScalar();
            if (count != 0)
            {
                SqlCommand command = new SqlCommand(updatequery, connect);
                command.ExecuteNonQuery();
            }
            else
            {
                SqlCommand command = new SqlCommand(insertquery, connect);
                command.Parameters.AddWithValue("@ProgramId", pro.Id);
                command.Parameters.AddWithValue("@Questions", pro.Questions_Test.Questions);
                command.Parameters.AddWithValue("@Developer", pro.Questions_Test.Developer);
                command.Parameters.AddWithValue("@Date", pro.Questions_Test.Date);
                command.ExecuteNonQuery();
            }
            SetHistory(userid, pro.Id, "Обновлен список тестовых заданий");
            connect.Close();
        }

        public void UpDateQuestionsPraktika(Pro pro, string userid)
        {

            SetZeroConfirmProgram(pro.Id);

            SqlConnection connect = new SqlConnection(connection_string);
            string testquery = string.Format("SELECT COUNT(*) as number FROM [Questions_Praktika] WHERE ProgramId=" + pro.Id);
            string updatequery = string.Format("UPDATE [Questions_Praktika] SET " +
                "Questions = '" + pro.Questions_Praktika.Questions + "', " +
                "Developer = '" + pro.Questions_Praktika.Developer + "', " +
                "Date = '" + pro.Questions_Praktika.Date.ToString("yyyy-MM-dd") + "' " +
                "WHERE ProgramId = " + pro.Id);
            string insertquery = string.Format("INSERT INTO [Questions_Praktika] " +
                "(ProgramId, Questions, Developer, Date) " +
                "VALUES(@ProgramId, @Questions, @Developer, @Date)");
            connect.Open();
            SqlCommand testcommand = new SqlCommand(testquery, connect);
            int count = (int)testcommand.ExecuteScalar();
            if (count != 0)
            {
                SqlCommand command = new SqlCommand(updatequery, connect);
                command.ExecuteNonQuery();
            }
            else
            {
                SqlCommand command = new SqlCommand(insertquery, connect);
                command.Parameters.AddWithValue("@ProgramId", pro.Id);
                command.Parameters.AddWithValue("@Questions", pro.Questions_Praktika.Questions);
                command.Parameters.AddWithValue("@Developer", pro.Questions_Praktika.Developer);
                command.Parameters.AddWithValue("@Date", pro.Questions_Praktika.Date);
                command.ExecuteNonQuery();
            }
            SetHistory(userid, pro.Id, "Обновлен список тем для практических занятий");
            connect.Close();
        }

        public void UpDateQuestionsKT(Pro pro, string userid)
        {

            SetZeroConfirmProgram(pro.Id);

            SqlConnection connect = new SqlConnection(connection_string);
            string testquery = string.Format("SELECT COUNT(*) as number FROM [Questions_KT] WHERE ProgramId=" + pro.Id);
            string updatequery = string.Format("UPDATE [Questions_KT] SET " +
                "Questions = '" + pro.Questions_KT.Questions + "', " +
                "Developer = '" + pro.Questions_KT.Developer + "', " +
                "Date = '" + pro.Questions_KT.Date.ToString("yyyy-MM-dd") + "' " +
                "WHERE ProgramId = " + pro.Id);
            string insertquery = string.Format("INSERT INTO [Questions_KT] " +
                "(ProgramId, Questions, Developer, Date) " +
                "VALUES(@ProgramId, @Questions, @Developer, @Date)");
            connect.Open();
            SqlCommand testcommand = new SqlCommand(testquery, connect);
            int count = (int)testcommand.ExecuteScalar();
            if (count != 0)
            {
                SqlCommand command = new SqlCommand(updatequery, connect);
                command.ExecuteNonQuery();
            }
            else
            {
                SqlCommand command = new SqlCommand(insertquery, connect);
                command.Parameters.AddWithValue("@ProgramId", pro.Id);
                command.Parameters.AddWithValue("@Questions", pro.Questions_KT.Questions);
                command.Parameters.AddWithValue("@Developer", pro.Questions_KT.Developer);
                command.Parameters.AddWithValue("@Date", pro.Questions_KT.Date);
                command.ExecuteNonQuery();
            }
            SetHistory(userid, pro.Id, "Обновлен список вопросов для контрольных точек");
            connect.Close();
        }

        public void UpDateQuestionsReferat(Pro pro, string userid)
        {

            SetZeroConfirmProgram(pro.Id);

            SqlConnection connect = new SqlConnection(connection_string);
            string testquery = string.Format("SELECT COUNT(*) as number FROM [Questions_Referat] WHERE ProgramId=" + pro.Id);
            string updatequery = string.Format("UPDATE [Questions_Referat] SET " +
                "Questions = '" + pro.Questions_Referat.Questions + "', " +
                "Developer = '" + pro.Questions_Referat.Developer + "', " +
                "Date = '" + pro.Questions_Referat.Date.ToString("yyyy-MM-dd") + "' " +
                "WHERE ProgramId = " + pro.Id);
            string insertquery = string.Format("INSERT INTO [Questions_Referat] " +
                "(ProgramId, Questions, Developer, Date) " +
                "VALUES(@ProgramId, @Questions, @Developer, @Date)");
            connect.Open();
            SqlCommand testcommand = new SqlCommand(testquery, connect);
            int count = (int)testcommand.ExecuteScalar();
            if (count != 0)
            {
                SqlCommand command = new SqlCommand(updatequery, connect);
                command.ExecuteNonQuery();
            }
            else
            {
                SqlCommand command = new SqlCommand(insertquery, connect);
                command.Parameters.AddWithValue("@ProgramId", pro.Id);
                command.Parameters.AddWithValue("@Questions", pro.Questions_Referat.Questions);
                command.Parameters.AddWithValue("@Developer", pro.Questions_Referat.Developer);
                command.Parameters.AddWithValue("@Date", pro.Questions_Referat.Date);
                command.ExecuteNonQuery();
            }
            SetHistory(userid, pro.Id, "Обновлен список тем для реферата");
            connect.Close();
        }

        public void UpDateQuestionsZachet(Pro pro, string userid)
        {

            SetZeroConfirmProgram(pro.Id);

            SqlConnection connect = new SqlConnection(connection_string);
            string testquery = string.Format("SELECT COUNT(*) as number FROM [Questions_Zachet] WHERE ProgramId=" + pro.Id);
            string updatequery = string.Format("UPDATE [Questions_Zachet] SET " +
                "Questions = '" + pro.Questions_Zachet.Questions + "', " +
                "Developer = '" + pro.Questions_Zachet.Developer + "', " +
                "Date = '" + pro.Questions_Zachet.Date.ToString("yyyy-MM-dd") + "' " +
                "WHERE ProgramId = " + pro.Id);
            string insertquery = string.Format("INSERT INTO [Questions_Zachet] " +
                "(ProgramId, Questions, Developer, Date) " +
                "VALUES(@ProgramId, @Questions, @Developer, @Date)");
            connect.Open();
            SqlCommand testcommand = new SqlCommand(testquery, connect);
            int count = (int)testcommand.ExecuteScalar();
            if (count != 0)
            {
                SqlCommand command = new SqlCommand(updatequery, connect);
                command.ExecuteNonQuery();
            }
            else
            {
                SqlCommand command = new SqlCommand(insertquery, connect);
                command.Parameters.AddWithValue("@ProgramId", pro.Id);
                command.Parameters.AddWithValue("@Questions", pro.Questions_Zachet.Questions);
                command.Parameters.AddWithValue("@Developer", pro.Questions_Zachet.Developer);
                command.Parameters.AddWithValue("@Date", pro.Questions_Zachet.Date);
                command.ExecuteNonQuery();
            }
            SetHistory(userid, pro.Id, "Обновлен список вопросов для зачета");
            connect.Close();
        }

        public void UpDateQuestionsExam(Pro pro, string userid)
        {

            SetZeroConfirmProgram(pro.Id);

            SqlConnection connect = new SqlConnection(connection_string);
            string testquery = string.Format("SELECT COUNT(*) as number FROM [Questions_Exam] WHERE ProgramId=" + pro.Id);
            string updatequery = string.Format("UPDATE [Questions_Exam] SET " +
                "Questions = '" + pro.Questions_Exam.Questions + "', " +
                "Developer = '" + pro.Questions_Exam.Developer + "', " +
                "Date = '" + pro.Questions_Exam.Date.ToString("yyyy-MM-dd") + "' " +
                "WHERE ProgramId = " + pro.Id);
            string insertquery = string.Format("INSERT INTO [Questions_Exam] " +
                "(ProgramId, Questions, Developer, Date) " +
                "VALUES(@ProgramId, @Questions, @Developer, @Date)");
            connect.Open();
            SqlCommand testcommand = new SqlCommand(testquery, connect);
            int count = (int)testcommand.ExecuteScalar();
            if (count != 0)
            {
                SqlCommand command = new SqlCommand(updatequery, connect);
                command.ExecuteNonQuery();
            }
            else
            {
                SqlCommand command = new SqlCommand(insertquery, connect);
                command.Parameters.AddWithValue("@ProgramId", pro.Id);
                command.Parameters.AddWithValue("@Questions", pro.Questions_Exam.Questions);
                command.Parameters.AddWithValue("@Developer", pro.Questions_Exam.Developer);
                command.Parameters.AddWithValue("@Date", pro.Questions_Exam.Date);
                command.ExecuteNonQuery();
            }
            SetHistory(userid, pro.Id, "Обновлен список вопросов для экзамена");
            connect.Close();
        }

        #endregion BlocksUpDate
        
    }
}
