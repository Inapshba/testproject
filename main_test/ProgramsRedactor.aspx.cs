﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using main_test.TableRows;
using main_test.QuestionsStructur;
using Microsoft.AspNet.Identity;
using System.IO;

namespace main_test
{
    public partial class ProgramsRedactor : System.Web.UI.Page
    {
        private int ProgramId;
        Pro pro = new Pro();

        protected void Page_Load(object sender, EventArgs e)
        {
            DownLoadErrorMes.Visible = false;
            ConnectErrorMes.Visible = false;
            ApproveError.Visible = false;
            ProgramId = Convert.ToInt32(Request.QueryString["ProgramID"]);
            ReplaceNulls(DisciplineStructureTable_GridView);
            
            if (!IsPostBack)
            {
                if (ProgramId != 0)
                {
                    ProgramReport.Text = "Рабочая программа №" + ProgramId.ToString();
                }
                else ProgramReport.Text = "Не найден идентификатор рабочей программы";
                try
                {
                    BaseWork task = new BaseWork();
                    task.TryConnect();
                }
                catch { ConnectErrorMes.Visible = true; }
                try
                {
                    pro = GetProgramFromBase(ProgramId);
                    FillControlls(pro, false);
                    AddDinamycControls(pro);
                }
                catch { DownLoadErrorMes.Visible = true; }
            }
            else
            {
                UpDateDinamycControls();
                try
                {
                    pro = GetProgramFromBase(ProgramId);
                    FillControlls(pro, true);
                }
                catch { DownLoadErrorMes.Visible = true; }
            }
        }

        //включение динамических элементов управления на основе данных из бд
        private void AddDinamycControls(Pro program)
        {
            int drafterscount = program.AdditionalDrafters.Count;
            BaseWork task = new BaseWork();
            if (drafterscount != 0)
                for (int i = 0; i < drafterscount; i++)
                {
                    Panel panel = new Panel();
                    panel.ID = "DraftersPanel_" + i.ToString();
                    panel.Style["margin-top"] = "2px";
                    TextBox newTextBox = new TextBox();
                    newTextBox.CssClass = "form-control";
                    newTextBox.Attributes["placeholder"] = "Академ. статус";
                    newTextBox.Width = 150;
                    newTextBox.ID = "AdditionalDrafterStatus_" + i.ToString();

                    DropDownList newList = new DropDownList();
                    newList.CssClass = "form-control";
                    newList.DataSourceID = "SqlDataSourceUsers";
                    newList.Style["margin-left"] = "3px";
                    newList.DataTextField = "UserName";
                    newList.DataValueField = "UserId";
                    newList.ID = "AdditionalDrafterId_" + i.ToString();
                    

                    panel.Controls.Add(newTextBox);
                    panel.Controls.Add(newList);
                    drafters_html_block.Controls.Add(panel);
                    newTextBox.Text = program.AdditionalDrafters[i].Status;
                    FindItem(newList, task.GetUserName(program.AdditionalDrafters[i].UserId));
                }
            AdditionalDraftersCount.Value = drafterscount.ToString();
        }

        //включение динамических элементов управления на основе созданных
        private void UpDateDinamycControls()
        {
            int drafterscount = Convert.ToInt32(AdditionalDraftersCount.Value);
            if (drafterscount != 0)
                for (int i = 0; i < drafterscount; i++)
                {
                    Panel panel = new Panel();
                    panel.ID = "DraftersPanel_" + i.ToString();
                    panel.Style["margin-top"] = "2px";
                    TextBox newTextBox = new TextBox();
                    newTextBox.CssClass = "form-control";
                    newTextBox.Attributes["placeholder"] = "Академ. статус";
                    newTextBox.Width = 150;
                    newTextBox.ID = "AdditionalDrafterStatus_" + i.ToString();

                    DropDownList newList = new DropDownList();
                    newList.CssClass = "form-control";
                    newList.DataSourceID = "SqlDataSourceUsers";
                    newList.Style["margin-left"] = "3px";
                    newList.DataTextField = "UserName";
                    newList.DataValueField = "UserId";
                    newList.ID = "AdditionalDrafterId_" + i.ToString();

                    panel.Controls.Add(newTextBox);
                    panel.Controls.Add(newList);
                    drafters_html_block.Controls.Add(panel);
                }
        }
        
        //замена нулей на пустые строки
        private void ReplaceNulls(GridView view)
        {
            try
            {
                for (int i = 0; i < view.Rows.Count; i++)
                    for (int j = 0; j < view.Rows[i].Cells.Count; j++)
                        if (view.Rows[i].Cells[j].Text == "0")
                            view.Rows[i].Cells[j].Text = "";
            }
            catch { }
        }

        //заполнение объекта рпд нужными данными с базы
        private Pro GetProgramFromBase(int programid)
        {
            BaseWork task = new BaseWork();
            Pro program = new Pro();
            program = task.GetProgram(programid);
            return program;
        }

        //поиск строки списка с искомым текстом 
        private void FindItem(DropDownList list, string itemText)
        {
            list.DataBind();
            for (int i = 0; i < list.Items.Count; i++)
                if (list.Items[i].Text == itemText)
                {
                    list.Items[i].Selected = true;
                    break;
                }
        }
        
        //заполнение полей данными рпд
        private void FillControlls(Pro program, bool reload)
        {
            if (reload) goto tablefiil;

            Institute.Text = program.Institute;
            Approver_Post.Text = program.Approver_Post;
            FindItem(Approver_Name, program.Approver_Name);
            Approve_Date.Text = program.Approve_Date.ToString("yyyy-MM-dd");
            Discipline.Text = program.Discipline;
            FindItem(Training_Direction, program.Training_Direction);
            FindItem(DropDown_Profil, program.Profil);
            Graduate_Qualification.Text = program.Graduate_Qualification;
            Training_Form.Text = program.Training_Form;
            StudyPeriod.Text = program.StudPeriod;
            City.Text = program.City;
            ObjectivesOfDiscipline.Text = program.ObjectivesOfDiscipline;
            DisciplineInOOP.Text = program.DisciplineInOOP;
            //PlanDescription.Text = program.PlanDescription;
            DisciplineStructureContent.Text = program.DisciplineStructureContent;
            SemesteresContent.Text = program.SemesteresContent;
            EducationalTechnology.Text = program.EducationalTechnology;
            MonitoringTools_Description.Text = program.MonitoringTools_Description;
            ZachetDescription.Text = program.ZachetDescription;
            Zachteno.Text = program.ZachetTable.ZachetDone;
            NeZachteno.Text = program.ZachetTable.ZachetUnDone;
            ExamDescription.Text = program.ExamDescription;
            AttestatGreat.Text = program.ExamTable.AttestatGreat;
            AttestatGood.Text = program.ExamTable.AttestatGood;
            AttestatSatisfactorily.Text = program.ExamTable.AttestatSatisfactorily;
            AttestatNotSatisfactorily.Text = program.ExamTable.AttestatNotSatisfactorily;
            BasicLiterature.Text = program.BasicLiterature;
            AdditionalLiterature.Text = program.AdditionalLiterature;
            ITResources.Text = program.ITResources;
            MaterialTechnicalSupport.Text = program.MaterialTechnicalSupport;
            StudentRecommendations.Text = program.StudentRecommendations;
            TeacherRecommendations.Text = program.TeacherRecommendations;
            DrafterStatus.Text = program.DrafterStatus;
            FindItem(DrafterName, program.DrafterName);
            DrafterName.Text = program.DrafterName;
            FindItem(DropDown_Department, program.Department);
            DraftDate.Text = program.DraftDate.ToString("yyyy-MM-dd");
            Protocol.Text = program.Protocol;
            DepartmentHeadStatus.Text = program.DepartmentHeadStatus;
            FindItem(DepartmentHeadName, program.DepartmentHeadName);
            AgreementDepartmentHeadStatus.Text = program.AgreementDepartmentHeadStatus;
            FindItem(AgreementDepartmentHeadName, program.AgreementDepartmentHeadName);
            AgreementDirectorStatus.Text = program.AgreementDirectorStatus;
            FindItem(AgreementDirectorName, program.AgreementDirectorName);

            
            try
            {
                ZachetQuestions.Text = program.Questions_Zachet.Questions;
                FindItem(DeveloperZachet, program.Questions_Zachet.Developer);
                ZachetQuestionsDate.Text = program.Questions_Zachet.Date.ToString("yyyy-MM-dd");
            }
            catch { }

            try
            {
                ExamQuestions.Text = program.Questions_Exam.Questions;
                FindItem(DeveloperExam, program.Questions_Exam.Developer);
                ExamQuestionsDate.Text = program.Questions_Exam.Date.ToString("yyyy-MM-dd");
            }
            catch { }

            try
            {
                ReferatTopics.Text = program.Questions_Referat.Questions;
                FindItem(DeveloperReferat, program.Questions_Referat.Developer);
                ReferatTopicsDate.Text = program.Questions_Referat.Date.ToString("yyyy-MM-dd");
            }
            catch { }

            try
            {
                KTTopics.Text = program.Questions_KT.Questions;
                FindItem(DeveloperKT, program.Questions_KT.Developer);
                KTDate.Text = program.Questions_KT.Date.ToString("yyyy-MM-dd");
            }
            catch { }

            try
            {
                PraktikTopics.Text = program.Questions_Praktika.Questions;
                FindItem(DeveloperPraktika, program.Questions_Praktika.Developer);
                PraktikaTopicsDate.Text = program.Questions_Praktika.Date.ToString("yyyy-MM-dd");
            }
            catch { }

            try
            {
                TestsFond.Text = program.Questions_Test.Questions;
                FindItem(DeveloperTest, program.Questions_Test.Developer);
                TestsFondDate.Text = program.Questions_Test.Date.ToString("yyyy-MM-dd");
            }
            catch { }

            try
            {
                CursesFond.Text = program.Questions_Curs.Questions;
                FindItem(DeveloperCurs, program.Questions_Curs.Developer);
                CursDate.Text = program.Questions_Curs.Date.ToString("yyyy-MM-dd");
            }
            catch { }

        tablefiil:
            {
                ptrowscount.Value = program.PlanTable.Count.ToString();

                for (int i = 0; i < program.PlanTable.Count; i++)
                {
                    TableRow row = new TableRow();
                    for (int j = 0; j < 3; j++)
                    {
                        TableCell cell = new TableCell();
                        row.Cells.Add(cell);
                    }
                    row.Cells[0].Text = program.PlanTable[i].Сompetence;
                    row.Cells[1].Text = program.PlanTable[i].Ability;
                    row.Cells[2].Text = program.PlanTable[i].Enumeration;
                    PlanTable.Rows.Add(row);
                }

                ftrowscount.Value = program.FondTable.Count.ToString();
                for (int i = 0; i < program.FondTable.Count; i++)
                {
                    TableRow row = new TableRow();
                    for (int j = 0; j < 2; j++)
                    {
                        TableCell cell = new TableCell();
                        row.Cells.Add(cell);
                    }
                    row.Cells[0].Text = program.FondTable[i].Сompetence;
                    row.Cells[1].Text = program.FondTable[i].Ability;
                    MonitoringTools_FondTable.Rows.Add(row);
                }

                CTrowscount.Value = program.CriterionsTable.Count.ToString();
                for (int i = 0; i < program.CriterionsTable.Count; i++)
                {
                    TableRow row = new TableRow();
                    for (int j = 0; j < 6; j++)
                    {
                        TableCell cell = new TableCell();
                        row.Cells.Add(cell);
                    }
                    row.Cells[0].Text = program.CriterionsTable[i].Competence;
                    row.Cells[1].Text = program.CriterionsTable[i].ToKnow_Description;
                    row.Cells[2].Text = program.CriterionsTable[i].ToKnow[0];
                    row.Cells[3].Text = program.CriterionsTable[i].ToKnow[1];
                    row.Cells[4].Text = program.CriterionsTable[i].ToKnow[2];
                    row.Cells[5].Text = program.CriterionsTable[i].ToKnow[3];
                    CriterionsTable.Rows.Add(row);

                    TableRow row_1 = new TableRow();
                    for (int j = 0; j < 6; j++)
                    {
                        TableCell cell = new TableCell();
                        row_1.Cells.Add(cell);
                    }
                    row_1.Cells[0].Text = "-//-//-";
                    row_1.Cells[1].Text = program.CriterionsTable[i].ToDo_Description;
                    row_1.Cells[2].Text = program.CriterionsTable[i].ToDo[0];
                    row_1.Cells[3].Text = program.CriterionsTable[i].ToDo[1];
                    row_1.Cells[4].Text = program.CriterionsTable[i].ToDo[2];
                    row_1.Cells[5].Text = program.CriterionsTable[i].ToDo[3];
                    CriterionsTable.Rows.Add(row_1);

                    TableRow row_2 = new TableRow();
                    for (int j = 0; j < 6; j++)
                    {
                        TableCell cell = new TableCell();
                        row_2.Cells.Add(cell);
                    }
                    row_2.Cells[0].Text = "-//-//-";
                    row_2.Cells[1].Text = program.CriterionsTable[i].ToMaster_Description;
                    row_2.Cells[2].Text = program.CriterionsTable[i].ToMaster[0];
                    row_2.Cells[3].Text = program.CriterionsTable[i].ToMaster[1];
                    row_2.Cells[4].Text = program.CriterionsTable[i].ToMaster[2];
                    row_2.Cells[5].Text = program.CriterionsTable[i].ToMaster[3];
                    CriterionsTable.Rows.Add(row_2);
                }
                

                PFT_1rowscount.Value = program.PFT_1.Count.ToString();
                for (int i = 0; i < program.PFT_1.Count; i++)
                {
                    TableRow row = new TableRow();
                    for (int j = 0; j < 6; j++)
                    {
                        TableCell cell = new TableCell();
                        row.Cells.Add(cell);
                    }
                    row.Cells[0].Text = program.PFT_1[i].Index;
                    row.Cells[1].Text = program.PFT_1[i].Formul;
                    row.Cells[2].Text = program.PFT_1[i].KomponentsList;
                    row.Cells[3].Text = program.PFT_1[i].CompetenceTechnology;
                    row.Cells[4].Text = program.PFT_1[i].AssessForm;
                    row.Cells[5].Text = program.PFT_1[i].Steps;
                    PrilFondTable_1.Rows.Add(row);
                }

                PFT_2rowscount.Value = program.PFT_2.Count.ToString();
                for (int i = 0; i < program.PFT_2.Count; i++)
                {
                    TableRow row = new TableRow();
                    for (int j = 0; j < 6; j++)
                    {
                        TableCell cell = new TableCell();
                        row.Cells.Add(cell);
                    }
                    row.Cells[0].Text = program.PFT_2[i].Index;
                    row.Cells[1].Text = program.PFT_2[i].Formul;
                    row.Cells[2].Text = program.PFT_2[i].KomponentsList;
                    row.Cells[3].Text = program.PFT_2[i].CompetenceTechnology;
                    row.Cells[4].Text = program.PFT_2[i].AssessForm;
                    row.Cells[5].Text = program.PFT_2[i].Steps;
                    PrilFondTable_2.Rows.Add(row);
                }

                PFT_3rowscount.Value = program.PFT_3.Count.ToString();
                for (int i = 0; i < program.PFT_3.Count; i++)
                {
                    TableRow row = new TableRow();
                    for (int j = 0; j < 6; j++)
                    {
                        TableCell cell = new TableCell();
                        row.Cells.Add(cell);
                    }
                    row.Cells[0].Text = program.PFT_3[i].Index;
                    row.Cells[1].Text = program.PFT_3[i].Formul;
                    row.Cells[2].Text = program.PFT_3[i].KomponentsList;
                    row.Cells[3].Text = program.PFT_3[i].CompetenceTechnology;
                    row.Cells[4].Text = program.PFT_3[i].AssessForm;
                    row.Cells[5].Text = program.PFT_3[i].Steps;
                    PrilFondTable_3.Rows.Add(row);
                }

                PATrowscount.Value = program.PAT.Count.ToString();
                for (int i = 0; i < program.PAT.Count; i++)
                {
                    TableRow row = new TableRow();
                    for (int j = 0; j < 4; j++)
                    {
                        TableCell cell = new TableCell();
                        row.Cells.Add(cell);
                    }
                    row.Cells[0].Text = (i + 1).ToString();
                    row.Cells[1].Text = program.PAT[i].Name;
                    row.Cells[2].Text = program.PAT[i].Spec;
                    row.Cells[3].Text = program.PAT[i].InFOS;
                    PrilAssessTable.Rows.Add(row);
                }

                PCT_zachetrowscount.Value = program.PCT_Zachet.Count.ToString();
                for (int i = 0; i < program.PCT_Zachet.Count; i++)
                {
                    TableRow row = new TableRow();
                    for (int j = 0; j < 5; j++)
                    {
                        TableCell cell = new TableCell();
                        row.Cells.Add(cell);
                    }
                    row.Cells[0].Text = program.PCT_Zachet[i].Discription;
                    row.Cells[1].Text = program.PCT_Zachet[i].Rezult;
                    row.Cells[2].Text = program.PCT_Zachet[i].Topics;
                    row.Cells[3].Text = program.PCT_Zachet[i].Zachet;
                    row.Cells[4].Text = program.PCT_Zachet[i].NeZachet;
                    PrilCompetenceTable_Zachet.Rows.Add(row);
                }

                PCT_examrowscount.Value = program.PCT_Exam.Count.ToString();
                for (int i = 0; i < program.PCT_Exam.Count; i++)
                {
                    TableRow row = new TableRow();
                    for (int j = 0; j < 7; j++)
                    {
                        TableCell cell = new TableCell();
                        row.Cells.Add(cell);
                    }
                    row.Cells[0].Text = program.PCT_Exam[i].Discription;
                    row.Cells[1].Text = program.PCT_Exam[i].Rezult;
                    row.Cells[2].Text = program.PCT_Exam[i].Topics;
                    row.Cells[3].Text = program.PCT_Exam[i].On2;
                    row.Cells[4].Text = program.PCT_Exam[i].On3;
                    row.Cells[5].Text = program.PCT_Exam[i].On4;
                    row.Cells[6].Text = program.PCT_Exam[i].On5;
                    PrilCompetenceTable_Exam.Rows.Add(row);
                }

                PCT_referatrowscount.Value = program.PCT_Referat.Count.ToString();
                for (int i = 0; i < program.PCT_Referat.Count; i++)
                {
                    TableRow row = new TableRow();
                    for (int j = 0; j < 7; j++)
                    {
                        TableCell cell = new TableCell();
                        row.Cells.Add(cell);
                    }
                    row.Cells[0].Text = program.PCT_Referat[i].Discription;
                    row.Cells[1].Text = program.PCT_Referat[i].Rezult;
                    row.Cells[2].Text = program.PCT_Referat[i].Topics;
                    row.Cells[3].Text = program.PCT_Referat[i].On2;
                    row.Cells[4].Text = program.PCT_Referat[i].On3;
                    row.Cells[5].Text = program.PCT_Referat[i].On4;
                    row.Cells[6].Text = program.PCT_Referat[i].On5;
                    PrilCompetenceTable_Referat.Rows.Add(row);
                }

                PCT_ktrowscount.Value = program.PCT_KT.Count.ToString();
                for (int i = 0; i < program.PCT_KT.Count; i++)
                {
                    TableRow row = new TableRow();
                    for (int j = 0; j < 7; j++)
                    {
                        TableCell cell = new TableCell();
                        row.Cells.Add(cell);
                    }
                    row.Cells[0].Text = program.PCT_KT[i].Discription;
                    row.Cells[1].Text = program.PCT_KT[i].Rezult;
                    row.Cells[2].Text = program.PCT_KT[i].Topics;
                    row.Cells[3].Text = program.PCT_KT[i].On2;
                    row.Cells[4].Text = program.PCT_KT[i].On3;
                    row.Cells[5].Text = program.PCT_KT[i].On4;
                    row.Cells[6].Text = program.PCT_KT[i].On5;
                    PrilCompetenceTable_KT.Rows.Add(row);
                }

                PCT_praktikarowscount.Value = program.PCT_Praktika.Count.ToString();
                for (int i = 0; i < program.PCT_Praktika.Count; i++)
                {
                    TableRow row = new TableRow();
                    for (int j = 0; j < 7; j++)
                    {
                        TableCell cell = new TableCell();
                        row.Cells.Add(cell);
                    }
                    row.Cells[0].Text = program.PCT_Praktika[i].Discription;
                    row.Cells[1].Text = program.PCT_Praktika[i].Rezult;
                    row.Cells[2].Text = program.PCT_Praktika[i].Topics;
                    row.Cells[3].Text = program.PCT_Praktika[i].On2;
                    row.Cells[4].Text = program.PCT_Praktika[i].On3;
                    row.Cells[5].Text = program.PCT_Praktika[i].On4;
                    row.Cells[6].Text = program.PCT_Praktika[i].On5;
                    PrilCompetenceTable_Praktika.Rows.Add(row);
                }

                PCT_testrowscount.Value = program.PCT_Test.Count.ToString();
                for (int i = 0; i < program.PCT_Test.Count; i++)
                {
                    TableRow row = new TableRow();
                    for (int j = 0; j < 7; j++)
                    {
                        TableCell cell = new TableCell();
                        row.Cells.Add(cell);
                    }
                    row.Cells[0].Text = program.PCT_Test[i].Discription;
                    row.Cells[1].Text = program.PCT_Test[i].Rezult;
                    row.Cells[2].Text = program.PCT_Test[i].Topics;
                    row.Cells[3].Text = program.PCT_Test[i].On2;
                    row.Cells[4].Text = program.PCT_Test[i].On3;
                    row.Cells[5].Text = program.PCT_Test[i].On4;
                    row.Cells[6].Text = program.PCT_Test[i].On5;
                    PrilCompetenceTable_Test.Rows.Add(row);
                }

                PCT_cursrowscount.Value = program.PCT_Curs.Count.ToString();
                for (int i = 0; i < program.PCT_Curs.Count; i++)
                {
                    TableRow row = new TableRow();
                    for (int j = 0; j < 7; j++)
                    {
                        TableCell cell = new TableCell();
                        row.Cells.Add(cell);
                    }
                    row.Cells[0].Text = program.PCT_Curs[i].Discription;
                    row.Cells[1].Text = program.PCT_Curs[i].Rezult;
                    row.Cells[2].Text = program.PCT_Curs[i].Topics;
                    row.Cells[3].Text = program.PCT_Curs[i].On2;
                    row.Cells[4].Text = program.PCT_Curs[i].On3;
                    row.Cells[5].Text = program.PCT_Curs[i].On4;
                    row.Cells[6].Text = program.PCT_Curs[i].On5;
                    PrilCompetenceTable_Curs.Rows.Add(row);
                }
            }
        }

        //обработка таблиц
        #region TableTasks
            
        protected void DisciplineStructureTable_GridView_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            BaseWork task = new BaseWork();
            task.SetHistory(User.Identity.GetUserId(), ProgramId, "Правка в позиции таблицы структуры дисциплины");
        }
        
        protected void DisciplineStructureTable_GridView_RowDeleted(object sender, GridViewDeletedEventArgs e)
        {
            BaseWork task = new BaseWork();
            task.SetHistory(User.Identity.GetUserId(), ProgramId, "Удалена позиция таблицы структуры дисциплины");
        }

        protected void CreateRowPlanTable_Click(object sender, EventArgs e)
        {
            BaseWork task = new BaseWork();
            PlanTableRow row = new PlanTableRow();
            row.Сompetence = Сompetence_PlanTable.SelectedItem.Text;
            row.Ability = Сompetence_PlanTable.SelectedItem.Value;
            row.Enumeration = Enumeration_PlanTable.Text;
            task.CreateRowInPlan(ProgramId, row, User.Identity.GetUserId());
            Response.Redirect(Request.Url.ToString());
        }

        protected void DeleteRowPlanTable_Click(object sender, EventArgs e)
        {
            BaseWork task = new BaseWork();
            PlanTableRow row = new PlanTableRow();
            try
            {
                int number = Convert.ToInt32(RowNumberPlanTable.Text);
                row.Сompetence = PlanTable.Rows[number].Cells[0].Text;
                row.Ability = PlanTable.Rows[number].Cells[1].Text;
                row.Enumeration = PlanTable.Rows[number].Cells[2].Text;
                task.DeleteRowInPlan(ProgramId, row, User.Identity.GetUserId());
            }
            catch { }
            Response.Redirect(Request.Url.ToString());
        }

        protected void CreateRowFondTable_Click(object sender, EventArgs e)
        {
            BaseWork task = new BaseWork();
            Competence com = new Competence();
            com = task.GetCompetence(Convert.ToInt32(Competence_FondTable.SelectedValue));
            FondTableRow row = new FondTableRow();
            row.Сompetence = com.Code;
            row.Ability = com.Description;
            task.CreateRowInFond(ProgramId, row, User.Identity.GetUserId());
            Response.Redirect(Request.Url.ToString());
        }

        protected void DeleteRowFondTable_Click(object sender, EventArgs e)
        {
            BaseWork task = new BaseWork();
            FondTableRow row = new FondTableRow();
            try
            {
                int number = Convert.ToInt32(RowNumberFondTable.Text);
                row.Сompetence = MonitoringTools_FondTable.Rows[number].Cells[0].Text;
                row.Ability = MonitoringTools_FondTable.Rows[number].Cells[1].Text;
                task.DeleteRowInFond(ProgramId, row, User.Identity.GetUserId());
            }
            catch { }
            Response.Redirect(Request.Url.ToString());
        }

        protected void CreateRowsCriterionsTable_Click(object sender, EventArgs e)
        {
            BaseWork task = new BaseWork();
            CriterionsTableRow row = new CriterionsTableRow();
            List<string> toknow = new List<string>();
            List<string> todo = new List<string>();
            List<string> tomaster = new List<string>();
            

            row.Competence = Competence_CriterionsTable.SelectedItem.Text + " - " + Competence_CriterionsTable.SelectedItem.Value;
            row.ToKnow_Description = IndicatorCriterionsTable_Know.Text;
            toknow.Add(CriterionCriterionsTable_Know_2.Text);
            toknow.Add(CriterionCriterionsTable_Know_3.Text);
            toknow.Add(CriterionCriterionsTable_Know_4.Text);
            toknow.Add(CriterionCriterionsTable_Know_5.Text);
            row.ToKnow = toknow;

            row.ToDo_Description = IndicatorCriterionsTable_Can.Text;
            todo.Add(CriterionCriterionsTable_Can_2.Text);
            todo.Add(CriterionCriterionsTable_Can_3.Text);
            todo.Add(CriterionCriterionsTable_Can_4.Text);
            todo.Add(CriterionCriterionsTable_Can_5.Text);
            row.ToDo = todo;

            row.ToMaster_Description = IndicatorCriterionsTable_Wield.Text;
            tomaster.Add(CriterionCriterionsTable_Wield_2.Text);
            tomaster.Add(CriterionCriterionsTable_Wield_3.Text);
            tomaster.Add(CriterionCriterionsTable_Wield_4.Text);
            tomaster.Add(CriterionCriterionsTable_Wield_5.Text);
            row.ToMaster = tomaster;

            task.CreateRowInCriterions(ProgramId, row, User.Identity.GetUserId());
            Response.Redirect(Request.Url.ToString());
        }
        protected void DeleteRowsCriterionsTable_Click(object sender, EventArgs e)
        {
            BaseWork task = new BaseWork();
            CriterionsTableRow row = new CriterionsTableRow();
            List<string> toknow = new List<string>();
            List<string> todo = new List<string>();
            List<string> tomaster = new List<string>();

            try
            {
                int number = Convert.ToInt32(RowNumberCriterionsTable.Text) + 1;
                if (CriterionsTable.Rows[number].Cells[0].Text == "-//-//-") goto end;

                row.Competence = CriterionsTable.Rows[number].Cells[0].Text;
                row.ToKnow_Description = CriterionsTable.Rows[number].Cells[1].Text;
                row.ToDo_Description = CriterionsTable.Rows[number + 1].Cells[1].Text;
                row.ToMaster_Description = CriterionsTable.Rows[number + 2].Cells[1].Text;
                for (int i = 2; i < 6; i++)
                {
                    toknow.Add(CriterionsTable.Rows[number].Cells[i].Text);
                    todo.Add(CriterionsTable.Rows[number + 1].Cells[i].Text);
                    tomaster.Add(CriterionsTable.Rows[number + 2].Cells[i].Text);
                }
                row.ToKnow = toknow;
                row.ToDo = todo;
                row.ToMaster = tomaster;
                task.DeleteRowInCriterions(ProgramId, row, User.Identity.GetUserId());
            }
            catch { }
            end: { }
            Response.Redirect(Request.Url.ToString());

        }

        protected void button_createRowInPFT_1_Click(object sender, EventArgs e)
        {
            CreateRowInPFTBase(0);
        }
        protected void button_createRowInPFT_2_Click(object sender, EventArgs e)
        {
            CreateRowInPFTBase(1);
        }
        protected void button_createRowInPFT_3_Click(object sender, EventArgs e)
        {
            CreateRowInPFTBase(2);
        }

        protected void button_deleteRowInPFT_1_Click(object sender, EventArgs e)
        {
            DeleteRowInPFTBase(PrilFondTable_1, 0);
        }
        protected void button_deleteRowInPFT_2_Click(object sender, EventArgs e)
        {
            DeleteRowInPFTBase(PrilFondTable_2, 1);
        }
        protected void button_deleteRowInPFT_3_Click(object sender, EventArgs e)
        {
            DeleteRowInPFTBase(PrilFondTable_3, 2);
        }

        private void CreateRowInPFTBase(int tableindex)
        {
            BaseWork task = new BaseWork();
            PrilFondTableRow row = new PrilFondTableRow();
            row.Index = PFTCom.SelectedItem.Text;
            row.Formul = PFTCom.SelectedItem.Value;
            row.KomponentsList = PFTKomponentsList.Text;
            row.CompetenceTechnology = PFTCompetenceTechnology.Text;
            row.AssessForm = PFTAssessForm.Text;
            row.Steps = PFTSteps.Text;
            task.CreateRowInPrilFond(tableindex, ProgramId, row, User.Identity.GetUserId());
            Response.Redirect(Request.Url.ToString());

        }

        private void DeleteRowInPFTBase(Table table, int tableindex)
        {
            BaseWork task = new BaseWork();
            PrilFondTableRow row = new PrilFondTableRow();
            try
            {
                int number = Convert.ToInt32(RowNumberPFT.Text) + 2;
                row.Index = table.Rows[number].Cells[0].Text;
                row.Formul = table.Rows[number].Cells[1].Text;
                row.KomponentsList = table.Rows[number].Cells[2].Text;
                row.CompetenceTechnology = table.Rows[number].Cells[3].Text;
                row.AssessForm = table.Rows[number].Cells[4].Text;
                row.Steps = table.Rows[number].Cells[5].Text;
                task.DeleteRowInPrilFond(tableindex, ProgramId, row, User.Identity.GetUserId());
            }
            catch { }
            Response.Redirect(Request.Url.ToString());

        }

        protected void addRowInPCTzachet_Click(object sender, EventArgs e)
        {
            BaseWork task = new BaseWork();
            PCTZachetTableRow row = new PCTZachetTableRow();
            row.Discription = DropDown_PCTZ.SelectedItem.Text + " - " + DropDown_PCTZ.SelectedItem.Value.ToString();
            row.Rezult = PCTZRezult.Text;
            row.Topics = PCTZTopics.Text;
            row.Zachet = PCTZZachet.Text;
            row.NeZachet = PCTZNeZachet.Text;
            task.CreateRowInPrilCompetence_Zachet(ProgramId, row, User.Identity.GetUserId());
            Response.Redirect(Request.Url.ToString());

        }
        protected void deleteRowInPCTzachet_Click(object sender, EventArgs e)
        {
            BaseWork task = new BaseWork();
            PCTZachetTableRow row = new PCTZachetTableRow();
            try
            {
                int number = Convert.ToInt32(RowNumberPCTZ.Text) + 2;
                row.Discription = PrilCompetenceTable_Zachet.Rows[number].Cells[0].Text;
                row.Rezult = PrilCompetenceTable_Zachet.Rows[number].Cells[1].Text;
                row.Topics = PrilCompetenceTable_Zachet.Rows[number].Cells[2].Text;
                row.Zachet = PrilCompetenceTable_Zachet.Rows[number].Cells[3].Text;
                row.NeZachet = PrilCompetenceTable_Zachet.Rows[number].Cells[4].Text;
                task.DeleteRowInPrilCompetence_Zachet(ProgramId, row, User.Identity.GetUserId());
            }
            catch { }
            Response.Redirect(Request.Url.ToString());

        }

        protected void button_addRowInPAT_Click(object sender, EventArgs e)
        {
            BaseWork task = new BaseWork();
            PrilAssessTableRow row = new PrilAssessTableRow();
            row.Name = PATName.Text;
            row.Spec = PATSpec.Text;
            row.InFOS = PATinFOS.Text;
            task.CreateRowInPrilAssess(ProgramId, row, User.Identity.GetUserId());
            Response.Redirect(Request.Url.ToString());

        }
        protected void button_deleteRowInPAT_Click(object sender, EventArgs e)
        {
            BaseWork task = new BaseWork();
            PrilAssessTableRow row = new PrilAssessTableRow();
            try
            {
                int number = Convert.ToInt32(RowNumberPAT.Text);
                row.Name = PrilAssessTable.Rows[number].Cells[1].Text;
                row.Spec = PrilAssessTable.Rows[number].Cells[2].Text;
                row.InFOS = PrilAssessTable.Rows[number].Cells[3].Text;
                task.DeleteRowInPrilAssess(ProgramId, row, User.Identity.GetUserId());
            }
            catch { }
            Response.Redirect(Request.Url.ToString());

        }

        protected void addRowInPCTExamTable_Click(object sender, EventArgs e)
        {
            BaseWork task = new BaseWork();
            PCTGeneral row = new PCTGeneral();
            row.Discription = DropDown_PCTE.SelectedItem.Text + " - " + DropDown_PCTE.SelectedItem.Value.ToString();
            row.Rezult = PCTERezult.Text;
            row.Topics = PCTETopics.Text;
            row.On2 = PCTE2.Text;
            row.On3 = PCTE3.Text;
            row.On4 = PCTE4.Text;
            row.On5 = PCTE5.Text;
            task.CreateRowInPrilCompetence(1, ProgramId, row, User.Identity.GetUserId());
            Response.Redirect(Request.Url.ToString());

        }
        protected void deleteRowInPCTExamTable_Click(object sender, EventArgs e)
        {
            DeleteRowInPCTBase(PrilCompetenceTable_Exam, 1, RowNumberPCTE.Text);
        }

        protected void addRowInPCTReferatTable_Click(object sender, EventArgs e)
        {
            BaseWork task = new BaseWork();
            PCTGeneral row = new PCTGeneral();
            row.Discription = DropDown_PCTR.SelectedItem.Text + " - " + DropDown_PCTR.SelectedItem.Value.ToString();
            row.Rezult = PCTRRezult.Text;
            row.Topics = PCTRTopics.Text;
            row.On2 = PCTR2.Text;
            row.On3 = PCTR3.Text;
            row.On4 = PCTR4.Text;
            row.On5 = PCTR5.Text;
            task.CreateRowInPrilCompetence(2, ProgramId, row, User.Identity.GetUserId());
            Response.Redirect(Request.Url.ToString());

        }
        protected void deleteRowInPCTReferatTable_Click(object sender, EventArgs e)
        {
            DeleteRowInPCTBase(PrilCompetenceTable_Referat, 2, RowNumberPCTR.Text);
        }

        protected void addRowInPCTKTTable_Click(object sender, EventArgs e)
        {
            BaseWork task = new BaseWork();
            PCTGeneral row = new PCTGeneral();
            row.Discription = DropDown_PCTKT.SelectedItem.Text + " - " + DropDown_PCTKT.SelectedItem.Value.ToString();
            row.Rezult = PCTKTRezult.Text;
            row.Topics = PCTKTTopics.Text;
            row.On2 = PCTKT2.Text;
            row.On3 = PCTKT3.Text;
            row.On4 = PCTKT4.Text;
            row.On5 = PCTKT5.Text;
            task.CreateRowInPrilCompetence(3, ProgramId, row, User.Identity.GetUserId());
            Response.Redirect(Request.Url.ToString());

        }
        protected void deleteRowInPCTKTTable_Click(object sender, EventArgs e)
        {
            DeleteRowInPCTBase(PrilCompetenceTable_KT, 3, RowNumberPCTKt.Text);
        }

        protected void addRowInPCTPraktikaTable_Click(object sender, EventArgs e)
        {
            BaseWork task = new BaseWork();
            PCTGeneral row = new PCTGeneral();
            row.Discription = DropDownPCTPR.SelectedItem.Text + " - " + DropDownPCTPR.SelectedItem.Value.ToString();
            row.Rezult = PCTPRezult.Text;
            row.Topics = PCTPTopics.Text;
            row.On2 = PCTP2.Text;
            row.On3 = PCTP3.Text;
            row.On4 = PCTP4.Text;
            row.On5 = PCTP5.Text;
            task.CreateRowInPrilCompetence(4, ProgramId, row, User.Identity.GetUserId());
            Response.Redirect(Request.Url.ToString());

        }
        protected void deleteRowInPCTPraktikaTable_Click(object sender, EventArgs e)
        {
            DeleteRowInPCTBase(PrilCompetenceTable_Praktika, 4, RowNumberPCTP.Text);
        }

        protected void addRowInPCTtestTable_Click(object sender, EventArgs e)
        {
            BaseWork task = new BaseWork();
            PCTGeneral row = new PCTGeneral();
            row.Discription = DropDown_PCTT.SelectedItem.Text + " - " + DropDown_PCTT.SelectedItem.Value.ToString();
            row.Rezult = PCTTRezult.Text;
            row.Topics = PCTTTopics.Text;
            row.On2 = PCTT2.Text;
            row.On3 = PCTT3.Text;
            row.On4 = PCTT4.Text;
            row.On5 = PCTT5.Text;
            task.CreateRowInPrilCompetence(5, ProgramId, row, User.Identity.GetUserId());
            Response.Redirect(Request.Url.ToString());

        }
        protected void deleteRowInPCTtestTable_Click(object sender, EventArgs e)
        {
            DeleteRowInPCTBase(PrilCompetenceTable_Test, 5, RowNumberPCTT.Text);
        }

        protected void addRowPCTcursTable_Click(object sender, EventArgs e)
        {
            BaseWork task = new BaseWork();
            PCTGeneral row = new PCTGeneral();
            row.Discription = DropDown_PCTC.SelectedItem.Text + " - " + DropDown_PCTC.SelectedItem.Value.ToString();
            row.Rezult = PCTCRezult.Text;
            row.Topics = PCTCTopics.Text;
            row.On2 = PCTC2.Text;
            row.On3 = PCTC3.Text;
            row.On4 = PCTC4.Text;
            row.On5 = PCTC5.Text;
            task.CreateRowInPrilCompetence(6, ProgramId, row, User.Identity.GetUserId());
            Response.Redirect(Request.Url.ToString());
        }
        protected void deleteRowPCTcursTable_Click(object sender, EventArgs e)
        {
            DeleteRowInPCTBase(PrilCompetenceTable_Curs, 6, PCTCursRowsNumber.Text);
        }

        private void DeleteRowInPCTBase(Table table, int tableindex, string numberstr)
        {
            BaseWork task = new BaseWork();
            PCTGeneral row = new PCTGeneral();
            try
            {
                int number = Convert.ToInt32(numberstr) + 2;
                row.Discription = table.Rows[number].Cells[0].Text;
                row.Rezult = table.Rows[number].Cells[1].Text;
                row.Topics = table.Rows[number].Cells[2].Text;
                row.On2 = table.Rows[number].Cells[3].Text;
                row.On3 = table.Rows[number].Cells[4].Text;
                row.On4 = table.Rows[number].Cells[5].Text;
                row.On5 = table.Rows[number].Cells[6].Text;
                task.DeleteRowInPrilCompetence(tableindex, ProgramId, row, User.Identity.GetUserId());
            }
            catch { }
            Response.Redirect(Request.Url.ToString());
        }

        

        #endregion TableTasks

        //обновления блоков
        #region UpDateBlocks

        protected void UpDateFirstBlock_Button_Click(object sender, EventArgs e)
        {
            BaseWork task = new BaseWork();
            pro.Id = ProgramId;
            pro.Institute = Institute.Text;
            pro.Approver_Post = Approver_Post.Text;
            pro.Approver_Name = Approver_Name.SelectedItem.Text;
            pro.Approver_Id = Approver_Name.SelectedItem.Value;

            try
            {
                string dateInput = Approve_Date.Text;
                DateTime d = DateTime.Parse(dateInput);
                pro.Approve_Date = d;
            }
            catch { }

            pro.Discipline = Discipline.Text;
            pro.Training_Direction = Training_Direction.Text;
            pro.Profil = DropDown_Profil.SelectedItem.Text;
            pro.Graduate_Qualification = Graduate_Qualification.Text;
            pro.Training_Form = Training_Form.Text;
            pro.StudPeriod = StudyPeriod.Text;
            pro.City = City.Text;
            string userid = User.Identity.GetUserId();

            task.UpDateFirstBlock(pro, userid);

        }

        protected void UpDateSecondBlock_Button_Click(object sender, EventArgs e)
        {
            BaseWork task = new BaseWork();
            pro.Id = ProgramId;
            pro.ObjectivesOfDiscipline = ObjectivesOfDiscipline.Text;
            pro.DisciplineInOOP = DisciplineInOOP.Text;
            //pro.PlanDescription = PlanDescription.Text;
            string userid = User.Identity.GetUserId();

            task.UpDateSecondBlock(pro, userid);
        }

        protected void UpDateThirdBlock_Button_Click(object sender, EventArgs e)
        {
            BaseWork task = new BaseWork();
            pro.Id = ProgramId;
            pro.DisciplineStructureContent = DisciplineStructureContent.Text;
            pro.SemesteresContent = SemesteresContent.Text;
            pro.EducationalTechnology = EducationalTechnology.Text;
            pro.MonitoringTools_Description = MonitoringTools_Description.Text;
            string userid = User.Identity.GetUserId();

            task.UpDateThirdBlock(pro, userid);
        }
        
        protected void UpDateFifthBlock_Button_Click1(object sender, EventArgs e)
        {
            BaseWork task = new BaseWork();
            pro.Id = ProgramId;
            pro.ExamDescription = ExamDescription.Text;
            pro.ExamTable.AttestatGreat = AttestatGreat.Text;
            pro.ExamTable.AttestatGood = AttestatGood.Text;
            pro.ExamTable.AttestatSatisfactorily = AttestatSatisfactorily.Text;
            pro.ExamTable.AttestatNotSatisfactorily = AttestatNotSatisfactorily.Text;

            pro.ZachetDescription = ZachetDescription.Text;
            pro.ZachetTable.ZachetDone = Zachteno.Text;
            pro.ZachetTable.ZachetUnDone = NeZachteno.Text;

            task.UpDateFourthBlock(pro, User.Identity.GetUserId());
            task.UpdateFifthBlock(pro, User.Identity.GetUserId());
        }

        protected void UpDateSixthBlock_Button_Click(object sender, EventArgs e)
        {
            BaseWork task = new BaseWork();
            pro.Id = ProgramId;
            pro.BasicLiterature = BasicLiterature.Text;
            pro.AdditionalLiterature = AdditionalLiterature.Text;
            pro.ITResources = ITResources.Text;
            pro.MaterialTechnicalSupport = MaterialTechnicalSupport.Text;
            pro.StudentRecommendations = StudentRecommendations.Text;
            pro.TeacherRecommendations = TeacherRecommendations.Text;
            string userid = User.Identity.GetUserId();

            task.UpDateSixthBlock(pro, userid);
        }

        protected void UpDateSeventhBlock_Button_Click(object sender, EventArgs e)
        {
            BaseWork task = new BaseWork();
            pro.Id = ProgramId;
            pro.DrafterStatus = DrafterStatus.Text;
            pro.DrafterName = DrafterName.SelectedItem.Text;
            pro.DrafterId = DrafterName.SelectedItem.Value;
            pro.DraftDate = Convert.ToDateTime(DraftDate.Text);
            pro.Department = DropDown_Department.SelectedItem.Text;
            pro.Protocol = Protocol.Text;
            pro.DepartmentHeadStatus = DepartmentHeadStatus.Text;
            pro.DepartmentHeadName = DepartmentHeadName.SelectedItem.Text;
            pro.DepartmentHeadId = DepartmentHeadName.SelectedItem.Value;
            pro.AgreementDepartmentHeadStatus = AgreementDepartmentHeadStatus.Text;
            pro.AgreementDepartmentHeadName = AgreementDepartmentHeadName.SelectedItem.Text;
            pro.AgreementDepartmentHeadId = AgreementDepartmentHeadName.SelectedItem.Value;
            pro.AgreementDirectorStatus = AgreementDirectorStatus.Text;
            pro.AgreementDirectorName = AgreementDirectorName.SelectedItem.Text;
            pro.AgreementDirectorId = AgreementDirectorName.SelectedItem.Value;

            List<AdditionalDrafter> drafters = new List<AdditionalDrafter>();
            int count = Convert.ToInt32(AdditionalDraftersCount.Value);
            for (int i = 0; i < count; i++)
            {
                AdditionalDrafter drafter = new AdditionalDrafter();
                Panel Bufpanel = new Panel();
                Bufpanel = (Panel)drafters_html_block.FindControl("DraftersPanel_" + i.ToString());
                TextBox textbox = new TextBox();
                DropDownList list = new DropDownList();
                textbox = (TextBox)Bufpanel.FindControl("AdditionalDrafterStatus_" + i.ToString());
                list = (DropDownList)Bufpanel.FindControl("AdditionalDrafterId_" + i.ToString());
                drafter.Status = textbox.Text;
                drafter.UserId = list.SelectedItem.Value;
                drafters.Add(drafter);
            }
            pro.AdditionalDrafters = drafters;

            task.UpDateSeventhBlock(pro, User.Identity.GetUserId());
        }

        #endregion UpDateBlocks

        #region UpDateQuestionsBlocks
        protected void UpDateTests_Button_Click(object sender, EventArgs e)
        {
            BaseWork task = new BaseWork();
            pro.Id = ProgramId;
            Questions_Test questions = new Questions_Test();
            questions.Questions = TestsFond.Text;
            questions.Developer = DeveloperTest.SelectedItem.Text;
            try
            {
                string dateInput = TestsFondDate.Text;
                DateTime d = DateTime.Parse(dateInput);
                questions.Date = d;
            }
            catch { }
            pro.Questions_Test = questions;
            task.UpDateQuestionsTestsFond(pro, User.Identity.GetUserId());
        }

        protected void UpDatePraktika_Button_Click(object sender, EventArgs e)
        {
            BaseWork task = new BaseWork();
            pro.Id = ProgramId;
            Questions_Praktika questions = new Questions_Praktika();
            questions.Questions = PraktikTopics.Text;
            questions.Developer = DeveloperPraktika.SelectedItem.Text;
            try
            {
                string dateInput = PraktikaTopicsDate.Text;
                DateTime d = DateTime.Parse(dateInput);
                questions.Date = d;
            }
            catch { }
            pro.Questions_Praktika = questions;
            task.UpDateQuestionsPraktika(pro, User.Identity.GetUserId());
        }

        protected void UpDateKT_Button_Click(object sender, EventArgs e)
        {
            BaseWork task = new BaseWork();
            pro.Id = ProgramId;
            Questions_KT questions = new Questions_KT();
            questions.Questions = KTTopics.Text;
            questions.Developer = DeveloperKT.SelectedItem.Text;
            try
            {
                string dateInput = KTDate.Text;
                DateTime d = DateTime.Parse(dateInput);
                questions.Date = d;
            }
            catch { }
            pro.Questions_KT = questions;
            task.UpDateQuestionsKT(pro, User.Identity.GetUserId());
        }

        protected void UpDateReferat_Button_Click(object sender, EventArgs e)
        {
            BaseWork task = new BaseWork();
            pro.Id = ProgramId;
            Questions_Referat questions = new Questions_Referat();
            questions.Questions = ReferatTopics.Text;
            questions.Developer = DeveloperReferat.SelectedItem.Text;
            try
            {
                string dateInput = ReferatTopicsDate.Text;
                DateTime d = DateTime.Parse(dateInput);
                questions.Date = d;
            }
            catch { }
            pro.Questions_Referat = questions;
            task.UpDateQuestionsReferat(pro, User.Identity.GetUserId());
        }

        protected void UpDateExam_Button_Click(object sender, EventArgs e)
        {
            BaseWork task = new BaseWork();
            pro.Id = ProgramId;
            Questions_Exam questions = new Questions_Exam();
            questions.Questions = ExamQuestions.Text;
            questions.Developer = DeveloperExam.SelectedItem.Text;
            try
            {
                string dateInput = ExamQuestionsDate.Text;
                DateTime d = DateTime.Parse(dateInput);
                questions.Date = d;
            }
            catch { }
            pro.Questions_Exam = questions;
            task.UpDateQuestionsExam(pro, User.Identity.GetUserId());
        }

        protected void UpDateZachet_Button_Click(object sender, EventArgs e)
        {
            BaseWork task = new BaseWork();
            pro.Id = ProgramId;
            Questions_Zachet questions = new Questions_Zachet();
            questions.Questions = ZachetQuestions.Text;
            questions.Developer = DeveloperZachet.SelectedItem.Text;
            try
            {
                string dateInput = ZachetQuestionsDate.Text;
                DateTime d = DateTime.Parse(dateInput);
                questions.Date = d;
            }
            catch { }
            pro.Questions_Zachet = questions;
            task.UpDateQuestionsZachet(pro, User.Identity.GetUserId());
        }

        protected void Update_Curs_Button_Click(object sender, EventArgs e)
        {
            BaseWork task = new BaseWork();
            pro.Id = ProgramId;
            Questions_Curs questions = new Questions_Curs();
            questions.Questions = CursesFond.Text;
            questions.Developer = DeveloperCurs.SelectedItem.Text;
            try
            {
                string dateInput = CursDate.Text;
                DateTime d = DateTime.Parse(dateInput);
                questions.Date = d;
            }
            catch { }
            pro.Questions_Curs = questions;
            task.UpDateQuestionsCursesFond(pro, User.Identity.GetUserId());
        }

        #endregion UpDateQuestionsBlocks

        //утверждение рпд
        protected void ApproveProgram_Button_Click(object sender, EventArgs e)
        {
            try
            {
                BaseWork task = new BaseWork();
                task.ApproveProgram(ProgramId);
                Response.Redirect("ProgramsViewRedactor.aspx");
            }
            catch { ApproveError.Visible = true; }
        }
        
        private void Report(int programid)
        {
            HtmlToOpenXMLBuilder builder = new HtmlToOpenXMLBuilder();
            Converter converter = new Converter();
            List<string> htmles = new List<string>();
            Pro pro = new Pro();
            BaseWork task = new BaseWork();
            pro = task.GetProgram(programid);
            htmles = converter.ToHtmlList(pro);
            builder.Build(htmles, programid);
        }

        //формирование файла рпд
        protected void Button_CreateFile_Click(object sender, EventArgs e)
        {
            try
            {
                Report(ProgramId);
            }
            catch
            {
                ErrorMes.Text = "Ошибка в генерации файла";
                ErrorMes.Visible = true;
                goto end;
            }

            try
            {
                string fileName = "Program_" + ProgramId.ToString() + ".docx";
                string url = HttpContext.Current.Server.MapPath("~/Files/" + fileName);

                FileStream MyFileStream = new FileStream(url, FileMode.Open);
                int filesizeINT = 0;
                long fileSize = MyFileStream.Length;
                filesizeINT = Convert.ToInt32(fileSize);
                byte[] Buffer = new byte[fileSize];
                MyFileStream.Read(Buffer, 0, filesizeINT);
                MyFileStream.Close();
                Response.AppendHeader("content-disposition", "attachment; filename=" + fileName);
                Response.BinaryWrite(Buffer);
                Response.End();
            }
            catch
            {
                ErrorMes.Text = "Файл создан, но произошла ошибка при его скачивании";
                ErrorMes.Visible = true;
            }
        end: { }
        }

        //открытие файла рпд
        protected void ProgramReport_Click(object sender, EventArgs e)
        {
            try
            {
                string fileName = "Program_" + ProgramId.ToString() + ".docx";
                string url = HttpContext.Current.Server.MapPath("~/Files/" + fileName);

                FileStream MyFileStream = new FileStream(url, FileMode.Open);
                int filesizeINT = 0;
                long fileSize = MyFileStream.Length;
                filesizeINT = Convert.ToInt32(fileSize);
                byte[] Buffer = new byte[fileSize];
                MyFileStream.Read(Buffer, 0, filesizeINT);
                MyFileStream.Close();
                Response.AppendHeader("content-disposition", "attachment; filename=" + fileName);
                Response.BinaryWrite(Buffer);
                Response.End();
            }
            catch
            {
                ErrorMes.Text = "Не удалось получить ссылку на файл";
                ErrorMes.Visible = true;
            }
        }

        //добавление составителя
        protected void Button_AddDrafter_Click(object sender, EventArgs e)
        {
            int count = Convert.ToInt32(AdditionalDraftersCount.Value);
            Panel newPanel = new Panel();
            newPanel.Style["margin-top"] = "2px";
            newPanel.ID = "DraftersPanel_" + count.ToString(); ;
            TextBox newTextBox = new TextBox();
            newTextBox.CssClass = "form-control";
            newTextBox.Attributes["placeholder"] = "Академ. статус";
            newTextBox.Width = 150;
            newTextBox.ID = "AdditionalDrafterStatus_" + count.ToString();

            DropDownList newList = new DropDownList();
            newList.CssClass = "form-control";
            newList.Style["margin-left"] = "3px";
            newList.DataSourceID = "SqlDataSourceUsers";
            newList.DataTextField = "UserName";
            newList.DataValueField = "UserId";
            newList.ID = "AdditionalDrafterId_" + count.ToString();

            newPanel.Controls.Add(newTextBox);
            newPanel.Controls.Add(newList);
            drafters_html_block.Controls.Add(newPanel);
            count++;
            AdditionalDraftersCount.Value = count.ToString();
        }

        //удаление составителя
        protected void Button_DeleteDrafter_Click(object sender, EventArgs e)
        {
            int count = Convert.ToInt32(AdditionalDraftersCount.Value);
            if (count != 0)
            {
                string panelid = "DraftersPanel_" + (count - 1).ToString();
                drafters_html_block.Controls.Remove(drafters_html_block.FindControl(panelid));
                count--;
                AdditionalDraftersCount.Value = count.ToString();
            }
        }

        //загрузка документа
        protected void ImportDoc_Button_Click(object sender, EventArgs e)
        {
            if ((uploader.HasFile) && (ProgramId > 0))
            {
                try
                {
                    //получаю файл в байтовом массиве
                    byte[] filearray = uploader.FileBytes;
                    string filename = "Program_" + ProgramId.ToString() + ".docx";
                    string path = HttpContext.Current.Server.MapPath("~/Files/" + filename);

                    if (File.Exists(path))
                        File.Delete(path);

                    File.WriteAllBytes(path, filearray);

                    if (HttpContext.Current.Request.Url.Host != "localhost")
                    {
                        string urlStr = "http://mpuprograms.ru/Files/" + filename;
                        BaseWork task = new BaseWork();
                        task.InsertFileURL(ProgramId, urlStr);
                    }
                }
                catch
                {
                    ErrorMes.Text = "Ошибка загрузки файла";
                    ErrorMes.Visible = true;
                }
            }
        }
    }
}