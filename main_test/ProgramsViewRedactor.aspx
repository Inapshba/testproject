﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Title="- Поиск программы" AutoEventWireup="true" CodeBehind="ProgramsViewRedactor.aspx.cs" Inherits="main_test.ProgramsViewRedactor" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent" ID="ProgramsViewRedactorContent">
    <link rel="stylesheet" href="Content/ContentFade.css" />
    <link rel="stylesheet" href="Content/CustomCheckBox.css" />
    <link rel="stylesheet" href="Content/Colored.css" />
    <link rel="stylesheet" href="Content/WorkStyle.css" />
    <script src="Scripts/ScrollTop.js"></script>
    <script src="Scripts/ScreenCheck.js"></script>
    <script type="text/javascript">
        function OnDeleteConfirm() {
            if (confirm('Переместить выбранные программы в корзину?'))
                return true;
            else
                return false;
        }
        function OnBasketConfirm() {
            if (confirm('Все программы в корзине будут удалены безвозвратно, продолжить?'))
                return true;
            else
                return false;
        }
    </script>
    <div class="body_fade">    
        <h2>Поиск программы</h2>
        <asp:Label runat="server" ID="SelectErrorMes" ForeColor="Red" Visible="false" Text="Выберите программу для редактирования" />
        <asp:Label runat="server" ID="UnKnownErrorMessage" ForeColor="Red" Visible="false" />
        <div>
            <asp:TextBox runat="server" placeholder="Поиск РПД..." ID="TextBox_Search" CssClass="form-control" />
            <asp:Button runat="server" ID="Button_Search" style="margin-bottom: 2px" CssClass="btn btn-default" Text="Найти" OnClick="Button_Search_Click" />
        </div>
        <asp:Label runat="server" ID="ConnectErrorMes" Text="Подключение к БД не выполнено" ForeColor="Red" Visible="false" />
        <div style="border: 2px solid #ddd; border-radius: 5px; height: 400px; overflow: auto;">
            <asp:CheckBoxList runat="server" CssClass="CustomCheckBox" ID="Programs_CheckBoxList" DataSourceID="SqlDataSourcePrograms" DataTextField="Viewer" DataValueField="id" />
            <asp:SqlDataSource ID="SqlDataSourcePrograms" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" ProviderName="System.Data.SqlClient" SelectCommand="SELECT *, (Discipline+' ('+CONVERT(varchar,Approve_Date,105)+')'+' ('+Status+')') AS Viewer, [Status] FROM [Programs]"></asp:SqlDataSource>
            <br />
        </div>
        <asp:Button runat="server" style="margin-top: 2px;" ID="Button_SelectAll" CssClass="btn dark" Text="Выбрать все" OnClick="Button_SelectAll_Click" />
        <asp:Button runat="server" style="margin-top: 2px;" ID="Button_UnSelectAll" CssClass="btn light" Text="Снять выделение" OnClick="Button_UnSelectAll_Click" /><br />
        <br />
        <div>
            <h3>Фильтры списка программ:</h3><br />
            <div class="row">
                <div class="col-md-3">
                     <strong>Статус: </strong>
                    <asp:DropDownList runat="server" CssClass="form-control" ID="FilterStatus_List" DataSourceID="SqlDataSourceProgramsStatuses" DataTextField="Status" DataValueField="id" OnDataBound="FilterStatus_List_DataBound" />
                    <asp:SqlDataSource ID="SqlDataSourceProgramsStatuses" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [ProgramsStatuses]"></asp:SqlDataSource>
                </div>
                <div class="col-md-2">
                    <strong>Год: </strong>
                    <asp:DropDownList runat="server" CssClass="form-control" style="margin-top:3px" ID="FilterYear_List" DataSourceID="SqlDataSourceYears" DataTextField="Year" DataValueField="Year" OnDataBound="FilterYear_List_DataBound" />
                    <asp:SqlDataSource ID="SqlDataSourceYears" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT DISTINCT YEAR(Approve_Date) as Year FROM [Programs]"></asp:SqlDataSource>
                </div>
                <div class="col-md-3">
                    <strong>Форма обучения: </strong>
                    <asp:DropDownList runat="server" CssClass="form-control" style="margin-top:3px" ID="FilterTrainingForm_List" DataSourceID="SqlDataSourceTraining_Forms" DataTextField="Name" DataValueField="id" OnDataBound="FilterTrainingForm_List_DataBound" />
                    <asp:SqlDataSource ID="SqlDataSourceTraining_Forms" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [Training_Forms]"></asp:SqlDataSource>
                </div>
            </div>
            <br />
            <strong>Профиль:</strong>
            <asp:DropDownList runat="server" CssClass="form-control" ID="FilterProfile_List" DataSourceID="SqlDataSourceProfiles" DataTextField="Name" DataValueField="Id" OnDataBound="FilterProfile_List_DataBound" /><br />
            <asp:SqlDataSource ID="SqlDataSourceProfiles" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [Profiles]"></asp:SqlDataSource>
            <br />
            <strong>Кафедра:</strong>
            <asp:DropDownList runat="server" CssClass="form-control" ID="FilterDepartment_List" DataSourceID="SqlDataSourceDepartments" DataTextField="Name" DataValueField="Id" OnDataBound="FilterDepartment_List_DataBound" /><br />
            <asp:SqlDataSource ID="SqlDataSourceDepartments" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [Departments]"></asp:SqlDataSource>
            <br />
            <strong>Дисциплина: </strong>
            <asp:DropDownList runat="server" CssClass="form-control" ID="FilterDiscipline_List" DataSourceID="SqlDataSourceDisciplines" DataTextField="Name" DataValueField="id" OnDataBound="FilterDiscipline_List_DataBound" /><br />
            <asp:SqlDataSource ID="SqlDataSourceDisciplines" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [Disciplines]"></asp:SqlDataSource>
            <br />

            <asp:Button runat="server" ID="FiltersUse_Button" CssClass="btn dark" Text="Применить фильтры" OnClick="FiltersUse_Button_Click" />
            <asp:Button runat="server" ID="FiltersClear_Button" CssClass="btn light" Text="Сбросить" OnClick="FiltersClear_Button_Click" />
        </div>
        <br />
        <div class="isDesktop" style="display: block">
             <asp:Button runat="server" CssClass="btn dark" style="margin-top:2px;" ID="Button_ShowHistory_Desktop" Text="История" OnClick="Button_ShowHistory_Click" />
             <asp:Button runat="server" CssClass="btn dark" style="margin-top:2px;" ID="Button_ProgramSendToRedactor_Desktop" Text="Редактировать" OnClick="Button_ProgramSendToRedactor_Click" />
             <asp:Button runat="server" CssClass="btn light" style="margin-top:2px;" ID="Button_ProgramCopy_Desktop" Text="Скопировать" OnClick="Button_ProgramCopy_Click" />
             <asp:Button runat="server" CssClass="btn light" style="margin-top:2px;" ID="Button_RecoverProgram_Desktop" Text="Восстановить" OnClick="Button_RecoverProgram_Click" />
             <asp:Button runat="server" CssClass="btn red" style="margin-top:2px;" ID="Button_DeleteProgram_Desktop" Text="В корзину" OnClick="Button_DeleteProgram_Click" OnClientClick="if (!OnDeleteConfirm()) return false;" />
             <asp:Button runat="server" CssClass="btn red" style="margin-top:2px;" ID="Button_ClearBasket_Desktop" Text="Очистить корзину" OnClick="Button_ClearBasket_Click" OnClientClick="if (!OnBasketConfirm()) return false;" />      
        </div>
        <div class="isMobile" style="display: none">
            <div class="container">
                <div class="row">
                    <div class="col-md-2">
                        <asp:Button runat="server" CssClass="btn dark" style="margin-top:2px; width:140px" ID="Button_ShowHistory_Mobile" Text="История" OnClick="Button_ShowHistory_Click" />
                        <asp:Button runat="server" CssClass="btn dark" style="margin-top:2px; width:140px" ID="Button_ProgramSendToRedactor_Mobile" Text="Редактировать" OnClick="Button_ProgramSendToRedactor_Click" />
                    </div>
                    <div class="col-md-2">
                        <asp:Button runat="server" CssClass="btn light" style="margin-top:2px; width:140px" ID="Button_ProgramCopy_Mobile" Text="Скопировать" OnClick="Button_ProgramCopy_Click" />
                        <asp:Button runat="server" CssClass="btn light" style="margin-top:2px; width:140px" ID="Button_RecoverProgram_Mobile" Text="Восстановить" OnClick="Button_RecoverProgram_Click" />
                    </div>
                    <div class="col-md-2">
                        <asp:Button runat="server" CssClass="btn red" style="margin-top:2px; width:140px" ID="Button_DeleteProgram_Mobile" Text="В корзину" OnClick="Button_DeleteProgram_Click" OnClientClick="if (!OnDeleteConfirm()) return false;" />
                        <asp:Button runat="server" CssClass="btn red" style="margin-top:2px; width:140px" ID="Button_ClearBasket_Mobile" Text="Очистить корзину" OnClick="Button_ClearBasket_Click" OnClientClick="if (!OnBasketConfirm()) return false;" />
                    </div>
                </div>
            </div>
        </div>
        <a href="#" class="scrollup">Наверх</a>
    </div>
</asp:Content>
