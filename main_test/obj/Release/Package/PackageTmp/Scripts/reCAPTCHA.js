﻿function captchaValid() {
    var response = grecaptcha.getResponse();
    if (response.length == 0) {
        event.preventDefault();
        alert('Установите флажок \"Я не робот\"');
    }
}