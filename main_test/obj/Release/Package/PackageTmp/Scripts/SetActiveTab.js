﻿$(function () {
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        sessionStorage.setItem('lastTab', $(this).attr('href'));
    });
    var lastTab = sessionStorage.getItem('lastTab');
    if (lastTab) {
        $('[href="' + lastTab + '"]').tab('show');
    }
});




