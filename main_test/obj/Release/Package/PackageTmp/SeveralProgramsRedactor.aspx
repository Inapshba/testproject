﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Title="- Сквозной редактор" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeBehind="SeveralProgramsRedactor.aspx.cs" Inherits="main_test.SeveralProgramsRedactor" Debug="true" ValidateRequest="false" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent" ID="DesignerContent">
    <script src="Scripts/SetActiveTab.js"></script>
    <script src="Scripts/ScrollTop.js"></script>
    <script src="Scripts/SetScrollPosition.js"></script>
    <script src="ckeditor/ckeditor.js"></script>
    <script src="ckeditor/adapters/jquery.js"></script>
    <script src="Scripts/ScreenCheck.js"></script>
    <link rel="stylesheet" href="Content/Colored.css" />
    <link rel="stylesheet" href="Content/WorkStyle.css" />
    <link rel="stylesheet" href="Content/CkeditorStyle.css" />
    <link rel="stylesheet" href="Content/Preloader.css" />

    <script type="text/javascript">
        function OnTableClearConfirm() {
            if (confirm('Очистить таблицу?'))
                return true;
            else
                return false;
        }
        function OnApproveConfirm() {
            if (confirm('Все данные верны и программу можно публиковать?'))
                return true;
            else
                return false;
        }
        function onlyNumbers(input) {
            input.value = input.value.replace(/[^\d\.]/g, "");
            if(input.value.match(/\./g).length > 1) {
            input.value = input.value.substr(0, input.value.lastIndexOf("."));
            }
        }
    </script>

    <script>
        //отоброжение прелоедера перед готовностью ckeditor к работе
        CKEDITOR.on("instanceReady", function () {
            document.getElementById('globalForm').style.display = 'block';
            document.getElementById('preloader').style.display = 'none';
            $("#globalForm").animate({opacity: "1"}, 500);
        });
    </script>

    <div id="preloader" class="body_fade" style="display:block; height:79vh;">
        <div class="cssload-loader">
            <div class="cssload-inner cssload-one"></div>
            <div class="cssload-inner cssload-two"></div>
            <div class="cssload-inner cssload-three"></div>
        </div>
    </div>

    <div id="globalForm" style="display:none">
    <h2>Сквозной редактор</h2>
    <asp:Panel runat="server" ID="programs_panel" style="margin-bottom:5px">
        <asp:Label runat="server" ID="Message" Text="Редактируемые программы:"/>
    </asp:Panel>
    <asp:Label runat="server" ID="ErrorMes" ForeColor="Red" Visible="false" />

    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" class="tabs-dark" href="#titul">Титул</a></li>
        <li><a data-toggle="tab" class="tabs-dark" href="#purposes">Цели и место</a></li>
        <li><a data-toggle="tab" class="tabs-dark" href="#discipline_structure">Структура и содержание</a></li>
        <li class="dropdown tabs-dark">
            <a data-toggle="dropdown" class="dropdown-toggle tabs-dark" href="#">
            Инструменты оценки 
            <b class="caret"></b>
            </a>
            <ul class="dropdown-menu">
                <li><a data-toggle="tab" href="#fond_evaluation_tools">Фонд оценочных средств</a></li>
                <li><a data-toggle="tab" href="#competences">Описание компетенций</a></li>
            </ul>
        </li>
        <li class="dropdown tabs-dark">
            <a data-toggle="dropdown" class="dropdown-toggle tabs-dark" href="#">
                Элементы контроля
                <b class="caret"></b>
            </a>
            <ul class="dropdown-menu">
                <li><a data-toggle="tab" class="tabs-dark" href="#zachet">Зачёт</a></li>
                <li><a data-toggle="tab" class="tabs-dark" href="#exam">Экзамен</a></li>
                <li><a data-toggle="tab" class="tabs-dark" href="#referat">Реферат</a></li>
                <li><a data-toggle="tab" class="tabs-dark" href="#kt">КТ</a></li>
                <li><a data-toggle="tab" class="tabs-dark" href="#prakrika">Практика</a></li>
                <li><a data-toggle="tab" class="tabs-dark" href="#test">Тест</a></li>
                <li><a data-toggle="tab" class="tabs-dark" href="#curs">Курсовые работы</a></li>
            </ul>
        </li>
        <li><a data-toggle="tab" class="tabs-dark" href="#discipline_provide">Обеспечение</a></li>
        <li><a data-toggle="tab" class="tabs-dark" href="#persons">Составители</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="titul">
            <h3>Титул</h3><br />
            <strong>Институт:</strong><br/>
            <div class="check_screen">
                <asp:TextBox runat="server" id="Institute" TextMode="MultiLine" CssClass="ckeditor"  />
            </div>
            <asp:Button runat="server" ID="Button_Institute" CssClass="btn light" style="margin-top: 2px" Text="Правка" OnClick="Button_Institute_Click" /><br/>
            <br />
            <strong>Должность утвердителя: </strong><br />
            <asp:TextBox runat="server" id="Approver_Post" CssClass="form-control" placeholder="Введите должность..."/>
            <asp:Button runat="server" ID="Button_ApproverPost" CssClass="btn light" style="margin-bottom: 2px" Text="Правка" OnClick="Button_ApproverPost_Click" /><br/>
            <br />
            <strong>Утвердитель: </strong><br />
            <asp:DropDownList runat="server" ID="Approver_Name" CssClass="form-control" DataSourceID="SqlDataSourceUsers" DataTextField="UserName" DataValueField="UserId" />
            <asp:SqlDataSource ID="SqlDataSourceUsers" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" 
                    SelectCommand="SELECT * FROM AspNetUsers INNER JOIN AspNetUserRoles ON AspNetUsers.Id=AspNetUserRoles.UserId INNER JOIN AspNetRoles ON AspNetRoles.Id = AspNetUserRoles.RoleId WHERE AspNetRoles.Name!='Guest' ORDER BY UserName"></asp:SqlDataSource>
            <asp:Button runat="server" ID="Button_ApproverName" CssClass="btn light" style="margin-bottom: 2px" Text="Правка" OnClick="Button_ApproverName_Click" /><br />
            <br />
            <strong>Дата: </strong><br />
            <asp:TextBox runat="server" id="Approve_Date" CssClass="form-control" TextMode="Date"/>
            <asp:Button runat="server" ID="Button_ApproveDate" CssClass="btn light" style="margin-bottom: 2px" Text="Правка" OnClick="Button_ApproveDate_Click" /><br/>
            <br />
            <strong>Рабочая программа дисциплины: </strong><br/>
            <asp:DropDownList runat="server" ID="Discipline" CssClass="form-control" DataSourceID="SqlDataSourceDisciplines" DataTextField="Name" DataValueField="Name"  />
            <asp:SqlDataSource ID="SqlDataSourceDisciplines" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [Disciplines]"></asp:SqlDataSource>
            <asp:Button runat="server" ID="Button_Discipline" CssClass="btn light" style="margin-bottom: 2px" Text="Правка" OnClick="Button_Discipline_Click" /><br />
            <br />
            <strong>Направление подготовки:</strong><br/>
            <asp:DropDownList runat="server" ID="Training_Direction" CssClass="form-control" DataSourceID="SqlDataSourceTrainingDirections" DataTextField="Viewer" DataValueField="Viewer" />
            <asp:SqlDataSource ID="SqlDataSourceTrainingDirections" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT *, (Designation+' ('+ Code+')') AS Viewer FROM [Training_Directions]"></asp:SqlDataSource>
            <asp:Button runat="server" ID="Button_TrainingDirection" CssClass="btn light" style="margin-bottom: 2px" Text="Правка" OnClick="Button_TrainingDirection_Click" /><br />
            <br/>
            <strong>Профиль:</strong><br/>
            <asp:DropDownList runat="server" ID="DropDown_Profil" CssClass="form-control" DataSourceID="SqlDataSourceProfiles" DataTextField="Name" DataValueField="Id" />
            <asp:SqlDataSource runat="server" ID="SqlDataSourceProfiles" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [Profiles]"  />
            <asp:Button runat="server" ID="Button_Profil" CssClass="btn light" style="margin-bottom: 2px" Text="Правка" OnClick="Button_Profil_Click" /><br />
            <br />
            <strong>Квалификация (степень) выпусника:</strong><br/>
            <asp:DropDownList runat="server" ID="Graduate_Qualification" CssClass="form-control" DataSourceID="SqlDataSourceQualifications" DataTextField="Name" DataValueField="Name" />
            <asp:SqlDataSource ID="SqlDataSourceQualifications" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [Qualifications]"></asp:SqlDataSource>
            <asp:Button runat="server" ID="Button_GraduateQualification" CssClass="btn light" style="margin-bottom: 2px" Text="Правка" OnClick="Button_GraduateQualification_Click" /><br />
            <br />
            <strong>Форма обучения:</strong><br/>
            <asp:DropDownList runat="server" id="Training_Form" CssClass="form-control" >
                <asp:ListItem>Очная</asp:ListItem>
                <asp:ListItem>Очно-заочная</asp:ListItem>
                <asp:ListItem>Заочная</asp:ListItem>
                <asp:ListItem>Экстернат</asp:ListItem>
            </asp:DropDownList>
            <asp:Button runat="server" ID="Button_TrainingForm" CssClass="btn light" style="margin-bottom: 2px" Text="Правка" OnClick="Button_TrainingForm_Click" /><br />
            <br/>
            <strong>Нормативный срок обучения:</strong><br />
            <asp:TextBox runat="server" ID="StudyPeriod" CssClass="form-control" onkeyup="return onlyNumbers(this);" onchange="return onlyNumbers(this);" />
            <asp:Button runat="server" ID="Button_StudyPeriod" CssClass="btn light" style="margin-bottom: 2px" Text="Правка" OnClick="Button_StudyPeriod_Click" /><br />
            <br />
            <strong>Город:</strong><br/>
            <asp:TextBox runat="server" id="City" CssClass="form-control" placeholder="Название города..." />
            <asp:Button runat="server" ID="Button_City" CssClass="btn light" style="margin-bottom: 2px" Text="Правка" OnClick="Button_City_Click" />
        </div>
        
        <div class="tab-pane fade" id="purposes">
            <h3>Цели</h3><br />
            <h4>Цели освоения дисциплины:</h4>
            <div class="check_screen">
                <asp:TextBox runat="server" id="ObjectivesOfDiscipline" CssClass="ckeditor" TextMode="MultiLine" />
            </div>
            <asp:Button runat="server" ID="Button_ObjectivesOfDiscipline" CssClass="btn light" style="margin-top: 2px" Text="Правка" OnClick="Button_ObjectivesOfDiscipline_Click" /><br />
            <br />
            <h4>Место дисциплины в структуре ООП аспирантуры:</h4>
            <div class="check_screen">
                <asp:TextBox runat="server" id="DisciplineInOOP" CssClass="ckeditor" TextMode="MultiLine" />
            </div>
            <asp:Button runat="server" ID="Button_DisciplineInOOP" CssClass="btn light" style="margin-top: 2px" Text="Правка" OnClick="Button_DisciplineInOOP_Click" /><br />
            <br />
            <%--<strong>Перечень планируемых результатов обучения по дисциплине (модулю),<br />
                соотнесенные с планируемыми результатами освоения образовательной программы:</strong><br />
            <div class="check_screen">
                <asp:TextBox runat="server" id="PlanDescription" CssClass="ckeditor" TextMode="MultiLine" />
            </div>
            <asp:Button runat="server" ID="Button_PlanDescription" CssClass="btn light" Text="Правка" OnClick="Button_PlanDescription_Click" /><br />
            <br />--%>
            <div>
                <asp:Table ID="PlanTable" runat="server" CssClass="table table-bordered" BorderWidth="1" GridLines="Both">
                    <asp:TableHeaderRow>
                          <asp:TableHeaderCell Text="Код компетенции" Width="10%" />
                          <asp:TableHeaderCell Text="В результате освоения образовательной программы обучающийся должен обладать" width="45%"/>
                          <asp:TableHeaderCell Text="Перечень планируемых результатов обучения по дисциплине"  width="45%"/>
                    </asp:TableHeaderRow>
                </asp:Table>
                <br />
                <asp:HiddenField runat="server" ID="ptrowscount" Value="0" />
               
                <asp:Table ID="PlanTable_control" CssClass="table table-bordered" runat="server" BorderWidth="1" GridLines="Both">
                    <asp:TableHeaderRow>
                          <asp:TableHeaderCell style="text-align: center" Text="Код..." ForeColor="Gray" Width="10%" />
                          <asp:TableHeaderCell style="text-align: center" Text="Результаты..." ForeColor="Gray" width="45%"/>
                    </asp:TableHeaderRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:DropDownList runat="server" CssClass="form-control" ID="Сompetence_PlanTable" DataSourceID="SqlDataSourceCompetences" DataTextField="Code" DataValueField="Description"  />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="ckeditor" TextMode="MultiLine" id="Enumeration_PlanTable" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
             <asp:SqlDataSource ID="SqlDataSourceCompetences" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [Competences]"></asp:SqlDataSource>
                <asp:Button runat="server" CssClass="btn light" style="margin-top: 2px" ID="CreateRowPlanTable" Text="Добавить" OnClick="CreateRowPlanTable_Click" />
                <asp:Button runat="server" CssClass="btn light" style="margin-top: 2px" ID="DeleteRowPlanTable" Text="Удалить" OnClick="DeleteRowPlanTable_Click" />
                <asp:Button runat="server" CssClass="btn light" style="margin-top: 2px" ID="Button_PlanTable" Text="Правка" OnClick="Button_PlanTable_Click" />
                
            </div>
        </div>

        <div class="tab-pane fade" id="discipline_structure">
            <h3>Структура</h3><br />
            <asp:Table runat="server" ID="DisciplineStructureTable" CssClass="table table-bordered" contenteditable="true" BorderWidth="1" GridLines="Both" >
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell text="n/n" RowSpan="2" />
                    <asp:TableHeaderCell text="Раздел" RowSpan="2"/>
                    <asp:TableHeaderCell text="Семестр" RowSpan="2"/>
                    <asp:TableHeaderCell text="Неделя семестра" RowSpan="2"/>
                    <asp:TableHeaderCell text="Виды учебной работы, включая самостоятельную работу студентов, и трудоемкость в часах" ColumnSpan="5"/>
                    <asp:TableHeaderCell text="Виды самостоятельной работы студентов" ColumnSpan="6"/>
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell text="Л"/>
                    <asp:TableHeaderCell text="П/С"/>
                    <asp:TableHeaderCell text="Лаб"/>
                    <asp:TableHeaderCell text="СРС"/>
                    <asp:TableHeaderCell text="КСР"/>
                    <asp:TableHeaderCell text="К.Р"/>
                    <asp:TableHeaderCell text="К.П"/>
                    <asp:TableHeaderCell text="РГР"/>
                    <asp:TableHeaderCell text="Под. к л/р"/>
                    <asp:TableHeaderCell text="К/р"/>
                    <asp:TableHeaderCell text="Форма аттестации"/>
                </asp:TableHeaderRow>
            </asp:Table>
            <br />
            <asp:HiddenField runat="server" ID="DSTrowscount" Value="0" />
            <strong style="color: darkred">Будьте внимательны при заполнении таблицы. После сортировки редактировать данные невозможно!</strong><br />
            <div style="border: 1px solid #ddd; border-radius: 5px">
                <asp:Table runat="server" ID="DisciplineStructureControlTable" style="border: 1px solid #ddd;" BorderWidth="1" GridLines="Both">
                    <asp:TableHeaderRow>
                            <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" text="Тема..."/>
                            <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" text="Семестр..."/>
                            <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" text="Неделя..." />
                            <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" text="Л..."/>
                            <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" text="П/С..."/>
                            <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" text="Лаб..."/>
                            <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" text="СРС..."/>
                            <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" text="КСР..."/>
                            <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" text="К.Р..."/>
                            <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" text="К.П..."/>
                            <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" text="РГР..."/>
                            <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" text="Под. к л/р..."/>
                            <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" text="К/р..."/>
                            <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" text="Форма аттестации..."/>
                        </asp:TableHeaderRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" ID="DSCT_topic" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" ID="DSCT_sem" TextMode="Number" Width="100%" min="0"/>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" ID="DSCT_week" TextMode="Number" Width="100%" min="0" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" ID="DSCT_L" TextMode="Number" Width="100%" min="0" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" ID="DSCT_PS" TextMode="Number" Width="100%" min="0" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" ID="DSCT_Lab" TextMode="Number" Width="100%" min="0" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" ID="DSCT_SRS" TextMode="Number" Width="100%" min="0" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" ID="DSCT_KSR"  TextMode="Number" Width="100%" min="0" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" ID="DSCT_KR" TextMode="Number" Width="100%" min="0" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" ID="DSCT_KP" TextMode="Number" Width="100%" min="0" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" ID="DSCT_RGR" TextMode="Number" Width="100%"  min="0"/>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" ID="DSCT_preptolr"  Width="100%"  />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" ID="DSCT_KRend" TextMode="Number" Width="100%" min="0" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:DropDownList runat="server" CssClass="form-control" ID="DSCT_AttestatForm">
                                <asp:ListItem Text="Зачет" />
                                <asp:ListItem Text="Экзамен" />
                            </asp:DropDownList>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <br />
                <div style="width: 35%">
                    <asp:Button runat="server" CssClass="btn light" Width="40%" ID="AddRowDSCT_Button" Text="Добавить" OnClick="AddRowDSCT_Button_Click" />
                    <asp:Button runat="server" CssClass="btn light" Width="40%" ID="DeleteRowDSCT_Button" Text="Удалить" OnClick="DeleteRowDSCT_Button_Click" />
                    <asp:TextBox runat="server" CssClass="form-control" Width="15%" ID="rowNumberDSCT" TextMode="Number" />
                    <br />
                    <asp:Button runat="server" CssClass="btn light" Width="40%" ID="SortDSCT_Button" Text="Сортировать" OnClick="SortDSCT_Button_Click" />
                    <asp:Button runat="server" CssClass="btn light" Width="56%" ID="DSTClear_Button" Text="Заполнить таблицу заново" OnClick="DSTClear_Button_Click" OnClientClick="if (!OnTableClearConfirm()) return false;" />
                    <br />
                    <asp:Button runat="server" CssClass="btn light" ID="Button_DSCT" Width="40%" Text="Правка" OnClick="Button_DSCT_Click" /><br>
                </div>
            </div>
            <br />
            <h4>Содержание дисциплины:</h4>
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" id="DisciplineStructureContent" TextMode="MultiLine" />
            </div>
            <asp:Button runat="server" CssClass="btn light" style="margin-top:3px" ID="DisciplineStructureContentFill_Button" Text="Заполнить" OnClick="DisciplineStructureContentFill_Button_Click" />
            <asp:Button runat="server" CssClass="btn light" style="margin-top:3px" ID="Button_DisciplineStructureContent" Text="Правка" OnClick="Button_DisciplineStructureContent_Click" /><br />
            <br />
            <h4>Содержание разделов дисциплины:</h4>
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" ID="SemesteresContent" TextMode="MultiLine"  />
            </div>
            <asp:Button runat="server" CssClass="btn light" style="margin-top:3px" ID="SemesteresContentFill_Button" Text="Заполнить" OnClick="SemesteresContentFill_Button_Click" />
            <asp:Button runat="server" CssClass="btn light" style="margin-top:3px" ID="Button_SemesteresContent" Text="Правка" OnClick="Button_SemesteresContent_Click" /><br />
            <br />
            <h4>Образовательные технологии:</h4>
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" id="EducationalTechnology" TextMode="MultiLine" />
            </div>
            <asp:Button runat="server" CssClass="btn light" style="margin-top:3px" ID="Button_EducationalTechnology" Text="Правка" OnClick="Button_EducationalTechnology_Click" /><br />
            <br />
            <h4>Оценочные средства для текущего контроля успеваемости:</h4>
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" ID="MonitoringTools_Description" TextMode="MultiLine" />
            </div>
            <asp:Button runat="server" CssClass="btn light" style="margin-top:3px" ID="Button_MonitoringToolsDescription" Text="Правка" OnClick="Button_MonitoringToolsDescription_Click" />
        </div>

        <div class="tab-pane fade" id="fond_evaluation_tools">
            <h3>Фонд оценочных средств</h3><br />
            <h4>Перечень оценочных средств</h4>
            <asp:Table runat="server" ID="PrilAssessTable" CssClass="table table-bordered" BorderWidth="1" GridLines="Both">
                 <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="№ ОС" Width="5%"/>
                    <asp:TableHeaderCell Text="Наименование оценочного средства" Width="30%" />
                    <asp:TableHeaderCell Text="Краткая характеристика оценочного средства" Width="30%" />
                    <asp:TableHeaderCell Text="Представление оценочного средства в ФОС" Width="30%" />
                </asp:TableHeaderRow>
            </asp:Table>
            <asp:HiddenField runat="server" ID="PATrowscount" Value="0" />
            <div style="border: 1px solid #ddd; border-radius: 5px">
                <asp:Table runat="server" CssClass="table table-bordered" BorderWidth="1" GridLines="Both">
                    <asp:TableRow>
                        <asp:TableCell Width="35%">
                            <asp:TextBox id="PATName" CssClass="form-control" Width="100%" runat="server" placeholder="Оценочное средство..." TextMode="MultiLine" />
                        </asp:TableCell>
                        <asp:TableCell Width="30%">
                            <asp:TextBox id="PATSpec" runat="server" CssClass="form-control" Width="100%" placeholder="Краткая характеристика..." TextMode="MultiLine" />
                        </asp:TableCell>
                        <asp:TableCell Width="30%">
                            <asp:TextBox id="PATinFOS" runat="server" CssClass="form-control" Width="100%" placeholder="Представление в ФОС..." TextMode="MultiLine" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Button runat="server" CssClass="btn light" ID="button_addRowInPAT" Text="Добавить" OnClick="button_addRowInPAT_Click" />
                <asp:Button runat="server" CssClass="btn light" ID="button_deleteRowInPAT" Text="Удалить" OnClick="button_deleteRowInPAT_Click" />
                <asp:Button runat="server" CssClass="btn light" ID="Button_PrilAssessTable" Text="Правка" OnClick="Button_PrilAssessTable_Click" />
            </div>
            <br />
            <em>Перечень компетенций с указанием этапов их формирования в процессе освоения образовательной программы.<br />
                В результате освоения дисциплины (модуля) формируются следующие компетенции:</em><br />
            <asp:Table ID="MonitoringTools_FondTable" CssClass="table table-bordered" runat="server" BorderWidth="1" GridLines="Both">
                <asp:TableRow>
                    <asp:TableHeaderCell Width="5%" Text="Код компетенции"  />
                    <asp:TableHeaderCell width="90%" Text="В результате освоения образовательной программы обучающийся должен обладать" />
                </asp:TableRow>
            </asp:Table>
            <asp:DropDownList runat="server" CssClass="form-control" ID="Competence_FondTable" DataSourceID="SqlDataSourceCompetences" DataTextField="Code" DataValueField="Id"  />
            <br />
            <asp:HiddenField runat="server" ID="ftrowscount" Value="0" />
            <asp:Button runat="server" CssClass="btn light" style="margin-top: 2px" ID="CreateRowFondTable" Text="Добавить" OnClick="CreateRowFondTable_Click" />
            <asp:Button runat="server" CssClass="btn light" style="margin-top: 2px" ID="DeleteRowFondTable" Text="Удалить" OnClick="DeleteRowFondTable_Click" />
            <asp:Button runat="server" CssClass="btn light" style="margin-top: 2px" ID="Button_MonitoringTools_FondTable" Text="Правка" OnClick="Button_MonitoringTools_FondTable_Click" />
           
            <br /><br />

            <strong>Описание показателей и критериев оценивания компетенций, <br />
                формируемых по итогам освоения дисциплины (модуля), описание шкал оценивания</strong><br />

            <asp:Table ID="CriterionsTable" CssClass="table table-bordered" runat="server" BorderWidth="1" GridLines="Both">
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell  RowSpan="2" text="Описание компетенции" width="200px"/>
                    <asp:TableHeaderCell  RowSpan="2" text="Показатель" width="200px"/>
                    <asp:TableHeaderCell ColumnSpan="4" text="Критерий оценивания" Width="600px"/>
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="2" />
                    <asp:TableHeaderCell Text="3" />
                    <asp:TableHeaderCell Text="4" />
                    <asp:TableHeaderCell Text="5" />
                </asp:TableHeaderRow>
            </asp:Table>
            <asp:DropDownList runat="server" CssClass="form-control" style="margin-left:15px; margin-bottom:5px" ID="Competence_CriterionsTable" DataSourceID="SqlDataSourceCompetences" DataTextField="Code" DataValueField="Description" />
            <br />
            <asp:HiddenField runat="server" ID="CTrowscount" Value="1" />
           
            <div class="container">
                <div class="row">
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control" Width="100%" style="max-width:100%" id="IndicatorCriterionsTable_Know" runat="server"   placeholder="Знать..."  /></div>
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control" Width="100%" style="max-width:100%" id="CriterionCriterionsTable_Know_2" runat="server" placeholder="На 2..." /></div>
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control" Width="100%" style="max-width:100%" id="CriterionCriterionsTable_Know_3" runat="server" placeholder="На 3..." /></div>
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control" Width="100%" style="max-width:100%" id="CriterionCriterionsTable_Know_4" runat="server" placeholder="На 4..." /></div>
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control" Width="100%" style="max-width:100%" id="CriterionCriterionsTable_Know_5" runat="server" placeholder="На 5..." /></div>
                </div>
                <div class="row">
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control" Width="100%" style="max-width:100%" id="IndicatorCriterionsTable_Can" runat="server"    placeholder="Уметь..."/></div>
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control" Width="100%" style="max-width:100%" id="CriterionCriterionsTable_Can_2" runat="server"  placeholder="На 2..."/></div>
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control" Width="100%" style="max-width:100%" id="CriterionCriterionsTable_Can_3" runat="server"  placeholder="На 3..."/></div>
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control" Width="100%" style="max-width:100%" id="CriterionCriterionsTable_Can_4" runat="server"  placeholder="На 4..."/></div>
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control" Width="100%" style="max-width:100%" id="CriterionCriterionsTable_Can_5" runat="server"  placeholder="На 5..."/></div>
                </div>
                <div class="row">
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control" Width="100%" style="max-width:100%" id="IndicatorCriterionsTable_Wield" runat="server"  placeholder="Владеть..."/></div>
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control" Width="100%" style="max-width:100%" id="CriterionCriterionsTable_Wield_2" runat="server" placeholder="На 2..." /></div>
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control" Width="100%" style="max-width:100%" id="CriterionCriterionsTable_Wield_3" runat="server" placeholder="На 3..." /></div>
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control" Width="100%" style="max-width:100%" id="CriterionCriterionsTable_Wield_4" runat="server" placeholder="На 4..." /></div>
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control" Width="100%" style="max-width:100%" id="CriterionCriterionsTable_Wield_5" runat="server" placeholder="На 5..." /></div>
                </div>
            </div>    
            <asp:Button runat="server" CssClass="btn light" style="margin-top: 5px; margin-left:15px;" ID="CreateRowsCriterionsTable" Text="Добавить" OnClick="CreateRowsCriterionsTable_Click" />
            <asp:Button runat="server" CssClass="btn light" style="margin-top: 5px;" ID="DeleteRowsCriterionsTable" Text="Удалить" OnClick="DeleteRowsCriterionsTable_Click" />
            <asp:Button runat="server" CssClass="btn light" style="margin-top: 5px;" ID="Button_CriterionsTable" Text="Правка" OnClick="Button_CriterionsTable_Click" />
            
            <br /><br />

            <h4 class="unselecteble">Форма промежуточной аттестации: зачет</h4>
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" id="ZachetDescription" TextMode="MultiLine" />
            </div>
            <br />
            <em class="unselecteble">Зачтено:</em><br />
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" ID="Zachteno" TextMode="MultiLine"/>
            </div>
            <br />
            <em class="unselecteble">Не зачтено:</em><br />
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" id="NeZachteno" TextMode="MultiLine"/>
            </div>
            <asp:Button runat="server" CssClass="btn light" style="margin-top:3px" ID="Button_Zachet" Text="Внести правки в зачёт" OnClick="Button_Zachet_Click" />

            <br /><br />
            
            <h4 class="unselecteble">Форма промежуточной аттестации: экзамен</h4>
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" id="ExamDescription" TextMode="MultiLine"/>
            </div>
            <br />
            <em class="unselecteble">Отлично:</em><br />
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" ID="AttestatGreat" TextMode="MultiLine"/>
            </div>
            <br />
            <em class="unselecteble">Хорошо:</em><br />
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" id="AttestatGood" TextMode="MultiLine"/>
            </div>
            <br />
            <em class="unselecteble">Удовлетворительно:</em><br />
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" id="AttestatSatisfactorily" TextMode="MultiLine"/>
            </div>
            <br />
            <em class="unselecteble">Неудовлетворительно:</em><br />
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" id="AttestatNotSatisfactorily" TextMode="MultiLine"/>
            </div>
            <asp:Button runat="server" CssClass="btn light" style="margin-top:3px" ID="Button_Exam" Text="Внести правки в экзамен" OnClick="Button_Exam_Click" />
        </div>

        <div class="tab-pane fade" id="discipline_provide">
            <h3>Учебно-методическое и информационное обеспечение дисциплины</h3><br />
            <h4>Основная литература: </h4>
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" id="BasicLiterature" TextMode="MultiLine" />
            </div>
            <asp:Button runat="server" CssClass="btn light" ID="Button_BasicLiterature" style="margin-top: 3px" Text="Правка" OnClick="Button_BasicLiterature_Click" /><br />
            <br />
            <h4>Дополнительная литература: </h4>
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" id="AdditionalLiterature" TextMode="MultiLine" />
            </div>
            <asp:Button runat="server" CssClass="btn light" ID="Button_AdditionalLiterature" style="margin-top: 3px" Text="Правка" OnClick="Button_AdditionalLiterature_Click" /><br />
            <br />
            <h4>Программное обеспечение и интернет-ресурсы: </h4>
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" id="ITResources" TextMode="MultiLine" />
            </div>
            <asp:Button runat="server" CssClass="btn light" ID="Button_ITResources" style="margin-top: 3px" Text="Правка" OnClick="Button_ITResources_Click" /><br />
            <br />
            <h4>Материально-техническое обеспечение дисциплины: </h4>
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" ID="MaterialTechnicalSupport" TextMode="MultiLine"/>
            </div>
            <asp:Button runat="server" CssClass="btn light" ID="Button_MaterialTechnicalSupport" style="margin-top: 3px" Text="Правка" OnClick="Button_MaterialTechnicalSupport_Click" /><br />
            <br />
            <h4>Методические рекомендации для студентов:</h4>
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" ID="StudentRecommendations" TextMode="MultiLine"/>
            </div>
            <asp:Button runat="server" CssClass="btn light" ID="Button_StudentRecommendations" style="margin-top: 3px" Text="Правка" OnClick="Button_StudentRecommendations_Click" /><br />
            <br />
            <h4>Методические рекомендации для преподавателя:</h4>
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" ID="TeacherRecommendations" TextMode="MultiLine"/>
            </div>
            <asp:Button runat="server" CssClass="btn light" ID="Button_TeacherRecommendations" style="margin-top: 3px" Text="Правка" OnClick="Button_TeacherRecommendations_Click" />
        </div>

        <div class="tab-pane fade" id="competences">
            <h3>Описание компетенций</h3><br />
            <asp:Table runat="server" CssClass="table table-bordered" ID="PrilFondTable_1" BorderWidth="1" GridLines="Both">
                <asp:TableRow>
                    <asp:TableCell Text="В процессе освоения данной дисциплины студент формирует 
                        и демонстрирует следующие универсальные компетенции:" ColumnSpan="6" />
                </asp:TableRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="Компетенции" ColumnSpan="2"/>
                    <asp:TableHeaderCell Text="Перечень компонентов" RowSpan="2"/>
                    <asp:TableHeaderCell Text="Технология формирования компетенции" RowSpan="2" />
                    <asp:TableHeaderCell Text="Форма оценочного средства" RowSpan="2" />
                    <asp:TableHeaderCell Text="Степени уровней освоения компетенций" RowSpan="2" />
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="Индекс" />
                    <asp:TableHeaderCell Text="Формулировка" />
                </asp:TableHeaderRow>
            </asp:Table>
            <asp:HiddenField runat="server" ID="PFT_1rowscount" Value="0" />
            <asp:Table runat="server" CssClass="table table-bordered" ID="PrilFondTable_2" BorderWidth="1" GridLines="Both">
                <asp:TableRow>
                    <asp:TableCell Text="В процессе освоения данной дисциплины студент формирует 
                        и демонстрирует следующие общепрофессиональные компетенции:" ColumnSpan="6" />
                </asp:TableRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="Компетенции" ColumnSpan="2"/>
                    <asp:TableHeaderCell Text="Перечень компонентов" RowSpan="2"/>
                    <asp:TableHeaderCell Text="Технология формирования компетенции" RowSpan="2" />
                    <asp:TableHeaderCell Text="Форма оценочного средства" RowSpan="2" />
                    <asp:TableHeaderCell Text="Степени уровней освоения компетенций" RowSpan="2" />
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="Индекс" />
                    <asp:TableHeaderCell Text="Формулировка" />
                </asp:TableHeaderRow>
            </asp:Table>
            <asp:HiddenField runat="server" ID="PFT_2rowscount" Value="0" />
            <asp:Table runat="server" CssClass="table table-bordered" ID="PrilFondTable_3" BorderWidth="1" GridLines="Both">
                <asp:TableRow>
                    <asp:TableCell Text="В процессе освоения данной дисциплины студент формирует 
                        и демонстрирует следующие профессиональные компетенции:" ColumnSpan="6" />
                </asp:TableRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="Компетенции" ColumnSpan="2"/>
                    <asp:TableHeaderCell Text="Перечень компонентов" RowSpan="2"/>
                    <asp:TableHeaderCell Text="Технология формирования компетенции" RowSpan="2" />
                    <asp:TableHeaderCell Text="Форма оценочного средства" RowSpan="2" />
                    <asp:TableHeaderCell Text="Степени уровней освоения компетенций" RowSpan="2" />
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="Индекс" />
                    <asp:TableHeaderCell Text="Формулировка" />
                </asp:TableHeaderRow>
            </asp:Table>
            <asp:HiddenField runat="server" ID="PFT_3rowscount" value="0" />


            <div style="border: 1px solid #ddd; border-radius: 5px">
                <asp:DropDownList runat="server" CssClass="form-control" ID="PFTCom" DataSourceID="SqlDataSourceCompetences" DataTextField="Code" DataValueField="Description" />
                <asp:Table runat="server" CssClass="table table-bordered" ID="DataRowInPrilFondTables" BorderWidth="1" GridLines="Both">
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" Text="Перечень компонентов..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" Text="Технология формирования компетенции..."/>
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" Text="Форма оценочного средства..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" Text="Степени уровней освоения компетенций..." />
                    </asp:TableHeaderRow>
                    <asp:TableRow>
                       <asp:TableCell>
                            <asp:TextBox id="PFTKomponentsList" CssClass="form-control" Width="100%" TextMode="MultiLine" runat="server"/>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox id="PFTCompetenceTechnology" CssClass="form-control" Width="100%" TextMode="MultiLine" runat="server" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox id="PFTAssessForm" runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox id="PFTSteps" runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>

                <asp:Button runat="server" CssClass="btn light" Width="25%" ID="button_createRowInPFT_1" Text="Добавить в универсальные" OnClick="button_createRowInPFT_1_Click" />
                <asp:Button runat="server" CssClass="btn light" Width="25%" ID="button_createRowInPFT_2" Text="Добавить в общепрофессиональные" OnClick="button_createRowInPFT_2_Click" />
                <asp:Button runat="server" CssClass="btn light" Width="25%" ID="button_createRowInPFT_3" Text="Добавить в профессиональные" OnClick="button_createRowInPFT_3_Click" />
                <br />
                <asp:Button runat="server" CssClass="btn light" Width="25%" ID="button_deleteRowInPFT_1" Text="Удалить из универсальных" OnClick="button_deleteRowInPFT_1_Click" />
                <asp:Button runat="server" CssClass="btn light" Width="25%" ID="button_deleteRowInPFT_2" Text="Удалить из общепрофессиональных" OnClick="button_deleteRowInPFT_2_Click" />
                <asp:Button runat="server" CssClass="btn light" Width="25%" ID="button_deleteRowInPFT_3" Text="Удалить из профессиональных" OnClick="button_deleteRowInPFT_3_Click" />
                <br />
                <asp:Button runat="server" ID="Button_PFT1" CssClass="btn light" Width="25%" Text="Правка УК" OnClick="Button_PFT1_Click" />
                <asp:Button runat="server" ID="Button_PFT2" CssClass="btn light" Width="25%" Text="Правка ОПК" OnClick="Button_PFT2_Click" />
                <asp:Button runat="server" ID="Button_PFT3" CssClass="btn light" Width="25%" Text="Правка ПК" OnClick="Button_PFT3_Click" />
            </div>
        </div>


        <div class="tab-pane fade" id="zachet">
            <h3>Элементы контроля</h3>
            <h4>Зачёт:</h4>
            <asp:Table runat="server" CssClass="table table-bordered" ID="PrilCompetenceTable_Zachet" BorderWidth="1" GridLines="Both">
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="Описание" RowSpan="3" />
                    <asp:TableHeaderCell Text="Контролируемый результат обучения" RowSpan="3" />
                    <asp:TableHeaderCell Text="Контролируемые темы (разделы) дисциплины" RowSpan="3" />
                    <asp:TableHeaderCell Text="Недифференцированный зачет" ColumnSpan="2" />
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="Критерии оценивания" ColumnSpan="2"/>
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="Зачтено" />
                    <asp:TableHeaderCell Text="Не зачтено" />
                </asp:TableHeaderRow>
            </asp:Table>
            <asp:HiddenField runat="server" ID="PCT_zachetrowscount" Value="0" />
            <div>
                <asp:Table runat="server" CssClass="table table-bordered" BorderWidth="1" GridLines="Both">
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Описание..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Результат..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Темы..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Зачтено при..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Не зачтено при..." />
                    </asp:TableHeaderRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:DropDownList runat="server" ID="DropDown_PCTZ" CssClass="form-control" DataSourceID="SqlDataSourceCompetences" DataTextField="Code" DataValueField="Description" />
                            <%--<asp:TextBox runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" id="PCTZDiscription"/>--%>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" id="PCTZRezult" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" id="PCTZTopics" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" id="PCTZZachet" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" id="PCTZNeZachet" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Button runat="server" CssClass="btn light" ID="addRowInPCTzachet" Text="Добавить" OnClick="addRowInPCTzachet_Click" />
                <asp:Button runat="server" CssClass="btn light" ID="deleteRowInPCTzachet" Text="Удалить" OnClick="deleteRowInPCTzachet_Click" />
                <asp:Button runat="server" CssClass="btn light" ID="Button_ZachetTab" Text="Правка" OnClick="Button_ZachetTab_Click" />
            </div>
            <br />
            <strong>Вопросы к зачёту:</strong><br />
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" ID="ZachetQuestions" TextMode="MultiLine"/>
            </div>
            <strong>Составитель:</strong> 
            <asp:DropDownList runat="server" CssClass="form-control" ID="DeveloperZachet" DataSourceID="SqlDataSourceUsers" DataTextField="UserName" DataValueField="UserId" />
            <strong>Дата:</strong> 
            <asp:TextBox runat="server" CssClass="form-control" ID="ZachetQuestionsDate" TextMode="Date" /><br />
            <asp:Button runat="server" CssClass="btn light" ID="Button_ZachetQ" Text="Правка" OnClick="Button_ZachetQ_Click" />
        </div>

        <div class="tab-pane fade" id="exam">
            <h3>Элементы контроля</h3>
            <h4>Экзамен:</h4>
            <asp:Table runat="server" CssClass="table table-bordered" ID="PrilCompetenceTable_Exam" BorderWidth="1" GridLines="Both">
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="Описание" RowSpan="3" />
                    <asp:TableHeaderCell Text="Контролируемый результат обучения" RowSpan="3" />
                    <asp:TableHeaderCell Text="Контролируемые темы (разделы) дисциплины" RowSpan="3" />
                    <asp:TableHeaderCell Text="Экзамен" ColumnSpan="4" />
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="Критерии оценивания" ColumnSpan="4"/>
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="2" />
                    <asp:TableHeaderCell Text="3" />
                    <asp:TableHeaderCell Text="4" />
                    <asp:TableHeaderCell Text="5" />
                </asp:TableHeaderRow>
            </asp:Table>
            <asp:HiddenField runat="server" ID="PCT_examrowscount" Value="0" />
            <div>
                <asp:Table runat="server" CssClass="table table-bordered" BorderWidth="1" GridLines="Both">
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Описание..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Результат..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Темы..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 2..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 3..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 4..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 5..." />
                    </asp:TableHeaderRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:DropDownList runat="server" ID="DropDown_PCTE" CssClass="form-control" DataSourceID="SqlDataSourceCompetences" DataTextField="Code" DataValueField="Description" />
                            <%--<asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTEDiscription" />--%>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTERezult" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTETopics" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTE2" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTE3" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTE4" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTE5" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>

                <asp:Button runat="server" CssClass="btn light" ID="addRowInPCTExamTable" Text="Добавить" OnClick="addRowInPCTExamTable_Click" />
                <asp:Button runat="server" CssClass="btn light" ID="deleteRowInPCTExamTable" Text="Удалить" OnClick="deleteRowInPCTExamTable_Click" />
                <asp:Button runat="server" CssClass="btn light" ID="Button_ExamTab" Text="Правка" OnClick="Button_ExamTab_Click" />
            </div>
            <br />
            <strong>Вопросы к экзамену:</strong><br />
            <div class="check_screen">
                <asp:TextBox runat="server" ID="ExamQuestions" CssClass="ckeditor" TextMode="MultiLine" />
            </div>
            <strong>Составитель:</strong> <asp:DropDownList runat="server" CssClass="form-control" ID="DeveloperExam" DataSourceID="SqlDataSourceUsers" DataTextField="UserName" DataValueField="UserId" />
            <strong>Дата:</strong> <asp:TextBox runat="server" CssClass="form-control" ID="ExamQuestionsDate" TextMode="Date" /><br />
            <asp:Button runat="server" CssClass="btn light" ID="Button_ExamQ" Text="Правка" OnClick="Button_ExamQ_Click" />
        </div>

        <div class="tab-pane fade" id="referat">
            <h3>Элементы контроля</h3>
            <h4>Реферат:</h4>
            <asp:Table runat="server" CssClass="table table-bordered" ID="PrilCompetenceTable_Referat" BorderWidth="1" GridLines="Both">
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="Описание" RowSpan="3" />
                    <asp:TableHeaderCell Text="Контролируемый результат обучения" RowSpan="3" />
                    <asp:TableHeaderCell Text="Контролируемые темы (разделы) дисциплины" RowSpan="3" />
                    <asp:TableHeaderCell Text="Реферат" ColumnSpan="4" />
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="Критерии оценивания" ColumnSpan="4"/>
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="2" />
                    <asp:TableHeaderCell Text="3" />
                    <asp:TableHeaderCell Text="4" />
                    <asp:TableHeaderCell Text="5" />
                </asp:TableHeaderRow>
            </asp:Table>
            <asp:HiddenField runat="server" ID="PCT_referatrowscount" Value="0" />
            <div>
                <asp:Table runat="server" CssClass="table table-bordered" BorderWidth="1" GridLines="Both">
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Описание..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Результат..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Темы..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 2..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 3..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 4..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 5..." />
                    </asp:TableHeaderRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:DropDownList runat="server" ID="DropDown_PCTR" CssClass="form-control" DataSourceID="SqlDataSourceCompetences" DataTextField="Code" DataValueField="Description" />
                            <%--<asp:TextBox runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" id="PCTRDiscription"/>--%>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" id="PCTRRezult" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" id="PCTRTopics" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" id="PCTR2" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" id="PCTR3" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" id="PCTR4" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" id="PCTR5" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Button runat="server" CssClass="btn light" ID="addRowInPCTReferatTable" Text="Добавить" OnClick="addRowInPCTReferatTable_Click" />
                <asp:Button runat="server" CssClass="btn light" ID="deleteRowInPCTReferatTable" Text="Удалить" OnClick="deleteRowInPCTReferatTable_Click" />
                <asp:Button runat="server" CssClass="btn light" ID="Button_ReferatTab" Text="Правка" OnClick="Button_ReferatTab_Click" />
            </div>
            <br />
            <strong>Темы рефератов:</strong><br />
            <div class="check_screen">
                <asp:TextBox runat="server" ID="ReferatTopics" CssClass="ckeditor" TextMode="MultiLine" />
            </div>
            <strong>Составитель:</strong> <asp:DropDownList runat="server" CssClass="form-control" ID="DeveloperReferat"  DataSourceID="SqlDataSourceUsers" DataTextField="UserName" DataValueField="UserId" />
            <strong>Дата:</strong> <asp:TextBox runat="server" ID="ReferatTopicsDate" CssClass="form-control" TextMode="Date" /><br />
            <asp:Button runat="server" CssClass="btn light" ID="Button_ReferatQ" Text="Правка" OnClick="Button_ReferatQ_Click" />
        </div>

        <div class="tab-pane fade" id="kt">
            <h3>Элементы контроля</h3>
            <h4>Контрольные работы:</h4>
            <asp:Table runat="server" CssClass="table table-bordered" ID="PrilCompetenceTable_KT" BorderWidth="1" GridLines="Both">
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="Описание" RowSpan="3" />
                    <asp:TableHeaderCell Text="Контролируемый результат обучения" RowSpan="3" />
                    <asp:TableHeaderCell Text="Контролируемые темы (разделы) дисциплины" RowSpan="3" />
                    <asp:TableHeaderCell Text="Контрольная работа" ColumnSpan="4" />
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="Критерии оценивания" ColumnSpan="4"/>
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="2" />
                    <asp:TableHeaderCell Text="3" />
                    <asp:TableHeaderCell Text="4" />
                    <asp:TableHeaderCell Text="5" />
                </asp:TableHeaderRow>
            </asp:Table>
            <asp:HiddenField runat="server" ID="PCT_ktrowscount" Value="0" />
            <div>
                <asp:Table runat="server" CssClass="table table-bordered" BorderWidth="1" GridLines="Both">
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Описание..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Результат..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Темы..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 2..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 3..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 4..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 5..." />
                    </asp:TableHeaderRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:DropDownList runat="server" ID="DropDown_PCTKT" CssClass="form-control" DataSourceID="SqlDataSourceCompetences" DataTextField="Code" DataValueField="Description" />
                            <%--<asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTKTDiscription"/>--%>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTKTRezult" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTKTTopics" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTKT2" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTKT3" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTKT4" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTKT5" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Button runat="server" CssClass="btn light" ID="addRowInPCTKTTable" Text="Добавить" OnClick="addRowInPCTKTTable_Click" />
                <asp:Button runat="server" CssClass="btn light" ID="deleteRowInPCTKTTable" Text="Удалить" OnClick="deleteRowInPCTKTTable_Click" />
                <asp:Button runat="server" CssClass="btn light" ID="Button_KTTab" Text="Правка" OnClick="Button_KTTab_Click" />
            </div>
            <br />
            <strong>Темы контрольных работ:</strong><br />
            <div class="check_screen">
                <asp:TextBox runat="server" ID="KTTopics" CssClass="ckeditor" TextMode="MultiLine" />
            </div>
            <strong>Составитель:</strong> <asp:DropDownList runat="server" CssClass="form-control" ID="DeveloperKT" DataSourceID="SqlDataSourceUsers" DataTextField="UserName" DataValueField="UserId" />
            <strong>Дата:</strong> <asp:TextBox runat="server" ID="KTDate" CssClass="form-control" TextMode="Date" /><br />
            <asp:Button runat="server" CssClass="btn light" ID="Button_KTQ" Text="Правка" OnClick="Button_KTQ_Click" />
        </div>

        <div class="tab-pane fade" id="prakrika">
            <h3>Элементы контроля</h3>
            <h4>Практические занятия:</h4>
            <asp:Table runat="server" CssClass="table table-bordered" ID="PrilCompetenceTable_Praktika" BorderWidth="1" GridLines="Both">
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="Описание" RowSpan="3" />
                    <asp:TableHeaderCell Text="Контролируемый результат обучения" RowSpan="3" />
                    <asp:TableHeaderCell Text="Контролируемые темы (разделы) дисциплины" RowSpan="3" />
                    <asp:TableHeaderCell Text="Практическое занятие" ColumnSpan="4" />
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="Критерии оценивания" ColumnSpan="4"/>
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="2" />
                    <asp:TableHeaderCell Text="3" />
                    <asp:TableHeaderCell Text="4" />
                    <asp:TableHeaderCell Text="5" />
                </asp:TableHeaderRow>
            </asp:Table>
            <asp:HiddenField runat="server" ID="PCT_praktikarowscount" Value="0" />
            <div>
                <asp:Table runat="server" CssClass="table table-bordered"  BorderWidth="1" GridLines="Both">
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Описание..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Результат..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Темы..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 2..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 3..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 4..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 5..." />
                    </asp:TableHeaderRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:DropDownList runat="server" ID="DropDown_PCTPR" CssClass="form-control" DataSourceID="SqlDataSourceCompetences" DataTextField="Code" DataValueField="Description" />
                            <%--<asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTPDiscription"/>--%>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTPRezult" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTPTopics" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTP2" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTP3" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTP4" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTP5" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Button runat="server" CssClass="btn light" ID="addRowInPCTPraktikaTable" Text="Добавить" OnClick="addRowInPCTPraktikaTable_Click" />
                <asp:Button runat="server" CssClass="btn light" ID="deleteRowInPCTPraktikaTable" Text="Удалить" OnClick="deleteRowInPCTPraktikaTable_Click" />
                <asp:Button runat="server" CssClass="btn light" ID="Button_PraktikaTab" Text="Правка" OnClick="Button_PraktikaTab_Click" />
            </div>
            <br />
            <strong>Темы практических занятий:</strong><br />
            <div class="check_screen">
                <asp:TextBox runat="server" ID="PraktikTopics" CssClass="ckeditor" TextMode="MultiLine" Width="500px" Height="200px" />
            </div>
            <strong>Составитель:</strong> <asp:DropDownList runat="server" CssClass="form-control" ID="DeveloperPraktika" DataSourceID="SqlDataSourceUsers" DataTextField="UserName" DataValueField="UserId"  />
            <strong>Дата:</strong> <asp:TextBox runat="server" CssClass="form-control" ID="PraktikaTopicsDate" TextMode="Date" /><br />
            <asp:Button runat="server" CssClass="btn light" ID="Button_PraktikaQ" Text="Правка" OnClick="Button_PraktikaQ_Click" />
        </div>
        
        <div class="tab-pane fade" id="test">
            <h3>Элементы контроля</h3>
            <h4>Тесты:</h4>
            <asp:Table runat="server" CssClass="table table-bordered" ID="PrilCompetenceTable_Test" BorderWidth="1" GridLines="Both">
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="Описание" RowSpan="3" />
                    <asp:TableHeaderCell Text="Контролируемый результат обучения" RowSpan="3" />
                    <asp:TableHeaderCell Text="Контролируемые темы (разделы) дисциплины" RowSpan="3" />
                    <asp:TableHeaderCell Text="Тест" ColumnSpan="4" />
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="Критерии оценивания" ColumnSpan="4"/>
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="2" />
                    <asp:TableHeaderCell Text="3" />
                    <asp:TableHeaderCell Text="4" />
                    <asp:TableHeaderCell Text="5" />
                </asp:TableHeaderRow>
            </asp:Table>
            <asp:HiddenField runat="server" ID="PCT_testrowscount" Value="0" />
            <div>
                <asp:Table runat="server" CssClass="table table-bordered" BorderWidth="1" GridLines="Both">
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Описание..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Результат..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Темы..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 2..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 3..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 4..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 5..." />
                    </asp:TableHeaderRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:DropDownList runat="server" ID="DropDown_PCTT" CssClass="form-control" DataSourceID="SqlDataSourceCompetences" DataTextField="Code" DataValueField="Description" />
                            <%--<asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTTDiscription"/>--%>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTTRezult" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTTTopics" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTT2" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTT3" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTT4" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTT5" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Button runat="server" CssClass="btn light" ID="addRowInPCTtestTable" Text="Добавить" OnClick="addRowInPCTtestTable_Click" />
                <asp:Button runat="server" CssClass="btn light" ID="deleteRowInPCTtestTable" Text="Удалить" OnClick="deleteRowInPCTtestTable_Click" />      
                <asp:Button runat="server" CssClass="btn light" ID="Button_TestTab" Text="Правка" OnClick="Button_TestTab_Click" />
            </div>
            <br />
            <strong>Фонд тестовых заданий:</strong><br />
            <div class="check_screen">
                <asp:TextBox runat="server" ID="TestsFond" CssClass="ckeditor" TextMode="MultiLine" />
            </div>
            <strong>Составитель:</strong> <asp:DropDownList runat="server" CssClass="form-control" ID="DeveloperTest" DataSourceID="SqlDataSourceUsers" DataTextField="UserName" DataValueField="UserId" />
            <strong>Дата:</strong> <asp:TextBox runat="server" ID="TestsFondDate" CssClass="form-control" TextMode="Date" /><br />
            <asp:Button runat="server" CssClass="btn light" ID="Button_TestQ" Text="Правка" OnClick="Button_TestQ_Click" />
        </div>
        
        <div class="tab-pane fade" id="curs">
           <h3>Элементы контроля</h3>
           <h4>Курсовые работы:</h4>
           <asp:Table runat="server" CssClass="table table-bordered" ID="PrilCompetenceTable_Curs" BorderWidth="1" GridLines="Both">
               <asp:TableHeaderRow>
                   <asp:TableHeaderCell Text="<strong>Описание</strong>" RowSpan="3" />
                   <asp:TableHeaderCell Text="<strong>Контролируемый результат обучения</strong>" RowSpan="3" />
                   <asp:TableHeaderCell Text="<strong>Контролируемые темы (разделы) дисциплины</strong>" RowSpan="3" />
                   <asp:TableHeaderCell Text="<strong>Тест</strong>" ColumnSpan="4" />
               </asp:TableHeaderRow>
               <asp:TableHeaderRow>
                   <asp:TableHeaderCell Text="<strong>Критерии оценивания</strong>" ColumnSpan="4"/>
               </asp:TableHeaderRow>
               <asp:TableHeaderRow>
                   <asp:TableHeaderCell Text="<strong>2</strong>" />
                   <asp:TableHeaderCell Text="<strong>3</strong>" />
                   <asp:TableHeaderCell Text="<strong>4</strong>" />
                   <asp:TableHeaderCell Text="<strong>5</strong>" />
               </asp:TableHeaderRow>
           </asp:Table>
           <asp:HiddenField runat="server" ID="PCT_cursrowscount" Value="0" />
           <div>
               <asp:Table runat="server" CssClass="table table-bordered" BorderWidth="1" GridLines="Both">
                   <asp:TableHeaderRow>
                       <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Код" />
                       <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Результат" />
                       <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Темы" />
                       <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 2" />
                       <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 3" />
                       <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 4" />
                       <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 5" />
                   </asp:TableHeaderRow>
                   <asp:TableRow>
                       <asp:TableCell>
                           <asp:DropDownList runat="server" ID="DropDown_PCTC" CssClass="form-control" DataSourceID="SqlDataSourceCompetences" DataTextField="Code" DataValueField="Description" />
                           <%--<asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTTDiscription"/>--%>
                       </asp:TableCell>
                       <asp:TableCell>
                           <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTCRezult" />
                       </asp:TableCell>
                       <asp:TableCell>
                           <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTCTopics" />
                       </asp:TableCell>
                       <asp:TableCell>
                           <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTC2" />
                       </asp:TableCell>
                       <asp:TableCell>
                           <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTC3" />
                       </asp:TableCell>
                       <asp:TableCell>
                           <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTC4" />
                       </asp:TableCell>
                       <asp:TableCell>
                           <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTC5" />
                       </asp:TableCell>
                   </asp:TableRow>
               </asp:Table>
               <asp:Button runat="server" CssClass="btn light" ID="addRowInPCTcursTable" Text="Добавить" OnClick="addRowInPCTcursTable_Click" />
               <asp:Button runat="server" CssClass="btn light" ID="deleteRowInPCTcursTable" Text="Удалить" OnClick="deleteRowInPCTcursTable_Click" />      
               <asp:Button runat="server" CssClass="btn light" ID="Button_CursTab" Text="Правка" OnClick="Button_CursTab_Click" />
           </div>
           <br />
           <strong>Темы курсовых работ:</strong><br />
           <div class="check_screen">
               <asp:TextBox runat="server" ID="CursesFond" Width="100%" Height="150px" TextMode="MultiLine" CssClass="ckeditor" />
           </div>
           <strong>Составитель:</strong> <asp:DropDownList runat="server" style="margin-top:5px" ID="DeveloperCurs" CssClass="form-control" DataSourceID="SqlDataSourceUsers" DataTextField="UserName" DataValueField="UserId" />
           <strong>Дата:</strong> <asp:TextBox runat="server" ID="CursDate" style="margin-top:5px" CssClass="form-control" TextMode="Date" />
           <asp:Button runat="server" CssClass="btn light" ID="Button_CursQ" Text="Правка" OnClick="Button_CursQ_Click" />
        </div>

        <div class="tab-pane fade" id="persons">
            <h3>Составители</h3><br />
            <div runat="server" id="drafters_html_block">
                <strong>Программу составил: </strong><br />
                <asp:TextBox runat="server" CssClass="form-control" placeholder="Академ. статус" ID="DrafterStatus" width="150px" />
                <asp:DropDownList runat="server" CssClass="form-control" ID="DrafterName" DataSourceID="SqlDataSourceDepartmentPersonal" DataTextField="UserName" DataValueField="UserId" /><br />
                <asp:SqlDataSource ID="SqlDataSourceDepartmentPersonal" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" 
                    SelectCommand="SELECT * FROM AspNetUsers INNER JOIN AspNetUserRoles ON AspNetUsers.Id=AspNetUserRoles.UserId INNER JOIN AspNetRoles ON AspNetRoles.Id = AspNetUserRoles.RoleId WHERE AspNetRoles.Name!='Guest' ORDER BY UserName"></asp:SqlDataSource>
                <asp:HiddenField runat="server" ID="AdditionalDraftersCount" Value="0" />
            </div>
            <asp:Button runat="server" ID="Button_AddDrafter" style="margin-top:2px" CssClass="btn light" Text="Добавить составителя" OnClick="Button_AddDrafter_Click" />
            <asp:Button runat="server" ID="Button_DeleteDrafter" style="margin-top:2px" CssClass="btn light" Text="Удалить составителя" OnClick="Button_DeleteDrafter_Click" /><br />
            <br />
            <strong>Кафедра:</strong><br />
            <asp:DropDownList runat="server" ID="DropDown_Department" CssClass="form-control" DataSourceID="SqlDataSourceDepartments" DataTextField="Name" DataValueField="Id" />
            <asp:SqlDataSource runat="server" ID="SqlDataSourceDepartments" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [Departments]" />
            <asp:TextBox runat="server" CssClass="form-control" ID="DraftDate" width="150px" TextMode="Date"/>
            <asp:TextBox runat="server" CssClass="form-control" placeholder="Протокол" ID="Protocol" width="150px" /><br />
            <asp:Button runat="server" CssClass="btn light" Width="150px" ID="Button_Developer" style="margin-top: 2px" Text="Внести правку" OnClick="Button_Developer_Click" /><br />
            <br />
            <strong>Заведующий кафедрой: </strong><br />
            <asp:TextBox runat="server" CssClass="form-control" placeholder="Должность, академ. статус" ID="DepartmentHeadStatus" width="150px" />
            <asp:DropDownList runat="server" CssClass="form-control" ID="DepartmentHeadName" DataSourceID="SqlDataSourceUsers" DataTextField="UserName" DataValueField="UserId" /><br />
            <asp:Button runat="server" CssClass="btn light" Width="150px" ID="Button_DepartmentHead" style="margin-top: 2px" Text="Внести правку" OnClick="Button_DepartmentHead_Click" /><br />
            <br />
            <strong>Программа согласована: </strong><br />
            <asp:TextBox runat="server" CssClass="form-control" placeholder="Должность, академ. статус" ID="AgreementDepartmentHeadStatus" width="150px" />
            <asp:DropDownList runat="server" CssClass="form-control" ID="AgreementDepartmentHeadName" DataSourceID="SqlDataSourceUsers" DataTextField="UserName" DataValueField="UserId" /><br />
            <asp:TextBox runat="server" CssClass="form-control" placeholder="Должность, академ. статус" ID="AgreementDirectorStatus" width="150px" />
            <asp:DropDownList runat="server" CssClass="form-control" ID="AgreementDirectorName" DataSourceID="SqlDataSourceUsers" DataTextField="UserName" DataValueField="UserId" /><br />
            <asp:Button runat="server" CssClass="btn light" Width="150px" ID="Button_Agreemers" style="margin-top: 2px" Text="Внести правку" OnClick="Button_Agreemers_Click" />
        </div>
    </div>
    <hr /><br />
    <asp:Button runat="server" CssClass="btn btn-lg yellow" ID="Button_Report" Text="Переформировать файлы" OnClick="Button_Report_Click" />
    <asp:Button runat="server" CssClass="btn btn-lg dark" ID="Button_ApproveProgram" Text="Утвердить программы" OnClick="Button_ApproveProgram_Click" OnClientClick="if (!OnApproveConfirm()) return false;" />
    <a href="#" class="scrollup">Наверх</a>
    </div>
</asp:Content>



