﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Title="- Конструктор" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeBehind="Designer.aspx.cs" Inherits="main_test.Designer" Debug="true" ValidateRequest="false" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent" ID="DesignerContent">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    <link rel="stylesheet" href="Content/fileUploader.css" />
    <link rel="stylesheet" href="Content/CustomSwitcher.css" />
    <link rel="stylesheet" href="Content/Colored.css"/>
    <link rel="stylesheet" href="Content/WorkStyle.css" />
    <link rel="stylesheet" href="Content/CkeditorStyle.css" />
    <link rel="stylesheet" href="Content/Preloader.css" />
    <script src="ckeditor/ckeditor.js"></script>
    <script src="ckeditor/adapters/jquery.js"></script>
    <script src="Scripts/ScreenCheck.js"></script>
    <script src="Scripts/SetActiveTab.js"></script>
    <script src="Scripts/ScrollTop.js"></script>
    <style>
        .check_screen {
            width: 50%;
        }
    </style>
    <script>
        //локальные рабочие скрипты
        function OnTableClearConfirm() {
            if (confirm('Очистить таблицу?'))
                return true;
            else
                return false;
        };
        function showFileName() {
            var input = document.getElementById('<%=uploader.ClientID %>');
            document.getElementById('<%=fileviewer.ClientID %>').innerHTML = input.files[0].name;
            return false;
        };
        function Change() {
            var but = document.getElementById('<%=reload.ClientID%>');
            but.click();
            return false;
        };
        function onlyNumbers(input) {
            input.value = input.value.replace(/[^\d\.]/g, "");
            if(input.value.match(/\./g).length > 1) {
            input.value = input.value.substr(0, input.value.lastIndexOf("."));
            }
        };
    </script>

    <script>
        //проверка на предмет отсутсвия данных
        function CheckElements() {
            var access = true;
            var dangerColor = '#ff0000';
            var approverPost = document.getElementById('<%=Approver_Post.ClientID%>');
            var city = document.getElementById('<%=City.ClientID%>');
            var drafterStat = document.getElementById('<%=DrafterStatus.ClientID%>');
            var protocol = document.getElementById('<%=Protocol.ClientID%>');
            var depHead = document.getElementById('<%=DepartmentHeadStatus.ClientID%>');
            var agreeHead = document.getElementById('<%=AgreementDepartmentHeadStatus.ClientID%>');
            var agreeDirector = document.getElementById('<%=AgreementDirectorStatus.ClientID%>');
            if (CKEDITOR.instances['<%=Institute.ClientID%>'].getData() == "") {
                document.getElementById('label_institute').style.color = dangerColor;
            }
            if (approverPost.value == "") {
                access = false; document.getElementById('label_approverPost').style.color = dangerColor;
            }
            if (city.value == "") {
                access = false; document.getElementById('label_city').style.color = dangerColor;
            }
            if (CKEDITOR.instances['<%=ObjectivesOfDiscipline.ClientID%>'].getData() == "") {
                document.getElementById('label_ObjectivesOfDiscipline').style.color = dangerColor;
            }
            if (CKEDITOR.instances['<%=DisciplineInOOP.ClientID%>'].getData() == "") {
                document.getElementById('label_DisciplineInOOP').style.color = dangerColor;
            }
            if (CKEDITOR.instances['<%=DisciplineStructureContent.ClientID%>'].getData() == "") {
                document.getElementById('label_DisciplineStructureContent').style.color = dangerColor;
            }
            if (CKEDITOR.instances['<%=SemesteresContent.ClientID%>'].getData() == "") {
                document.getElementById('label_SemesteresContent').style.color = dangerColor;
            }
            if (CKEDITOR.instances['<%=MonitoringTools_Description.ClientID%>'].getData() == "") {
                document.getElementById('label_MonitoringTools_Description').style.color = dangerColor;
            }
            if (CKEDITOR.instances['<%=EducationalTechnology.ClientID%>'].getData() == "") {
                document.getElementById('label_EducationalTechnology').style.color = dangerColor;
            }
            if (CKEDITOR.instances['<%=ZachetDescription.ClientID%>'].getData() == "") {
                document.getElementById('label_ZachetDescription').style.color = dangerColor;
            }
            if (CKEDITOR.instances['<%=ExamDescription.ClientID%>'].getData() == "") {
                document.getElementById('label_ExamDescription').style.color = dangerColor;
            }
            if (CKEDITOR.instances['<%=BasicLiterature.ClientID%>'].getData() == "") {
                document.getElementById('label_BasicLiterature').style.color = dangerColor;
            }
            if (CKEDITOR.instances['<%=MaterialTechnicalSupport.ClientID%>'].getData() == "") {
                document.getElementById('label_MaterialTechnicalSupport').style.color = dangerColor;
            }
            if (drafterStat.value == "") {
                access = false; document.getElementById('label_drafters').style.color = dangerColor;
            }
            if (protocol.value == "") {
                access = false; document.getElementById('label_dep').style.color = dangerColor;
            }
            if (depHead.value == "") {
                access = false; document.getElementById('label_depHead').style.color = dangerColor;
            }
            if (agreeHead.value == "") {
                access = false; document.getElementById('label_agreements').style.color = dangerColor;
            }
            if (agreeDirector.value == "") {
                access = false; document.getElementById('label_agreements').style.color = dangerColor;
            }

            return access;
        };
        function Validate() {
            //отмена запроса при осутсвии некоторых данных
            var access = CheckElements();
            if (access == false)
                if (confirm('Некоторые поля не заполнены, продолжить отправку?'))
                    return true;
                else
                    return false;
            else
                return true;
        };
    </script>

    <script>
        //отоброжение прелоедера перед готовностью ckeditor к работе
        CKEDITOR.on("instanceReady", function () {
            var loading = document.getElementById('loading_inputs');
            loading.className = 'fast_blinkText';
            ShowBody();
            setTimeout(LoadingHide, 1500);
        });
        function LoadingHide() {
            var loading = document.getElementById('loading_inputs');
            loading.hidden = true;
        }
        function ShowBody() {
            var body = document.getElementById('globalForm');
            var preloader = document.getElementById('preloader');
            var placeHolder = document.getElementById('placeHolder');
            preloader.style.display = 'none';
            placeHolder.style.display = 'none';
            body.style.display = 'block';
        }
        function PreloaderView() {
            var preloader = document.getElementById('preloader');
            preloader.style.display = 'block';
        }
        window.onload = function () {
            var counter = localStorage.getItem('postBackCounts');
            if (counter == null) {
                PreloaderView();
                counter = 0;
            }
            else {
                var loading = document.getElementById('loading_inputs');
                var body = document.getElementById('globalForm');
                var preloader = document.getElementById('preloader');
                var placeHolder = document.getElementById('placeHolder');
                preloader.style.display = 'none';
                placeHolder.style.display = 'none';
                body.style.display = 'block';
                loading.hidden = false;
                counter++;
            }
            localStorage.setItem('postBackCounts', counter);
        }
    </script>

    <%-- id в разметке расставлены не просто так. По ним выстроены связи в системые. Хватит их менять!!!--%>
    <div id="placeHolder" style="top:0px; left:0px; width:100vh; height:75vh; background-color:white; display:block"></div>
    
    <div id="preloader" class="body_fade" style="display:none">
        <div class="cssload-loader">
            <div class="cssload-inner cssload-one"></div>
            <div class="cssload-inner cssload-two"></div>
            <div class="cssload-inner cssload-three"></div>
        </div>
    </div>
    
    <div id="globalForm" style="display:none">
    <div id="loading_inputs" hidden="hidden" style="position:fixed; top:10vh; right:15vh;">
        <h3>Загрузка полей...</h3>
    </div>
    <h2>Конструктор учебных программ</h2><br />
    <asp:Label runat="server" ID="ErrorMes" ForeColor="Red" Visible="false" />
    
    <asp:Button runat="server" ID="reload" style="display: none" />

    <div class="switcher" title="Функция позволяет автоматически устанавливать связи между некоторыми полями">
        <h4 class="unselecteble">Включить автоматическую фильтрацию</h4>
        <input class="switcher__input" runat="server" onchange="Change();" ClientIdMode="static" type="checkbox" name="watched" id="switcher">
        <label class="switcher__label" for="switcher">
            <span class="switcher__text"></span>
        </label>
    </div>

    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" class="tabs-dark" href="#titul">Титул</a></li>
        <li><a data-toggle="tab" class="tabs-dark" href="#purposes">Цели и место</a></li>
        <li><a data-toggle="tab" class="tabs-dark" href="#discipline_structure">Структура и содержание</a></li>
        <li class="dropdown tabs-dark">
            <a data-toggle="dropdown" class="dropdown-toggle tabs-dark" href="#">
            Инструменты оценки 
            <b class="caret"></b>
            </a>
            <ul class="dropdown-menu">
                <li><a data-toggle="tab" href="#fond_evaluation_tools">Фонд оценочных средств</a></li>
                <li><a data-toggle="tab" href="#competences">Описание компетенций</a></li>
            </ul>
        </li>
        <li class="dropdown tabs-dark">
            <a data-toggle="dropdown" class="dropdown-toggle tabs-dark" href="#">
                Элементы контроля
                <b class="caret"></b>
            </a>
            <ul class="dropdown-menu">
                <li><a data-toggle="tab" class="tabs-dark" href="#zachet">Зачёт</a></li>
                <li><a data-toggle="tab" class="tabs-dark" href="#exam">Экзамен</a></li>
                <li><a data-toggle="tab" class="tabs-dark" href="#referat">Реферат</a></li>
                <li><a data-toggle="tab" class="tabs-dark" href="#kt">КТ</a></li>
                <li><a data-toggle="tab" class="tabs-dark" href="#prakrika">Практика</a></li>
                <li><a data-toggle="tab" class="tabs-dark" href="#test">Тест</a></li>
                <li><a data-toggle="tab" class="tabs-dark" href="#curs">Курсовые работы</a></li>
            </ul>
        </li>
        <li><a data-toggle="tab" class="tabs-dark" href="#discipline_provide">Обеспечение</a></li>
        <li><a data-toggle="tab" class="tabs-dark" href="#persons">Составители</a></li>
    </ul>

    <div class="tab-content">
        <asp:SqlDataSource runat="server" ID="SqlDataSourceCompetencesFiltered" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [Competences]" />

        <div class="tab-pane active" ID="titul">
            <h3 class="unselecteble">Титул</h3><br />
            <strong id="label_institute">Институт: <span style="color:red">*</span></strong><br/>
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" id="Institute" TextMode="MultiLine"  />
            </div>
            <br />
            <strong id="label_approverPost">Должность утвердителя: <span style="color:red">*</span></strong><br />
            <asp:TextBox runat="server" CssClass="form-control" id="Approver_Post" placeholder="Введите должность..."/><br/>
            <br />
            <strong>Утвердитель: <span style="color:red">*</span></strong><br />
            <asp:DropDownList runat="server" CssClass="form-control" ID="Approver_Name" DataSourceID="SqlDataSourceUsers" DataTextField="UserName" DataValueField="UserId" />
                <asp:SqlDataSource ID="SqlDataSourceUsers" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" 
                    SelectCommand="SELECT * FROM AspNetUsers INNER JOIN AspNetUserRoles ON AspNetUsers.Id=AspNetUserRoles.UserId INNER JOIN AspNetRoles ON AspNetRoles.Id = AspNetUserRoles.RoleId WHERE AspNetRoles.Name!='Guest' ORDER BY UserName"></asp:SqlDataSource><br />
            <br />
            <strong>Дата: <span style="color:red">*</span></strong><br />
            <asp:TextBox runat="server" CssClass="form-control" id="Approve_Date" TextMode="Date" /><br/>
            <br />
            <strong>Рабочая программа дисциплины: <span style="color:red">*</span></strong><br/>
            <asp:DropDownList runat="server" CssClass="form-control" ID="Discipline" DataSourceID="SqlDataSourceDisciplines" DataTextField="Name" DataValueField="id" AutoPostBack="true" OnSelectedIndexChanged="Discipline_SelectedIndexChanged" />
                <asp:SqlDataSource ID="SqlDataSourceDisciplines" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [Disciplines]"></asp:SqlDataSource><br />
            <br />
            <strong>Направление подготовки: <span style="color:red">*</span></strong><br/>
            <asp:DropDownList runat="server" CssClass="form-control" ID="Training_Direction" AutoPostBack="true" OnSelectedIndexChanged="Training_Direction_SelectedIndexChanged" DataSourceID="SqlDataSourceTrainingDirections" DataTextField="Viewer" DataValueField="id" />
                <asp:SqlDataSource ID="SqlDataSourceTrainingDirections" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT *, (Designation+' ('+ Code+')') AS Viewer FROM [Training_Directions]"></asp:SqlDataSource><br/>
            <br />
            <strong>Профиль: <span style="color:red">*</span></strong><br/>
            <asp:DropDownList runat="server" ID="DropDown_Profil" AutoPostBack="true" CssClass="form-control" DataSourceID="SqlDataSourceProfiles" DataTextField="Name" DataValueField="id" />
            <asp:SqlDataSource runat="server" ID="SqlDataSourceProfiles" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [Profiles]"  />
            <br />
            <br />
            <strong>Квалификация (степень) выпусника: <span style="color:red">*</span></strong><br/>
            <asp:DropDownList runat="server" CssClass="form-control" ID="Graduate_Qualification" DataSourceID="SqlDataSourceQualifications" DataTextField="Name" DataValueField="Name" />
                <asp:SqlDataSource ID="SqlDataSourceQualifications" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [Qualifications]"></asp:SqlDataSource><br />
            <br />
            <strong>Форма обучения: <span style="color:red">*</span></strong><br/>
            <asp:DropDownList runat="server" CssClass="form-control" id="Training_Form" >
                <asp:ListItem>Очная</asp:ListItem>
                <asp:ListItem>Очно-заочная</asp:ListItem>
                <asp:ListItem>Заочная</asp:ListItem>
                <asp:ListItem>Экстернат</asp:ListItem>
            </asp:DropDownList><br/>
            <br />
            <strong>Нормативный срок обучения:</strong><br />
            <asp:TextBox runat="server" CssClass="form-control" ID="StudyPeriod" onkeyup="return onlyNumbers(this);" onchange="return onlyNumbers(this);" /><br />
            <br />
            <strong id="label_city">Город: <span style="color:red">*</span></strong><br/>
            <asp:TextBox runat="server" CssClass="form-control" id="City" placeholder="Название города..." /><br />
        </div>
        
        <div class="tab-pane fade" ID="purposes">
            <h3 class="unselecteble">Цели</h3><br />
            <h4 class="unselecteble" id="label_ObjectivesOfDiscipline">Цели освоения дисциплины: <span style="color:red">*</span></h4>
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" id="ObjectivesOfDiscipline" TextMode="MultiLine" />
            </div>
            <br />
            <h4 class="unselecteble" id="label_DisciplineInOOP">Место дисциплины в структуре ООП аспирантуры: <span style="color:red">*</span></h4>
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" id="DisciplineInOOP" TextMode="MultiLine" />
            </div>
            <br />
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <strong>Дисциплины до:</strong><br />
                        <asp:TextBox runat="server" CssClass="form-control" style="width:100%; height:150px; max-width:100%; background-color:#fff" ID="DisciplineInOOPBefore" TextMode="MultiLine" ReadOnly="true" Text="Изучение данной дисциплины базируется на следующих дисциплинах, прохождении практик:" />
                        <br />
                        <asp:DropDownList runat="server" ID="DropDown_DisciplineInOOPBefore" CssClass="form-control" DataSourceID="SqlDataSourceDisciplines" DataTextField="Name" DataValueField="id" />
                        <asp:SqlDataSource runat="server" ID="SqlDataSourceDisciplinesBefore" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" />
                        <asp:Button runat="server" style="margin-bottom:2px" ID="Button_AddDisciplineInOOPBefore" CssClass="btn light" Text="Добавить" OnClick="Button_AddDisciplineInOOPBefore_Click" />
                        <asp:Button runat="server" style="margin-bottom:2px" ID="Button_DeleteDisciplineInOOPBefore" CssClass="btn light" Text="Удалить" OnClick="Button_DeleteDisciplineInOOPBefore_Click" />
                    </div>
                    <div class="col-md-5">
                        <strong>Дисциплины после:</strong><br />
                        <asp:TextBox runat="server" CssClass="form-control" style="width:100%; height:150px; max-width:100%; background-color:#fff" ID="DisciplineInOOPAfter"  TextMode="MultiLine" ReadOnly="true" Text="Основные положения дисциплины должны быть использованы в дальнейшем при изучении следующих дисциплин:"/>
                        <br />
                        <asp:DropDownList runat="server" ID="DropDown_DisciplineInOOPAfter"  CssClass="form-control" DataSourceID="SqlDataSourceDisciplines" DataTextField="Name" DataValueField="id" />
                        <asp:SqlDataSource runat="server" ID="SqlDataSourceDisciplinesAfter" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" />
                        <asp:Button runat="server" style="margin-bottom:2px" ID="Button_AddDisciplineInOOPAfter" CssClass="btn light" Text="Добавить" OnClick="Button_AddDisciplineInOOPAfter_Click" />
                        <asp:Button runat="server" style="margin-bottom:2px" ID="Button_DeleteDisciplineInOOPAfter" CssClass="btn light" Text="Удалить" OnClick="Button_DeleteDisciplineInOOPAfter_Click" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5">
                        <asp:Button runat="server" ID="Button_PasteInOOP" style="margin-top:2px" Text="Вставить в поле" CssClass="btn light" OnClick="Button_PasteInOOP_Click" />
                    </div>
                </div>
            </div>
            <br />
            <%--<strong>Перечень планируемых результатов обучения по дисциплине (модулю),<br />
                соотнесенные с планируемыми результатами освоения образовательной программы:</strong><br />
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="form-control" Width="100%" Height="150px" id="PlanDescription" TextMode="MultiLine" />
            </div>
            <br />--%>
            <div>
                <asp:Table ID="PlanTable" CssClass="table table-bordered" runat="server" BorderWidth="1" GridLines="Both">
                    <asp:TableHeaderRow>
                          <asp:TableHeaderCell  Width="10%" style="text-align:center"><strong>Код компетенции</strong></asp:TableHeaderCell>
                          <asp:TableHeaderCell  Width="45%" style="text-align:center"><strong>В результате освоения образовательной программы обучающийся должен обладать</strong></asp:TableHeaderCell>
                          <asp:TableHeaderCell  Width="45%" style="text-align:center"><strong>Перечень планируемых результатов обучения по дисциплине</strong></asp:TableHeaderCell>
                    </asp:TableHeaderRow>
                </asp:Table>

                <asp:Table ID="PlanTable_control" CssClass="table table-bordered" runat="server" BorderWidth="1" GridLines="Both">
                    <asp:TableHeaderRow>
                          <asp:TableHeaderCell style="text-align: center" Text="Код..." ForeColor="Gray" Width="10%" />
                          <asp:TableHeaderCell style="text-align: center" Text="Результаты..." ForeColor="Gray" width="45%"/>
                    </asp:TableHeaderRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:DropDownList runat="server" CssClass="form-control" ID="Сompetence_PlanTable" DataSourceID="SqlDataSourceCompetences" DataTextField="Code" DataValueField="Description" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="ckeditor" style="display: inline; width: 100%" TextMode="MultiLine" id="Enumeration_PlanTable"/>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>

                <asp:HiddenField runat="server" ID="ptrowscount" Value="0" />
                <asp:SqlDataSource ID="SqlDataSourceCompetences" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [Competences]"></asp:SqlDataSource>
            </div>
            <asp:Button runat="server" Class="btn light" ID="CreateRowPlanTable" Text="Добавить" OnClick="CreateRowPlanTable_Click" />
            <asp:Button runat="server" Class="btn light" ID="DeleteRowPlanTable" Text="Удалить" OnClick="DeleteRowPlanTable_Click" />
        </div>

        <div class="tab-pane fade" ID="discipline_structure">
            <h3 class="unselecteble">Структура</h3>
            <h4 class="unselecteble">Вы можете заполнить таблицу вручную или загрузить файл расчасовки</h4>
            <asp:Button runat="server" ID="ImportDSCT_Button" Text="Заполнить" CssClass="btn light" OnClick="ImportDSCT_Button_Click" />
            <label id="fileviewer" runat="server">Файл не выбран</label><strong>:</strong>
                <br />
                <div class="upload" style="margin-top:5px">
                    <label class="label">
                        <i class="material-icons">attach_file</i>
                        <asp:FileUpload runat="server" ID="uploader" CssClass="hide" onchange="showFileName();"/>
                    </label>
                </div>
            <asp:Table runat="server" CssClass="table table-bordered" ID="DisciplineStructureTable" contenteditable="true" BorderWidth="1" GridLines="Both" >
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell text="<strong>n/n</strong>" RowSpan="2" />
                    <asp:TableHeaderCell text="<strong>Раздел</strong>" RowSpan="2"/>
                    <asp:TableHeaderCell text="<strong>Семестр</strong>" RowSpan="2"/>
                    <asp:TableHeaderCell text="<strong>Неделя семестра</strong>" RowSpan="2"/>
                    <asp:TableHeaderCell text="<strong>Виды учебной работы, включая самостоятельную работу студентов, и трудоемкость в часах</strong>" ColumnSpan="5"/>
                    <asp:TableHeaderCell text="<strong>Виды самостоятельной работы студентов</strong>" ColumnSpan="6"/>
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell text="<strong>Л</strong>"/>
                    <asp:TableHeaderCell text="<strong>П/С</strong>"/>
                    <asp:TableHeaderCell text="<strong>Лаб</strong>"/>
                    <asp:TableHeaderCell text="<strong>СРС</strong>"/>
                    <asp:TableHeaderCell text="<strong>КСР</strong>"/>
                    <asp:TableHeaderCell text="<strong>К.Р</strong>"/>
                    <asp:TableHeaderCell text="<strong>К.П</strong>"/>
                    <asp:TableHeaderCell text="<strong>РГР</strong>"/>
                    <asp:TableHeaderCell text="<strong>Под. к л/р</strong>"/>
                    <asp:TableHeaderCell text="<strong>К/р</strong>"/>
                    <asp:TableHeaderCell text="<strong>Форма аттестации</strong>"/>
                </asp:TableHeaderRow>
            </asp:Table>
            <asp:HiddenField runat="server" ID="DSTrowscount" Value="0" />
            <strong style="color: darkred">Будьте внимательны при заполнении таблицы. После сортировки редактировать данные невозможно!</strong><br />
            <div style="border: 1px solid #ddd; border-radius: 5px">
                <asp:Table runat="server" style="border: 1px solid #ddd;" ID="DisciplineStructureControlTable" BorderWidth="1" GridLines="Both">
                     <asp:TableHeaderRow>
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" text="Тема..."/>
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" text="Семестр..."/>
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" text="Неделя..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" text="Л..."/>
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" text="П/С..."/>
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" text="Лаб..."/>
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" text="СРС..."/>
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" text="КСР..."/>
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" text="К.Р..."/>
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" text="К.П..."/>
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" text="РГР..."/>
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" text="Под. к л/р..."/>
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" text="К/р..."/>
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" text="Форма аттестации..."/>
                    </asp:TableHeaderRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" ID="DSCT_topic" />
                        </asp:TableCell><asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" ID="DSCT_sem" TextMode="Number" Width="100%" min="0"/>
                        </asp:TableCell><asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" ID="DSCT_week" TextMode="Number" Width="100%" min="0" />
                        </asp:TableCell><asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" ID="DSCT_L" TextMode="Number" Width="100%" min="0" />
                        </asp:TableCell><asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" ID="DSCT_PS" TextMode="Number" Width="100%" min="0" />
                        </asp:TableCell><asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" ID="DSCT_Lab" TextMode="Number" Width="100%" min="0" />
                        </asp:TableCell><asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" ID="DSCT_SRS" TextMode="Number" Width="100%" min="0" />
                        </asp:TableCell><asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" ID="DSCT_KSR" TextMode="Number" Width="100%" min="0" />
                        </asp:TableCell><asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" ID="DSCT_KR" TextMode="Number" Width="100%" min="0" />
                        </asp:TableCell><asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" ID="DSCT_KP" TextMode="Number" Width="100%" min="0" />
                        </asp:TableCell><asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" ID="DSCT_RGR" TextMode="Number" Width="100%" min="0" />
                        </asp:TableCell><asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" ID="DSCT_preptolr" Width="100%"  />
                        </asp:TableCell><asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" ID="DSCT_KRend" TextMode="Number" Width="100%" min="0" />
                        </asp:TableCell><asp:TableCell>
                            <asp:DropDownList runat="server" CssClass="form-control" ID="DSCT_AttestatForm">
                                <asp:ListItem Text="Зачет" />
                                <asp:ListItem Text="Экзамен" />
                            </asp:DropDownList>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>  
                <br />
                <div style="width: 35%">
                    <asp:Button runat="server" style="margin-bottom:4px" CssClass="btn light" ID="AddRowDSCT_Button" Text="Добавить" OnClick="AddRowDSCT_Button_Click" />
                    <asp:Button runat="server" style="margin-bottom:4px" CssClass="btn light" ID="DeleteRowDSCT_Button" Text="Удалить №" OnClick="DeleteRowDSCT_Button_Click" />
                    <asp:TextBox runat="server" CssClass="form-control" style="display: inline;"  ID="rowNumberDSCT" TextMode="Number" /><br />
                    <asp:Button runat="server" style="margin-top:3px" CssClass="btn light" ID="SortDSCT_Button" Text="Сортировать" OnClick="SortDSCT_Button_Click" />
                    <asp:Button runat="server" style="margin-top:3px" CssClass="btn light" ID="ExportDSCT_Button" Text="Экспортировать таблицу" OnClick="ExportDSCT_Button_Click" /><br />
                    <asp:Button runat="server" style="margin-top:3px" CssClass="btn light" ID="DSTClear_Button" Text="Заполнить заново" OnClick="DSTClear_Button_Click" OnClientClick="if (!OnTableClearConfirm()) return false;" />
                </div>
            </div>
            <br />
            <h4 class="unselecteble" id="label_DisciplineStructureContent">Содержание дисциплины: <span style="color:red">*</span></h4>
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" Width="100%" id="DisciplineStructureContent" TextMode="MultiLine" />
                <asp:Button runat="server" CssClass="btn light" style="margin-top:3px" ID="DisciplineStructureContentFill_Button" Text="Заполнить" OnClick="DisciplineStructureContentFill_Button_Click" />
            </div>
            <br />
            <h4 class="unselecteble" id="label_SemesteresContent">Содержание разделов дисциплины: <span style="color:red">*</span></h4>
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" Width="100%" ID="SemesteresContent" TextMode="MultiLine"  />
                <asp:Button runat="server" CssClass="btn light" style="margin-top:3px" ID="SemesteresContentFill_Button" Text="Заполнить" OnClick="SemesteresContentFill_Button_Click" />
            </div>
            <br />
            <h4 class="unselecteble" id="label_EducationalTechnology">Образовательные технологии: <span style="color:red">*</span></h4>
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" Width="100%" Height="150px" id="EducationalTechnology" TextMode="MultiLine" />
            </div>
            <br />
            <h4 class="unselecteble" id="label_MonitoringTools_Description">Оценочные средства для текущего контроля успеваемости: <span style="color:red">*</span></h4>
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" Width="100%" Height="150px" ID="MonitoringTools_Description"  TextMode="MultiLine" />
            </div>    
        </div>

        <div class="tab-pane fade" ID="fond_evaluation_tools">
            <h3 class="unselecteble">Фонд оценочных средств</h3><br />
            <h4 class="unselecteble">Перечень оценочных средств</h4>
            <asp:Table runat="server" CssClass="table table-bordered" ID="PrilAssessTable" BorderWidth="1" GridLines="Both">
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="№ ОС" Width="5%"/>
                    <asp:TableHeaderCell Text="Наименование оценочного средства" Width="30%" />
                    <asp:TableHeaderCell Text="Краткая характеристика оценочного средства" Width="30%" />
                    <asp:TableHeaderCell Text="Представление оценочного средства в ФОС" Width="30%" />
                </asp:TableHeaderRow>
            </asp:Table>
            <asp:HiddenField runat="server" ID="PATrowscount" Value="0" />
            <div style="border: 1px solid #ddd; border-radius: 5px">
                <asp:Table runat="server" CssClass="table table-bordered" BorderWidth="1" GridLines="Both">
                    <asp:TableRow>
                        <asp:TableCell Width="35%">
                            <asp:TextBox id="PATName" CssClass="form-control" Width="100%" runat="server" placeholder="Оценочное средство..." TextMode="MultiLine" />
                        </asp:TableCell>
                        <asp:TableCell Width="30%">
                            <asp:TextBox id="PATSpec" runat="server" CssClass="form-control" Width="100%" placeholder="Краткая характеристика..." TextMode="MultiLine" />
                        </asp:TableCell>
                        <asp:TableCell Width="30%">
                            <asp:TextBox id="PATinFOS" runat="server" CssClass="form-control" Width="100%" placeholder="Представление в ФОС..." TextMode="MultiLine" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>

                <asp:Button runat="server" CssClass="btn light" ID="button_addRowInPAT" Text="Добавить" OnClick="button_addRowInPAT_Click" />
                <asp:Button runat="server" CssClass="btn light" ID="button_deleteRowInPAT" Text="Удалить" OnClick="button_deleteRowInPAT_Click" />
            </div>
            <br />
            <em>Перечень компетенций с указанием этапов их формирования в процессе освоения образовательной программы.<br />
                В результате освоения дисциплины (модуля) формируются следующие компетенции:</em><br />
            <asp:Table ID="MonitoringTools_FondTable" CssClass="table table-bordered" runat="server" BorderWidth="1" GridLines="Both">
                <asp:TableRow>
                    <asp:TableHeaderCell Width="5%" Text="<strong>Код компетенции</strong>"  />
                    <asp:TableHeaderCell width="90%" Text="<strong>В результате освоения образовательной программы обучающийся должен обладать</strong>" />
                </asp:TableRow>
            </asp:Table>
            <asp:DropDownList runat="server" CssClass="form-control"  ID="Competence_FondTable" DataSourceID="SqlDataSourceCompetencesFiltered" DataTextField="Code" DataValueField="Description"  />
            <br />
            <asp:Button runat="server" CssClass="btn light" style="margin-top: 2px" ID="CreateRowFondTable" Text="Добавить" OnClick="CreateRowFondTable_Click" />
            <asp:Button runat="server" CssClass="btn light" style="margin-top: 2px" ID="DeleteRowFondTable" Text="Удалить" OnClick="DeleteRowFondTable_Click" />
            <asp:HiddenField runat="server" ID="ftrowscount" Value="0" />
            <br />
            <br />
            <strong>Описание показателей и критериев оценивания компетенций, <br />
                формируемых по итогам освоения дисциплины (модуля), описание шкал оценивания</strong><br />
            <asp:Table ID="CriterionsTable" CssClass="table table-bordered" runat="server" BorderWidth="1" GridLines="Both">
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell  RowSpan="2" text="<strong>Описание компетенции</strong>" width="200px"/>
                    <asp:TableHeaderCell  RowSpan="2" text="<strong>Показатель</strong>" width="200px"/>
                    <asp:TableHeaderCell ColumnSpan="4" text="<strong>Критерий оценивания</strong>" Width="600px"/>
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>2</strong>" />
                    <asp:TableHeaderCell Text="<strong>3</strong>" />
                    <asp:TableHeaderCell Text="<strong>4</strong>" />
                    <asp:TableHeaderCell Text="<strong>5</strong>" />
                </asp:TableHeaderRow>
            </asp:Table>
            <asp:DropDownList runat="server" CssClass="form-control" style="margin-left: 15px; margin-bottom:5px" ID="Competence_CriterionsTable" DataSourceID="SqlDataSourceCompetencesFiltered" DataTextField="Code" DataValueField="Description" />
            <br />
            <div class="container">
                <div class="row">
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control" Width="100%" style="max-width: 100%" id="IndicatorCriterionsTable_Know" runat="server"   placeholder="Знать..."  /></div>
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control" Width="100%" style="max-width: 100%" id="CriterionCriterionsTable_Know_2" runat="server" placeholder="На 2..." /></div>
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control" Width="100%" style="max-width: 100%" id="CriterionCriterionsTable_Know_3" runat="server" placeholder="На 3..." /></div>
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control" Width="100%" style="max-width: 100%" id="CriterionCriterionsTable_Know_4" runat="server" placeholder="На 4..." /></div>
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control" Width="100%" style="max-width: 100%" id="CriterionCriterionsTable_Know_5" runat="server" placeholder="На 5..." /></div>
                </div>
                <div class="row">
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control" Width="100%" style="max-width: 100%" id="IndicatorCriterionsTable_Can" runat="server"    placeholder="Уметь..."/></div>
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control" Width="100%" style="max-width: 100%" id="CriterionCriterionsTable_Can_2" runat="server"  placeholder="На 2..."/></div>
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control" Width="100%" style="max-width: 100%" id="CriterionCriterionsTable_Can_3" runat="server"  placeholder="На 3..."/></div>
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control" Width="100%" style="max-width: 100%" id="CriterionCriterionsTable_Can_4" runat="server"  placeholder="На 4..."/></div>
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control" Width="100%" style="max-width: 100%" id="CriterionCriterionsTable_Can_5" runat="server"  placeholder="На 5..."/></div>
                </div>
                <div class="row">
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control col-md-2" Width="100%" style="max-width: 100%" id="IndicatorCriterionsTable_Wield" runat="server"  placeholder="Владеть..."/> </div>
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control col-md-2" Width="100%" style="max-width: 100%" id="CriterionCriterionsTable_Wield_2" runat="server" placeholder="На 2..." /></div>
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control col-md-2" Width="100%" style="max-width: 100%" id="CriterionCriterionsTable_Wield_3" runat="server" placeholder="На 3..." /></div>
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control col-md-2" Width="100%" style="max-width: 100%" id="CriterionCriterionsTable_Wield_4" runat="server" placeholder="На 4..." /></div>
                    <div class="col-md-2"><asp:TextBox TextMode="MultiLine" CssClass="form-control col-md-2" Width="100%" style="max-width: 100%" id="CriterionCriterionsTable_Wield_5" runat="server" placeholder="На 5..." /></div>
                </div>
            </div>
            
            <asp:Button runat="server" CssClass="btn light" ID="CreateRowsCriterionsTable" style="margin-top:5px; margin-left: 15px;" Text="Добавить" OnClick="CreateRowsCriterionsTable_Click" />
            <asp:Button runat="server" CssClass="btn light" ID="DeleteRowsCriterionsTable" style="margin-top:5px" Text="Удалить" OnClick="DeleteRowsCriterionsTable_Click" />
            <asp:HiddenField runat="server" ID="CTrowscount" Value="1" />
            
            <br /><br />

            <h4 class="unselecteble" id="label_ZachetDescription">Форма промежуточной аттестации: зачет <span style="color:red">*</span></h4>
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" id="ZachetDescription" TextMode="MultiLine" />
            </div>
            <br />
            <em>Зачтено:</em><br />
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" ID="Zachteno" TextMode="MultiLine"/>
            </div>
            <br />
            <em>Не зачтено:</em><br />
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" id="NeZachteno" TextMode="MultiLine"/>
            </div>

            <br />
            
            <h4 class="unselecteble" id="label_ExamDescription">Форма промежуточной аттестации: экзамен <span style="color:red">*</span></h4>
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" id="ExamDescription" TextMode="MultiLine"/>
            </div>
            <br />
            <em>Отлично:</em><br />
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" ID="AttestatGreat" TextMode="MultiLine"/>
            </div>
            <br />
            <em>Хорошо:</em><br />
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" id="AttestatGood" TextMode="MultiLine"/>
            </div>
            <br />
            <em>Удовлетворительно:</em><br />
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" id="AttestatSatisfactorily" TextMode="MultiLine"/>
            </div>
            <br />
            <em>Неудовлетворительно:</em><br />
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" id="AttestatNotSatisfactorily" TextMode="MultiLine"/>
            </div>
        </div>

        <div class="tab-pane fade" ID="discipline_provide">
            <h3 class="unselecteble">Учебно-методическое и информационное обеспечение дисциплины</h3><br />
            <h4 class="unselecteble" id="label_BasicLiterature">Основная литература: <span style="color:red">*</span></h4>
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" Width="100%" Height="150px" id="BasicLiterature" TextMode="MultiLine" />
            </div>
            <br />
            <h4 class="unselecteble">Дополнительная литература:</h4>
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" Width="100%" Height="150px" id="AdditionalLiterature" TextMode="MultiLine" />
            </div>
            <br />
            <h4 class="unselecteble">Программное обеспечение и интернет-ресурсы:</h4>
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" Width="100%" Height="150px" id="ITResources"  TextMode="MultiLine" />
            </div>
            <br />
            <h4 class="unselecteble" id="label_MaterialTechnicalSupport">Материально-техническое обеспечение дисциплины: <span style="color:red">*</span></h4><br />
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" Width="100%" Height="150px" ID="MaterialTechnicalSupport" TextMode="MultiLine"/>
            </div>
            <br />
            <h4 class="unselecteble">Методические рекомендации для студентов:</h4>
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" Width="100%" Height="150px" ID="StudentRecommendations" TextMode="MultiLine"/>
            </div>
            <br />
            <h4 class="unselecteble">Методические рекомендации для преподавателя:</h4>
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" Width="100%" Height="150px" ID="TeacherRecommendations" TextMode="MultiLine"/>
            </div>
        </div>

        <div class="tab-pane fade" ID="competences">
            <h3 class="unselecteble">Описание компетенций</h3><br />
            <h4 class="unselecteble">Универсальные компетенции</h4>
            <asp:Table runat="server" CssClass="table table-bordered" ID="PrilFondTable_1" BorderWidth="1" GridLines="Both">
                <asp:TableRow>
                    <asp:TableCell Text="В процессе освоения данной дисциплины студент формирует и демонстрирует следующие <strong>универсальные</strong> компетенции:" ColumnSpan="6" />
                </asp:TableRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Компетенции</strong>" ColumnSpan="2"/>
                    <asp:TableHeaderCell Text="<strong>Перечень компонентов</strong>" RowSpan="2"/>
                    <asp:TableHeaderCell Text="<strong>Технология формирования компетенции</strong>" RowSpan="2" />
                    <asp:TableHeaderCell Text="<strong>Форма оценочного средства</strong>" RowSpan="2" />
                    <asp:TableHeaderCell Text="<strong>Степени уровней освоения компетенций</strong>" RowSpan="2" />
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Индекс</strong>" />
                    <asp:TableHeaderCell Text="<strong>Формулировка</strong>" />
                </asp:TableHeaderRow>
            </asp:Table>
            <asp:HiddenField runat="server" ID="PFT_1rowscount" Value="0" />
            <br />
            <h4 class="unselecteble">Общепрофессиональные компетенции</h4>
            <asp:Table runat="server" CssClass="table table-bordered" ID="PrilFondTable_2" BorderWidth="1" GridLines="Both">
                <asp:TableRow>
                    <asp:TableCell Text="В процессе освоения данной дисциплины студент формирует и демонстрирует следующие <strong>общепрофессиональные</strong> компетенции:" ColumnSpan="6" />
                </asp:TableRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Компетенции</strong>" ColumnSpan="2"/>
                    <asp:TableHeaderCell Text="<strong>Перечень компонентов</strong>" RowSpan="2"/>
                    <asp:TableHeaderCell Text="<strong>Технология формирования компетенции</strong>" RowSpan="2" />
                    <asp:TableHeaderCell Text="<strong>Форма оценочного средства</strong>" RowSpan="2" />
                    <asp:TableHeaderCell Text="<strong>Степени уровней освоения компетенций</strong>" RowSpan="2" />
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Индекс</strong>" />
                    <asp:TableHeaderCell Text="<strong>Формулировка</strong>" />
                </asp:TableHeaderRow>
            </asp:Table>
            <asp:HiddenField runat="server" ID="PFT_2rowscount" Value="0" />
            <br />
            <h4 class="unselecteble">Профессиональные компетенции</h4>
            <asp:Table runat="server" CssClass="table table-bordered" ID="PrilFondTable_3" BorderWidth="1" GridLines="Both">
                <asp:TableRow>
                    <asp:TableCell Text="В процессе освоения данной дисциплины студент формирует и демонстрирует следующие <strong>профессиональные</strong> компетенции:" ColumnSpan="6" />
                </asp:TableRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Компетенции</strong>" ColumnSpan="2"/>
                    <asp:TableHeaderCell Text="<strong>Перечень компонентов</strong>" RowSpan="2"/>
                    <asp:TableHeaderCell Text="<strong>Технология формирования компетенции</strong>" RowSpan="2" />
                    <asp:TableHeaderCell Text="<strong>Форма оценочного средства</strong>" RowSpan="2" />
                    <asp:TableHeaderCell Text="<strong>Степени уровней освоения компетенций</strong>" RowSpan="2" />
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Индекс</strong>" />
                    <asp:TableHeaderCell Text="<strong>Формулировка</strong>" />
                </asp:TableHeaderRow>
            </asp:Table>
            <asp:HiddenField runat="server" ID="PFT_3rowscount" value="0" />

                <asp:DropDownList runat="server" style="margin-bottom:2px" CssClass="form-control" ID="PFTCom" DataSourceID="SqlDataSourceCompetencesFiltered" DataTextField="Code" DataValueField="Description" />
                <asp:Table runat="server" CssClass="table table-bordered" ID="DataRowInPrilFondTables" BorderWidth="1" GridLines="Both">
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" Text="Перечень компонентов..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" Text="Технология формирования компетенции..."/>
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" Text="Форма оценочного средства..." />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align: center" Text="Степени уровней освоения компетенций..." />
                    </asp:TableHeaderRow>
                    <asp:TableRow>
                       <asp:TableCell>
                            <asp:TextBox id="PFTKomponentsList" CssClass="form-control" Width="100%" TextMode="MultiLine" runat="server"/>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox id="PFTCompetenceTechnology" CssClass="form-control" Width="100%" TextMode="MultiLine" runat="server" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox id="PFTAssessForm" runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox id="PFTSteps" runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>

                <asp:Button runat="server" CssClass="btn light" style="margin-top:2px;" ID="button_createRowInPFT_1" Text="Добавить в универсальные" OnClick="button_createRowInPFT_1_Click" />
                <asp:Button runat="server" CssClass="btn light" style="margin-top:2px;" ID="button_createRowInPFT_2" Text="Добавить в общепрофессиональные" OnClick="button_createRowInPFT_2_Click" />
                <asp:Button runat="server" CssClass="btn light" style="margin-top:2px;" ID="button_createRowInPFT_3" Text="Добавить в профессиональные" OnClick="button_createRowInPFT_3_Click" />
                <br />                                          
                <asp:Button runat="server" CssClass="btn light" style="margin-top:2px;" ID="button_deleteRowInPFT_1" Text="Удалить из универсальных" OnClick="button_deleteRowInPFT_1_Click" />
                <asp:Button runat="server" CssClass="btn light" style="margin-top:2px;" ID="button_deleteRowInPFT_2" Text="Удалить из общепрофессиональных" OnClick="button_deleteRowInPFT_2_Click" />
                <asp:Button runat="server" CssClass="btn light" style="margin-top:2px;" ID="button_deleteRowInPFT_3" Text="Удалить из профессиональных" OnClick="button_deleteRowInPFT_3_Click" />
            
        </div>

        <div class="tab-pane fade" id="zachet">
            <h3 class="unselecteble">Элемент контроля - зачёт</h3>
            <h4 class="unselecteble">Критерии оценки:</h4>
            <asp:Table runat="server" CssClass="table table-bordered" ID="PrilCompetenceTable_Zachet" BorderWidth="1" GridLines="Both">
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Описание</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Контролируемый результат обучения</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Контролируемые темы (разделы) дисциплины</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Недифференцированный зачет</strong>" ColumnSpan="2" />
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Критерии оценивания</strong>" ColumnSpan="2"/>
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Зачтено</strong>" />
                    <asp:TableHeaderCell Text="<strong>Не зачтено</strong>" />
                </asp:TableHeaderRow>
            </asp:Table>
            <asp:HiddenField runat="server" ID="PCT_zachetrowscount" Value="0" />
            <div>
                <asp:Table runat="server" CssClass="table table-bordered" BorderWidth="1" GridLines="Both">
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Код" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Результат" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Темы" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Зачтено при" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Не зачтено при" />
                    </asp:TableHeaderRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:DropDownList runat="server" ID="DropDown_PCTZ" CssClass="form-control" DataSourceID="SqlDataSourceCompetencesFiltered" DataTextField="Code" DataValueField="Description" />
                            <%--<asp:TextBox runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" id="PCTZDiscription"/>--%>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" id="PCTZRezult" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" id="PCTZTopics" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" id="PCTZZachet" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" id="PCTZNeZachet" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Button runat="server" CssClass="btn light" ID="addRowInPCTzachet" Text="Добавить" OnClick="addRowInPCTzachet_Click" />
                <asp:Button runat="server" CssClass="btn light" ID="deleteRowInPCTzachet" Text="Удалить" OnClick="deleteRowInPCTzachet_Click" />
            </div>
            <br />
            <h4 class="unselecteble">Вопросы к зачёту:</h4>
            <div class="check_screen">
                <asp:TextBox runat="server" CssClass="ckeditor" Width="100%" Height="150px" ID="ZachetQuestions" TextMode="MultiLine" />
            </div>
            <strong>Составитель:</strong> 
            <asp:DropDownList runat="server" CssClass="form-control" style="margin-top:5px" ID="DeveloperZachet" DataSourceID="SqlDataSourceUsers" DataTextField="UserName" DataValueField="UserId" />
            <strong>Дата:</strong> 
            <asp:TextBox runat="server" CssClass="form-control" style="margin-top:5px" ID="ZachetQuestionsDate" TextMode="Date" />
        </div>

        <div class="tab-pane fade" id="exam">
            <h3 class="unselecteble">Элемент контроля - экзамен</h3>
            <h4 class="unselecteble">Критерии оценки:</h4>
            <asp:Table runat="server" CssClass="table table-bordered" ID="PrilCompetenceTable_Exam" BorderWidth="1" GridLines="Both">
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Описание</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Контролируемый результат обучения</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Контролируемые темы (разделы) дисциплины</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Экзамен</strong>" ColumnSpan="4" />
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Критерии оценивания</strong>" ColumnSpan="4"/>
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>2</strong>" />
                    <asp:TableHeaderCell Text="<strong>3</strong>" />
                    <asp:TableHeaderCell Text="<strong>4</strong>" />
                    <asp:TableHeaderCell Text="<strong>5</strong>" />
                </asp:TableHeaderRow>
            </asp:Table>
            <asp:HiddenField runat="server" ID="PCT_examrowscount" Value="0" />
            <div>
                <asp:Table runat="server" CssClass="table table-bordered" BorderWidth="1" GridLines="Both">
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Код" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Результат" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Темы" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 2" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 3" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 4" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 5" />
                    </asp:TableHeaderRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:DropDownList runat="server" ID="DropDown_PCTE" CssClass="form-control" DataSourceID="SqlDataSourceCompetencesFiltered" DataTextField="Code" DataValueField="Description" />
                            <%--<asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTEDiscription" />--%>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTERezult" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTETopics" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTE2" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTE3" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTE4" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTE5" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Button runat="server" CssClass="btn light" ID="addRowInPCTExamTable" Text="Добавить" OnClick="addRowInPCTExamTable_Click" />
                <asp:Button runat="server" CssClass="btn light" ID="deleteRowInPCTExamTable" Text="Удалить" OnClick="deleteRowInPCTExamTable_Click" />
            </div>
            <br />
            <strong>Вопросы к экзамену:</strong><br />
            <div class="check_screen">
                <asp:TextBox runat="server" ID="ExamQuestions" Width="100%" Height="150px" CssClass="ckeditor" TextMode="MultiLine" />
            </div>
            <strong>Составитель:</strong> <asp:DropDownList runat="server" style="margin-top:5px" CssClass="form-control" ID="DeveloperExam" DataSourceID="SqlDataSourceUsers" DataTextField="UserName" DataValueField="UserId" />
            <strong>Дата:</strong> <asp:TextBox runat="server" style="margin-top:5px" CssClass="form-control" ID="ExamQuestionsDate" TextMode="Date" />
        </div>

        <div class="tab-pane fade" id="referat">
            <h3 class="unselecteble">Элемент контроля - реферат</h3>
            <h4 class="unselecteble">Критерии оценки:</h4>
            <asp:Table runat="server" CssClass="table table-bordered" ID="PrilCompetenceTable_Referat" BorderWidth="1" GridLines="Both">
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Описание</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Контролируемый результат обучения</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Контролируемые темы (разделы) дисциплины</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Реферат</strong>" ColumnSpan="4" />
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Критерии оценивания</strong>" ColumnSpan="4"/>
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>2</strong>" />
                    <asp:TableHeaderCell Text="<strong>3</strong>" />
                    <asp:TableHeaderCell Text="<strong>4</strong>" />
                    <asp:TableHeaderCell Text="<strong>5</strong>" />
                </asp:TableHeaderRow>
            </asp:Table>
            <asp:HiddenField runat="server" ID="PCT_referatrowscount" Value="0" />
            <div>
                <asp:Table runat="server" CssClass="table table-bordered" BorderWidth="1" GridLines="Both">
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Код" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Результат" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Темы" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 2" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 3" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 4" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 5" />
                    </asp:TableHeaderRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:DropDownList runat="server" ID="DropDown_PCTR" CssClass="form-control" DataSourceID="SqlDataSourceCompetencesFiltered" DataTextField="Code" DataValueField="Description" />
                            <%--<asp:TextBox runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" id="PCTRDiscription"/>--%>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" id="PCTRRezult" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" id="PCTRTopics" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" id="PCTR2" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" id="PCTR3" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" id="PCTR4" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine" id="PCTR5" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Button runat="server" CssClass="btn light" ID="addRowInPCTReferatTable" Text="Добавить" OnClick="addRowInPCTReferatTable_Click" />
                <asp:Button runat="server" CssClass="btn light" ID="deleteRowInPCTReferatTable" Text="Удалить" OnClick="deleteRowInPCTReferatTable_Click" />
            </div>
            <br />
            <strong>Темы рефератов:</strong><br />
            <div class="check_screen">
                <asp:TextBox runat="server" ID="ReferatTopics" Width="100%" Height="150px" CssClass="ckeditor" TextMode="MultiLine" />
            </div>
            <strong>Составитель:</strong> <asp:DropDownList runat="server" style="margin-top:5px" CssClass="form-control" ID="DeveloperReferat"  DataSourceID="SqlDataSourceUsers" DataTextField="UserName" DataValueField="UserId" />
            <strong>Дата:</strong> <asp:TextBox runat="server" CssClass="form-control" style="margin-top:5px" ID="ReferatTopicsDate" TextMode="Date" />
        </div>

        <div class="tab-pane fade" id="kt">
            <h3 class="unselecteble">Элемент контроля - контрольная точка</h3>
            <h4 class="unselecteble">Критерии оценки:</h4>
            <asp:Table runat="server" CssClass="table table-bordered" ID="PrilCompetenceTable_KT" BorderWidth="1" GridLines="Both">
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Описание</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Контролируемый результат обучения</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Контролируемые темы (разделы) дисциплины</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Контрольная работа</strong>" ColumnSpan="4" />
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Критерии оценивания</strong>" ColumnSpan="4"/>
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>2</strong>" />
                    <asp:TableHeaderCell Text="<strong>3</strong>" />
                    <asp:TableHeaderCell Text="<strong>4</strong>" />
                    <asp:TableHeaderCell Text="<strong>5</strong>" />
                </asp:TableHeaderRow>
            </asp:Table>
            <asp:HiddenField runat="server" ID="PCT_ktrowscount" Value="0" />
            <div>
                <asp:Table runat="server" CssClass="table table-bordered" BorderWidth="1" GridLines="Both">
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Код" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Результат" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Темы" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 2" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 3" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 4" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 5" />
                    </asp:TableHeaderRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:DropDownList runat="server" ID="DropDown_PCTKT" CssClass="form-control" DataSourceID="SqlDataSourceCompetencesFiltered" DataTextField="Code" DataValueField="Description" />
                            <%--<asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTKTDiscription"/>--%>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTKTRezult" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTKTTopics" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTKT2" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTKT3" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTKT4" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTKT5" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Button runat="server" CssClass="btn light" ID="addRowInPCTKTTable" Text="Добавить" OnClick="addRowInPCTKTTable_Click" />
                <asp:Button runat="server" CssClass="btn light" ID="deleteRowInPCTKTTable" Text="Удалить" OnClick="deleteRowInPCTKTTable_Click" />
            </div>
            <br />
            <strong>Темы контрольных работ:</strong><br />
            <div class="check_screen">
                <asp:TextBox runat="server" ID="KTTopics" Width="100%" Height="150px" CssClass="ckeditor" TextMode="MultiLine" />
            </div>
            <strong>Составитель:</strong> <asp:DropDownList runat="server" style="margin-top:5px" CssClass="form-control" ID="DeveloperKT" DataSourceID="SqlDataSourceUsers" DataTextField="UserName" DataValueField="UserId" />
            <strong>Дата:</strong> <asp:TextBox runat="server" ID="KTDate" style="margin-top:5px" CssClass="form-control" TextMode="Date" />
        </div>
        
        <div class="tab-pane fade" id="prakrika">
            <h3 class="unselecteble">Элемент контроля - практическое занятие</h3>
            <h4 class="unselecteble">Критерии оценки:</h4>
            <asp:Table runat="server" CssClass="table table-bordered" ID="PrilCompetenceTable_Praktika" BorderWidth="1" GridLines="Both">
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Описание</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Контролируемый результат обучения</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Контролируемые темы (разделы) дисциплины</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Практическое занятие</strong>" ColumnSpan="4" />
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Критерии оценивания</strong>" ColumnSpan="4"/>
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>2</strong>" />
                    <asp:TableHeaderCell Text="<strong>3</strong>" />
                    <asp:TableHeaderCell Text="<strong>4</strong>" />
                    <asp:TableHeaderCell Text="<strong>5</strong>" />
                </asp:TableHeaderRow>
            </asp:Table>
            <asp:HiddenField runat="server" ID="PCT_praktikarowscount" Value="0" />
            <div>
                <asp:Table runat="server" CssClass="table table-bordered"  BorderWidth="1" GridLines="Both">
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Код" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Результат" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Темы" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 2" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 3" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 4" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 5" />
                    </asp:TableHeaderRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:DropDownList runat="server" ID="DropDown_PCTPR" CssClass="form-control" DataSourceID="SqlDataSourceCompetencesFiltered" DataTextField="Code" DataValueField="Description" />
                            <%--<asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTPDiscription"/>--%>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTPRezult" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTPTopics" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTP2" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTP3" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTP4" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTP5" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Button runat="server" CssClass="btn light" ID="addRowInPCTPraktikaTable" Text="Добавить" OnClick="addRowInPCTPraktikaTable_Click" />
                <asp:Button runat="server" CssClass="btn light" ID="deleteRowInPCTPraktikaTable" Text="Удалить" OnClick="deleteRowInPCTPraktikaTable_Click" />
            </div>
            <br />
            <strong>Темы практических занятий:</strong><br />
            <div class="check_screen">
                <asp:TextBox runat="server" ID="PraktikTopics" Width="100%" Height="150px" CssClass="ckeditor" TextMode="MultiLine" />
            </div>
            <strong>Составитель:</strong> <asp:DropDownList runat="server" style="margin-top:5px" CssClass="form-control" ID="DeveloperPraktika" DataSourceID="SqlDataSourceUsers" DataTextField="UserName" DataValueField="UserId"  />
            <strong>Дата:</strong> <asp:TextBox runat="server" ID="PraktikaTopicsDate" style="margin-top:5px" CssClass="form-control" TextMode="Date" />
        </div>
        
        <div class="tab-pane fade" id="test">
            <h3 class="unselecteble">Элемент контроля - тест</h3>
            <h4 class="unselecteble">Критерии оценки:</h4>
            <asp:Table runat="server" CssClass="table table-bordered" ID="PrilCompetenceTable_Test" BorderWidth="1" GridLines="Both">
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Описание</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Контролируемый результат обучения</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Контролируемые темы (разделы) дисциплины</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Тест</strong>" ColumnSpan="4" />
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Критерии оценивания</strong>" ColumnSpan="4"/>
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>2</strong>" />
                    <asp:TableHeaderCell Text="<strong>3</strong>" />
                    <asp:TableHeaderCell Text="<strong>4</strong>" />
                    <asp:TableHeaderCell Text="<strong>5</strong>" />
                </asp:TableHeaderRow>
            </asp:Table>
            <asp:HiddenField runat="server" ID="PCT_testrowscount" Value="0" />
            <div>
                <asp:Table runat="server" CssClass="table table-bordered" BorderWidth="1" GridLines="Both">
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Код" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Результат" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Темы" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 2" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 3" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 4" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 5" />
                    </asp:TableHeaderRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:DropDownList runat="server" ID="DropDown_PCTT" CssClass="form-control" DataSourceID="SqlDataSourceCompetencesFiltered" DataTextField="Code" DataValueField="Description" />
                            <%--<asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTTDiscription"/>--%>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTTRezult" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTTTopics" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTT2" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTT3" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTT4" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTT5" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Button runat="server" CssClass="btn light" ID="addRowInPCTtestTable" Text="Добавить" OnClick="addRowInPCTtestTable_Click" />
                <asp:Button runat="server" CssClass="btn light" ID="deleteRowInPCTtestTable" Text="Удалить" OnClick="deleteRowInPCTtestTable_Click" />      
            </div>
            <br />
            <strong>Фонд тестовых заданий:</strong><br />
            <div class="check_screen">
                <asp:TextBox runat="server" ID="TestsFond" Width="100%" Height="150px" TextMode="MultiLine" CssClass="ckeditor" />
            </div>
            <strong>Составитель:</strong> <asp:DropDownList runat="server" style="margin-top:5px" ID="DeveloperTest" CssClass="form-control" DataSourceID="SqlDataSourceUsers" DataTextField="UserName" DataValueField="UserId" />
            <strong>Дата:</strong> <asp:TextBox runat="server" ID="TestsFondDate" style="margin-top:5px" CssClass="form-control" TextMode="Date" />
        </div>

        <div class="tab-pane fade" id="curs">
            <h3 class="unselecteble">Элемент контроля - курсовая работа</h3>
            <h4 class="unselecteble">Критерии оценки:</h4>
            <asp:Table runat="server" CssClass="table table-bordered" ID="PrilCompetenceTable_Curs" BorderWidth="1" GridLines="Both">
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Описание</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Контролируемый результат обучения</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Контролируемые темы (разделы) дисциплины</strong>" RowSpan="3" />
                    <asp:TableHeaderCell Text="<strong>Тест</strong>" ColumnSpan="4" />
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>Критерии оценивания</strong>" ColumnSpan="4"/>
                </asp:TableHeaderRow>
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="<strong>2</strong>" />
                    <asp:TableHeaderCell Text="<strong>3</strong>" />
                    <asp:TableHeaderCell Text="<strong>4</strong>" />
                    <asp:TableHeaderCell Text="<strong>5</strong>" />
                </asp:TableHeaderRow>
            </asp:Table>
            <asp:HiddenField runat="server" ID="PCT_cursrowscount" Value="0" />
            <div>
                <asp:Table runat="server" CssClass="table table-bordered" BorderWidth="1" GridLines="Both">
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Код" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Результат" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="Темы" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 2" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 3" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 4" />
                        <asp:TableHeaderCell ForeColor="Gray" style="text-align:center" Text="На 5" />
                    </asp:TableHeaderRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:DropDownList runat="server" ID="DropDown_PCTC" CssClass="form-control" DataSourceID="SqlDataSourceCompetencesFiltered" DataTextField="Code" DataValueField="Description" />
                            <%--<asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTTDiscription"/>--%>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTCRezult" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTCTopics" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTC2" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTC3" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTC4" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" id="PCTC5" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Button runat="server" CssClass="btn light" ID="addRowInPCTcursTable" Text="Добавить" OnClick="addRowInPCTcursTable_Click" />
                <asp:Button runat="server" CssClass="btn light" ID="deleteRowInPCTcursTable" Text="Удалить" OnClick="deleteRowInPCTcursTable_Click"  />      
            </div>
            <br />
            <strong>Темы курсовых работ:</strong><br />
            <div class="check_screen">
                <asp:TextBox runat="server" ID="CursesFond" Width="100%" Height="150px" TextMode="MultiLine" CssClass="ckeditor" />
            </div>
            <strong>Составитель:</strong> <asp:DropDownList runat="server" style="margin-top:5px" ID="DeveloperCurs" CssClass="form-control" DataSourceID="SqlDataSourceUsers" DataTextField="UserName" DataValueField="UserId" />
            <strong>Дата:</strong> <asp:TextBox runat="server" ID="CursDate" style="margin-top:5px" CssClass="form-control" TextMode="Date" />
        </div>

        <div class="tab-pane fade" ID="persons">
            <h3 class="unselecteble">Составители</h3><br />
            <div runat="server" id="drafters_html_block">
                <strong id="label_drafters">Программу составил(и): <span style="color:red">*</span></strong><br />
                <asp:TextBox runat="server" CssClass="form-control" placeholder="Академ. статус" ID="DrafterStatus" style="margin-bottom:2px" />
                <asp:DropDownList runat="server" CssClass="form-control" ID="DrafterName" DataSourceID="SqlDataSourceDepartmentPersonal" DataTextField="UserName" DataValueField="UserId" /><br />
                <asp:SqlDataSource ID="SqlDataSourceDepartmentPersonal" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" 
                    SelectCommand="SELECT * FROM AspNetUsers INNER JOIN AspNetUserRoles ON AspNetUsers.Id=AspNetUserRoles.UserId INNER JOIN AspNetRoles ON AspNetRoles.Id = AspNetUserRoles.RoleId WHERE AspNetRoles.Name!='Guest' ORDER BY UserName"></asp:SqlDataSource>
                <asp:HiddenField runat="server" ID="AdditionalDraftersCount" Value="0" />
            </div>
            <asp:Button runat="server" ID="Button_AddDrafter" style="margin-top:2px" CssClass="btn light" Text="Добавить составителя" OnClick="Button_AddDrafter_Click" />
            <asp:Button runat="server" ID="Button_DeleteDrafter" style="margin-top:2px" CssClass="btn light" Text="Удалить составителя" OnClick="Button_DeleteDrafter_Click"  /><br />
            <br />
            <strong id="label_dep">Кафедра: <span style="color:red">*</span></strong><br />
            <asp:DropDownList runat="server" ID="DropDown_Department" AutoPostBack="true" OnSelectedIndexChanged="DropDown_Department_SelectedIndexChanged" CssClass="form-control" DataSourceID="SqlDataSourceDepartments" DataTextField="Name" DataValueField="Id" />
            <asp:SqlDataSource runat="server" ID="SqlDataSourceDepartments" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [Departments]" />
            <asp:TextBox runat="server" CssClass="form-control" ID="DraftDate" TextMode="Date"/>
            <asp:TextBox runat="server" CssClass="form-control" placeholder="Протокол" ID="Protocol"  /><br />
            <br />
            <strong id="label_depHead">Заведующий кафедрой: <span style="color:red">*</span></strong><br />
            <asp:TextBox runat="server" CssClass="form-control" placeholder="Должность, академ. статус" ID="DepartmentHeadStatus" style="margin-bottom:2px" />
            <asp:DropDownList runat="server" CssClass="form-control" ID="DepartmentHeadName"  DataSourceID="SqlDataSourceUsers" DataTextField="UserName" DataValueField="UserId"/><br />
            <br />
            <strong id="label_agreements">Программа согласована: <span style="color:red">*</span></strong><br />
            <asp:TextBox runat="server" CssClass="form-control" placeholder="Должность, академ. статус" ID="AgreementDepartmentHeadStatus" style="margin-bottom:2px" />
            <asp:DropDownList runat="server" CssClass="form-control" ID="AgreementDepartmentHeadName"  DataSourceID="SqlDataSourceUsers" DataTextField="UserName" DataValueField="UserId"/><br />
            <asp:TextBox runat="server" CssClass="form-control" placeholder="Должность, академ. статус" ID="AgreementDirectorStatus" style="margin-bottom:2px" />
            <asp:DropDownList runat="server" CssClass="form-control" ID="AgreementDirectorName"  DataSourceID="SqlDataSourceUsers" DataTextField="UserName" DataValueField="UserId"/><br />
            <hr />
            <asp:Button runat="server" CssClass="btn btn-lg dark" id="Button_Create" Text="Отправить программу на рассмотрение" OnClientClick="if (!Validate()) return false;" OnClick="Button_Create_Click"  />
        </div>
    </div>
    <a href="#" class="scrollup">Наверх</a>
    </div>
</asp:Content>
