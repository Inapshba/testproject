﻿<%@ Page Title="- Главная" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="main_test._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <link rel="stylesheet" href="Content/ContentFade.css" />
    <link rel="stylesheet" href="Content/Colored.css" />
    <link rel="stylesheet" href="Content/CustomDiv.css" />
    <link rel="stylesheet" href="Content/BoxStyles.css" />
    <script src="Scripts/ScrollTop.js"></script>
    <link rel="stylesheet" href="Content/WorkStyle.css" />
    
    <div class="body_fade">
        <div class="jumbotron" style="background-color: transparent;">
            <h1>Рабочие программы Московского политехнического университета</h1>
            <p class="lead">Данный сайт предназначен для создания, редактирования и просмотра рабочих программ сотрудниками Московского политехнического университета</p>
            <p><a href="About.aspx" class="btn btn-lg dark">Подробнее &raquo;</a></p>
        </div>

            <div class="row" id="row_1_html" runat="server">
                <div class="post-item col-md-4" onclick="location.href='ProgramsViewRedactor.aspx';">
	             	<div class="item-content shadow_1">
	             		<div class="item-body">
	             			<h2>Управление РПД</h2>
	             			<p>Раздел администрирования рабочими программами</p>
	             		</div>
	             		<div class="item-footer">
	             			<a href="ProgramsViewRedactor.aspx" class="link"><span>Открыть &raquo;</span></a>
	             		</div>
	             	</div>
	             </div>

	             <div runat="server" id="block_UserAdmin" class="post-item col-md-4">
	             	<div class="item-content shadow_1" onclick="location.href='Admin.aspx'">
	             		<div class="item-body">
	             			<h2>Пользователи</h2>
	             			<p>Раздел управления учетными записями пользователей системы</p>
	             		</div>
	             		<div class="item-footer">
	             			<a href="Admin.aspx" class="link"><span>Открыть &raquo;</span></a>
	             		</div>
	             	</div>
	             </div>

	             <div runat="server" id="block_WordBookAdmin" class="post-item col-md-4">
	             	<div class="item-content shadow_1" onclick="location.href='WordBookAdmin.aspx'">
	             		<div class="item-body">
	             			<h2>Справочники</h2>
	             			<p>Раздел управления справочными материалами</p>
	             		</div>
	             		<div class="item-footer">
	             			<a href="WordBookAdmin.aspx" class="link"><span>Открыть &raquo;</span></a>
	             		</div>
	             	</div>
	             </div>
            </div>
	        
            <div class="row">
                <div class="post-item col-md-4">
	            	<div class="item-content shadow_1" onclick="location.href='Designer.aspx'">
	            		<div class="item-body">
	            			<h2>Конструктор</h2>
	            			<p>Раздел разработки учебных программ</p>
	            		</div>
	            		<div class="item-footer">
	            			<a href="Designer.aspx" class="link"><span>Открыть &raquo;</span></a>
	            		</div>
	            	</div>
	            </div>

	            <div class="post-item col-md-4">
	            	<div class="item-content shadow_1" onclick="location.href='MyPrograms.aspx'">
	            		<div class="item-body">
	            			<h2>Мои программы</h2>
	            			<p>Раздел отслеживания истории составленных вами программ</p>
	            		</div>
	            		<div class="item-footer">
	            			<a href="MyPrograms.aspx" class="link"><span>Открыть &raquo;</span></a>
	            		</div>
	            	</div>
	            </div>

	            <div class="post-item col-md-4">
	            	<div class="item-content shadow_1" onclick="location.href='RPDConfirm.aspx'">
	            		<div class="item-body">
	            			<h2>РПД на подпись</h2>
	            			<p>Перечень документов, в которых вы указаны как подписант</p>
	            		</div>
	            		<div class="item-footer">
	            			<a href="RPDConfirm.aspx" class="link"><span>Открыть &raquo;</span></a>
	            		</div>
	            	</div>
	            </div>
            </div>

	        <div class="row">
                <div class="post-item col-md-4">
	            	<div class="item-content shadow_1" onclick="location.href='ProgramsView.aspx'">
	            		<div class="item-body">
	            			<h2>Просмотр</h2>
	            			<p>В данном разделе можно посмотреть утвержденные рабочие программы</p>
	            		</div>
	            		<div class="item-footer">
	            			<a href="ProgramsView.aspx" class="link"><span>Открыть &raquo;</span></a>
	            		</div>
	            	</div>
	            </div>
	        </div>

        <a href="#" class="scrollup">Наверх</a>
    </div>
</asp:Content>
