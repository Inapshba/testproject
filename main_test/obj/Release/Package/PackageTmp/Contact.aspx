﻿<%@ Page Title="- Контакты" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="main_test.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <link rel="stylesheet" href="Content/ContentFade.css" />
    <div class="body_fade">
        <h1>Контакты</h1>
        <h4>Московский политехнический университет</h4>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p><strong>Главный корпус</strong><br />107023, г. Москва, ул. Б. Семеновская, 38 <br />E-mail: <a href="mailto:mospolytech@mospolytech.ru">mospolytech@mospolytech.ru</a><br />Сайт: <a href="https://mospolytech.ru">mospolytech.ru</a></p>
                </div>
                <div class="col-md-6">
                    <p><strong>Корпус ВШПиМ</strong><br />127550, Москва, ул. Прянишникова, 2А <br />E-mail: <a href="mailto:info@mgup.ru">info@mgup.ru</a><br />Сайт: <a href="http://mgup.ru">mgup.ru</a></p>
                </div>
            </div>
        </div>
        <strong>Техподдержка нашего сайта - </strong><a href="mailto:support@mpuprograms.ru">support@mpuprograms.ru</a>
        <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A53940238984d5882559def984dcd40bf045c46df8750104679ec6925b8c5dfa2&amp;width=100%25&amp;height=462&amp;lang=ru_RU&amp;scroll=true"></script>
        <br />
        <p>Для того чтобы воспользоваться услугами нашего сайта, необходимо пройти <a href="Account/Register.aspx">регистрацию</a> и дождаться подтверждения</p>
    </div>
</asp:Content>
