﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Title="- Справочники" AutoEventWireup="true" Async="true" CodeBehind="WordBookAdmin.aspx.cs" Inherits="main_test.WordBookAdmin" Debug="true" ValidateRequest="false" %>

<asp:Content runat="server" ID="WordBooks" ContentPlaceHolderID="MainContent">
    <script src="Scripts/SetActiveTab.js"></script>
    <script src="Scripts/ScrollTop.js"></script>
    <script src="ckeditor/ckeditor.js"></script>
    <script src="ckeditor/adapters/jquery.js"></script>
    <script src="Scripts/ScreenCheck.js"></script>
    <link rel="stylesheet" href="Content/Colored.css"/>
    <link rel="stylesheet" href="Content/WorkStyle.css" />
    <link rel="stylesheet" href="Content/CkeditorStyle.css" />
    <link rel="stylesheet" href="Content/fileUploader.css" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script>
        function OnDelete() {
            if (confirm('Удалить?'))
                return true;
            else
                return false;
        }
    </script>

    <script>
        function showFile(uploader) {
            var input = document.getElementById(uploader.id);
            var f = input.files[0];
            var fileType = f.name.split('.').pop();
            if (fileType == 'xlsx' || fileType == 'xls') {
                document.getElementById(input.id + '_view').innerText = input.files[0].name;
                var reader = new FileReader();
                reader.onload = (function(theFile) {
                    return function(e) {
                    // Render thumbnail.
                    };
                })(f);
                reader.readAsDataURL(f);
            }
            else {
                alert('Выберите файлы с расширением .xls или .xlsx');
            }
            return false;
        }

        function enterNumber(input) {
            var max = Number.parseInt(input.getAttribute('max'));
            if (Number.parseInt(input.value) > max) {
                input.value = max;
            }
        }
        window.onloadend = LoadCheck();
        window.onload = LoadCheck();
        window.onabort = LoadCheck();
        function LoadCheck() {
            var head = document.getElementById('head');
            head.className = '';
            return false;
        }
        function PleaseWait() {
            var head = document.getElementById('head');
            head.className = 'blinkText';
            return false;
        }
    </script>

    <h2 id="head">Справочники сайта</h2>
    <asp:Label runat="server" ID="ErrorMes" ForeColor="Red" style="margin-bottom: 5px" Visible="false" />
    <h5 runat="server" ID="Message_Label" style="margin-bottom: 5px" Visible="false"></h5>
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" class="tabs-dark" href="#competences">Компетенции</a></li>
        <li><a data-toggle="tab" class="tabs-dark" href="#disciplines">Дисциплины</a></li>
        <li><a data-toggle="tab" class="tabs-dark" href="#qualifications">Степени выпускников</a></li>
        <li class="dropdown tabs-dark">
            <a data-toggle="dropdown" class="dropdown-toggle tabs-dark" href="#">
            Подразделения 
            <b class="caret"></b>
            </a>
            <ul class="dropdown-menu">
                <li><a data-toggle="tab" class="tabs-dark" href="#training_directions">Направления</a></li>
                <li><a data-toggle="tab" class="tabs-dark" href="#profils">Профили</a></li>
                <li><a data-toggle="tab" class="tabs-dark" href="#institutes">Факультеты</a></li>
                <li><a data-toggle="tab" class="tabs-dark" href="#departments">Кафедры</a></li>
            </ul>
        </li>
        <li><a data-toggle="tab" class="tabs-dark" href="#training_plans">Учебные планы</a></li>
        <li><a data-toggle="tab" class="tabs-dark" href="#fgos">ФГОС</a></li>
    </ul>

    <div class="tab-content">

        <div id="competences" class="tab-pane active">
            <div class="container" style="padding:0px">
                <div class="col-md-6" style="padding:0px">
                    <h4>Ручное заполнение</h4>
                    <strong>Компетенция: </strong> 
                    <asp:DropDownList runat="server" CssClass="form-control" ID="Competence_DropDownList" DataSourceID="SqlDataSourceCompetences" DataTextField="Code" DataValueField="id" AutoPostBack="true" OnSelectedIndexChanged="Competence_DropDownList_SelectedIndexChanged"/>
                    <asp:SqlDataSource ID="SqlDataSourceCompetences" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [Competences]"></asp:SqlDataSource>
                    <%--<asp:Button runat="server" ID="FillCompetenceControlls_Button" style="margin-bottom:2px" Text="Заполнить поля" CssClass="btn light" OnClick="FillCompetenceControlls_Button_Click" />--%>
                    <br />
                    <br />
                    <strong>ФГОС: </strong>
                    <asp:DropDownList runat="server" ID="DropDown_CompetencesFGOS" CssClass="form-control" DataSourceID="SqlDataSourceFGOS" DataTextField="Name" DataValueField="id" OnDataBound="DropDown_CompetencesFGOS_DataBound" />
                    <br /><br />
                    <strong>Код: </strong>
                    <asp:TextBox runat="server" CssClass="form-control" ID="CompetenceCode_TextBox" /><br/>
                    <br />
                    <strong>В результате освоения компетенции слюучающийся должен обладать:</strong>
                    <asp:TextBox runat="server" ID="CompetenceDescription_TextBox" CssClass="ckeditor" TextMode="MultiLine" Text="Способностью " Height="150px" />
                    <br />
                    <asp:Button runat="server" ID="CreateCompetence_Button" Text="Создать" CssClass="btn dark" OnClick="CreateCompetence_Button_Click" />
                    <asp:Button runat="server" ID="RedactCompetence_Button" Text="Обновить" CssClass="btn light" OnClick="RedactCompetence_Button_Click" />    
                    <asp:Button runat="server" ID="DeleteCompetence_Button" CssClass="btn red" style="margin-top:2px" Text="Удалить выбранную" OnClientClick="if (!OnDelete()) return false;" OnClick="DeleteCompetence_Button_Click"/>   
                </div>
                <div class="col-md-6">
                    <h4>Загрузка компетенций</h4>
                     Вы также можете загрузить excel-файл с компитенциями со своего устройства в систему. Для этого:
                    <label style="margin-bottom:0px">
                        <a> выберите файл </a>
                        <asp:FileUpload runat="server" ID="uploader_competences" accept=".xlsx" CssClass="hide" onchange="showFile(this)"/>
                    </label>
                     и нажмите "Загрузить" ниже. Далее определите нужные вам столбцы и нажмите "Внести компетенции".
                    <br /><br>
                    <label runat="server" id="uploader_competences_view">Файл не выбран</label><br />
                    <asp:Button runat="server" ID="UploadCompetences_Button" Text="Загрузить" CssClass="btn light" OnClick="UploadCompetences_Button_Click" OnClientClick="PleaseWait()"/>
                    <br /><br />
                    <div runat="server" id="PreviewCompetences_Div" visible="false">
                        <strong>Выгруженная таблица: </strong>
                        <div style="overflow:auto; width:100%; height:272px; margin-top:9px; border: 1px solid gray; margin-top:2px">
                            <asp:Table runat="server" CssClass="table table-bordered" ID="PreviewCompetences_Table" BorderWidth="1" GridLines="Both" />
                        </div>
                        <br />
                        <table>
                            <tr>
                                <td><strong>Столбец кода: </strong></td>
                                <td><asp:TextBox runat="server" ID="CodeInExcelCompetences_TextBox" TextMode="Number" AutoPostBack="false" CssClass="form-control" Width="60" MaxLength="2" min="1" oninput="enterNumber(this)" /></td>
                            </tr>
                            <tr>
                                <td><strong>Столбец описания: </strong></td>
                                <td><asp:TextBox runat="server" ID="DescriptionInExcelCompetences_TextBox" TextMode="Number" AutoPostBack="false" CssClass="form-control" style="margin-top:5px" Width="60" MaxLength="2" min="1" oninput="enterNumber(this)" /></td>
                            </tr>
                            <tr>
                                <td><strong>Начальная строка: </strong></td>
                                <td><asp:TextBox runat="server" ID="RowStartInExcelCompetences_TextBox" TextMode="Number" AutoPostBack="false" CssClass="form-control" style="margin-top:5px" Width="60" MaxLength="2" min="1" oninput="enterNumber(this)" /></td>
                            </tr>
                            <tr>
                                <td><strong>ФГОС: </strong></td>
                                <td><asp:DropDownList runat="server" ID="FGOSForUploadCompetences_DropDown" CssClass="form-control" style="margin-top:5px" DataSourceID="SqlDataSourceFGOS" DataTextField="Name" DataValueField="id" OnDataBound="FGOSForUploadCompetences_DropDown_DataBound" /></td>
                            </tr>
                            <tr>
                                <td colspan="2"><asp:Button runat="server" ID="PushUploadedCompetences_Button" style="margin-top:5px; width:100%" Text="Внести компетенции" CssClass="btn dark" OnClick="PushUploadedCompetences_Button_Click" OnClientClick="PleaseWait()" /></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="tab-pane fade" id="disciplines">
            <div class="container" style="padding:0px">
                <div class="col-md-6" style="padding:0px">
                    <h4>Ручное заполнение</h4>
                    <strong>Дисциплина: </strong>
                    <asp:DropDownList runat="server" ID="Disciplines_DropDownList" CssClass="form-control" style="max-width:100%" DataSourceID="SqlDataSourceDisciplines" DataTextField="Name" DataValueField="id" AutoPostBack="true" OnSelectedIndexChanged="Disciplines_DropDownList_SelectedIndexChanged" />
                    <asp:SqlDataSource runat="server" ID="SqlDataSourceDisciplines" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [Disciplines]" />
                    <br /><br />
                    <strong>Название: </strong>
                    <asp:TextBox runat="server" ID="Discipline_TextBox" CssClass="form-control" />
                    <br /><br />
                    <asp:Button runat="server" ID="CreateDiscipline_Button" Text="Создать" CssClass="btn dark" OnClick="CreateDiscipline_Button_Click" />
                    <asp:Button runat="server" ID="RedactDiscipline_Button" Text="Обновить" CssClass="btn light" OnClick="RedactDiscipline_Button_Click" />
                    <asp:Button runat="server" ID="DeleteDiscipline_Button" Text="Удалить выбранную" CssClass="btn red" OnClientClick="if (!OnDelete()) return false;" OnClick="DeleteDiscipline_Button_Click" />
                    <br /><br />
                    <div style="padding-left:15px">
                        <h4>Место дисциплины в структуре ООП</h4>
                        <strong>Дисциплины до:</strong><br /> 
                        <asp:TextBox runat="server" CssClass="form-control" style="width:100%; height:150px; max-width:100%; background-color:#fff" ID="DisciplineInOOPBefore_TextBox" TextMode="MultiLine" ReadOnly="true" />
                        <asp:DropDownList runat="server" ID="DropDown_DisciplineInOOPBefore" CssClass="form-control" style="max-width:100%; margin-bottom:5px" DataSourceID="SqlDataSourceDisciplines" DataTextField="Name" DataValueField="id" />
                        <asp:Button runat="server" style="margin-bottom:2px" ID="Button_AddDisciplineInOOPBefore" CssClass="btn light" Text="Добавить" OnClick="Button_AddDisciplineInOOPBefore_Click" />
                        <asp:Button runat="server" style="margin-bottom:2px" ID="Button_DeleteDisciplineInOOPBefore" CssClass="btn light" Text="Удалить" OnClick="Button_DeleteDisciplineInOOPBefore_Click" />
                        <br /><br />
                        <strong>Дисциплины после:</strong><br />
                        <asp:TextBox runat="server" CssClass="form-control" style="width:100%; height:150px; max-width:100%; background-color:#fff" ID="DisciplineInOOPAfter_TextBox"  TextMode="MultiLine" ReadOnly="true" />
                        <asp:DropDownList runat="server" ID="DropDown_DisciplineInOOPAfter"  CssClass="form-control" style="max-width:100%; margin-bottom:5px" DataSourceID="SqlDataSourceDisciplines" DataTextField="Name" DataValueField="id" />
                        <asp:Button runat="server" style="margin-bottom:2px" ID="Button_AddDisciplineInOOPAfter" CssClass="btn light" Text="Добавить" OnClick="Button_AddDisciplineInOOPAfter_Click" />
                        <asp:Button runat="server" style="margin-bottom:2px" ID="Button_DeleteDisciplineInOOPAfter" CssClass="btn light" Text="Удалить" OnClick="Button_DeleteDisciplineInOOPAfter_Click" />
                    </div>
                </div>
                <div class="col-md-6">
                    <h4>Загрузка дисциплин</h4>
                    Вы также можете загрузить excel-файл с дисциплинами и их связями в стурктуре ООП. Для этого:
                    <label style="margin-bottom:0px">
                        <a> выберите файл </a>
                        <asp:FileUpload runat="server" ID="uploader_disciplines" accept=".xlsx" CssClass="hide" onchange="showFile(this)"/>
                    </label>
                    и нажмите "Загрузить" ниже. По умолчанию считается, что значимые строки таблицы начинаются с третьей.
                    <br /><br>
                    <label runat="server" id="uploader_disciplines_view">Файл не выбран</label><br />
                    <asp:Button runat="server" ID="UploadDisciplines_Button" Text="Загрузить" CssClass="btn light" OnClick="UploadDisciplines_Button_Click" OnClientClick="PleaseWait()" />
                    <br /><br />
                    <div runat="server" id="PreviewDisciplines_Div" visible="false">
                        <strong>Выгруженная таблица: </strong>
                        <div style="overflow:auto; width:100%; height:272px; margin-top:9px; border: 1px solid gray; margin-top:2px">
                            <asp:Table runat="server" CssClass="table table-bordered" ID="PreviewDisciplines_Table" BorderWidth="1" GridLines="Both" />
                        </div>
                        <div style="text-align:right; margin-top:5px">
                            <asp:Button runat="server" ID="PushUploadedDisciplines_Button" Text="Внести дисциплины" CssClass="btn dark" OnClick="PushUploadedDisciplines_Button_Click" OnClientClick="PleaseWait()" />
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="tab-pane fade" id="qualifications">
            <h4>Степени и квалификации выпускников</h4>
            <asp:DropDownList runat="server" ID="Qualifications_DropDownList" CssClass="form-control" DataSourceID="SqlDataSourceQualifications" DataTextField="Name" DataValueField="id" />
            <asp:SqlDataSource runat="server" ID="SqlDataSourceQualifications" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [Qualifications]"  />
            <br /><br />
            <asp:TextBox runat="server" ID="Qualifications_TextBox" placeholder="Степень/квалификация..." CssClass="form-control" />
            <br /><br />
            <asp:Button runat="server" ID="CreateQualifications_Button" CssClass="btn dark" Text="Создать" OnClick="CreateQualifications_Button_Click" />
            <asp:Button runat="server" ID="RedactQualifications_Button" CssClass="btn light" Text="Обновить" OnClick="RedactQualifications_Button_Click" />
            <asp:Button runat="server" ID="DeleteQualifications_Button" CssClass="btn red" style="margin-top:2px" Text="Удалить выбранную" OnClientClick="if (!OnDelete()) return false;" OnClick="DeleteQualifications_Button_Click" />
        </div>

        <div class="tab-pane fade" id="training_directions">
            <h4>Направления подготовки</h4>
            <asp:DropDownList runat="server" ID="TrainingDirections_DropDownList" CssClass="form-control" DataSourceID="SqlDataSourceTrainingDirections" DataTextField="Viewer" DataValueField="id" AutoPostBack="true" OnSelectedIndexChanged="TrainingDirections_DropDownList_SelectedIndexChanged" />
            <asp:SqlDataSource runat="server" ID="SqlDataSourceTrainingDirections" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT *, (Designation+' ('+ Code+')') AS Viewer FROM [Training_Directions]" />
            <%--<asp:Button runat="server" ID="FillTrainingDirectionsControlls_Button" Text="Заполнить поля" style="margin-bottom:2px" CssClass="btn light" OnClick="FillTrainingDirectionsControlls_Button_Click" />--%>
            <br /><br />
            <strong>Название (без кода):</strong><br />
            <asp:TextBox runat="server" ID="TrainingDirectionName_TextBox" Width="500px" CssClass="form-control"  />
            <br /><br />
            <strong>Код (без скобок):</strong><br />
            <asp:TextBox runat="server" ID="TrainingDirectionCode_TextBox" Width="500px" CssClass="form-control"  />
            <br /><br />
            <strong>Профили направления:</strong>
            <div class="check_screen">
                <asp:TextBox runat="server" ID="TextBox_TrainingDirectionProfiles" TextMode="MultiLine" Width="100%" Height="150px" CssClass="form-control" ReadOnly="true" style="background-color:#fff" />
            </div>
            <asp:DropDownList runat="server" ID="DropDown_TrainingDirectionProfiles" style="margin-top:5px;" CssClass="form-control" DataSourceID="SqlDataSourceProfiles" DataTextField="Name" DataValueField="Id" />
            <br />
            <asp:Button runat="server" ID="Button_CreateTrainingDirectionProfile" CssClass="btn light" Text="Добавить" style="margin-top:5px" OnClick="Button_CreateTrainingDirectionProfile_Click"  />
            <asp:Button runat="server" ID="Button_DeleteTrainingDirectionProfile" CssClass="btn light" Text="Удалить" style="margin-top:5px" OnClick="Button_DeleteTrainingDirectionProfile_Click" />
            <br /><br />
            <asp:Button runat="server" ID="CreateTrainingDirection_Button" CssClass="btn dark" Text="Создать" OnClick="CreateTrainingDirection_Button_Click" />
            <asp:Button runat="server" ID="RedactTrainingDirection_Button" CssClass="btn light" Text="Обновить" OnClick="RedactTrainingDirection_Button_Click" />
            <asp:Button runat="server" ID="DeleteTrainingDirection_Button" CssClass="btn red" style="margin-top:2px" Text="Удалить выбранное" OnClientClick="if (!OnDelete()) return false;" OnClick="DeleteTrainingDirection_Button_Click" />
        </div>

        <div class="tab-pane fade" id="profils">
            <h4>Профили</h4>
            <asp:DropDownList runat="server" ID="DropDownList_Profiles" CssClass="form-control" DataSourceID="SqlDataSourceProfiles" DataTextField="Name" DataValueField="id" AutoPostBack="true" OnSelectedIndexChanged="DropDownList_Profiles_SelectedIndexChanged" />
            <%--<asp:Button runat="server" ID="Button_FillControllsProfile" CssClass="btn light" Text="Заполнить поля" OnClick="Button_FillControllsProfile_Click" />--%>
            <asp:SqlDataSource runat="server" ID="SqlDataSourceProfiles" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [Profiles]" />
            <br /><br />
            <strong>Наименование:</strong><br />
            <asp:TextBox runat="server" ID="TextBox_Profiles" CssClass="form-control" />
            <br /><br />
            <strong>Программы профиля:</strong>
            <div class="check_screen">
                <asp:TextBox runat="server" ID="TextBox_ProfilePrograms" Width="100%" Height="150px" CssClass="form-control" style="background-color:#fff" ReadOnly="true" TextMode="MultiLine" />
            </div>
            <asp:DropDownList runat="server" ID="DropDown_ProfilePrograms" CssClass="form-control" style="margin-top:5px" DataSourceID="SqlDataSourcePrograms" DataTextField="Viewer" DataValueField="Id" />
            <asp:SqlDataSource runat="server" ID="SqlDataSourcePrograms" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT *, Discipline+' ('+CONVERT(varchar,Approve_Date,105)+')' AS Viewer FROM [Programs]"  />
            <br />
            <asp:Button runat="server" ID="Button_CreateProfileProgram" CssClass="btn light" style="margin-top:5px" Text="Добавить" OnClick="Button_CreateProfileProgram_Click" />
            <asp:Button runat="server" ID="Button_DeleteProfileProgram" CssClass="btn light" style="margin-top:5px" Text="Удалить" OnClick="Button_DeleteProfileProgram_Click" />
            <br /><br />
            <asp:Button runat="server" ID="Button_Create_Profile" Text="Создать" CssClass="btn dark" OnClick="Button_Create_Profile_Click" />
            <asp:Button runat="server" ID="Button_Update_Profile" Text="Обновить" CssClass="btn light" OnClick="Button_Update_Profile_Click"  />
            <asp:Button runat="server" ID="Button_Delete_Profile" Text="Удалить выбранный" style="margin-top:2px" CssClass="btn red" OnClientClick="if (!OnDelete()) return false;" OnClick="Button_Delete_Profile_Click"  />
        </div>

        <div class="tab-pane fade" id="departments">
            <h4>Кафедры</h4>
            <asp:DropDownList runat="server" ID="DropDownList_Departments" CssClass="form-control" DataSourceID="SqlDataSourceDepartments" DataTextField="Name" DataValueField="id" AutoPostBack="true" OnSelectedIndexChanged="DropDownList_Departments_SelectedIndexChanged" />
            <%--<asp:Button runat="server" ID="Button_FillControllsDepartment" CssClass="btn light" Text="Заполнить поля" OnClick="Button_FillControllsDepartment_Click" />--%>
            <br />
            <asp:SqlDataSource runat="server" ID="SqlDataSourceDepartments" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [Departments]" />
            <br />
            <strong>Название:</strong><br />
            <asp:TextBox runat="server" ID="TextBox_Department" CssClass="form-control"  />
            <br /><br />
            <strong>Сотрудники кафедры:</strong>
            <div class="check_screen">
                <asp:TextBox runat="server" ID="TextBox_DepartmentPersonal" CssClass="form-control" ReadOnly="true" style="background-color:#fff" TextMode="MultiLine" Width="100%" Height="150px" />
            </div>
            <asp:DropDownList runat="server" ID="DropDown_DepartmentPersonal" CssClass="form-control" style="margin-top:5px" DataSourceID="SqlDataSourcePersonal" DataTextField="UserName" DataValueField="Id" />
            <asp:SqlDataSource runat="server" ID="SqlDataSourcePersonal" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [AspNetUsers]" />
            <br />
            <asp:Button runat="server" ID="Button_CreateDepartmentPersonal" CssClass="btn dark" style="margin-top:5px" Text="Добавить" OnClick="Button_CreateDepartmentPersonal_Click" />
            <asp:Button runat="server" ID="Button_DeleteDepartmentPersonal" CssClass="btn red" style="margin-top:5px" Text="Удалить" OnClick="Button_DeleteDepartmentPersonal_Click"/>
            <a href="Account/Register.aspx" class="btn light" style="margin-top:5px">Новый пользователь &rarr;</a>
            <br /><br />
            <asp:Button runat="server" ID="Button_Create" Text="Создать" CssClass="btn dark" OnClick="Button_Create_Click" />
            <asp:Button runat="server" ID="Button_Update" Text="Обновить" CssClass="btn light" OnClick="Button_Update_Click"  />
            <asp:Button runat="server" ID="Button_Delete" Text="Удалить выбранную" style="margin-top:2px" CssClass="btn red" OnClientClick="if (!OnDelete()) return false;" OnClick="Button_Delete_Click"  />
        </div>

        <div class="tab-pane fade" id="institutes">
            <h4>Факультеты</h4>
            <asp:DropDownList runat="server" ID="DropDown_Institutes" DataSourceID="SqlDataSourceInstitutes" DataTextField="Name" DataValueField="id" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="DropDown_Institutes_SelectedIndexChanged" />
            <%--<asp:Button runat="server" ID="Button_FillConreollsInstitute" Text="Заполнить поля" CssClass="btn light" OnClick="Button_FillConreollsInstitute_Click" />--%>
            <asp:SqlDataSource runat="server" ID="SqlDataSourceInstitutes" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [Institutes]"  />
            <br /><br />
            <strong>Наименование:</strong><br />
            <asp:TextBox runat="server" ID="TextBox_Institute" CssClass="form-control"/>
            <br /><br />
            <strong>Кафедры:</strong>
            <div class="check_screen">
                <asp:TextBox runat="server" ID="TextBox_InstituteDepartments" CssClass="form-control" style="background-color:#fff" TextMode="MultiLine" ReadOnly="true" Width="100%" Height="150px" />
            </div>
            <asp:DropDownList runat="server" ID="DropDown_InstituteDepartments" CssClass="form-control" style="margin-top:5px" DataSourceID="SqlDataSourceDepartments" DataTextField="Name" DataValueField="Id" />
            <br />
            <asp:Button runat="server" ID="Button_CreateInstituteDepartment" CssClass="btn light" style="margin-top:5px;" Text="Добавить" OnClick="Button_CreateInstituteDepartment_Click" />
            <asp:Button runat="server" ID="Button_DeleteInstituteDepartment" CssClass="btn light" style="margin-top:5px;" Text="Удалить" OnClick="Button_DeleteInstituteDepartment_Click" />
            <br /><br />
            <asp:Button runat="server" ID="Button_CreateInstitute" Text="Создать" CssClass="btn dark" OnClick="Button_CreateInstitute_Click" />
            <asp:Button runat="server" ID="Button_UpdateInstitute" Text="Обновить" CssClass="btn light" OnClick="Button_UpdateInstitute_Click"  />
            <asp:Button runat="server" ID="Button_DeleteInstitute" Text="Удалить выбранный" style="margin-top:2px" CssClass="btn red" OnClientClick="if (!OnDelete()) return false;" OnClick="Button_DeleteInstitute_Click"  />
        </div>

        <div class="tab-pane fade" id="training_plans">
            <h4>Учебные планы</h4>
            <asp:DropDownList runat="server" CssClass="form-control" ID="DropDown_TrainingPlans" DataSourceID="SqlDataSourceTrainingPlans" DataTextField="Viewer" DataValueField="id" AutoPostBack="true" OnSelectedIndexChanged="DropDown_TrainingPlans_SelectedIndexChanged"/>
            <asp:SqlDataSource runat="server" ID="SqlDataSourceTrainingPlans" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT *, (Name + ' (' + Year + ')') AS Viewer FROM [Training_Plans]" />
            <%--<asp:Button runat="server" ID="Button_FillControllsTrainingPlans" Text="Заполнить поля" CssClass="btn light" OnClick="Button_FillControllsTrainingPlans_Click" />--%>
            <br /><br />
            <strong>Название:</strong><br />
            <asp:TextBox runat="server" ID="TextBox_PlanName" CssClass="form-control" />
            <br />
            <br />
            <strong>Год:</strong><br />
            <asp:DropDownList runat="server" CssClass="form-control" ID="DropDown_TrainingPlansYears">
                <asp:ListItem Text="2015" />
                <asp:ListItem Text="2016" />
                <asp:ListItem Text="2017" />
                <asp:ListItem Text="2018" />
                <asp:ListItem Text="2019" />
                <asp:ListItem Text="2020" />
                <asp:ListItem Text="2021" />
                <asp:ListItem Text="2022" />
            </asp:DropDownList>
            <br />
            <br />
            <strong>Перечень направлений учебного плана:</strong>
            <div class="check_screen">
                <asp:TextBox runat="server" ID="TextBox_TrainingPlanDirections" CssClass="form-control" TextMode="MultiLine" Width="100%" Height="150px" style="background-color:#fff" ReadOnly="true" />
            </div>
            <asp:DropDownList runat="server" CssClass="form-control" style="margin-top:5px" ID="DropDown_TrainingPlanDirections" DataSourceID="SqlDataSourceTrainingDirections" DataTextField="Viewer" DataValueField="id" />
            <br />
            <asp:Button runat="server" style="margin-top:5px;" ID="Button_AddDirectionInTrainingPlan" Text="Внести" CssClass="btn light" OnClick="Button_AddDirectionInTrainingPlan_Click" />
            <asp:Button runat="server" style="margin-top:5px;" ID="Button_DeleteDirectionInTrainingPlan" Text="Удалить" CssClass="btn light" OnClick="Button_DeleteDirectionInTrainingPlan_Click" />
            <br />
            <br />
            <asp:Button runat="server" ID="Button_CreateTrainingPlan" Text="Добавить" CssClass="btn dark" OnClick="Button_CreateTrainingPlan_Click" />
            <asp:Button runat="server" ID="Button_UpdateTrainingPlan" Text="Обновить" CssClass="btn light" OnClick="Button_UpdateTrainingPlan_Click"/>
            <asp:Button runat="server" ID="Button_DeleteTrainingPlan" Text="Удалить выбранный" style="margin-top:2px" CssClass="btn red" OnClientClick="if (!OnDelete()) return false;" OnClick="Button_DeleteTrainingPlan_Click"  />
        </div>

        <div class="tab-pane fade" id="fgos">
            <h4>Федеральные государственные образовательные стандарты</h4>
            <asp:DropDownList runat="server" ID="DropDown_FGOS" CssClass="form-control" DataSourceID="SqlDataSourceFGOS" DataTextField="Name" DataValueField="id" AutoPostBack="true" OnSelectedIndexChanged="DropDown_FGOS_SelectedIndexChanged" />
            <asp:SqlDataSource runat="server" ID="SqlDataSourceFGOS" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [FGOS]" />
            <br /><br />
            <strong>Наименование в системе:</strong><br />
            <asp:TextBox runat="server" ID="TextBox_FGOSName" CssClass="form-control" />
            <br /><br />
            <strong>Описание стандарта</strong>
            <div class="check_screen">
                <asp:TextBox runat="server" ID="TextBox_FGOS" TextMode="MultiLine" CssClass="ckeditor" />
            </div>
            <br />
            <strong>Учебные планы:</strong><br />
            <asp:TextBox runat="server" ID="TextBox_FGOSTRainingPlans" CssClass="form-control" TextMode="MultiLine" ReadOnly="true" style="background-color:#fff" Width="550px" Height="150px" />
            <br />
            <asp:DropDownList runat="server" ID="DropDown_FGOSTrainingPlans" CssClass="form-control" DataSourceID="SqlDataSourceTrainingPlans" DataTextField="Viewer" DataValueField="id" />
            <br />
            <asp:Button runat="server" ID="Button_CreateTrainingPlanFGOS" CssClass="btn light" Text="Добавить" style="margin-top: 5px;" OnClick="Button_CreateTrainingPlanFGOS_Click"/>
            <asp:Button runat="server" ID="Button_DeleteTrainingPlanFGOS" CssClass="btn light" Text="Удалить"  style="margin-top: 5px;" OnClick="Button_DeleteTrainingPlanFGOS_Click"/>
            <br /><br />
            <asp:Button runat="server" ID="Button_CreateFGOS" Text="Создать" CssClass="btn dark" OnClick="Button_CreateFGOS_Click"  />
            <asp:Button runat="server" ID="Button_UpdateFGOS" Text="Обновить" CssClass="btn light" OnClick="Button_UpdateFGOS_Click" />
            <asp:Button runat="server" ID="Button_DeleteFGOS" Text="Удалить выбранный" style="margin-top:2px" CssClass="btn red" OnClientClick="if (!OnDelete()) return false;"  OnClick="Button_DeleteFGOS_Click"  />
        </div>

    </div>
    <a href="#" class="scrollup">Наверх</a>
</asp:Content>
