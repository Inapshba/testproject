﻿<%@ Page Language="C#" Title="- История программы" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProgramHistory.aspx.cs" Inherits="main_test.ProgramHistory" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent" ID="ProgramHistoryContent">
    <script src="Scripts/SetActiveTab.js"></script>
    <script src="Scripts/ScrollTop.js"></script>
    <link rel="stylesheet"  href="Content/Colored.css"/>
    <script>
        function OnTableClearConfirm() {
            if (confirm('Очистить таблицу?'))
                return true;
            else
                return false;
        }
    </script>
    <style type="text/css">
        strong {
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            cursor:default;
        }
        .form-control {
            width: auto;
            display: inline;
        }
            .form-control:focus {
                border-color: rgba(0,0,0,.075);
                outline: 0;
                -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(0, 0, 0, .6);
                        box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(0, 0, 0, .6);
            }
        /*всплывающая кнопка для автоматического скролла на начало страницы*/
        .scrollup {
            width: 40px;
            height: 40px;
            opacity: 0.3;
            position: fixed;
            bottom: 50px;
            right: 100px;
            display: none;
            text-indent: -9999px;
            background: url('icons/icon_top.png') no-repeat;
        }
    </style>
    <h2 runat="server" id="DownLoadError" visible="false">Не удалось загрузить историю :(</h2>
    <asp:Label runat="server" ID="ErrorMes" Visible="false" ForeColor="Red" />
    <h2 runat="server" id="program_html"></h2>
    <h4 runat="server" id="Period_html">Полная история</h4>
    <asp:TextBox runat="server" style="margin-bottom: 2px" CssClass="form-control" ID="Period_1_TextBox" TextMode="Date" />
    <asp:TextBox runat="server" style="margin-bottom: 2px" CssClass="form-control" ID="Period_2_TextBox" TextMode="Date" />
    <asp:Button runat="server" style="margin-bottom: 2px" CssClass="btn light" ID="Period_Button" Text="Определить период" OnClick="Period_Button_Click" />
    <br />
    <asp:TextBox runat="server" ID="History_TextBox" ReadOnly="true" TextMode="MultiLine" CssClass="form-control" Height="400px" Width="100%" BackColor="White"  /><br />
    <asp:Button runat="server" style="margin-top: 2px" CssClass="btn dark" ID="Period_Clear_Button" Text="Сбросить период" OnClick="Period_Clear_Button_Click" />
    <a href="#" class="scrollup">Наверх</a>
</asp:Content>
