﻿<%@ Page Title="- Управление пользователями" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Admin.aspx.cs" Inherits="main_test.Admin" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
<link rel="stylesheet" href="Content/ContentFade.css" />
<link rel="stylesheet" href="Content/Colored.css" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="Content/fileUploader.css" />
<link rel="stylesheet" href="Content/WorkStyle.css" />
<script src="Scripts/SetScrollPosition.js"></script>

<style>
    .hide {
            position: absolute;
            overflow: hidden;
            opacity: 0;
            z-index: -1;
            width: 0.1px;
            height: 0.1px;
        }
</style>
    <script>
        function showFile() {
                var input = document.getElementById('<%=uploader.ClientID %>');
            document.getElementById('<%=fileviewer.ClientID %>').innerHTML = input.files[0].name;
            var f = input.files[0];
            if (!f.type.match('image.*')) {
                alert("Image only please....");
            }
            var reader = new FileReader();
            reader.onload = (function(theFile) {
                return function(e) {
                    // Render thumbnail.
                    var img = document.getElementById('<%=Image_Sign.ClientID%>');
                    img.style.display = 'block';
                    img.setAttribute('src', e.target.result);
                };
            })(f);
            reader.readAsDataURL(f);
            return false;
        }
    </script>
<div class="body_fade">
    <asp:Label runat="server" ID="ErrorMes" Visible="false" ForeColor="Red" />
    <div class="container">
        <div class="row" style="text-align:right">
            <div class="col-md-12">
                <asp:Button runat="server" ID="Button_NewUser" CssClass="btn dark" style="margin-top:10px" Text="Регистрация нового пользователя &rarr;" OnClick="Button_NewUser_Click" />
                <%--<a href="Account/Register.aspx?Admin=1" class="btn dark" style="margin-top:10px">Регистрация нового пользователя &rarr;</a>--%>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h2>Неподтвержденные пользователи:</h2>
                <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="SqlDataSource1" CellPadding="3" GridLines="Vertical" Height="152px"  CssClass="table table-bordered" AllowPaging="True" OnRowDeleted="GridView2_RowDeleted" OnRowDeleting="GridView2_RowDeleting">
                    <Columns>
                        <asp:BoundField DataField="UserName" HeaderText="Пользователь" SortExpression="UserName" ReadOnly="true" />
                        <asp:BoundField DataField="Id" HeaderText="Id" SortExpression="Id" Visible="False" />
                        <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" ReadOnly="True" />
                        <asp:CheckBoxField DataField="EmailConfirmed" HeaderText="Подтвержден" SortExpression="EmailConfirmed" Visible="true" />
                        <asp:BoundField DataField="PasswordHash" HeaderText="PasswordHash" SortExpression="PasswordHash" ReadOnly="True" Visible="False" />
                        <asp:BoundField DataField="SecurityStamp" HeaderText="SecurityStamp" SortExpression="SecurityStamp" Visible="False" />
                        <asp:BoundField DataField="PhoneNumber" HeaderText="PhoneNumber" SortExpression="PhoneNumber" Visible="False" />
                        <asp:CheckBoxField DataField="PhoneNumberConfirmed" HeaderText="PhoneNumberConfirmed" SortExpression="PhoneNumberConfirmed" Visible="False" />
                        <asp:CheckBoxField DataField="TwoFactorEnabled" HeaderText="TwoFactorEnabled" SortExpression="TwoFactorEnabled" Visible="False" />
                        <asp:BoundField DataField="LockoutEndDateUtc" HeaderText="LockoutEndDateUtc" SortExpression="LockoutEndDateUtc" Visible="False" />
                        <asp:CheckBoxField DataField="LockoutEnabled" HeaderText="LockoutEnabled" SortExpression="LockoutEnabled" Visible="False" />
                        <asp:BoundField DataField="AccessFailedCount" HeaderText="AccessFailedCount" SortExpression="AccessFailedCount" Visible="False" />
                        <asp:CommandField ShowDeleteButton="True" DeleteText="Удалить" />
                    </Columns>
                </asp:GridView>

                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" 
                    DeleteCommand="DELETE FROM [AspNetUsers] WHERE [Id] = @Id" 
                    InsertCommand="INSERT INTO [AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (@Id, @Email, @EmailConfirmed, @PasswordHash, @SecurityStamp, @PhoneNumber, @PhoneNumberConfirmed, @TwoFactorEnabled, @LockoutEndDateUtc, @LockoutEnabled, @AccessFailedCount, @UserName)" 
                    SelectCommand="SELECT * FROM [AspNetUsers] WHERE [LockoutEnabled] = 1" 
                    UpdateCommand="UPDATE [AspNetUsers] SET [Email] = (SELECT [Email] FROM [AspNetUsers] WHERE [Id] = @Id), [EmailConfirmed] = 1, [PasswordHash] = (SELECT [PasswordHash] from [AspNetUsers] WHERE [Id] = @Id), [SecurityStamp] = (SELECT [SecurityStamp] FROM [AspNetUsers] WHERE [Id] = @Id), [PhoneNumber] = @PhoneNumber, [PhoneNumberConfirmed] = 1, [TwoFactorEnabled] = 0, [LockoutEndDateUtc] = @LockoutEndDateUtc, [LockoutEnabled] = 0, [AccessFailedCount] = 0, [UserName] = (SELECT [UserName] FROM [AspNetUsers] WHERE [Id] = @Id) WHERE [Id] = @Id ">
                    <DeleteParameters>
                        <asp:Parameter Name="Id" Type="String" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="Id" Type="String" />
                        <asp:Parameter Name="Email" Type="String" />
                        <asp:Parameter Name="EmailConfirmed" Type="Boolean" />
                        <asp:Parameter Name="PasswordHash" Type="String" />
                        <asp:Parameter Name="SecurityStamp" Type="String" />
                        <asp:Parameter Name="PhoneNumber" Type="String" />
                        <asp:Parameter Name="PhoneNumberConfirmed" Type="Boolean" />
                        <asp:Parameter Name="TwoFactorEnabled" Type="Boolean" />
                        <asp:Parameter Name="LockoutEndDateUtc" Type="DateTime" />
                        <asp:Parameter Name="LockoutEnabled" Type="Boolean" />
                        <asp:Parameter Name="AccessFailedCount" Type="Int32" />
                        <asp:Parameter Name="UserName" Type="String" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="Email" Type="String" />
                        <asp:Parameter Name="EmailConfirmed" Type="Boolean" />
                        <asp:Parameter Name="PasswordHash" Type="String" />
                        <asp:Parameter Name="SecurityStamp" Type="String" />
                        <asp:Parameter Name="PhoneNumber" Type="String" />
                        <asp:Parameter Name="PhoneNumberConfirmed" Type="Boolean" />
                        <asp:Parameter Name="TwoFactorEnabled" Type="Boolean" />
                        <asp:Parameter Name="LockoutEndDateUtc" Type="DateTime" />
                        <asp:Parameter Name="LockoutEnabled" Type="Boolean" />
                        <asp:Parameter Name="AccessFailedCount" Type="Int32" />
                        <asp:Parameter Name="UserName" Type="String" />
                        <asp:Parameter Name="Id" Type="String" />
                    </UpdateParameters>
                </asp:SqlDataSource>

            </div>
            <div class="col-md-6">

                <h2>Действующие пользователи:</h2>
                <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="SqlDataSource2" CssClass="table table-bordered" CellPadding="3" GridLines="Vertical" OnDataBound="GridView1_DataBound" OnRowDeleting="GridView1_RowDeleting">
                    <Columns>
                        <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" SortExpression="Id" Visible="False" />
                        <asp:BoundField DataField="UserName" HeaderText="Пользователь" SortExpression="UserName" />
                        <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                        <asp:BoundField DataField="RoleName" HeaderText="Роль" SortExpression="RoleName" />
                        <asp:CheckBoxField DataField="EmailConfirmed" HeaderText="EmailConfirmed" SortExpression="EmailConfirmed" Visible="False" />
                        <asp:BoundField DataField="PasswordHash" HeaderText="PasswordHash" SortExpression="PasswordHash" Visible="False" />
                        <asp:BoundField DataField="SecurityStamp" HeaderText="SecurityStamp" SortExpression="SecurityStamp" Visible="False" />
                        <asp:BoundField DataField="PhoneNumber" HeaderText="PhoneNumber" SortExpression="PhoneNumber" Visible="False" />
                        <asp:CheckBoxField DataField="PhoneNumberConfirmed" HeaderText="PhoneNumberConfirmed" SortExpression="PhoneNumberConfirmed" Visible="False" />
                        <asp:CheckBoxField DataField="TwoFactorEnabled" HeaderText="TwoFactorEnabled" SortExpression="TwoFactorEnabled" Visible="False" />
                        <asp:BoundField DataField="LockoutEndDateUtc" HeaderText="LockoutEndDateUtc" SortExpression="LockoutEndDateUtc" Visible="False" />
                        <asp:CheckBoxField DataField="LockoutEnabled" HeaderText="LockoutEnabled" SortExpression="LockoutEnabled" Visible="False" />
                        <asp:BoundField DataField="AccessFailedCount" HeaderText="AccessFailedCount" SortExpression="AccessFailedCount" Visible="False" />
                        <asp:CommandField ShowDeleteButton="True" DeleteText="Удалить" />
                    </Columns>
                </asp:GridView>
    
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" 
                    DeleteCommand="DELETE FROM [AspNetUsers] WHERE [Id] = @original_Id" 
                    InsertCommand="INSERT INTO [AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (@Id, @Email, @EmailConfirmed, @PasswordHash, @SecurityStamp, @PhoneNumber, @PhoneNumberConfirmed, @TwoFactorEnabled, @LockoutEndDateUtc, @LockoutEnabled, @AccessFailedCount, @UserName)" 
                    OldValuesParameterFormatString="original_{0}" 
                    SelectCommand="SELECT AspNetUsers.Id AS Id, AspNetUsers.UserName AS UserName, AspNetUsers.Email, AspNetRoles.Name AS RoleName FROM AspNetUsers INNER JOIN AspNetUserRoles ON AspNetUsers.Id = AspNetUserRoles.UserId INNER JOIN AspNetRoles ON AspNetUserRoles.RoleId = AspNetRoles.Id WHERE AspNetUsers.LockoutEnabled = 0" 
                    UpdateCommand="UPDATE [AspNetUsers] SET [Email] = @Email, [EmailConfirmed] = @EmailConfirmed, [PasswordHash] = @PasswordHash, [SecurityStamp] = @SecurityStamp, [PhoneNumber] = @PhoneNumber, [PhoneNumberConfirmed] = @PhoneNumberConfirmed, [TwoFactorEnabled] = @TwoFactorEnabled, [LockoutEndDateUtc] = @LockoutEndDateUtc, [LockoutEnabled] = @LockoutEnabled, [AccessFailedCount] = @AccessFailedCount, [UserName] = @UserName WHERE [Id] = @original_Id">
                    <DeleteParameters>
                        <asp:Parameter Name="original_Id" Type="String" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="Id" Type="String" />
                        <asp:Parameter Name="Email" Type="String" />
                        <asp:Parameter Name="EmailConfirmed" Type="Boolean" />
                        <asp:Parameter Name="PasswordHash" Type="String" />
                        <asp:Parameter Name="SecurityStamp" Type="String" />
                        <asp:Parameter Name="PhoneNumber" Type="String" />
                        <asp:Parameter Name="PhoneNumberConfirmed" Type="Boolean" />
                        <asp:Parameter Name="TwoFactorEnabled" Type="Boolean" />
                        <asp:Parameter Name="LockoutEndDateUtc" Type="DateTime" />
                        <asp:Parameter Name="LockoutEnabled" Type="Boolean" />
                        <asp:Parameter Name="AccessFailedCount" Type="Int32" />
                        <asp:Parameter Name="UserName" Type="String" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="Email" Type="String" />
                        <asp:Parameter DefaultValue="0" Name="EmailConfirmed" Type="Boolean" />
                        <asp:Parameter Name="PasswordHash" Type="String" />
                        <asp:Parameter Name="SecurityStamp" Type="String" />
                        <asp:Parameter Name="PhoneNumber" Type="String" />
                        <asp:Parameter DefaultValue="0" Name="PhoneNumberConfirmed" Type="Boolean" />
                        <asp:Parameter Name="TwoFactorEnabled" Type="Boolean" />
                        <asp:Parameter Name="LockoutEndDateUtc" Type="DateTime" />
                        <asp:Parameter Name="LockoutEnabled" Type="Boolean" />
                        <asp:Parameter Name="AccessFailedCount" Type="Int32" />
                        <asp:Parameter Name="UserName" Type="String" />
                        <asp:Parameter Name="original_Id" Type="String" />
                    </UpdateParameters>
                </asp:SqlDataSource>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">                
                <h2>Выдать роль пользователям:</h2>
                <h4>Выбор пользователя:</h4>
                <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control" DataSourceID="SqlDataSource3" DataTextField="UserName" DataValueField="Id"/>
                <h4>Выбор роли:</h4>
                <asp:DropDownList ID="DropDownList2" runat="server" CssClass="form-control" DataSourceID="SqlDataSource4" DataTextField="Name" DataValueField="Id"/>
                <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" CssClass="btn light" Text="Подтвердить"/>
                <asp:Label ID="Label1" runat="server" Text="Label" Visible="False"></asp:Label>
            </div>
            <div class="col-md-3">
                <h2>Загрузка подписи пользователя:</h2>
                <h4>Выбор пользователя:</h4>
                <asp:DropDownList ID="DropDown_Sign" OnSelectedIndexChanged="DropDown_Sign_SelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control" DataSourceID="SqlDataSource3" DataTextField="UserName" DataValueField="Id"/>
                <br />
                <label id="fileviewer" runat="server">Файл не выбран</label><strong>:</strong>
                <br />
                <div style="width:100%; margin-top:5px; margin-bottom:5px;">
                    <img runat="server" id="Image_Sign" src="~/icons/attach.png" style="display:none; max-width:100%; height:auto;" />
                </div>
                <div class="upload" style="margin-top:5px">
                    <label class="label">
                        <i class="material-icons">attach_file</i>
                        <asp:FileUpload runat="server" ID="uploader" CssClass="hide" accept=".png" onchange="showFile();"/>
                    </label>
                </div>
                <asp:Button runat="server" ID="Button_SetSign" Text="Привязать" CssClass="btn light" OnClick="Button_SetSign_Click" />
            </div>
        </div>
    </div>

    

    <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT [Id], [Name] FROM [AspNetRoles]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT [UserName], [Id] FROM [AspNetUsers]"></asp:SqlDataSource>
    
    <asp:SqlDataSource ID="SqlDataSource6" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT AspNetUsers.Email, AspNetUsers.UserName, AspNetRoles.Name FROM AspNetRoles INNER JOIN AspNetUserRoles ON AspNetRoles.Id = AspNetUserRoles.RoleId INNER JOIN AspNetUsers ON AspNetUserRoles.UserId = AspNetUsers.Id"></asp:SqlDataSource>

</div>
</asp:Content>
