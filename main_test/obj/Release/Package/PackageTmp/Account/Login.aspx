﻿
<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="main_test.Account.Login" Async="true" %>

<asp:Content runat="server" ID="LoginContent" ContentPlaceHolderID="MainContent">


<%@ Register Src="~/Account/OpenAuthProviders.ascx" TagPrefix="uc" TagName="OpenAuthProviders" %>
    <link rel="stylesheet" href="../Content/Colored.css" />
    <style type="text/css">
        .wrapper_login_form {margin-left:70px; margin-right:70px;}
        .unselecteble {
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            cursor:default;
        }
    </style>

    <div class="row">
            <section id="loginForm" class="wrapper_login_form">
                <div class="form-horizontal">
                    <br />
                    <br />
                    <asp:PlaceHolder runat="server" ID="ErrorMessage" Visible="false">
                        <p class="text-danger">
                            <asp:Literal runat="server" ID="FailureText" />
                        </p>
                    </asp:PlaceHolder>
                    <div>
                        <asp:Label runat="server" AssociatedControlID="Email">Логин</asp:Label><br />
                            <asp:TextBox runat="server" ID="Email" CssClass="form-control" TextMode="Email" />
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                                CssClass="text-danger" ErrorMessage="Поле адреса электронной почты заполнять обязательно." />
                    </div>
                    <div>
                        <asp:Label runat="server" AssociatedControlID="Password">Пароль</asp:Label><br />
                            <asp:TextBox runat="server" ID="Password" CssClass="form-control" TextMode="Password"/>
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="Password" CssClass="text-danger" ErrorMessage="Поле пароля заполнять обязательно." />
                       
                    </div>

                    <div>
                        <asp:Button runat="server" OnClick="LogIn" Text="Вход" CssClass="btn dark" />
                        <a style="margin-left:10px" href="Register.aspx">Регистрация</a>
                    </div>
                    
                    <br />

                    <a href="Forgot.aspx">Забыли пароль?</a>

                   <%-- <asp:HyperLink runat="server" ID="ForgotPasswordHyperLink">Forgot your password?</asp:HyperLink>
                    
                    <section id="socialLoginForm">
                        <uc:OpenAuthProviders runat="server" ID="OpenAuthLogin" />
                    </section>--%>

                    <%--<div class="form-group">
                        <div class="col-md-offset-3">
                                <asp:CheckBox runat="server" ID="RememberMe" />
                                <asp:Label runat="server" AssociatedControlID="RememberMe" CssClass="unselecteble">Запомнить меня</asp:Label>
                        </div>
                    </div>--%>
                    
                </div>
            </section>
        </div>
</asp:Content>
