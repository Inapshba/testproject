﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HtmlToOpenXml;
using System.IO;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;

namespace main_test
{
    public partial class HtmlToOpenXMLBuilder
    {
        string ReturnExtension(string fileExtension)
        {
            // In the long run this should go in a class
            switch (fileExtension)
            {
                case ".htm":
                case ".html":
                case ".log":
                    return "text/HTML";
                case ".txt":
                    return "text/plain";
                case ".docx":
                    return "application/ms-word";
                case ".tiff":
                case ".tif":
                    return "image/tiff";
                case ".asf":
                    return "video/x-ms-asf";
                case ".avi":
                    return "video/avi";
                case ".zip":
                    return "application/zip";
                case ".xls":
                case ".csv":
                    return "application/vnd.ms-excel";
                case ".gif":
                    return "image/gif";
                case ".jpg":
                case "jpeg":
                    return "image/jpeg";
                case ".bmp":
                    return "image/bmp";
                case ".wav":
                    return "audio/wav";
                case ".mp3":
                    return "audio/mpeg3";
                case ".mpg":
                case "mpeg":
                    return "video/mpeg";
                case ".rtf":
                    return "application/rtf";
                case ".asp":
                    return "text/asp";
                case ".pdf":
                    return "application/pdf";
                case ".fdf":
                    return "application/vnd.fdf";
                case ".ppt":
                    return "application/mspowerpoint";
                case ".dwg":
                    return "image/vnd.dwg";
                case ".msg":
                    return "application/msoutlook";
                case ".xml":
                case ".sdxl":
                    return "application/xml";
                case ".xdp":
                    return "application/vnd.adobe.xdp+xml";
                default:
                    return "application/octet-stream";
            }
        }
        
        public void Build(List<string> htmles, int programid)
        {
            string newfilename = "Program_" + programid.ToString() + ".docx";
            string newFilePath = HttpContext.Current.Server.MapPath("~/Files/" + newfilename);
            
            if (File.Exists(newFilePath)) File.Delete(newFilePath);

            using (MemoryStream generatedDocument = new MemoryStream())
            {
                using (WordprocessingDocument package = WordprocessingDocument.Create(generatedDocument, WordprocessingDocumentType.Document))
                {
                    MainDocumentPart mainPart = package.MainDocumentPart;
                    if (mainPart == null)
                    {
                        mainPart = package.AddMainDocumentPart();
                        new Document(new Body()).Save(mainPart);
                    }

                    HtmlConverter converter = new HtmlConverter(mainPart);

                    for (int i = 0; i < htmles.Count; i++)
                        converter.ParseHtml(htmles[i]);

 
                    mainPart.Document.Save();
                }


                if (File.Exists(newFilePath))
                    File.Delete(newFilePath);

                File.WriteAllBytes(newFilePath, generatedDocument.ToArray());

                if (HttpContext.Current.Request.Url.Host != "localhost")
                {
                    string urlStr = "http://mpuprograms.ru/Files/" + newfilename;
                    BaseWork task = new BaseWork();
                    task.InsertFileURL(programid, urlStr);
                }
            }
        }
    }
}