﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using main_test.Models;

namespace main_test
{
 
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!User.IsInRole("Admin"))
            {
                block_UserAdmin.Disabled = true;
                block_WordBookAdmin.Disabled = true;
                block_UserAdmin.Visible = false;
                block_WordBookAdmin.Visible = false;
            }

            if ((!User.IsInRole("Moderator")) && (!User.IsInRole("Admin")))
            {
                row_1_html.Disabled = true;
                row_1_html.Visible = false;
            }
        }
    }
}